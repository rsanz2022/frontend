'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">platform documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AccessManagerModule.html" data-type="entity-link">AccessManagerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AccessManagerModule-7714acff6ac34f509cc515ec97db9a60"' : 'data-target="#xs-components-links-module-AccessManagerModule-7714acff6ac34f509cc515ec97db9a60"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AccessManagerModule-7714acff6ac34f509cc515ec97db9a60"' :
                                            'id="xs-components-links-module-AccessManagerModule-7714acff6ac34f509cc515ec97db9a60"' }>
                                            <li class="link">
                                                <a href="components/AccessManagerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccessManagerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CompaniesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CompaniesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CompanyComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CompanyComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GroupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewUserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewUserComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserDetailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserDetailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AccessManagerRoutingModule.html" data-type="entity-link">AccessManagerRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' : 'data-target="#xs-components-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' :
                                            'id="xs-components-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' : 'data-target="#xs-injectables-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' :
                                        'id="xs-injectables-links-module-AppModule-97e0630bcad12a770458596e342f9a56"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/Globals.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>Globals</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' : 'data-target="#xs-components-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' :
                                            'id="xs-components-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' }>
                                            <li class="link">
                                                <a href="components/RequestComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RequestComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ResetComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ResetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SigninComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SigninComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' : 'data-target="#xs-injectables-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' :
                                        'id="xs-injectables-links-module-AuthModule-16dbacba2071359bc42d8d1e71ed188e"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthRoutingModule.html" data-type="entity-link">AuthRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AvatarModule.html" data-type="entity-link">AvatarModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AvatarModule-7b44c18c55ee39721375e5a7728681bd"' : 'data-target="#xs-components-links-module-AvatarModule-7b44c18c55ee39721375e5a7728681bd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AvatarModule-7b44c18c55ee39721375e5a7728681bd"' :
                                            'id="xs-components-links-module-AvatarModule-7b44c18c55ee39721375e5a7728681bd"' }>
                                            <li class="link">
                                                <a href="components/AvatarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AvatarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreModule-01a05b5dceb30b0cf6b6e29de5a134ef"' : 'data-target="#xs-injectables-links-module-CoreModule-01a05b5dceb30b0cf6b6e29de5a134ef"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreModule-01a05b5dceb30b0cf6b6e29de5a134ef"' :
                                        'id="xs-injectables-links-module-CoreModule-01a05b5dceb30b0cf6b6e29de5a134ef"' }>
                                        <li class="link">
                                            <a href="injectables/AlertService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AlertService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SidenavService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SidenavService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardModule.html" data-type="entity-link">DashboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DashboardModule-00859433df7783e3962f18704b777088"' : 'data-target="#xs-components-links-module-DashboardModule-00859433df7783e3962f18704b777088"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardModule-00859433df7783e3962f18704b777088"' :
                                            'id="xs-components-links-module-DashboardModule-00859433df7783e3962f18704b777088"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DashboardComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GaugeStatusModule.html" data-type="entity-link">GaugeStatusModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-GaugeStatusModule-1965d421fccf4d935fde0d4d5f868e1c"' : 'data-target="#xs-components-links-module-GaugeStatusModule-1965d421fccf4d935fde0d4d5f868e1c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GaugeStatusModule-1965d421fccf4d935fde0d4d5f868e1c"' :
                                            'id="xs-components-links-module-GaugeStatusModule-1965d421fccf4d935fde0d4d5f868e1c"' }>
                                            <li class="link">
                                                <a href="components/GaugeDetailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GaugeDetailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GaugeStatusComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GaugeStatusComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TimeControlsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimeControlsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GaugeStatusRoutingModule.html" data-type="entity-link">GaugeStatusRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GraphQLModule.html" data-type="entity-link">GraphQLModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/HydrographModule.html" data-type="entity-link">HydrographModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HydrographModule-451162e50e2a83300a95b73a58d3d250"' : 'data-target="#xs-components-links-module-HydrographModule-451162e50e2a83300a95b73a58d3d250"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HydrographModule-451162e50e2a83300a95b73a58d3d250"' :
                                            'id="xs-components-links-module-HydrographModule-451162e50e2a83300a95b73a58d3d250"' }>
                                            <li class="link">
                                                <a href="components/HydrographComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HydrographComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HydroplotterModule.html" data-type="entity-link">HydroplotterModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' : 'data-target="#xs-components-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' :
                                            'id="xs-components-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' }>
                                            <li class="link">
                                                <a href="components/ChartComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HydroplotterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HydroplotterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PDFDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PDFDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SettingsDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SettingsDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' : 'data-target="#xs-injectables-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' :
                                        'id="xs-injectables-links-module-HydroplotterModule-69f7830e26a3406ff8ae2ae185d9fecf"' }>
                                        <li class="link">
                                            <a href="injectables/WindowRef.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>WindowRef</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/HyetographModule.html" data-type="entity-link">HyetographModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HyetographModule-5a70e2ea6dd8e8167fe64f2cb371fa2e"' : 'data-target="#xs-components-links-module-HyetographModule-5a70e2ea6dd8e8167fe64f2cb371fa2e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HyetographModule-5a70e2ea6dd8e8167fe64f2cb371fa2e"' :
                                            'id="xs-components-links-module-HyetographModule-5a70e2ea6dd8e8167fe64f2cb371fa2e"' }>
                                            <li class="link">
                                                <a href="components/HyetographComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HyetographComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MapsModule.html" data-type="entity-link">MapsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MapsModule-c2b3c3054fa5243bffe1273fafbf9624"' : 'data-target="#xs-components-links-module-MapsModule-c2b3c3054fa5243bffe1273fafbf9624"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MapsModule-c2b3c3054fa5243bffe1273fafbf9624"' :
                                            'id="xs-components-links-module-MapsModule-c2b3c3054fa5243bffe1273fafbf9624"' }>
                                            <li class="link">
                                                <a href="components/LayerListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LayerListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LayerPaneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LayerPaneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LayerSelectComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LayerSelectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MapsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MapsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PickerPanelComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PickerPanelComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PopupContentComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PopupContentComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MapsRoutingModule.html" data-type="entity-link">MapsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link">MaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NavbarModule.html" data-type="entity-link">NavbarModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NavbarModule-7a7f9901a65b7bff2be7256c30ac7a1e"' : 'data-target="#xs-components-links-module-NavbarModule-7a7f9901a65b7bff2be7256c30ac7a1e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NavbarModule-7a7f9901a65b7bff2be7256c30ac7a1e"' :
                                            'id="xs-components-links-module-NavbarModule-7a7f9901a65b7bff2be7256c30ac7a1e"' }>
                                            <li class="link">
                                                <a href="components/AppMenuComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppMenuComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavbarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserMenuComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserMenuComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProfileModule.html" data-type="entity-link">ProfileModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProfileModule-e00a2417ebf18c4bb8c5598661593bd9"' : 'data-target="#xs-components-links-module-ProfileModule-e00a2417ebf18c4bb8c5598661593bd9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProfileModule-e00a2417ebf18c4bb8c5598661593bd9"' :
                                            'id="xs-components-links-module-ProfileModule-e00a2417ebf18c4bb8c5598661593bd9"' }>
                                            <li class="link">
                                                <a href="components/PictureDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PictureDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProfileComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProfileRoutingModule.html" data-type="entity-link">ProfileRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RoutingModule.html" data-type="entity-link">RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-405e89127f9aaf364f6d56b7e7933c10"' : 'data-target="#xs-components-links-module-SharedModule-405e89127f9aaf364f6d56b7e7933c10"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-405e89127f9aaf364f6d56b7e7933c10"' :
                                            'id="xs-components-links-module-SharedModule-405e89127f9aaf364f6d56b7e7933c10"' }>
                                            <li class="link">
                                                <a href="components/PageNotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PageNotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TimepickerModule.html" data-type="entity-link">TimepickerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TimepickerModule-4524887667e3ffef9efb5c81051851a1"' : 'data-target="#xs-components-links-module-TimepickerModule-4524887667e3ffef9efb5c81051851a1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TimepickerModule-4524887667e3ffef9efb5c81051851a1"' :
                                            'id="xs-components-links-module-TimepickerModule-4524887667e3ffef9efb5c81051851a1"' }>
                                            <li class="link">
                                                <a href="components/TimepickerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimepickerComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/WatchpointsModule.html" data-type="entity-link">WatchpointsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-WatchpointsModule-d23eedf883bdbd88da9619a441630510"' : 'data-target="#xs-components-links-module-WatchpointsModule-d23eedf883bdbd88da9619a441630510"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-WatchpointsModule-d23eedf883bdbd88da9619a441630510"' :
                                            'id="xs-components-links-module-WatchpointsModule-d23eedf883bdbd88da9619a441630510"' }>
                                            <li class="link">
                                                <a href="components/DetailsPaneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DetailsPaneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GridComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GridComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PointImageButtonComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PointImageButtonComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WatchpointsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">WatchpointsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/WatchpointsRoutingModule.html" data-type="entity-link">WatchpointsRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/RequestComponent.html" data-type="entity-link">RequestComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ResetComponent.html" data-type="entity-link">ResetComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/SigninComponent.html" data-type="entity-link">SigninComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TimeControlsComponent-1.html" data-type="entity-link">TimeControlsComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Company.html" data-type="entity-link">Company</a>
                            </li>
                            <li class="link">
                                <a href="classes/DataObject.html" data-type="entity-link">DataObject</a>
                            </li>
                            <li class="link">
                                <a href="classes/GaugeDetail.html" data-type="entity-link">GaugeDetail</a>
                            </li>
                            <li class="link">
                                <a href="classes/GaugeSum.html" data-type="entity-link">GaugeSum</a>
                            </li>
                            <li class="link">
                                <a href="classes/Group.html" data-type="entity-link">Group</a>
                            </li>
                            <li class="link">
                                <a href="classes/Hydroplot.html" data-type="entity-link">Hydroplot</a>
                            </li>
                            <li class="link">
                                <a href="classes/Permission.html" data-type="entity-link">Permission</a>
                            </li>
                            <li class="link">
                                <a href="classes/Product.html" data-type="entity-link">Product</a>
                            </li>
                            <li class="link">
                                <a href="classes/Project.html" data-type="entity-link">Project</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AccessManagerService.html" data-type="entity-link">AccessManagerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AlertService.html" data-type="entity-link">AlertService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ColorService.html" data-type="entity-link">ColorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/Data.html" data-type="entity-link">Data</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DataService.html" data-type="entity-link">DataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GaugesService.html" data-type="entity-link">GaugesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GaugeStatusService.html" data-type="entity-link">GaugeStatusService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HydrographService.html" data-type="entity-link">HydrographService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HyetographService.html" data-type="entity-link">HyetographService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayerListService.html" data-type="entity-link">LayerListService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayerPaneService.html" data-type="entity-link">LayerPaneService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayerPrepService.html" data-type="entity-link">LayerPrepService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayersService.html" data-type="entity-link">LayersService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayersStoreService.html" data-type="entity-link">LayersStoreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MapsService.html" data-type="entity-link">MapsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MyUserService.html" data-type="entity-link">MyUserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationsService.html" data-type="entity-link">NotificationsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RouteDataService.html" data-type="entity-link">RouteDataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SidenavService.html" data-type="entity-link">SidenavService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SourceService.html" data-type="entity-link">SourceService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StylesService.html" data-type="entity-link">StylesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TimepickerService.html" data-type="entity-link">TimepickerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TimesStoreService.html" data-type="entity-link">TimesStoreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WatchpointsService.html" data-type="entity-link">WatchpointsService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/ErrorInterceptor.html" data-type="entity-link">ErrorInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuardService.html" data-type="entity-link">AuthGuardService</a>
                            </li>
                            <li class="link">
                                <a href="guards/RoleGuardService.html" data-type="entity-link">RoleGuardService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ApiResponse.html" data-type="entity-link">ApiResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-1.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-2.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-3.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-4.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-5.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-6.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-7.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-8.html" data-type="entity-link">Response</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response-9.html" data-type="entity-link">Response</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});