module.exports = {
  client: {
    service: {
      name: "api",
      url: "http://vaibackend-env.yrvdmbd4ny.us-west-2.elasticbeanstalk.com/graphql",
      // optional headers
      headers: {
        Authorization: "Bearer eyJraWQiOiJoTHR6Uk5ieGZMaGkwdmdZQ2Y4aUIwZFhBeTdYQVRcLzZ2SUFZaE5hazh1OD0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI0MTIxMjJkNS01Njc2LTQwNjUtOWE2OS0zZTE5ZjgzZWZmNDkiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLXdlc3QtMi5hbWF6b25hd3MuY29tXC91cy13ZXN0LTJfeGdTMFFXUlJHIiwiY29nbml0bzp1c2VybmFtZSI6InRlc3QuYWRtaW4iLCJhdWQiOiI2cTZmMXQ5YjVkMTRyajlpZ3RrNTFiZzY4cSIsImV2ZW50X2lkIjoiNDI5YWM2ZWEtNzAxMy0xMWU5LTkwMzQtZGRmYzlhMmQ2NDQxIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1NTcxNTYzNjMsIm5hbWUiOiJUZXN0IiwiZXhwIjoxNTU3MTYzODI1LCJpYXQiOjE1NTcxNjAyMjUsImZhbWlseV9uYW1lIjoiQWRtaW4iLCJlbWFpbCI6InRlc3QuYWRtaW5AZ3JvdW5kc3RhdGVzdHVkaW9zLmNvbSJ9.VaMBO6NZdr08xCnwStxNV3xxtOVNNQiU5GiS32kd2fzAvj6UNrTsMk6-YAxsNSYnH_jTj5Pv_S9kWxPrtoLYAcGH_S2XNIholF66iMwwOI64Fa4ZT0U8YilvAPP8NUQDiyWZHXJEMuHZEZsK6RnQJyDC3qkwYmYg6vUWql9dSXm2-aYzbtJwZEnH_-DQxPy_ZEABz4IqU4wJz5-sU3slEcKGivN49IXXCDFSfRJtQbowTZMuzP8znzJzVRtAe8yPINB1olm9PUNe7hm4zHT9l71tQ8jb8dZfI1Eo1pvqQCw8Gs7WzIybOLML0Ju3QQLeOrx7lpBTkflAYJqd1hzF_w"
      },
      // optional disable SSL validation check
      // skipSSLValidation: true
    }
  }
};