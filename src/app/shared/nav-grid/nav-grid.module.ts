import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavGridComponent } from './nav-grid.component';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [ NavGridComponent ],
  imports: [
    CommonModule,
    RouterModule,

    MatIconModule,
    MatGridListModule
  ],
  exports: [
    NavGridComponent
  ]
})
export class NavGridModule { }
