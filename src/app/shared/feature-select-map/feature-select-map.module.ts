import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { environment } from '@env/environment';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SearchModule } from '../../shared';
import { MatTooltipModule } from '@angular/material/tooltip';

import { FeatureSelectMapComponent } from './feature-select-map.component';

@NgModule({
  declarations: [ FeatureSelectMapComponent ],
  exports: [ FeatureSelectMapComponent ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    SearchModule,
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken
    }),

    MatProgressSpinnerModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule,
  ]
})
export class FeatureSelectMapModule { }
