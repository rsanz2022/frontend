import { TestBed } from '@angular/core/testing';

import { FeatureSelectMapService } from './feature-select-map.service';

describe('FeatureSelectMapService', () => {
  let service: FeatureSelectMapService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FeatureSelectMapService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
