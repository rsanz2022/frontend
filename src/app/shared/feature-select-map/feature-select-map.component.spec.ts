import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureSelectMapComponent } from './feature-select-map.component';

describe('FeatureSelectMapComponent', () => {
  let component: FeatureSelectMapComponent;
  let fixture: ComponentFixture<FeatureSelectMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureSelectMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureSelectMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
