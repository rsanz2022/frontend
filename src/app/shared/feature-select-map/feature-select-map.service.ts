import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Apollo, gql } from 'apollo-angular';
import { FeatureCollection } from 'geojson';
import { map, take } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

interface Response {
  getGARRIntersectingIds?: string[];
}

@Injectable({
  providedIn: 'root'
})
export class FeatureSelectMapService {

  styles$ = new ReplaySubject<any>(1);

  constructor(
    private http: HttpClient,
    private apollo: Apollo
  ) { }

  getIntersects(project, mosaic, sourceIds, sourceLayer?): Promise<string[]> {
    const query = gql`
      query GARR(
          $project:String! $mosaic:String!
          $sourceLayer:String! $sourceIds:[String]!
      ) {
        getGARRIntersectingIds(
          project:$project sourceLayer:$sourceLayer
          targetLayer:$mosaic sourceIds:$sourceIds)
    }`;

    return this.apollo.query<Response>({ query,
        variables: { mosaic, project, sourceLayer, sourceIds }})
      .pipe(map(res => res.data.getGARRIntersectingIds))
      .toPromise();
  }

  getGeojson = (url: string): Promise<FeatureCollection> => this.http
    .get<FeatureCollection>(url).toPromise();

  setStyles = (): void => {
    this.http.get('/assets/feature-select-styles.json')
      .pipe(take(1))
      .subscribe(this.styles$);
  }
}
