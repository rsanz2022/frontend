import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FeatureCollection } from 'geojson';
import * as mapboxgl from 'mapbox-gl';
import { Project } from '../models';
import { FeatureSelectMapService } from './feature-select-map.service';
import * as extent from '@mapbox/geojson-extent';
import { MediaObserver } from '@angular/flex-layout';
import { ReplaySubject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import Auth from '@aws-amplify/auth';

@Component({
  selector: 'app-feature-select-map',
  templateUrl: './feature-select-map.component.html',
  styleUrls: ['./feature-select-map.component.scss'],
})
export class FeatureSelectMapComponent implements OnChanges {
  featuresLoading = true;
  featureCollection: FeatureCollection;
  featureTotal: number;
  mapgl: mapboxgl.Map;
  mapgl$ = new ReplaySubject<mapboxgl.Map>(1);
  source$ = new ReplaySubject<mapboxgl.VectorSource>(1);
  popup = new mapboxgl.Popup({
    closeButton: false,
    closeOnClick: false,
  });
  canvas: HTMLElement;
  box: HTMLElement; // draw box element
  filterFill; // current filter for fill layer
  filterLine; // current filter for line layer
  startPos: mapboxgl.Point; // starting xy coordinates when `mousedown` occurs
  currentPos: mapboxgl.Point; // current xy coordinates when `mousemove` or `mouseup` occurs
  searchMarker: mapboxgl.Marker;
  isMouseDown = false;
  mapSelect = false;
  showSearch = false;
  shifted = false;
  alted = false;
  domain: string;
  mouseOnMap = false;
  icons = [
    'gauges',
    'gauges-gray',
    'watchpoints',
    'watchpoints-gray',
    'soil-moisture',
    'soil-moisture-gray',
    'marker',
  ];
  markerMode = false;
  removeMarker = false;
  cursor;
  _scrollZoom = true;
  style: any = {};
  mask: string;
  origin = window.location.origin;
  private token: string;

  @Input() layer: mapboxgl.AnyLayer;
  @Input() features: string[];
  @Output() featuresChange = new EventEmitter<string[]>();
  @Input() disableListeners = false;
  @Input() enableIntersects = true;
  @Input() project: Project;
  @Output() loading = new EventEmitter<boolean>();
  @Output() error = new EventEmitter<string>();
  @Input() markers: mapboxgl.LngLatLike[] = [];
  @Output() markersChange = new EventEmitter<mapboxgl.LngLatLike[]>();
  @Input() scrollZoom = true;
  @Input() markerLimit = 0;
  @Input() invalid = false;

  @HostListener('document:keydown', ['$event'])
  @HostListener('document:keyup', ['$event'])
  onKeyPress = (event: KeyboardEvent): void => {
    if (this.mapSelect && !this.showSearch && !this.disableListeners) {
      if (this.mouseOnMap) event.preventDefault();
      switch (event.key) {
        case 'Shift':
        case 'Control':
          this.shifted = event.shiftKey;
          break;
        case 'Alt':
          this.alted = event.altKey;
          break;
      }
    }
  };

  @HostListener('window:blur', [])
  onBlur = (): void => this.onMouseUp();

  constructor(
    public media: MediaObserver,
    private service: FeatureSelectMapService,
    private sanitizer: DomSanitizer,
    iconRegistry: MatIconRegistry
  ) {
    this.service.setStyles();
    iconRegistry.addSvgIcon(
      'remove_location',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/remove_location-24px.svg'
      )
    );
    Auth.currentSession().then(
      (session) => (this.token = session.getIdToken().getJwtToken())
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.layer && changes.layer.currentValue) {
      this.setLayer(changes.layer.previousValue, changes.layer.currentValue);
    }
    if (changes.features && changes.features.currentValue) {
      this.setLayerFilters();
      const { currentValue } = changes.features;
      currentValue.sort();
      if (
        JSON.stringify(this.features || []) !== JSON.stringify(currentValue)
      ) {
        this.selectFeatures(currentValue);
      }
    }
    if (changes.scrollZoom && changes.scrollZoom.firstChange) {
      this._scrollZoom = this.scrollZoom;
    }
  }

  onMarkerDragEnd = (marker: mapboxgl.Marker, index: number): void => {
    this.markers[index] = marker.getLngLat().toArray() as mapboxgl.LngLatLike;
    this.markersChange.emit(this.markers);
  };

  onMouseClick = (ev: mapboxgl.MapMouseEvent): void => {
    if (!this.markerMode || this.removeMarker || !ev.lngLat) return; // two events fired in ngx 4.x
    if (this.markerLimit && this.markers.length >= this.markerLimit) return;
    this.markers.push(ev.lngLat.toArray() as mapboxgl.LngLatLike);
    this.markersChange.emit(this.markers);
  };

  onMarkerClick = (marker: mapboxgl.LngLatLike): void => {
    if (this.removeMarker) {
      const index = this.markers.indexOf(marker);
      this.markers.splice(index, 1);
      this.markersChange.emit(this.markers);
    }
  };

  private setLayer(oldLayer: mapboxgl.Layer, newLayer: mapboxgl.Layer) {
    if (newLayer && newLayer.metadata) {
      this.domain = newLayer.metadata.symbolicName;
      this.mask = newLayer.metadata.mask;
      this.featureCollection = null;
      this.featureTotal = null;
      if (newLayer.metadata.markerMode) {
        this.setMarkerMode();
        this.setMapSelect(false);
        this.mapgl$.pipe(take(1)).subscribe(() => {
          this.fitMapBounds(newLayer.metadata.bounds || this.project.bounds);
          this.setFeaturesLoading(false);
        });
      } else {
        this.setStyle();
        this.setFeaturesLoading(true);
        this.setMarkerMode(false);
        this.setMapSelect();
        const geojson = this.getEndpoint(newLayer.metadata);
        this.service.getGeojson(geojson).then(
          (res: FeatureCollection) => {
            this.featureTotal = res.features.length;
            this.featureCollection = res;
            this.mapgl$
              .pipe(take(1))
              .subscribe(() => this.fitMapBounds(newLayer.metadata.bounds));
            if (
              !!oldLayer &&
              this.features &&
              this.features.length &&
              this.enableIntersects
            ) {
              this.service
                .getIntersects(
                  this.project.symbolicName,
                  newLayer.metadata.symbolicName,
                  this.features,
                  oldLayer.metadata.symbolicName
                )
                .then(
                  (res) => {
                    this.selectFeatures(res);
                    this.setFeaturesLoading(false);
                  },
                  () => this.error.emit('Error getting intersecting Source IDs')
                );
            } else if (this.features && this.features.length) {
              // TODO: check that this else if block is necessary
              this.mapgl$.pipe(take(1)).subscribe(() => {
                this.selectFeatures(this.features);
                this.setFeaturesLoading(false);
              });
            } else {
              this.setLayerFilters();
              this.setFeaturesLoading(false);
            }
          },
          () =>
            this.error.emit('Error getting layer shapes. Please, try again.')
        );
      }
    }
  }

  private getEndpoint = (meta): string =>
    (meta.data && meta.data.source ? meta.data.source.endpoint : false) ||
    `${window.location.origin}/api/${this.project.symbolicName}/rainvieux/mosaic/${this.domain}/feature.json`;

  private setMarkerMode = (activate = true): void => {
    this.markerMode = activate;
  };

  setStyle = (): any => {
    this.service.styles$
      .pipe(
        take(1),
        map(
          (styles) =>
            styles[this.layer.type][
              (this.layer as mapboxgl.Layer).metadata.layerType
            ] || styles[this.layer.type]
        )
      )
      .subscribe((style) => (this.style = style));
  };

  onMapLoad = (mapgl: mapboxgl.Map): void => {
    this.mapgl = mapgl;
    this.canvas = this.mapgl.getCanvasContainer();
    this.mapgl$.next(mapgl);
  };

  onSourceLoad = (event: mapboxgl.MapSourceDataEvent): void => {
    if (event.sourceId === 'domain') {
      this.source$.next(
        event.isSourceLoaded ? (event.source as mapboxgl.VectorSource) : null
      );
    }
  };

  setMapSelect = (activate = true): void => {
    this.mapSelect = activate;
    this.mapgl$.pipe(take(1)).subscribe(() => {
      this.cursor = activate ? 'crosshair' : this.markerMode ? 'pointer' : null;
      this.mapgl.dragPan[activate ? 'disable' : 'enable']();
      this.canvas[(activate ? 'add' : 'remove') + 'EventListener'](
        'mousedown',
        this.onMouseDown,
        true
      );
    });
  };

  onMouseOverMap = (isOverMap = true): void => {
    if (isOverMap !== this.mouseOnMap) this.mouseOnMap = isOverMap;
  };

  // Return the xy coordinates of the mouse position
  mousePos(e: MouseEvent): mapboxgl.Point {
    const rect = this.canvas.getBoundingClientRect();
    return new mapboxgl.Point(
      e.clientX - rect.left - this.canvas.clientLeft,
      e.clientY - rect.top - this.canvas.clientTop
    );
  }

  onMouseDown = (e: MouseEvent): void => {
    this.isMouseDown = true;
    this.onMouseLeavePopup();
    // Capture the first xy coordinates
    this.startPos = this.mousePos(e);
    document.addEventListener('mousemove', this.onMouseMove);
    document.addEventListener('mouseup', this.onMouseUp);
  };

  onMouseMove = (e: MouseEvent): void => {
    // Capture the ongoing xy coordinates
    this.currentPos = this.mousePos(e);

    // Append the box element if it doesnt exist
    if (!this.box) {
      this.box = document.createElement('div');
      this.box.classList.add('boxdraw');
      this.canvas.appendChild(this.box);
    }

    const minX = Math.min(this.startPos.x, this.currentPos.x),
      maxX = Math.max(this.startPos.x, this.currentPos.x),
      minY = Math.min(this.startPos.y, this.currentPos.y),
      maxY = Math.max(this.startPos.y, this.currentPos.y);

    // Adjust width and xy position of the box element ongoing
    const pos = 'translate(' + minX + 'px,' + minY + 'px)';
    this.box.style.transform = pos;
    this.box.style.width = maxX - minX + 'px';
    this.box.style.height = maxY - minY + 'px';
  };

  onMouseUp = (e?: MouseEvent): void => {
    this.isMouseDown = false;
    document.removeEventListener('mousemove', this.onMouseMove);
    document.removeEventListener('mouseup', this.onMouseUp);
    if (e) {
      const bbox: [mapboxgl.Point, mapboxgl.Point] = [
        this.startPos,
        this.mousePos(e),
      ];
      const features = this.mapgl
        .queryRenderedFeatures(bbox, { layers: ['domain-hidden'] })
        .map((feature) => feature.properties.id)
        .filter((id, pos, array) => array.indexOf(id) === pos);

      this.selectFeatures(features, e);
    }

    if (this.box) {
      this.box.parentNode.removeChild(this.box);
      this.box = null;
    }
  };

  onMouseMovePopup(e): void {
    if (this.isMouseDown) return;

    const coordinates = e.lngLat;
    const description = e.features[0].properties.name;

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
      coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    // Populate the popup and set its coordinates
    // based on the feature found.
    this.popup.setLngLat(coordinates).setHTML(description).addTo(this.mapgl);
  }

  onMouseLeavePopup(): void {
    this.popup.remove();
  }

  fitMapBounds = (projBounds?: mapboxgl.LngLatBoundsLike): void => {
    const bounds = new mapboxgl.LngLatBounds(
      projBounds || extent(this.featureCollection)
    );
    this.mapgl.fitBounds(bounds, {
      padding: {
        top: 24,
        bottom: 24,
        left: 24,
        right: 48,
      },
      duration: 0,
    });
  };

  selectAllFeatures = (): void => {
    const ids = this.featureCollection.features
      .map((feature) => feature.properties.id)
      .filter((id, pos, array) => array.indexOf(id) === pos);
    this.selectFeatures(ids);
  };

  selectFeatures = (features: string[], e?: MouseEvent): void => {
    if (features) {
      let selected = [...this.features];
      if ((e?.shiftKey || e?.ctrlKey) && features.length) {
        features = features.filter((f) => !selected.includes(f));
        selected = selected.concat(features);
      } else if (e?.altKey && features.length) {
        selected = selected.filter((el) => !features.includes(el));
      } else {
        selected = features;
      }
      this.features = selected;
      this.featuresChange.emit(selected);
      this.setLayerFilters();
    }
  };

  private setFeaturesLoading = (isLoading = true): void => {
    if (this.featuresLoading !== isLoading) {
      this.featuresLoading = isLoading;
      this.loading.emit(isLoading);
    }
  };

  setLayerFilters = (): void => {
    this.filterFill = ['in', 'id'].concat(this.features);
    this.filterLine = ['!in', 'id'].concat(this.features);
  };

  public transformRequest = (url: string): mapboxgl.RequestParameters => {
    const protocol = window.location.protocol;
    const host = window.location.host;
    if (
      url.startsWith(`${protocol}//${host}/api/`) ||
      url.startsWith(`/api/`) ||
      url.startsWith(`api/`)
    ) {
      return {
        url,
        headers: { Authorization: 'Bearer ' + this.token }
      };
    }
  };
}
