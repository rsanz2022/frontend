import {
  Component,
  OnInit,
  ViewChild,
  EventEmitter,
  ElementRef,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { SearchService } from './search.service';
import { Places } from '../models';
import * as mapboxgl from 'mapbox-gl';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import * as extent from '@mapbox/geojson-extent';
import { LayerPrepService } from '@app/features/maps/resources/services/layer-prep.service';
import { HttpClient } from '@angular/common/http';
import { UserService } from '@app/core';
import { Feature, Geometry } from 'geojson';

interface Source {
  layer?: mapboxgl.Layer;
  features: Feature<Geometry, { [name: string]: any; }>[];
  type: string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {
  @ViewChild('search', { static: true }) searchField: ElementRef;
  searchControl = new FormControl();
  searchItems: any[] = [];
  selected = '';
  @Input() padding: mapboxgl.PaddingOptions;
  @Input() layerIds: string[] = [];
  @Input() sources: Source[];
  private _searchMarker: mapboxgl.Marker;
  get searchMarker(): mapboxgl.Marker {
    return this._searchMarker;
  }
  set searchMarker(v: mapboxgl.Marker) {
    this._searchMarker = v;
    this.onMarker.emit(v);
  }
  private _isShown = false;
  @Input()
  get isShown(): boolean {
    return this._isShown;
  }
  set isShown(v) {
    if (v && v !== this._isShown) {
      this.removeMarker();
      setTimeout(() => this.searchField.nativeElement.focus());
    } else {
      this.searchItems = [];
      this.searchField.nativeElement.value = '';
    }
    this._isShown = v;
  }
  private _mapgl: mapboxgl.Map;
  @Input()
  get mapgl(): mapboxgl.Map {
    return this._mapgl;
  }
  set mapgl(mapgl: mapboxgl.Map) {
    if (mapgl) {
      this._mapgl = mapgl;
      this.mapgl.on('dragstart', this.onDragStart);
    }
  }
  @Output() onClose = new EventEmitter();
  @Output() onMarker = new EventEmitter<mapboxgl.Marker>();

  constructor(
    private userService: UserService,
    private service: SearchService,
    private prepService: LayerPrepService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.searchControl.valueChanges
      .pipe(debounceTime(250))
      .subscribe(this.onChangeSearch);
  }

  ngOnDestroy(): void {
    if (this.mapgl) {
      this.mapgl.off('dragstart', this.onDragStart);
    }
  }

  onDragStart = (): void => {
    this.removeMarker();
    this.removeOutline();
    if (this.isShown) {
      this.onClose.emit();
    }
  };

  onChangeSearch = (query: string): void => {
    this.searchItems = [];
    if (query.length > 2) {
      query = query.toLowerCase();
      if (this.sources) {
        this.sources.forEach((s) => {
          if (!s.features) return;
          const features = s.features.filter((feat) => {
            const searchItems = feat.properties.name + feat.properties.id;
            return searchItems.toLowerCase().includes(query);
          });
          this.searchItems.push(
            ...features.map((feat) => ({
              ...feat,
              layer: s.layer,
              source: s,
              place_name: feat.properties.name || feat.properties.id,
            }))
          );
        });
      }
      if (this.mapgl) {
        const filter = [
          'any',
          ['in', query, ['downcase', ['to-string', ['get', 'name']]]],
          ['in', query, ['downcase', ['to-string', ['get', 'id']]]],
        ];
        const queryOptions = {
          filter,
          layers: this.layerIds,
          bbox: this.userService.project.getBounds(5),
        };
        const features = this.mapgl
          .queryRenderedFeatures(null, queryOptions)
          .map((f) => ({
            ...f,
            place_name: f.properties.name || f.properties.id,
          }))
          .filter(
            (feat, pos, array): boolean =>
              array
                .map((f) => f.properties.id + f.properties.name)
                .indexOf(feat.properties.id + feat.properties.name) === pos
          );
        this.searchItems.push(...features);
      }
      this.service
        .getPlaces(query, this.mapgl.getCenter())
        .then((res: Places) =>
          this.searchItems.push(
            ...res.features.filter((feat) => feat['relevance'] >= 0.5)
          )
        );
    }
  };

  displayFn(place?): string {
    return place ? place.place_name : undefined;
  }

  addShapeLayer = (data): void => {
    this.mapgl
      .addLayer({
        id: 'search-highlight',
        type: 'line',
        source: {
          type: 'geojson',
          data: data,
        },
        layout: { 'line-cap': 'round', 'line-join': 'round' },
        paint: {
          'line-width': [
            'interpolate',
            ['exponential', 1.5],
            ['zoom'],
            5,
            0.75,
            18,
            32,
          ],
        },
      })
      .fitBounds(extent({ ...data }), {
        padding: this.padding,
      });
  };

  gotoPlace = ({ option }: MatAutocompleteSelectedEvent): void => {
    this.removeOutline();
    this.removeMarker();
    const layer = option.value.layer;
    const source = option.value.source;
    if (layer && layer.type !== 'symbol' && option.value.properties.id) {
      const id = option.value.properties.id;
      if (layer.metadata.popup) {
        const url = layer.metadata.popup.outlineUrl;
        this.http
          .get(this.prepService.getEndpointWithParams(url, { id }))
          .toPromise()
          .then(this.addShapeLayer);
      } else this.addShapeLayer(option.value.geometry);
    } else if (source && source.type === 'geojson' && option.value.geometry) {
      this.addShapeLayer(option.value.geometry);
    } else {
      option.value.center =
        option.value.center ||
        option.value._vectorTileFeature.toGeoJSON(
          option.value._vectorTileFeature._x,
          option.value._vectorTileFeature._y,
          option.value._vectorTileFeature._z
        ).geometry.coordinates;
      if (!layer) {
        this.searchMarker = new mapboxgl.Marker({ color: '#ffd740' })
          .setLngLat(option.value.center)
          .addTo(this.mapgl);
      }

      this.mapgl.flyTo({
        center: option.value.center || option.value.geometry.coordinates,
        zoom: 15,
      });
    }

    this.onClose.emit(this.searchMarker);
  };

  removeMarker = (): void => {
    if (this.searchMarker) {
      this.searchMarker.remove();
      this.searchMarker = undefined;
    }
  };

  removeOutline = (): void => {
    if (this.mapgl.getSource('search-highlight')) {
      this.mapgl
        .removeLayer('search-highlight')
        .removeSource('search-highlight');
    }
  };
}
