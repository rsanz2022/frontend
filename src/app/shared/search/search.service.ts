import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Places } from '../models';

@Injectable({ providedIn: 'root' })
export class SearchService {

  constructor(private http: HttpClient) { }

  getPlaces(query, mapCenter): Promise<Places> {
    const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${
      query}.json?types=district,place,locality,neighborhood,address,poi`;
    const requestOptions = {
      params: {
        access_token: environment.mapbox.accessToken,
        proximity: mapCenter.lng + ',' + mapCenter.lat,
        country: 'us'
      },
    };
    
    return this.http.get<Places>(url, requestOptions).toPromise();
  }
}
