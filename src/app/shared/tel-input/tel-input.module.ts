import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelInputComponent } from './tel-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ TelInputComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [ TelInputComponent ]
})
export class TelInputModule { }
