import { TestBed } from '@angular/core/testing';

import { AppConfigStoreService } from './app-config-store.service';

describe('AppConfigStoreService', () => {
  let service: AppConfigStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppConfigStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
