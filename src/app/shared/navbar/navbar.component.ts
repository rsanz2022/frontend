import { Component, OnInit } from '@angular/core';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { SidenavService, RouteDataService, UserService } from '@app/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatIconRegistry } from '@angular/material/icon';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  title = '';

  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private routeData: RouteDataService,
    private titleService: Title,
    public sidenav: SidenavService,
    public media: MediaObserver,
    public userService: UserService,
  ) {
    iconRegistry
      .addSvgIcon(
        'vlogo',
        sanitizer.bypassSecurityTrustResourceUrl('./assets/images/v-logo.svg')
      );
  }

  ngOnInit(): void {
    this.routeData.getData().subscribe((data): void => {
      this.title = data.title;
      this.titleService.setTitle(this.title);
    });
  }
}
