import { Organization, User } from './';
export class Group {
  id?: number;
  name?: string;
  description?: string;
  company?: Organization;
  users?: User[];
}
