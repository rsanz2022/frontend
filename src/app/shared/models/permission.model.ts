export class Permission {
  type: string;
  name: string;
  description?: string;
  crud: number[];
}
