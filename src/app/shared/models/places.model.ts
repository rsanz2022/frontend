export interface Places {
  type: string;
  query: string[];
  features: mapboxgl.MapboxGeoJSONFeature[];
  attribution: string;
}