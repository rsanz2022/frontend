export class Basemap {
  name: string;
  url: string;
  dark?: boolean;
}