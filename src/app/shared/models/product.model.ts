export class Product {
  _id: string;
  name: {
    vds: string;
    platform?: string;
  };
}
