export class DdfDetailsModel {
  id: string;
  displayId: string;
  categorizationLabel: string;
  tableLabel: string;
  mostSignificantDuration: number;
  durationResults: [{
    duration: number;
    description: string;
    maxRainfall: number;
    startOfMax: number;
    endOfMax: number;
    frequencyID: number;
    percentNextFrequency: number;
    percentNextThreat?: number;
    categories: [{
      frequency_id: string;
      description: string;
      depth: number;
    }];
  }];
}
