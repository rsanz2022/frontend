export class DdfModel {
  id: string;
  displayId: string;
  duration: number;
  frequencyID?: number;
  percentNextFrequency?: number;
  percentNextThreat?: number;
  maxRainfall: number;
  threatId?: number;
}
