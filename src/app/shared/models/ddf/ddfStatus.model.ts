export class DdfStatusModel {
  significantDuration: {
    label: string;
    ids: [{
      id: string;
      displayId: string;
      significantDuration: number;
    }];
  };
  durationResults: [{
    id: string;
    displayId: string;
    duration: number;
    maxRainfall: number;
    frequencyID: number;
    percentNextFrequency: number;
    hasThreats: boolean;
    threatID: number;
    percentNextThreat: number;
  }];
}
