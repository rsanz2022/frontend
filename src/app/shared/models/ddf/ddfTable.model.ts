export class DdfTableModel {
  id: string;
  description: string;
  categoryLabel: string;
  meta: {
    names: string;
    source: string;
  };
  levels: [{
    threat_id: string;
    label: string;
    color: string;
    description: string;
  }];
}
