export interface ElementEvent {
  type: string;
  properties: { [key: string]: any };
}
