import { Injectable } from '@angular/core';
import { LngLatBoundsLike } from 'mapbox-gl';
import { Time, TimeMaker } from './models';

export interface StorageOption {
  type: string;
  temp: boolean;
  id: string;
  pid: number; // Project ID
  value: MapStyle | LngLatBoundsLike | Time | string | boolean;
}

interface StorageArray {
  0: number;
  1: boolean;
  2: string;
  3: number; // Project ID
  4: MapStyle | LngLatBoundsLike | Time | string | boolean;
}

interface MapStyle {
  name: string;
  url: string;
}

@Injectable({
  providedIn: 'root',
})
export class AppConfigStoreService {
  private readonly types = ['time', 'map', 'rv-opt'];

  getConfig(id: string, pid: number, type?: string): StorageOption {
    const config = this.getConfigs().find(
      (config) =>
        config.id === id &&
        config.pid === pid &&
        (type ? config.type === type : true)
    );
    switch (config?.type) {
      case 'time':
        config.value = TimeMaker.create(config.value as Time);
        break;
    }
    return config;
  }

  getConfigs(): StorageOption[] {
    const localString = localStorage.getItem('ms');
    const sessionString = sessionStorage.getItem('ms');

    return [
      ...JSON.parse(localString || '[]').map(this.getOption),
      ...JSON.parse(sessionString || '[]').map(this.getOption),
    ].filter(c => !!c);
  }

  setConfigs(options: StorageOption[]): void {
    const saved = this.getConfigs(),
      local = [],
      session = [];

    for (let index = 0; index < options.length; index++) {
      const option = options[index];
      const savedIndex = saved.findIndex(
        (saved) => saved.id === option.id && saved.pid === option.pid
      );
      if (savedIndex === -1) {
        if (option.temp) session.push(option);
        else local.push(option);
      } else {
        saved[savedIndex] = option;
      }
    }

    for (let index = 0; index < saved.length; index++) {
      const option = saved[index];
      if (option.temp) {
        session.push(option);
      } else {
        local.push(option);
      }
    }

    localStorage.setItem('ms', JSON.stringify(local.map(this.getArray)));
    sessionStorage.setItem('ms', JSON.stringify(session.map(this.getArray)));
  }

  private getOption = (config: StorageArray): StorageOption => {
    if (!config) return;
    return {
      type: this.types[config[0]],
      temp: config[1],
      id: config[2],
      pid: config[3],
      value: config[4],
    };
  };

  private getArray = (option: StorageOption): StorageArray => {
    const type: number = this.types.indexOf(option.type);
    if (type < 0) {
      console.error(
        `Config type ${option.type} is unrecognized. Config option with ID ${option.id} will not be saved.`
      );
    } else {
      return [type, option.temp, option.id, option.pid, option.value];
    }
  };
}
