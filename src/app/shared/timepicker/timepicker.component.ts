import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Project, Time } from '../models';
import { UserService } from '../../core';
import { TimepickerService } from './timepicker.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment-timezone';
import * as DateValidator from '../directives/date-validators.directive';
import { Subscription } from 'rxjs';
import { Globals } from '../../globals';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatDateFormats,
} from '@angular/material/core';
import { MediaObserver } from '@angular/flex-layout';
import { AppConfigStoreService } from '../app-config-store.service';

export const MY_FORMATS: MatDateFormats = {
  parse: { dateInput: '' },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'YYYY/MM/DD',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class TimepickerComponent implements OnInit, OnDestroy, OnChanges {
  private rxSubs = new Subscription();
  periods = this.service.getPeriods();
  tzOffset = '';
  project: Project = this.userService.project;
  timesForm: FormGroup;
  done = false;
  selectedPeriod: number;
  matDatepickerMax: Date; // timezone not implemented in matDatepicker

  // todo: shouldn't be initialized to cover all use cases (i.e. future)
  @Input() max = new Date();

  // required in @Component selector option
  private _time: Time;
  @Input()
  get time(): Time {
    return this._time;
  }
  set time(v: Time) {
    this._time = { ...v };
  }

  private _enableRealtime = false;
  @Input()
  get enableRealtime(): boolean {
    return this._enableRealtime;
  }
  set enableRealtime(enable: boolean) {
    this._enableRealtime = coerceBooleanProperty(enable);
  }

  // only select a single point in time
  private _momentOnly = false;
  @Input()
  get momentOnly(): boolean {
    return this._momentOnly;
  }
  set momentOnly(enable: boolean) {
    this._momentOnly = coerceBooleanProperty(enable);
  }

  @Output() timesChange = new EventEmitter<Time>();

  constructor(
    private fb: FormBuilder,
    private service: TimepickerService,
    private userService: UserService,
    private configStore: AppConfigStoreService,
    public globals: Globals,
    public media: MediaObserver
  ) {}

  ngOnInit(): void {
    if (this.time === null || this.time === undefined) {
      throw new TypeError('The input "time" is required');
    }
    if (this.max === null || this.max === undefined) {
      throw new TypeError('The input "max" is required');
    }
    this.service.configCheck(this.time, this.momentOnly);
    this.tzOffset = this.project.getOffset(this.max);
    this.prepForm();
  }

  ngOnDestroy(): void {
    this.rxSubs.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.max) {
      this.onMaxChange(changes.max.currentValue);
    }
  }

  onMaxChange = (v: Date): void => {
    const isDate = Object.prototype.toString.call(v) === '[object Date]';
    if (v && !isDate) {
      this.max = new Date(v);
    }

    const dString = moment.tz(v, this.project.timezone).format('YYYY/MM/DD');
    this.matDatepickerMax = new Date(dString);

    if (this.timesForm) {
      this.timesForm.get('maxDate').setValue(this.max, { emitEvent: false });
    }
  };

  onSave = (): void => {
    const realtime = this.timesForm.get('realtime').value;
    const end = realtime
      ? this.max
        ? moment(this.max)
        : moment()
      : this.controlAsDate('end');
    const response: Time = {
      realtime: this.enableRealtime && realtime,
      end: end.toDate(),
      start:
        realtime || this.momentOnly
          ? new Date(end.valueOf() - (this.getDuration() || 60) * 6e4)
          : this.controlAsDate('start').toDate(),
    };
    this.done = true;
    this.timesChange.emit(response);
    this.configStore.setConfigs([{
      id: 'timepicker',
      type: 'time',
      pid: this.project.id,
      value: response,
      temp: true
    }]);
  };

  onCancel = (): void => {
    this.done = true;
    this.timesChange.emit();
  };

  onPeriodChange(change): void {
    const end = this.controlAsDate('end');
    if (change.value && end && end.isValid()) {
      const start = moment(end).subtract(change.value, 'minutes');
      if (start.isValid()) {
        const opts = { emitEvent: false };
        this.timesForm.get('startDate').setValue(start, opts);
        this.timesForm.get('startTime').setValue(start.format('HH:mm'), opts);
      }
    }
  }

  private prepForm(): void {
    const controls = {
      maxDate: new FormControl(this.max),
      realtime: new FormControl(this.time.realtime),
      _momentOnly: new FormControl(this.momentOnly), // for validation
      _timezone: new FormControl(this.project.timezone),
    };
    this.timesForm = this.fb.group(controls);
    this.rxSubs.add(
      this.timesForm
        .get('realtime')
        .valueChanges.subscribe(this.onRealtimeChange)
    );
    const timeControls = this.getTimeControls();
    const validators = [];
    if (!!this.max) {
      validators.push(DateValidator.maximums);
    }
    if (!this.momentOnly) {
      validators.push(DateValidator.range, DateValidator.duration);
    }
    timeControls.forEach((prop: string) => {
      const controlValue = moment.tz(
        prop.includes('start') ? this.time.start : this.time.end,
        this.project.timezone
      );

      const control = prop.includes('Date')
        ? new FormControl(controlValue, [DateValidator.incompleteDate])
        : new FormControl(controlValue.format('HH:mm'), [
            DateValidator.incompleteTime,
          ]);

      this.timesForm.addControl(prop, control);
      this.rxSubs.add(
        this.timesForm
          .get(prop)
          .valueChanges.subscribe((v) => this.onChangeTimeControl(v, prop))
      );
    });
    this.timesForm.setValidators(validators);
    this.selectedPeriod = this.getDuration();
  }

  private onChangeTimeControl = (val, prop): void => {
    if (val) {
      const isEnd = prop.includes('end');
      const newTime = {};
      if (isEnd) {
        const end = this.controlAsDate('end');
        newTime['end'] = end ? end.toDate() : new Date(this.time.end);
        if (!this.momentOnly) {
          this.onPeriodChange({ value: this.selectedPeriod });
          const start = this.controlAsDate('start');
          newTime['start'] = start ? start.toDate() : new Date(this.time.start);
        }
      } else {
        const start = this.controlAsDate('start');
        newTime['start'] = start ? start.toDate() : new Date(this.time.start);
      }

      for (const key in newTime) {
        if (newTime.hasOwnProperty(key) && !newTime[key]) {
          delete newTime[key];
        }
      }

      this.selectedPeriod = this.getDuration();
      this.time = { ...this.time, ...(newTime as Time) };
    }
  };

  private onRealtimeChange = (val): void => {
    if (val === true) {
      if (!this.momentOnly) {
        const duration = this.getDuration();
        if (duration > 0) {
          this.selectedPeriod = duration;
        } else {
          this.selectedPeriod = 60;
        }
      }
      this.resetInvalidControls();
    } else {
      this.selectedPeriod = this.getDuration();
    }
  };

  private controlAsDate(control: string): moment.Moment {
    const vals = {
      d: this.timesForm.get(control + 'Date'),
      t: this.timesForm.get(control + 'Time'),
    };
    if (
      !vals.t ||
      !vals.t.value ||
      !vals.d ||
      !vals.d.value ||
      !vals.d.value.isValid()
    ) {
      return null;
    }
    const isoString = `${vals.d.value.format('YYYY-MM-DD')}T${vals.t.value}`;
    return moment.tz(
      isoString,
      'YYYY-MM-DDTHH:mm',
      true,
      this.project.timezone
    );
  }

  private getTimeControls(): string[] {
    const timeControls = ['endDate', 'endTime', 'startDate', 'startTime'];
    if (this.momentOnly) {
      timeControls.splice(2, 2);
    }
    return timeControls;
  }

  // current duration in minutes between start and end
  private getDuration = (): number => {
    const start = this.controlAsDate('start');
    const end = this.controlAsDate('end');
    if (!this.momentOnly && !!start && !!end) {
      const d = end.diff(start, 'minutes');
      return !!this.periods.find((r) => r.value === d) ? d : 0;
    }
    return 0;
  };

  private resetInvalidControls = (): void => {
    const timeControls = this.getTimeControls();
    timeControls.forEach((prop: string) => {
      const isDate = prop.includes('Date');
      const isStart = prop.includes('start');
      const ctrl = this.timesForm.get(prop);
      if (ctrl && ctrl.invalid) {
        const dateObj: Date = isStart ? this.time.start : this.time.end;
        const ctrlValue = moment(dateObj);
        ctrl.setValue(isDate ? ctrlValue : ctrlValue.format('HH:mm'), {
          emitEvent: false,
        });
      }
    });
  };
}
