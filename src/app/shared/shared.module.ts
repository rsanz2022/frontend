import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NavbarModule } from './navbar/navbar.module';
import { PageNotFoundComponent } from './page-not-found.component';

@NgModule({
  imports: [
    CommonModule,
    NavbarModule
  ],
  declarations: [
    PageNotFoundComponent
  ],
  exports: [
    NavbarModule,
    PageNotFoundComponent,
    FormsModule
  ]
})
export class SharedModule { }
