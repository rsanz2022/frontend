import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HydrographComponent } from '../hydrograph/hydrograph.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [HydrographComponent],
  exports: [HydrographComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FlexLayoutModule
  ]
})
export class HydrographModule { }
