import { TestBed } from '@angular/core/testing';

import { HydrographService } from './hydrograph.service';

describe('HydrographService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HydrographService = TestBed.get(HydrographService);
    expect(service).toBeTruthy();
  });
});
