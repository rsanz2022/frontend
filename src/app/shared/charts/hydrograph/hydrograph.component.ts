import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  ElementRef,
} from '@angular/core';
import { UserService } from '@app/core';
import { HydrographService } from './hydrograph.service';
import * as c3 from 'c3';
import * as moment from 'moment-timezone';
import { LngLat } from 'mapbox-gl';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Time } from '@app/shared/models';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

export interface POITime extends Time {
  fakeNow?: Date;
  fakeNRT?: boolean;
}

@Component({
  selector: 'app-hydrograph',
  templateUrl: './hydrograph.component.html',
  styleUrls: ['./hydrograph.component.scss'],
})
export class HydrographComponent implements OnInit, AfterViewInit {
  private _id;
  private viewInit = new BehaviorSubject<boolean>(false);
  timezone: string;
  timestamp: number = null;
  chart: any;
  loading = true;
  error: string;
  chartId = 'chart-' + Date.now();

  private _hideNow = false;
  get hideNow(): boolean {
    return this._hideNow;
  }
  @Input()
  set hideNow(value: boolean) {
    this._hideNow = coerceBooleanProperty(value);
  }
  @Input() times: POITime;
  @Input() showLegend = false;
  @Input() groupTooltip = false;
  @Input() attributes: string[] = [];
  @Input() whatif: string;
  @Input() lngLat: LngLat;
  @Input() set id(id: string) {
    this._id = id;
    this.error = '';
    this.loading = true;
    setTimeout(() =>
      this.getChartData().subscribe({
        next: (chartConfig) => {
          if (chartConfig) {
            this.chart = c3.generate(chartConfig);
            this.chart.flush();
          }
        },
        error: this.onError,
      })
    );
  }
  @Input() set update(timestamp: number) {
    if (timestamp > this.timestamp) {
      if (this.timestamp !== null) {
        this.loading = true;
        this.chart = this.chart.destroy();
        this.getChartData().subscribe({
          next: (config) => {
            this.loading = false;
            this.error = '';
            this.chart = c3.generate(config);
            return config;
          },
          error: this.onError,
        });
      }
      this.timestamp = timestamp;
    }
  }

  constructor(
    private elRef: ElementRef,
    private userService: UserService,
    private hydrographService: HydrographService
  ) {}

  ngOnInit(): void {
    if (!this.times && !this.whatif) {
      throw new TypeError(`'times' input parameter is required.`);
    }
  }

  ngAfterViewInit(): void {
    this.viewInit.next(true);
    this.timezone = this.userService.project.timezone;
  }

  getChartData = (): Observable<any> => {
    const elWidth = this.elRef.nativeElement.offsetWidth;
    const elHeight = this.elRef.nativeElement.offsetHeight;
    let req;
    switch (this.attributes[0]) {
      case 'inundation':
        req = this.hydrographService.getInundation(
          this.lngLat,
          this.times,
          this.whatif
        );
        break;

      case 'velocity':
        req = this.hydrographService.getVelocity(this._id, this.times);
        break;

      case 'soil-moisture':
        req = this.hydrographService.getSoilMoisture(this._id, this.times);
        break;

      default:
        req = this.hydrographService.getRawData(
          this._id,
          this.times,
          this.whatif
        );
        break;
    }

    return req.pipe(
      map((res: any) => {
        const isNA = res?.status === 'NA';
        this.loading = false;
        if (!res || isNA) {
          const time = moment
            .tz(this.times.end, this.timezone)
            .format('YYYY/MM/DD HH:mm');
          const msg = isNA
            ? 'Inundation unavailable at this location for ' + time
            : 'Hydrograph unavailable';
          this.error = msg;
          return throwError(msg);
        }
        const chartConfig: any = this.hydrographService.getChartSettings({
          bindto: '#' + this.chartId,
          isInundation: this.attributes.includes('inundation'),
          isVelocity: this.attributes.includes('velocity'),
          timezone: this.userService.project.timezone,
          elHeight,
          elWidth,
          legend: this.showLegend,
          groupTooltip: this.groupTooltip,
          hideNow: this.hideNow,
          ...res,
        });

        if (!!this.times?.fakeNow) {
          chartConfig.grid.x.lines = [
            {
              value: this.times.fakeNow,
              text: `Time: ${moment
                .tz(this.times.fakeNow, this.userService.project.timezone)
                .format('HH:mm')}`,
            },
          ];
        }
        return chartConfig;
      })
    );
  };

  onError = (): void => {
    this.loading = false;
    this.error = 'Hydrograph unavailable';
  };
}
