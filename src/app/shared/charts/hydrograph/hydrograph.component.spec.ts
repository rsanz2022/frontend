import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HydrographComponent } from './hydrograph.component';

describe('HydrographComponent', () => {
  let component: HydrographComponent;
  let fixture: ComponentFixture<HydrographComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HydrographComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HydrographComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
