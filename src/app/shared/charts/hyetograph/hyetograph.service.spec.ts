import { TestBed } from '@angular/core/testing';

import { HyetographService } from './hyetograph.service';

describe('HyetographService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HyetographService = TestBed.get(HyetographService);
    expect(service).toBeTruthy();
  });
});
