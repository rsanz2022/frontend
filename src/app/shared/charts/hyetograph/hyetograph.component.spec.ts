import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HyetographComponent } from './hyetograph.component';

describe('HyetographComponent', () => {
  let component: HyetographComponent;
  let fixture: ComponentFixture<HyetographComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HyetographComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HyetographComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
