import { AfterViewInit, Component, Input } from '@angular/core';
import * as c3 from 'c3';
import { ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';

interface VAIChartConfig {
  axis: { x?: string; y?: string; y2?: string };
  data: c3.Data;
}

@Component({
  selector: 'app-scatter-plot',
  template: '<div [id]="id"></div>',
  styleUrls: ['./scatter-plot.component.scss'],
})
export class ScatterPlotComponent implements AfterViewInit {
  id = 'chart' + Date.now();
  chart$ = new ReplaySubject(1);
  chart: c3.ChartAPI;
  private readonly config = {
    transition: { duration: 0 },
    data: {
      columns: [],
      colors: { 'Trend Line': '#aaa', Gauges: '#0097a7' },
    },
    axis: {
      x: { tick: { fit: false }, min: 0, padding: { left: 0, right: 0 } },
      y: { min: 0, padding: { top: 0, bottom: 0 } },
    },
    grid: { x: { show: true }, y: { show: true } },
    padding: { right: 8, left: 32 },
    tooltip: { show: false },
    legend: { show: false },
    point: { focus: { expand: { enabled: false } } },
  };
  @Input() size: { width?: number; height?: number };
  @Input()
  set data(v: VAIChartConfig) {
    if (!v) return;
    this.chart$.pipe(take(1)).subscribe(() => {
      this.chart.load({
        columns: v.data.columns,
        types: v.data.types,
        unload: true,
        xs: v.data.xs,
      });
    });
    this.chart.axis.labels(v.axis);
    setTimeout(this.chart.resize, 500);
  }

  ngAfterViewInit(): void {
    this.chart = c3.generate({
      ...this.config,
      bindto: '#' + this.id,
      size: this.size,
      oninit: () => this.chart$.next(null),
    });
  }
}
