import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import * as moment from 'moment-timezone';
import { UserService } from './core';
import { onAuthUIStateChange, AuthState } from '@aws-amplify/ui-components';
import { Subscription } from 'rxjs';
import { TimepickerService } from './shared/timepicker/timepicker.service';
import { environment } from '@env/environment';
import { Project } from './shared/models';

declare const gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  projSubs: Subscription[] = [];
  signedIn = false;
  user = false;
  error = false;
  project: Project;

  constructor(
    private userService: UserService,
    private router: Router,
    private tpService: TimepickerService,
  ) {
    onAuthUIStateChange(this.handleStateChange);
  }

  ngOnInit(): void {
    /* Google Analytics */
    gtag('config', 'UA-35239755-5', {
      debug_mode: +!environment.production,
      custom_map: {
        dimension1: 'org',
        dimension2: 'proj',
        dimension3: 'internal',
      },
    });
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe({
        next: (event: NavigationEnd) =>
          gtag('config', 'UA-35239755-5', {
            page_path: event.urlAfterRedirects,
          }),
        error: (e) => console.error(e)
      });

    moment.updateLocale('en', {
      relativeTime: {
        future: 'in %s',
        past: '%s ago',
        s: 'a few seconds',
        ss: '%d seconds',
        m: '1 minute',
        mm: '%d minutes',
        h: '1 hour',
        hh: '%d hours',
        d: '24 hours',
        dd: '%d days',
        M: '1 month',
        MM: '%d months',
        y: '1 year',
        yy: '%d years',
      },
    });

    this.userService.currentUser.subscribe((user) => (this.user = !!user));
    this.userService.project$.subscribe((proj) => {
      this.clearSubs();
      this.tpService.maxtimes = [];
      this.project = proj;
      this.maxtimeSub();
      if (proj.products.includes('rules-manager')) {
        this.projSubs.push(
          this.tpService.alertsObs$(proj.symbolicName).subscribe()
        );
      }
    });
  }

  private maxtimeSub = () => {
    if (!this.signedIn) return;
    this.projSubs.push(
      this.tpService
        .maxtimesObs$(this.project)
        .subscribe({ complete: this.maxtimeSub })
    );
  };

  private handleStateChange = (event: AuthState): void => {
    switch (event) {
      case 'signedin':
        if (!this.signedIn) {
          this.signedIn = true;
          this.userService.myUser(true).subscribe({
            next: (user) => {
              if (
                user.company &&
                user.company.projects &&
                user.company.projects.length
              ) {
                this.userService.setUser(user);
                this.userService.setCurrentProject();
              } else this.error = true;
            },
            error: () => (this.error = true)
          });
        }
        break;

      default:
        this.user = null;
        this.clearSubs();
        this.signedIn = false;
        break;
    }
  };

  private clearSubs = (): void => {
    this.projSubs.forEach((s) => s.unsubscribe());
    this.projSubs = [];
  };
}
