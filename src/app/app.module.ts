import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { DashboardModule } from './features';
import { SharedModule } from './shared';
import { Globals } from './globals';
import { GraphQLModule } from './graphql.module';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { AmplifyUIAngularModule } from '@aws-amplify/ui-angular';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './core/auth.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AmplifyUIAngularModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    CoreModule,
    DashboardModule,
    SharedModule,
    GraphQLModule,
    FlexLayoutModule,
    MatMomentDateModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    Title,
    Globals,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
