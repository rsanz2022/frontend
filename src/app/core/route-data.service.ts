import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { map, filter } from 'rxjs/operators';
import { Observable, from } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class RouteDataService {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  getData(): Observable<any> {
    return from(this.router.events)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) { route = route.firstChild; }
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        map(route => route.data['value'])
    );
  }
}
