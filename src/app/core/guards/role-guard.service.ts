import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from '../user.service';
import { Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';


@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(
    public router: Router,
    public userService: UserService
  ) {}
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const expectedRole = route.data.expectedRole;
    return this.userService.currentUser.pipe(
      first(u => !!u),
      map(user => user.role <= expectedRole)
    );
  }
}
