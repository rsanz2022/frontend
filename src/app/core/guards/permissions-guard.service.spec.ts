import { TestBed } from '@angular/core/testing';

import { PermissionsGuardService } from './permissions-guard.service';

describe('PermissionsGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PermissionsGuardService = TestBed.get(PermissionsGuardService);
    expect(service).toBeTruthy();
  });
});
