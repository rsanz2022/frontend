import { TestBed } from '@angular/core/testing';

import { BetaGuardService } from './beta-guard.service';

describe('BetaGuardService', () => {
  let service: BetaGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BetaGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
