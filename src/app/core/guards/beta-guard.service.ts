import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { environment } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class BetaGuardService  implements CanActivate {
  canActivate(): boolean {
    return !environment.production;
  }
}
