import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  OnDestroy,
  Input,
} from '@angular/core';
import * as moment from 'moment-timezone';
import { RadarInfo, RadarLatest } from '../radar-info.model';
import { ForecastService } from '../forecast.service';
import { UserService } from '@app/core';
import { Subscription, interval } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { TimeSelectDialogComponent } from '../time-select-dialog/time-select-dialog.component';
import { Project } from '@app/shared/models';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface RadarOptions {
  accumulation?: boolean;
  opacity?: number;
}

type AgeTypes = 'late' | 'old';

interface AgeStatusResponse {
  message: string;
  className: AgeTypes;
}

@Component({
  selector: 'app-radar-info',
  templateUrl: './radar-info.component.html',
  styleUrls: ['./radar-info.component.scss'],
})
export class RadarInfoComponent implements OnInit, OnDestroy {
  private rxSubs: Subscription[] = [];
  radars: RadarInfo[];
  showOptions = false;
  hoursDiff: number;
  ageStatus: AgeTypes;
  shapes;
  infoError = 0;
  updateInterval: Subscription;
  isLatest = true;
  private project: Project;
  @Input() loading = true;
  @Input() error = false;
  private _opacity: number;
  @Input()
  get opacity(): number {
    return +this._opacity;
  }
  set opacity(v: number) {
    if (this._opacity !== undefined && this._opacity !== v) {
      this.optionsChange.emit({ opacity: v });
    }
    this._opacity = v;
  }
  @Output() selectRadar = new EventEmitter<RadarInfo>();
  @Output() optionsChange = new EventEmitter<RadarOptions>();
  private _accumulation = false;
  get accumulation(): boolean {
    return this._accumulation;
  }
  set accumulation(v: boolean) {
    this._accumulation = v;
    this.optionsChange.emit({ accumulation: v });
  }
  private _radar: RadarInfo;
  public get radar(): RadarInfo {
    return this._radar;
  }
  public set radar(v: RadarInfo) {
    v.selectedShape = v.selectedShape || null;
    this._radar = v;
    this.shapes = v.shapes;
    this.selectRadar.emit(this._radar);
    this.setHoursDiff();
  }

  constructor(
    public service: ForecastService,
    private userService: UserService,
    private dialog: MatDialog,
    private snack: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(this.userService.project$.subscribe(this.onProjectUpdate));
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
    this.updateInterval.unsubscribe();
    this.snack.dismiss();
  }

  openTimesDialog = (): void => {
    const dialogRef = this.dialog.open(TimeSelectDialogComponent, {
      data: this.radar,
      autoFocus: false,
    });

    dialogRef.beforeClosed().subscribe(this.onCloseTimeDialog);
  };

  onCloseTimeDialog = (t: RadarLatest): void => {
    if (t) {
      const radar = this.radars.find((radar) => radar.id === this.radar.id);
      const selectedIndex = radar.available.findIndex((a) => a.time === t.time);
      const selectedTime: RadarLatest = selectedIndex > -1
        ? radar.available[selectedIndex]
        : radar.latest;
      radar.latest = selectedTime;
      this.radar = radar;
      this.isLatest = selectedIndex < 1;
    }
  };

  getRadarInfo = (): void => {
    this.service.getInfo(this.project).then(
      (radars) => {
        this.infoError = 0;
        this.radars = radars;
        if (this.radar) {
          const newRadar = radars.find((e) => e.id === this.radar.id);
          if (newRadar) {
            newRadar.selectedShape = this.radar.selectedShape;
            if (newRadar.available) {
              newRadar.latest = this.isLatest
                ? newRadar.available[0]
                : newRadar.available.find(
                    (a) => a.time === this.radar.latest.time
                  ) || newRadar.latest;
            }
            this.radar = newRadar;
          } else this.radar = radars[0];
        } else {
          this.radar = radars[0];
        }
      },
      (err) => {
        if (this.infoError < 3) {
          console.error(err);
          setTimeout(this.getRadarInfo, 15000);
          this.infoError++;
        }
      }
    );
  };

  onProjectUpdate = (proj: Project): void => {
    this.project = proj;
    if (!!this.updateInterval) {
      this.updateInterval.unsubscribe();
    }
    this.updateInterval = interval(1000 * 60 * 2.5).subscribe(
      this.getRadarInfo
    );
    this.getRadarInfo();
  };

  selectShape = (r, selectedShape): void => {
    const radar = this.radars.find((radar) => radar.id === r.id);
    radar.selectedShape = selectedShape;
    this.radar = radar;
  };

  private setHoursDiff = (): void => {
    this.snack.dismiss();
    const latestTime = this.radar.available
      ? this.radar.available[0].time
      : this.radar.latest.time;

    // [yellow, red] for PV, HRRR/NDFD, and other
    const thresholds = [
      [0.5, 1],
      [4, 5],
      [9, 12],
    ];
    this.hoursDiff = moment().diff(latestTime, 'hours', true);
    let status: AgeStatusResponse;

    if (this.radar.type === 'previeux') {
      status = this.getAgeStatus(thresholds[0]);
    } else if (
      this.radar.meta.source === 'HRRR' ||
      this.radar.meta.source === 'NDFD'
    ) {
      status = this.getAgeStatus(thresholds[1]);
    } else {
      status = this.getAgeStatus(thresholds[2]);
    }

    this.ageStatus = status.className;

    if (status.className) {
      this.snack.open(status.message, 'OK', { panelClass: status.className });
    }
  };

  private getAgeStatus(thresholds: number[]): AgeStatusResponse {
    let msg = 'Latest data was received over ';
    let className: AgeTypes;
    let duration: string;
    if (this.hoursDiff >= thresholds[0] && this.hoursDiff < thresholds[1]) {
      duration = moment.duration(thresholds[0], 'hours').humanize();
      className = 'late';
    }
    if (this.hoursDiff >= thresholds[1]) {
      duration = moment.duration(thresholds[1], 'hours').humanize();
      className = 'old';
    }
    msg += duration + ' ago.';
    return { message: msg, className };
  }
}
