import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeSelectDialogComponent } from './time-select-dialog.component';

describe('TimeSelectDialogComponent', () => {
  let component: TimeSelectDialogComponent;
  let fixture: ComponentFixture<TimeSelectDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeSelectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSelectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
