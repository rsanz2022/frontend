import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RadarInfo } from '../radar-info.model';

@Component({
  selector: 'app-time-select-dialog',
  templateUrl: './time-select-dialog.component.html',
  styleUrls: ['./time-select-dialog.component.scss']
})
export class TimeSelectDialogComponent {

  constructor(
    public dialog: MatDialogRef<TimeSelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RadarInfo
  ) {}

}
