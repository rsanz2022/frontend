import {Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Basemap, Project } from '@app/shared/models';
import { map } from 'rxjs/operators';
import { RadarInfo, Frame, RadarLatest } from './radar-info.model';
import * as mapboxgl from 'mapbox-gl';
import { UserService } from '@app/core';


import { VectorData } from './forecast.component';
import * as moment from 'moment-timezone';

interface Response {
  getRadars?: RadarInfo[];
}

@Injectable({ providedIn: 'root' })
export class ForecastService {
  readonly basemaps = [
    { name: 'Streets', id: 'streets-v11' },
    { name: 'Dark', id: 'dark-v10' },
    { name: 'Light', id: 'light-v10' },
    { name: 'Satellite', id: 'satellite-streets-v11' }
  ];
  basemap = this.basemaps[0];
  hideOptions = true;

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private apollo: Apollo
  ) {}

  getStyles = (): Promise<Basemap[]> =>
    this.http.get<Basemap[]>('assets/basemaps.json').toPromise();

  getInfo = (project: Project): Promise<RadarInfo[]> => {
    const query = {
      query: gql`
        query Previeux($project: String!) {
          getRadars(project: $project) {
            id
            name
            type
            bounds
            latest {
              time
              zrDisplayName
              zr_a
              zr_b
              bias
              frames { time file }
            }
            shapes { id name bounds }
            meta {
              source
              durationHours
              intervalDisplay
              interval_minutes
            }
          }
        }
      `,
      variables: { project: project.symbolicName }
    };
    return this.apollo
      .query<Response>(query)
      .pipe(
        map(res => {
          const radars = res.data.getRadars;
          if (!radars || !radars.length) {
            throw new Error(`Query "getRadars" error`);
          }

          radars.forEach(r => this.prepForecast(r, project.timezone));
          return radars as RadarInfo[];
        })
      ).toPromise();
  };

  prepForecast = (r: any, tz: string): void => {
    if (Array.isArray(r.latest)) {
      r.latest.forEach((l, i) => {
        if (i === 0) l.isLatestTime = true;
        l.displayTime = moment
          .tz(l.time, tz).format('YYYY/MM/DD HH:mm');
        l.frames.forEach(f => f.tzOffset = this.userService.project
          .getOffset( new Date(f.time)));
      });
      r.available = r.latest.sort(this.sortAvailableDecending);
      r.latest = r.latest[0];
    }
    if (r.latest.frames) {
      r.latest.frames = r.latest.frames.map(frame => ({
        ...frame,
        tzOffset: this.userService.project.getOffset(
          new Date(frame.time)
        )
      }));
    }
    r['bbox'] = this.getBbox(r.bounds);
    r['title'] = r.type === 'nws' ?
      r.meta.source : `PreVieux: ${r.name}`;
  }
  sortAvailableDecending(a: RadarLatest, b: RadarLatest): number {
    if (a.time < b.time) return 1;
    if (a.time > b.time) return -1;
    return 0;
  }

  getBbox(b: number[][]): number[][] {
    const sw = b[0];
    const ne = b[1];
    return [[sw[0], ne[1]], ne, [ne[0], sw[1]], sw];
  }

  getLayer(info: RadarInfo): mapboxgl.Layer {
    return {
      id: 'forecast',
      metadata: info,
      type: 'fill',
      source: {
        type: 'vector',
        tiles: [this.getUrl(info)]
      }
    };
  }

  getScaleExpression(
    projId: string,
    max = 12
  ): Promise<mapboxgl.Expression> {
    const url = `/api/${projId}/rainvieux/scale/expression?scaleMax=${max}`;
    return this.http.get<mapboxgl.Expression>(url).toPromise();
  }

  getData(
    projId: string,
    info: RadarInfo,
    frame: Frame,
    accum: boolean,
    historical: boolean
  ): Promise<VectorData[]> {

    let url: string;
    const queryParams: string[] = [
      'accum=' + !!accum,
      'r=' + (historical ? frame.time : info.available[0].time),
    ];
    if (info.type === 'previeux') {
      const basin = info.selectedShape ? '/basins/' + info.selectedShape.id : '';
      const path = 'previeux';
      const image = `/${info.latest.time}/frames/${frame.time}${basin}`;
      url = `/api/${projId}/${path}/${info.id}${image}/data`;
    } else {
      const basin = info.selectedShape ? '/basins/' + info.selectedShape.id : '';
      const path = 'nws/slideshow/legacy';
      url = `/api/${projId}/${path}/${info.id}${basin}/${frame.time}/data.json`;
      if (historical) {
        queryParams.push(`time=${info.latest.time}`)
      }
    }
    return this.http
      .get<VectorData[]>(url + `?${queryParams.join('&')}`)
      .toPromise();
  }

  getUrl(info: RadarInfo): string {
    const basin = info.selectedShape ? '/basins/' + info.selectedShape.id : '';
    // adjusted since nws basin layer mvt donesn't have id only shape id
    const infoId = info.type === 'previeux' || basin.length === 0 ? `/${info.id}` : '';
    const path = info.type === 'previeux' ? 'previeux' : 'nws/slideshow';
    return `${window.location.origin}/api/${this.userService.project.symbolicName}/${path}${infoId}${basin}/{z}/{x}/{y}.mvt`;
  }

  getMaskUrl(radar: RadarInfo): string {
    if (radar.type === 'previeux' && !radar.selectedShape) {
      return `/api/service/map/radar/${radar.id}/rangering`;
    }
  }

  convertExpression(exp: mapboxgl.Expression, max: number): mapboxgl.Expression {
    const change = max / exp[exp.length - 2];
    const newExp = exp.map((item, index) =>
      index > 2 && index % 2 !== 0
        ? Math.round(item * change * 1e5) / 1e5
        : item
    ) as mapboxgl.Expression;
    return newExp;
  }
}
