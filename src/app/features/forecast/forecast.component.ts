import { Component, OnDestroy, OnInit } from '@angular/core';
import { ForecastService } from './forecast.service';
import { UserService } from '@app/core';
import { RadarInfo, Frame, RadarOptions } from './radar-info.model';
import mapboxgl from 'mapbox-gl';
import moment from 'moment-timezone';
import { forkJoin, ReplaySubject, Subject, Subscription } from 'rxjs';
import { first, take } from 'rxjs/operators';
import { Project } from '@app/shared/models';
import { MediaObserver } from '@angular/flex-layout';
import { BehaviorSubject } from 'rxjs';
import Auth from '@aws-amplify/auth';

export interface VectorData {
  vid: number;
  val: number;
}

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss'],
})
export class ForecastComponent implements OnDestroy, OnInit {
  private rxSubs: Subscription[] = [];
  project: Project;
  loadingTiles = true;
  loadingFrame = true;
  progressTimer: ReturnType<typeof setTimeout>;
  animating: false;
  progressCounter = 0;
  preloading = true;
  error = false;
  sourceData$ = new BehaviorSubject<mapboxgl.MapSourceDataEvent>(null);
  smallScreen = false;
  private _layer: mapboxgl.Layer;
  readonly layer$ = new ReplaySubject<mapboxgl.Layer>(1);
  get layer(): mapboxgl.Layer {
    return this._layer;
  }
  set layer(v: mapboxgl.Layer) {
    this._layer = v;
    this.layer$.next(v);
  }
  maskUrl = '';
  scale = {
    src: '',
    width: 40,
    show: false,
  };
  expressionSrc: mapboxgl.Expression;
  units: string;
  opacity = 0.8;
  accum = false;
  bounds: mapboxgl.LngLatBoundsLike = this.userService.project.bounds || [
    [-134.648, 51.835],
    [-64.511, 21.943],
  ];
  clickedFeatures = [];
  popup: any;
  ttips: { val: number | string; name: string }[];
  mapgl: mapboxgl.Map;
  readonly mapgl$ = new ReplaySubject<mapboxgl.Map>(1);
  frames: Frame[];
  frame: number;
  token: string;

  readonly layerData$ = new BehaviorSubject<VectorData[][]>([]);
  get layerData(): VectorData[][] {
    return this.layerData$.getValue();
  }
  set layerData(d: VectorData[][]) {
    this.layerData$.next(d);
  }

  public radar: RadarInfo;

  constructor(
    public service: ForecastService,
    private userService: UserService,
    private media: MediaObserver
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(
      this.userService.project$.subscribe((p) => (this.project = p)),
      this.media
        .asObservable()
        .subscribe(() => (this.smallScreen = this.media.isActive('lt-md')))
    );

    Auth.currentSession().then(
      (session) => (this.token = session.getIdToken().getJwtToken())
    );
  }

  setRadar = (radar: RadarInfo): void => {
    this.popup = null;
    if (radar) {
      const newLayer =
        !this.radar ||
        this.radar.id !== radar.id ||
        this.radar.selectedShape !== radar.selectedShape;
      if (newLayer || this.radar.latest.time !== radar.latest.time) {
        this.radar = { ...radar };
        this.layer = null;
        this.loadingTiles = true;
        this.sourceData$.next(null);
        this.createLayer();
        this.maskUrl = this.service.getMaskUrl(this.radar);
        if (newLayer) {
          this.bounds = this.radar.selectedShape
            ? this.radar.selectedShape.bounds
            : this.radar.bounds;
        }
        this.setFrames(this.radar.latest.frames);
      }
    } else this.handleError('Radar is null');
  };

  setFrame(f: number): void {
    this.frame = f;
    this.layerData$
      .pipe(first((data: VectorData[][]) => !!data.length))
      .subscribe(this.loadFrame);
  }

  private setFrames(frames: Frame[]) {
    this.layerData = [];
    this.loadingFrame = true;
    this.preloading = true;
    this.frames = [...frames];
    setTimeout(() => {
      this.getLayerData(this.frame, this.frame).then((currentFrame) => {
        this.layerData = currentFrame;
        setTimeout(() =>
          this.sourceData$
            .pipe(first((e) => !!e && !this.loadingTiles))
            .subscribe(() =>
              this.getLayerData().then((allFrames) => {
                allFrames.splice(this.frame, 1, currentFrame[0]);
                this.layerData = allFrames;
                this.preloading = false;
                this.setFrame(this.frame);
              }, this.handleError)
            )
        );
      }, this.handleError);
    });
  }

  ngOnDestroy = (): void => {
    this.rxSubs.forEach((s) => s.unsubscribe());
  };

  private handleError = (e): void => {
    this.error = true;
    this.preloading = false;
    this.loadingFrame = false;
    console.error('Forecast Data Error:', e);
  };

  public setMap = (v: mapboxgl.Map): void => {
    this.mapgl = v;
    this.mapgl$.next(v);
  };

  changeBasemap = (v: { id: string; name: string }): void => {
    this.loadingTiles = true;
    this.sourceData$.next(null);
    this.service.basemap = v;
    this.rxSubs.push(
      this.sourceData$
        .pipe(first((ev) => !!ev && !this.loadingTiles))
        .subscribe(this.loadFrame)
    );
  };

  public trackLayerBy = (idx: number, data: { id: string }): string => {
    return data.id;
  };

  private setScale = (): void => {
    this.scale.show = false;
    const isPreVieux = this.radar.type === 'previeux';
    const isMetric = this.project.measurement.system === 'metric';
    const max =
      isPreVieux || this.accum ? (isMetric ? 150 : 6) : isMetric ? 40 : 1.5;
    this.scale.width = max > 100 ? 50 : 40;
    const projId = this.userService.project.symbolicName;
    const exp = this.service.convertExpression(this.expressionSrc, max);
    this.scale.src = `/api/${projId}/rainvieux/scale/${this.scale.width}/240.png?pixelRatio=${window.devicePixelRatio}&scaleMax=${max}`;
    this.layer.paint = { ...this.layer.paint, 'fill-color': exp };
    const dataType = isPreVieux && !this.accum ? 'rain-rate' : 'rain';
    this.units = this.project.getUnitConfig(dataType).options[0].abbr;
    this.scale.show = true;
  };

  optionsChange = (event: RadarOptions): void => {
    for (const key in event) {
      if (event.hasOwnProperty(key)) {
        const element = event[key];
        switch (key) {
          case 'opacity':
            this.opacity = element;
            this.layer.paint = {
              ...this.layer.paint,
              'fill-opacity': this.opacity,
            };
            break;

          case 'accumulation':
            if (this.accum !== element) {
              this.accum = element;
              this.setFrames(this.frames);
              this.setScale();
            }
        }
      }
    }
  };

  public onMapClick = (e: mapboxgl.MapMouseEvent): void => {
    if (
      e.point &&
      this.radar &&
      !this.animating &&
      this.mapgl &&
      !this.loadingFrame &&
      !this.loadingTiles
    ) {
      this.clickedFeatures = this.mapgl.queryRenderedFeatures(e.point, {
        layers: ['forecast'],
      });
      if (this.clickedFeatures.length) {
        const geometry = this.clickedFeatures[0].geometry;
        this.popup = {
          lngLat:
            geometry && geometry.type === 'Point'
              ? geometry.coordinates
              : e.lngLat,
        };
        this.ttips = null;
      }
    }
  };

  onMouseMove = (e: mapboxgl.MapMouseEvent): void => {
    if (
      !e ||
      e.type !== 'mousemove' ||
      !this.mapgl ||
      this.smallScreen ||
      this.animating
    ) {
      this.ttips = null;
    } else {
      const features = this.mapgl
        .queryRenderedFeatures(e.point, { layers: ['forecast'] })
        .filter((f) => !!f.state.val);

      if (features.length) {
        const node = document.getElementById('map-tooltip');
        const isMetric = this.project.measurement.system === 'metric';
        const precision = isMetric ? 1 : 1000;
        this.ttips = features.map((feat) => ({
          val: Math.round(feat.state.val * precision) / precision,
          name: feat.properties.name || feat.properties.id,
        }));
        node.style.left = `${e.originalEvent.clientX + 10}px`;
        node.style.top = `${e.originalEvent.clientY + 12}px`;
        this.mapgl.getCanvas().style.cursor = 'pointer';
      } else {
        this.ttips = null;
        this.mapgl.getCanvas().style.cursor = '';
      }
    }
  };

  createLayer = (): void => {
    this.layer = null;
    setTimeout(() => {
      this.layer = {
        ...this.service.getLayer(this.radar),
        paint: {
          'fill-opacity': this.opacity,
          'fill-color': 'rgba(255,255,255,0)',
        },
      };
      const projId = this.project.symbolicName;
      const request = [this.service.getScaleExpression(projId)];
      Promise.all(this.expressionSrc ? [] : request).then((res) => {
        this.expressionSrc = this.expressionSrc || res[0];
        this.setScale();
      });
    });
  };

  getLayerData = (
    startIndex = 0,
    endIndex = this.radar.latest.frames.length
  ): Promise<VectorData[][]> => {
    // checking nws for historical since historical data is seperate
    let historical = false;
    if (this.radar.type === 'nws') {
      historical = this.radar.latest.time !== this.radar.available[0].time;
    }
    const requests = this.radar.latest.frames
      .filter((v, i) => i >= startIndex && i <= endIndex)
      .map((frame) =>
        this.service
          .getData(
            this.project.symbolicName,
            this.radar,
            frame,
            this.accum,
            historical
          )
          .catch((error) => {
            console.warn(
              `Error retrieving data for ${moment(frame.time).format(
                'YYYY/MM/DD HH:mm'
              )}`
            );
            return error;
          })
      );

    return Promise.all(requests);
  };

  onSourceData = (event?: mapboxgl.MapSourceDataEvent): void => {
    if (!event || (this.layer && event.sourceId === this.layer.id)) {
      setTimeout(() => {
        if (this.loadingTiles) {
          clearTimeout(this.progressTimer);
          this.loadingTiles = !this.mapgl.isSourceLoaded('forecast');
          if (this.loadingTiles) {
            this.progressTimer = setTimeout(() => {
              if (!event) {
                if (this.progressCounter < 3) {
                  this.progressCounter++;
                  this.onSourceData();
                } else {
                  this.loadingTiles = false;
                  this.error = true;
                }
              }
            }, 2500);
          }
        }
        this.sourceData$.next(event);
      });
    }
  };

  loadFrame = (): void => {
    const requests = [
      this.layer$.pipe(first((l) => !!l)),
      this.mapgl$.pipe(take(1)),
    ];
    forkJoin(requests).subscribe((res) => {
      const mapgl = res[1] as mapboxgl.Map;
      const data = this.layerData[this.frame];
      const forecastLoaded = new Subject();

      forecastLoaded.pipe(take(1)).subscribe(() => {
        mapgl.removeFeatureState({
          source: 'forecast',
          sourceLayer: 'shapefile-layer',
        });
        if (Array.isArray(data)) {
          this.setFeatureState(data, this.frame, this.radar.id, () =>
            setTimeout(() => (this.loadingFrame = false))
          );
        }
      });

      if (mapgl.getSource('forecast')) {
        forecastLoaded.next(0);
      } else {
        mapgl.on('sourcedata', (ev) => {
          if (ev.sourceId === 'forecast') forecastLoaded.next(0);
        });
      }
    });
  };

  setFeatureState = (
    data: VectorData[],
    frame: number,
    radarId: string,
    _callback: () => void
  ): void => {
    for (let i = data.length - 1; i >= 0; i--) {
      if (frame !== this.frame || radarId !== this.radar.id) break;
      const feature = {
        id: data[i].vid,
        source: 'forecast',
        sourceLayer: 'shapefile-layer',
      };
      this.mapgl.setFeatureState(feature, { val: data[i].val });
    }
    return frame !== this.frame || radarId !== this.radar.id
      ? null
      : _callback();
  };

  public transformRequest = (url: string): mapboxgl.RequestParameters => {
    const protocol = window.location.protocol;
    const host = window.location.host;
    if (
      url.startsWith(`${protocol}//${host}/api/`) ||
      url.startsWith(`/api/`) ||
      url.startsWith(`api/`)
    ) {
      return {
        url,
        headers: { Authorization: 'Bearer ' + this.token }
      };
    }
  };
}
