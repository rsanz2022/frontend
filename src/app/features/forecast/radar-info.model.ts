import { LngLatBoundsLike } from "mapbox-gl";

export class RadarInfo {
  id?: string;
  name?: string;
  title?: string;
  type?: string;
  bounds?: LngLatBoundsLike;
  bbox?: LngLatBoundsLike;
  available?: RadarLatest[];
  latest?: RadarLatest;
  shapes?: RadarShape[];
  selectedShape?: RadarShape;
  meta?: RadarMeta;
  isLatestTime?: boolean;
}

export interface RadarMeta {
  source: string;
  units?: string;
  durationHours: number;
  interval_minutes?: number;
  product?: string;
  intervalDisplay: string;
}

export interface RadarLatest {
  time?: number;
  frames?: Frame[];
  displayTime?: string;
}

export interface RadarShape {
  id?: string;
  name?: string;
  bounds?: LngLatBoundsLike;
  endpoint?: string;
}

export interface Frame {
  time?: number;
  file?: string;
  tzOffset?: string;
}

export interface RadarOptions {
  accumulation?: boolean;
}