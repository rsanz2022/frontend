import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Frame } from '../radar-info.model';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { Subscription, timer, queueScheduler } from 'rxjs';
import { UserService } from '@app/core';

export interface CurrentFrame {
  index: number;
  frame: Frame;
}

@Component({
  selector: 'app-time-controls',
  templateUrl: './time-controls.component.html',
  styleUrls: ['./time-controls.component.scss'],
})
export class TimeControlsComponent implements OnInit {
  private rxSubs: Subscription[] = [];
  isMobile = true;
  isPlaying = false;
  wasPlaying = false;
  loopInterval: Subscription;
  @Input() accum;
  private _loading = true;
  @Input()
  get loading(): boolean {
    return this._loading;
  }
  set loading(v: boolean) {
    const stateChange = this._loading !== v;
    this._loading = v;
    if (stateChange && !v && this.wasPlaying) this.onClickPlay();
  }

  private oldRadarId: string;
  @Input() radarId: string;

  private _frame = 0;
  get frame(): number {
    return this._frame;
  }
  set frame(v: number) {
    this._frame = v;
    if (this.frames && this.frames.length) {
      this.changeFrame.emit(v);
    }
  }

  private _frames: Frame[];
  @Input()
  get frames(): Frame[] {
    return this._frames;
  }
  set frames(frames: Frame[]) {
    this._frames = frames;
    // keep frame if same layer
    const isSame = this.oldRadarId === this.radarId;
    this.frame = !isSame ? 0 : this.frame;

    this.oldRadarId = this.radarId;
    if (this.loopInterval) {
      this.wasPlaying = isSame && this.isPlaying;
      this.isPlaying = false;
      this.loopInterval.unsubscribe();
    }
  }

  @Output() changeFrame = new EventEmitter<number>();
  @Output() animating = new EventEmitter<boolean>();

  constructor(
    public mediaObserver: MediaObserver,
    public userService: UserService
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(this.mediaObserver.media$.subscribe(this.onMediaChange));
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
    if (this.loopInterval) {
      this.loopInterval.unsubscribe();
    }
  }

  onMediaChange = (change: MediaChange): void => {
    if (change) {
      this.isMobile = this.mediaObserver.isActive('lt-sm');
    }
  };

  onClickPlay(play?: boolean): void {
    this.isPlaying = play ?? !this.isPlaying;
    this.animating.emit(this.isPlaying);
    if (this.isPlaying) {
      this.loopInterval = timer(0, 500, queueScheduler).subscribe(() => {
        this.frame = this.frame === this.frames.length - 1 ? 0 : this.frame + 1;
      });
    } else if (this.loopInterval) this.loopInterval.unsubscribe();
  }
}
