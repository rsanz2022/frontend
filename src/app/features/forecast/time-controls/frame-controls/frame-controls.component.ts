import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-frame-controls',
  templateUrl: './frame-controls.component.html',
  styleUrls: ['./frame-controls.component.scss']
})
export class FrameControlsComponent {
  Arr = Array; // Array type captured in a variable
  @Output() newFrame = new EventEmitter<number>();
  @Input() loading = true;
  @Input() isPlaying = false;
  @Input() fLength = 0;
  @Input() accum;

  public _frame = 0;
    @Input()
    set frame(v: number) {
      this._frame = v;
      if (this.isPlaying || this.loading) {
        this.sliderFrame = v;
      }
    }

  public sliderFrame = 0;

  setSliderFrame(v: string): void {
    if (!this.isPlaying && !this.loading) {
      const val = parseInt(v);
      if (this.sliderFrame !== val) {
        this.sliderFrame = val;
        if (!this.isPlaying) {
          setTimeout(() => {
            this.newFrame.emit(val);
          });
        }
      }
    }
  }
}
