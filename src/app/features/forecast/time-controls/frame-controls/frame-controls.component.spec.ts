import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameControlsComponent } from './frame-controls.component';

describe('FrameControlsComponent', () => {
  let component: FrameControlsComponent;
  let fixture: ComponentFixture<FrameControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
