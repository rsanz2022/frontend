import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForecastComponent } from '../forecast/forecast.component';
import { ForecastRoutingModule } from './forecast-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { environment } from '@env/environment';
import { TimeControlsComponent } from './time-controls/time-controls.component';
import { RadarInfoComponent } from './radar-info/radar-info.component';
import { FormsModule } from '@angular/forms';
import { ButtonGroupModule } from '@app/shared';
import { TimeSelectDialogComponent } from './time-select-dialog/time-select-dialog.component';
import { FrameControlsComponent } from './time-controls/frame-controls/frame-controls.component';
import { HyetographModule } from '@app/shared/charts/hyetograph/hyetograph.module';
import { PopupContentComponent } from './popup-content/popup-content.component'
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CorePipesModule } from '@app/core';

@NgModule({
  declarations: [
    ForecastComponent,
    TimeControlsComponent,
    RadarInfoComponent,
    TimeSelectDialogComponent,
    FrameControlsComponent,
    PopupContentComponent],
  imports: [
    CommonModule,
    ForecastRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ButtonGroupModule,
    HyetographModule,
    CorePipesModule,

    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSliderModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRippleModule,
    MatDialogModule,
    MatListModule,
    MatSnackBarModule,

    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken
    })
  ]
})
export class ForecastModule { }
