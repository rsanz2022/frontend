import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Time } from '@app/shared/models';
import { RadarInfo, RadarShape } from '../radar-info.model';

@Component({
  selector: 'forecast-popup-content',
  templateUrl: './popup-content.component.html',
  styleUrls: ['./popup-content.component.scss']
})
export class PopupContentComponent implements OnChanges{
  selectedFeature: any;
  showCharts: string[] = [];
  timestamp: number;
  times: Time;
  source: {
    type: string,
    id: string,
    name: string,
    selectedShape: RadarShape,
    interval: number, // minutes
    duration: number, // hours
  };
  @Input() radar: RadarInfo;
  @Input() features: any[];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.radar) {
      this.setRadar(changes.radar.currentValue);
    }
    if (changes.features) {
      this.featuresUpdate();
    }
  }

  private setRadar(radarInfo: RadarInfo) {
    if (!!this.radar) {
      const latest = radarInfo.latest;
      this.times = {
          realtime: latest.time !== this.radar.available[0].time,
          end: new Date(latest.time)
      };
      this.timestamp = Date.now();
      this.source = {
        type: radarInfo.type,
        id: radarInfo.id,
        name: radarInfo.name,
        selectedShape: radarInfo.selectedShape,
        interval: radarInfo.meta.interval_minutes,
        duration: radarInfo.meta.durationHours
      };
    } else {
      this.source = null;
      this.times = null;
    }
  }

  featuresUpdate(): void {
    this.selectedFeature = undefined;
    if (this.features.length === 1) {
      this.selectFeature(this.features[0]);
    }
  }

  selectFeature(feature: any): void {
    this.selectedFeature = feature;
    this.showCharts = [''];
  }
}
