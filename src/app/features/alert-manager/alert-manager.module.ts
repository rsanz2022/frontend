import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertManagerComponent } from './alert-manager.component';
import { AlertManagerRoutingModule } from './alert-manager-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ListComponent } from './components/list/list.component';
import { NavComponent } from './components/nav/nav.component';
import { DetailsComponent } from './components/details/details.component';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { FeatureSelectMapModule } from '@app/shared/feature-select-map/feature-select-map.module';
import { MatSelectModule } from '@angular/material/select';
import { ActionsDialogComponent } from './components/actions-dialog/actions-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { RuleComponent } from './components/details/rule/rule.component';
import { NotificationsComponent } from './components/details/notifications/notifications.component';
import { MatTabsModule } from '@angular/material/tabs';
import { TelInputModule } from '@app/shared';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSliderModule } from '@angular/material/slider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { environment } from '@env/environment';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    AlertManagerComponent,
    ListComponent,
    NavComponent,
    DetailsComponent,
    ActionsDialogComponent,
    RuleComponent,
    NotificationsComponent,
  ],
  imports: [
    CommonModule,
    AlertManagerRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FeatureSelectMapModule,
    TelInputModule,
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken,
    }),

    MatIconModule,
    MatRippleModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatListModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatDialogModule,
    MatButtonModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatAutocompleteModule,
  ],
})
export class AlertManagerModule {}
