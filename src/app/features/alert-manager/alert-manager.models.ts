import { AbstractControlOptions } from '@angular/forms';

export interface RuleType {
  id: string;
  name?: string;
  group: string;
  descriptor?: string;
  description?: string;
  fields?: { [key: string]: RuleField };
  window?: number;
  durationWindow?: DurationWindow;
}

export interface DurationWindow {
  min: number;
  max: number;
  step: number;
}

export interface RuleField {
  id?: string;
  dataType?: string;
  name?: string;
  optional?: boolean;
  options?: FieldOption[];
}

export interface FieldOption {
  id: string;
  label: string;
}

export interface GenericRule {
  meta: RuleMeta;
  sourceId?: string;
  layer?: string;
  idList?: string[];
  aggregation?: string;
  dataType?: string;
  threshold?: number;
  thresholdString?: string;
  thresholdIsUpperBound?: boolean;
  probabilityThreshold?: number;
  precipitationMinimum?: number;
  frequencyLabel?: string;
  unit?: string;
  duration?: number;
  windowFuture?: number;
  windowPast?: number;
  points?: [number, number][];
  thresholdDescription?: string;
}

export interface RuleMeta {
  id?: string;
  project?: string;
  name?: string;
  description?: string;
  notificationIds?: string[];
  priority?: number;
  dashboardIcon?: string;
  isActive?: boolean;
  created?: number;
  createdBy?: string;
  modified?: number;
  modifiedBy?: string;
  ruleType?: string;
}

export interface GenericNotification {
  meta: NotificationMeta;
  contacts: string[];
  contactTypes: string[];
  subject: string;
  subjectIncludesRuleName: boolean;
  includeRuleDefinition: boolean;
  includeDetails: boolean;
  notes: string;
  timeout: number;
  activeHours: string;
  activeDays: string;
  region: string;
  color: string;
  showTimer: boolean;
  options: string[];
  graphOptions: string[];
  geoJSON: string[];
}

export interface NotificationMeta {
  id: string;
  notificationType?: string;
  project: string;
  name: string;
  description?: string;
  created: number;
  createdBy: string;
  modified?: number;
  modifiedBy?: string;
}

export interface FilterGroup {
  name: string;
  icon: string;
  color: string;
  subs?: string[];
}

export interface FilterCategory {
  title: string;
  groups: FilterGroup[];
}

export interface RuleSource {
  name: string;
  id: string;
  layerName: string;
  layerID: string;
}

export interface RuleWindowFuture {
  id: string;
  name: string;
  window: number;
}

export interface DataType {
  symbolicName: string;
  displayName: string;
}

/** notificationId is required on modifyNotification */
export interface NotificationQuery {
  project: string;
  notificationId?: string;
  name: string;
  description?: string;
  notificationType: string;
  contacts?: string[];
  contactTypes?: string[];
  subject?: string;
  subjectIncludesRuleName?: boolean;
  includeRuleDefinition?: boolean;
  includeDetails?: boolean;
  notes?: string;
  timeout?: number;
  activeHours?: string;
  activeDays?: string;
  region?: string;
  color?: string;
  showTimer?: boolean;
  options?: string[];
  graphOptions?: string[];
  ruleFormat?: string;
  geoJSON?: string[];
}

/** ruleId is required on modifyRule */
export interface RuleQuery {
  project: string;
  ruleId: string;
  name: string;
  description?: string;
  ruleType: string;
  notificationIds: string[];
  priority: number;
  dashboardIcon?: string;
  sourceId?: string;
  layer?: string;
  idList?: string[];
  aggregation?: string;
  dataType?: string;
  threshold?: number;
  thresholdIsUpperBound?: boolean;
  probabilityThreshold?: number;
  precipitationMinimum?: number;
  frequencyLabel?: string;
  unit?: string;
  duration?: number;
  windowFuture?: number;
  windowPast?: number;
  points?: [number, number][];
}

export interface DialogData {
  title?: string;
  message?: string;
  rejectBtn?: string;
  confirmBtn?: string;
}

export interface RuleMutationEvent {
  type: 'create' | 'update' | 'delete';
  rule: GenericRule;
  targetId?: string;
}

export interface FormDefaults extends AbstractControlOptions {
  meta: AbstractControlOptions;
}

export interface Profile {
  id: string;
  name: string;
  phone?: string;
}
