import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlertManagerComponent } from './alert-manager.component';

const routes: Routes = [
  {
    path: '',
    component: AlertManagerComponent,
    data: { title: 'Rules Manager' },
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AlertManagerRoutingModule { }
