import { Component, OnInit } from '@angular/core';
import { UserService } from '@app/core';
import { Project } from '@app/shared/models';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { AccessManagerService } from '../access-manager';
import {
  FilterCategory,
  GenericRule,
  Profile,
  RuleMutationEvent,
  RuleType,
} from './alert-manager.models';
import { AlertManagerService } from './alert-manager.service';

interface RuleNav {
  type: string;
  group: string;
  sub: string;
}

@Component({
  selector: 'app-alert-manager',
  templateUrl: './alert-manager.component.html',
  styleUrls: ['./alert-manager.component.scss'],
})
export class AlertManagerComponent implements OnInit {
  rxSub = new Subscription();
  ruleTypes: RuleType[];
  _rules: GenericRule[];
  rules: GenericRule[];
  selected: GenericRule;
  project: Project;
  groups: Profile[];
  users: Profile[];
  typeStyles: FilterCategory;
  nav: RuleNav = { type: undefined, group: undefined, sub: undefined };

  constructor(
    private service: AlertManagerService,
    private userService: UserService,
    private amService: AccessManagerService
  ) {}

  ngOnInit(): void {
    this.rxSub.add(this.userService.project$.subscribe(this.onProject));
  }

  onProject = (project: Project): void => {
    this.project = project;
    this.groups = this.userService.user.company.groups
      .map((g) => ({
        id: g.id + '',
        name: g.name,
      }))
      .sort((a, b) => this.service.sortAlphabeticallyBy(a, b, 'name'));
    this.amService.getUsers().subscribe(
      (users) =>
        (this.users = users
          .map((u) => ({
            id: u.id + '',
            name: `${u.firstName} ${u.lastName}`,
            phone: u.phone,
          }))
          .sort((a, b) => this.service.sortAlphabeticallyBy(a, b, 'name')))
    );
    this.service.loadSources(project.symbolicName).pipe(take(1)).subscribe();
    this.service
      .getAll(project.symbolicName)
      .pipe(take(1))
      .subscribe((res) => {
        this.ruleTypes = res.ruleTypes;
        this._rules = res.rules.map((r) => ({
          ...r,
          thresholdDescription: this.service.getThresholdDescription(r),
        }));
        this.rules = [...this._rules];
        this.setStyles();
      });
  };

  onMutation = (ev: RuleMutationEvent): void => {
    ev.rule.thresholdDescription = this.service.getThresholdDescription(
      ev.rule
    );
    switch (ev.type) {
      case 'delete':
        this._rules = this._rules.filter(
          (rule) => rule.meta.id !== ev.targetId
        );
        this.rules = this.rules.filter((rule) => rule.meta.id !== ev.targetId);
        this.selected = undefined;
        break;
      case 'create':
        this._rules.push(ev.rule);
        this._rules.sort((a, b) =>
          this.service.sortAlphabeticallyBy(a.meta, b.meta, 'name')
        );
        if (this.filterCheck(ev.rule, this.nav)) {
          this.rules.push(ev.rule);
          this.rules.sort((a, b) =>
            this.service.sortAlphabeticallyBy(a.meta, b.meta, 'name')
          );
        }
        this.selected = ev.rule;
        break;

      default:
        const _i = this._rules.findIndex(
          (rule) => rule.meta.id === ev.targetId
        );
        this._rules[_i] = ev.rule;
        if (this.filterCheck(ev.rule, this.nav)) {
          const i = this.rules.findIndex(
            (rule) => rule.meta.id === ev.targetId
          );
          this.rules[i] = ev.rule;
        }
        this.selected = ev.rule;
        break;
    }
  };

  onNavSelect = (ev: RuleNav): void => {
    this.nav = ev;
    this.rules = this._rules.filter((r) => this.filterCheck(r, ev));
  };

  filterCheck = (r: GenericRule, ev: RuleNav, id?: string): boolean => {
    switch (ev.type) {
      case 'Types':
        if (ev.sub) {
          const ruleType = this.ruleTypes.find(
            (t) =>
              t.group.toLowerCase() === ev.group.toLowerCase() &&
              ev.sub.toLowerCase() === t.descriptor.toLowerCase()
          );
          return ruleType.id === r.meta.ruleType;
        }
        const allSubs = this.ruleTypes
          .filter((t) => t.group.toLowerCase() === ev.group.toLowerCase())
          .map((t) => t.id)
          .join('');
        return allSubs.includes(r.meta.ruleType);

      case 'Importance':
        const nameKey = ['Critical', 'Moderate', 'Low impact'];
        return nameKey[r.meta.priority] === ev.group;

      default:
        return true;
    }
  };

  private setStyles = (): void => {
    this.service
      .getAlertTypes(this.ruleTypes)
      .pipe(take(1))
      .subscribe((types) => (this.typeStyles = types));
  };
}
