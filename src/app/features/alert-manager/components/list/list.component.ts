import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSelectionList } from '@angular/material/list';
import { take } from 'rxjs/operators';
import { FilterGroup, GenericRule, RuleType } from '../../alert-manager.models';
import { AlertManagerService } from '../../alert-manager.service';

@Component({
  selector: 'alert-manager-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnChanges {
  sForm: FormGroup;
  filteredList: GenericRule[];
  typeStyles: { [key: string]: FilterGroup };
  ruleTypesMap: { [key: string]: RuleType } = {};
  @Input() groups: FilterGroup[];
  @Input() rules: GenericRule[];
  @ViewChild('rulesList') rulesList: MatSelectionList;
  @Input() 
    get selected(): GenericRule {
      return this._selected;
    }
    set selected(v: GenericRule) {
      this._selected = v;
      this.selectedChange.emit(v);
      if (!!v) {
        // todo: select rule in list, when passed in 
        // using this.rulesList.compareWith();
      } else if (this.rulesList) {
        this.rulesList.deselectAll();
      }
    }
    private _selected: GenericRule;
  @Output() selectedChange  = new EventEmitter<GenericRule>();

  constructor(
    private fb: FormBuilder,
    private service: AlertManagerService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.setRuleTypes();
    if (changes.rules) {
      if (changes.rules.isFirstChange()) {
        this.sForm = this.fb.group({search: ''});
        this.sForm.get('search').valueChanges.subscribe(this.filterList);
      }
      this.filteredList = this.rules;
      this.sForm.reset();
    }
    if (changes.groups && changes.groups.currentValue) {
      this.typeStyles = {};
      this.service.ruleTypes$.pipe(take(1)).subscribe(res => {
        res.forEach(type => this.typeStyles[type.id] =
            this.groups.find(group => group.name === type.group));
      });
      
    }
  }

  private setRuleTypes = (): void => {
    this.service.ruleTypes$.pipe(take(1)).subscribe(types => {
      types.forEach(t => this.ruleTypesMap[t.id] = t);
    });
  }

  filterList = (searchText: string): void => {
    if (searchText && searchText.length > 2) {
      this.filteredList = this.rules.filter(rule => {
        return JSON.stringify(rule)
          .toLowerCase()
          .includes(searchText.toLowerCase());
      });
    } else {
      this.filteredList = this.rules;
    }
  }

}
