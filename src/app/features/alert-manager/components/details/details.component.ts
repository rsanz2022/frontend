import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Project } from '@app/shared/models';
import { MyTel } from '@app/shared/tel-input/tel-input.component';
import { BehaviorSubject, firstValueFrom, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import {
  FormDefaults,
  GenericNotification,
  GenericRule,
  Profile,
  RuleMutationEvent,
  RuleType,
} from '../../alert-manager.models';
import { AlertManagerService } from '../../alert-manager.service';
import { ActionsDialogComponent } from '../actions-dialog/actions-dialog.component';
import { invalidProfile } from './notifications/profile.validator';

@Component({
  selector: 'alert-manager-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnChanges {
  @Input() project: Project;
  @Input() groups: Profile[];
  @Input() users: Profile[];
  @Input() ruleTypes: RuleType[];
  @Input() rule: GenericRule;
  @Output() mutation = new EventEmitter<RuleMutationEvent>();

  selectedIndex: number;
  notificationForm = this._fb.group({});
  ruleForm: FormGroup;
  notificationTypes: RuleType[];
  notifTabActive = false;
  newRule = true;
  defaultRuleValues: GenericRule = { meta: {} };
  invalidNotifications = true;
  loading$ = new BehaviorSubject(true);

  constructor(
    private _fb: FormBuilder,
    private service: AlertManagerService,
    private _dialog: MatDialog,
    private _snack: MatSnackBar
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.rule) {
      if (!this.ruleForm && !changes.rule.currentValue) {
        this.service
          .detailFormDefaults()
          .pipe(take(1))
          .subscribe((defaults) => {
            this.setDefaultRuleValues(defaults);
            this.ruleForm = this.getDefaultFormGroup(this.defaultRuleValues);
            this.clearNotifications();
          });
      } else {
        if (this.ruleForm.dirty || this.notificationForm.dirty) {
          const title = 'Continue working?';
          const message =
            'If you leave now, the current changes to this rule and its notifications will be lost.';
          const dialogRef = this._dialog.open(ActionsDialogComponent, {
            data: {
              title,
              message,
              rejectBtn: 'Leave',
              confirmBtn: 'Stay',
            },
          });
          dialogRef.beforeClosed().subscribe((confirmed) => {
            if (!confirmed) this.onRuleChange(this.rule);
          });
        } else this.onRuleChange(this.rule);
      }
    }
  }

  onNotificationIds = (ids: string[]): void => {
    const idCtrl = this.ruleForm.get('meta.notificationIds');
    idCtrl.setValue(ids);
  };

  private setDefaultRuleValues = (defaults: FormDefaults): void => {
    Object.keys(defaults).forEach((key) => {
      if (key === 'meta') {
        Object.keys(defaults.meta).forEach((mKey) => {
          this.defaultRuleValues.meta[mKey] =
            defaults.meta[mKey][0] === null
              ? null
              : defaults.meta[mKey][0]['value'] || defaults.meta[mKey][0];
        });
      } else {
        this.defaultRuleValues[key] =
          defaults[key][0] === null
            ? null
            : defaults[key][0].value || defaults[key][0];
      }
    });
  };

  private getDefaultFormGroup = (defaults: any): FormGroup => {
    const group = this._fb.group({
      ...defaults,
      meta: this._fb.group({
        ...defaults.meta,
        name: [defaults.meta.name, [Validators.required]],
        ruleType: [defaults.meta.ruleType, [Validators.required]],
        priority: [defaults.meta.priority, [Validators.required]],
      }),
    });
    return group;
  };

  onClickDelete = (ruleId: string): void => {
    const dialogRef = this._dialog.open(ActionsDialogComponent, {
      data: {
        title: 'Delete rule',
        message: 'Are you sure you want to delete this rule?',
        rejectBtn: 'No thanks',
        confirmBtn: 'Delete now',
      },
    });

    dialogRef.beforeClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.service
          .archiveRule(this.project.symbolicName, ruleId)
          .pipe(take(1))
          .subscribe((res) => {
            if (!res) {
              this._snack.open('Could not delete rule', null, {
                panelClass: 'error',
                duration: 6000,
                horizontalPosition: 'right',
              });
            } else {
              const rule = this.ruleForm.getRawValue();
              this.onRuleChange(null);
              this.mutation.emit({
                type: 'delete',
                rule,
                targetId: ruleId,
              });
            }
          });
      }
    });
  };

  private getRuleWithDefaults = (rule?: GenericRule): GenericRule => ({
    ...this.defaultRuleValues,
    ...(rule || {}),
    meta: {
      ...this.defaultRuleValues.meta,
      ...(rule ? rule.meta : {}),
    },
  });

  private onRuleChange = (rule: GenericRule): void => {
    this.loading$.next(true);
    const ruleWithDefaults = this.getRuleWithDefaults(rule);
    if (!rule) ruleWithDefaults.meta.ruleType['disabled'] = false;
    this.ruleForm.reset(ruleWithDefaults);
    /**
     * not sure why the ID & points won't update with reset()...
     */
    this.ruleForm
      .get('meta.id')
      .setValue(ruleWithDefaults.meta.id, { emitEvent: false });
    this.ruleForm
      .get('points')
      .setValue(ruleWithDefaults.points, { emitEvent: false });

    const ids = rule && rule.meta ? rule.meta.notificationIds : [];
    this.newRule = !!ids;

    // Clear current arrays
    Object.keys(this.notificationForm.controls).forEach((key) => {
      (this.notificationForm.get(key + '.list') as FormArray).clear();
      (this.notificationForm.get(key + '.types') as FormArray)?.clear();
    });

    if (ids.length) {
      forkJoin([
        this.service.notificationTypes$.pipe(take(1)),
        this.service
          .getNotifications(this.project.symbolicName, ids)
          .pipe(take(1)),
      ]).subscribe(([types, notifs]: [RuleType[], GenericNotification[]]) => {
        types.forEach((type) => {
          const notif = notifs.find((n) => n.meta.notificationType === type.id);
          if (notif) this.makeFormArray(notif.meta.notificationType, notif);
          else this.makeFormArray(type.id);
        });
        this.loading$.next(false);
      });
    } else {
      this.clearNotifications();
      this.loading$.next(false);
    }
  };

  private clearNotifications = (): void => {
    this.service.notificationTypes$.pipe(take(1)).subscribe((types) => {
      types.forEach((type) => this.makeFormArray(type.id));
      this.notificationTypes = types;
    });
  };

  private makeFormArray = (key: string, notif?: GenericNotification): void => {
    const typeCtrl = this.notificationForm.get(key);
    const timeoutHours = 6;
    if (!typeCtrl) {
      const newGroup = this._fb.group({
        id: [''],
        name: [''],
        timeout: [timeoutHours * 3.6e6],
        timeoutDisplay: [timeoutHours],
        list: this._fb.array([]),
        types: this._fb.array([]),
      });
      this.notificationForm.addControl(key, newGroup);
    } else {
      typeCtrl.reset({
        timeout: timeoutHours * 3.6e6,
        timeoutDisplay: timeoutHours,
      });
    }

    if (notif) {
      const opts = { emitEvent: false };
      this.notificationForm.get(key + '.id').setValue(notif.meta.id, opts);
      this.notificationForm.get(key + '.name').setValue(notif.meta.name, opts);
      this.notificationForm.get(key + '.timeout').setValue(notif.timeout, opts);
      this.notificationForm
        .get(key + '.timeoutDisplay')
        .setValue(notif.timeout / 3.6e6, opts);
      if (notif.contacts && notif.contacts.length) {
        const types = notif.contactTypes || [];
        notif.contacts.forEach((contact, i) => {
          this.addTypeControl(key, contact, types[i] || 'SIMPLE');
        });
      }
      if (notif.geoJSON && notif.geoJSON.length) {
        notif.geoJSON.forEach((geoJSON) => this.addTypeControl(key, geoJSON));
      }
    } else if (key === 'Map') {
      this.addTypeControl(key);
    }
  };

  public addTypeControl = (id: string, contact = '', type?: string): void => {
    let value: any =
      id === 'Phone' || id === 'Text'
        ? new MyTel(
            contact.substring(2, 5),
            contact.substring(5, 8),
            contact.substring(8)
          )
        : contact;

    const validators = [];

    if (type) {
      validators.push(Validators.required);
      if (id === 'Email' && type === 'SIMPLE') {
        validators.push(
          Validators.email,
          Validators.pattern(
            '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'
          )
        );
      }
      if (type !== 'SIMPLE') {
        value = contact;
        const profiles = type === 'USER' ? this.users : this.groups;
        validators.push(invalidProfile(profiles));
      }
    }

    const list = this.notificationForm.get(id + '.list') as FormArray;
    list.push(this._fb.control(value, validators));
    if (type) {
      const types = this.notificationForm.get(id + '.types') as FormArray;
      types.push(this._fb.control(type));
    }
  };

  undoChanges = (): void => this.onRuleChange(this.rule);

  /**
   * this is very inefficient. multiple requests can be merged with graphql
   */
  onClickSave = (): Promise<any> => {
    const invalids = [];
    this.ruleForm.markAllAsTouched();
    this.notificationForm.markAllAsTouched();

    if (this.ruleForm.invalid) {
      invalids.push('rule');
    }
    if (this.notificationForm.invalid) {
      invalids.push('notifications');
    }
    if (invalids.length) {
      this.ruleForm.markAsPristine();
      this.notificationForm.markAsPristine();
      this._snack.open('Invalid ' + invalids.join(' & ') + ' item(s)', null, {
        panelClass: 'error',
        duration: 6000,
        horizontalPosition: 'right',
      });
      return;
    }
    this.loading$.next(true);
    const notifReqs = this.service.getNotifReqs(
      this.project.symbolicName,
      this.notificationForm
    );
    const request = notifReqs.length
      ? firstValueFrom(forkJoin(notifReqs))
      : Promise.resolve(null);

    return request.then(this.saveRule);
  };

  private saveRule = (res?: [string, string][]): void => {
    const noEvents = { emitEvent: false };
    const rule = { ...this.ruleForm.getRawValue() };
    const meta = { ...rule.meta };
    const projectName = this.project.symbolicName;
    delete rule.meta;
    if (!!res) {
      // if there were changes to notifications
      for (let index = 0; index < res.length; index++) {
        const ids = res[index];
        const [oldId, newId] = ids;
        const i: number = meta.notificationIds.indexOf(oldId);

        // make sure oldId is found and a newId was returned
        if (i < 0 || !newId) {
          this._snack.open('There was a problem saving your rule.');
          this.loading$.next(false);
          throw new Error(`Could not update notifications.`);
        }

        // if newId is 'remove', remove from rule's meta, otherwise replace
        if (newId === 'remove') {
          meta.notificationIds.splice(i, 1);
        } else {
          meta.notificationIds.splice(i, 1, newId);
        }
      }
      meta.notificationIds = meta.notificationIds.filter(
        (id) => !id.startsWith('-')
      );
    }
    const isNew = meta.id === null || meta.id.startsWith('-');
    const variables = {
      ...rule,
      ...meta,
      ruleId: isNew ? null : meta.id,
      project: projectName,
    };

    this.service.saveRule(variables).subscribe((ruleId: number) => {
      if (ruleId === null) {
        console.error('saveRule responded with null', variables);
        this._snack.open(
          'There was a problem saving your rule. Please, try again.',
          null,
          {
            panelClass: 'error',
            duration: 6000,
          }
        );
        this.ruleForm.markAllAsTouched();
        this.loading$.next(false);
      } else {
        const targetId = this.ruleForm.get('meta.id').value;
        this.ruleForm.get('meta.id').setValue(ruleId + '', noEvents);
        this.mutation.emit({
          type: isNew ? 'create' : 'update',
          rule: this.ruleForm.getRawValue(),
          targetId,
        });
        this.notificationForm.markAsPristine();
        this.ruleForm.markAsPristine();
        this._snack.open('Rule saved', null, { duration: 6000 });
      }
    });
  };

  // private getInvalidControls(
  //     formGroup: FormGroup,
  //     invalidCtrls: string[] = [],
  //     parent = ''
  // ) {
  //   for (const name in formGroup.controls) {
  //     if (Object.prototype.hasOwnProperty.call(formGroup.controls, name)) {
  //       const ctrl = formGroup.get(name);
  //       if (ctrl['controls']) {
  //         const newParent = parent + (parent ? '.' : '') + name;
  //         this.getInvalidControls(ctrl as FormGroup, invalidCtrls, newParent);
  //       } else if (ctrl.invalid) {
  //         const famName = parent + (parent ? '.' : '') + name;
  //         invalidCtrls.push(famName);
  //       }

  //       if (parent === 'Text.list') {
  //         console.log(ctrl.invalid);
  //       }
  //     }
  //   }
  //   return invalidCtrls;
  // }
}
