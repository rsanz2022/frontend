import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { UserService } from '@app/core';
import { Project, TypeOption } from '@app/shared/models';
import { take, takeWhile } from 'rxjs/operators';
import { RuleType, DataType } from '../../../alert-manager.models';
import { AlertManagerService } from '../../../alert-manager.service';
import * as mapboxgl from 'mapbox-gl';
import { LngLatLike } from 'mapbox-gl';
import { ReplaySubject, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.scss'],
})
export class RuleComponent implements OnDestroy, AfterViewInit {
  private rxSubs = new Subscription();
  ruleType: RuleType;
  units: TypeOption[];
  layer: mapboxgl.Layer;
  frequencies: string[] = [];
  durations: number[] = [];
  layers: mapboxgl.Layer[] = [];
  dataTypes: DataType[] = [];
  readonly filteredSources$ = new ReplaySubject<mapboxgl.Layer[]>(1);
  loading = true;
  @Input() project: Project;
  @Input() ruleTypes: RuleType[] = [];
  @Input() ruleForm: FormGroup;
  @Input() newRule = true;

  get features(): string[] {
    return this.ruleForm.get('idList').value || [];
  }
  set features(v: string[]) {
    if (JSON.stringify(this.features) !== JSON.stringify(v.sort())) {
      const idListCtrl = this.ruleForm.get('idList');
      idListCtrl.setValue(v);
      idListCtrl.markAsDirty();
    }
  }

  get markers(): LngLatLike[] {
    return this.ruleForm.get('points').value;
  }
  set markers(v: LngLatLike[]) {
    const pointsCtrl = this.ruleForm.get('points');
    pointsCtrl.setValue(v);
    pointsCtrl.markAsDirty();
  }

  constructor(
    private service: AlertManagerService,
    public userService: UserService,
    public dialog: MatDialog
  ) {}

  ngAfterViewInit(): void {
    this.userService.project.getUnitConfig('rain');
    this.rxSubs.add(this.filteredSources$.subscribe(this.onSourcesChange));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.project) {
      this.onProjectChange(changes.project.currentValue);
    }
    if (changes.ruleForm && !!changes.ruleForm.currentValue) {
      this.loading = false;
      // todo: remove timeout & fix ExpressionChangedAfterItHasBeenCheckedError
      setTimeout(this.onFormChange);
    }
  }

  private onFormChange = (): void => {
    this.rxSubs.add(
      this.ruleForm
        .get('meta.ruleType')
        .valueChanges.subscribe(this.onRuleTypeChange)
    );
    if (this.ruleForm.get('meta.ruleType').value)
      this.onRuleTypeChange(this.ruleForm.get('meta.ruleType').value);

    this.rxSubs.add(
      this.ruleForm.get('layer').valueChanges.subscribe(this.onLayerChange)
    );
    if (this.ruleForm.get('layer').value) {
      this.onLayerChange(this.ruleForm.get('layer').value);
    }

    this.ruleForm
      .get('sourceId')
      .valueChanges.subscribe(() =>
        this.ruleForm.get('layer').setValue('Radar')
      );

    this.rxSubs.add(
      this.ruleForm.get('idList').valueChanges.subscribe(this.onIdListChange)
    );
    if (this.ruleForm.get('idList').value)
      this.onIdListChange(this.ruleForm.get('idList').value);

    this.rxSubs.add(
      this.ruleForm.get('meta.id').valueChanges.subscribe(this.enableRuleType)
    );
    if (!this.ruleForm.get('meta.id').value.startsWith('-'))
      this.enableRuleType();

    this.rxSubs.add(
      this.ruleForm.get('dataType').valueChanges.subscribe(this.prepUnits)
    );
  };

  ngOnDestroy(): void {
    this.rxSubs.unsubscribe();
  }

  private onIdListChange = (idList: string[]): void => {
    const ctrl = this.ruleForm.get('dataType');
    ctrl.clearValidators();
    ctrl.disable({ emitEvent: false });
    if (this.ruleType && this.ruleType.fields.dataType && idList.length) {
      this.service
        .getDataTypes(this.project.symbolicName, idList)
        .pipe(take(1))
        .subscribe((types) => {
          ctrl.enable({ emitEvent: false });
          ctrl.setValidators([Validators.required]);
          this.dataTypes = types;
          const current = types.find(
            (type) => type.symbolicName === ctrl.value
          );
          if (!current) ctrl.setValue('', { emitEvent: false });
        });
    } else this.dataTypes = [];
  };

  private onProjectChange = (proj): void => {
    if (!proj) return;
    this.enableRuleType(false);
    this.service.sources$.pipe(take(1)).subscribe(() => this.enableRuleType());
  };

  private enableRuleType = (enable = true) => {
    if (this.ruleForm) {
      const typeCtrl = this.ruleForm.get('meta.ruleType');
      const ruleId = this.ruleForm.get('meta.id').value;
      if (typeCtrl && typeCtrl.enabled !== enable) {
        typeCtrl[enable && ruleId.startsWith('-') ? 'enable' : 'disable']({
          emitEvent: false,
        });
      }
    }
  };

  private onRuleTypeChange = (typeId: string): void => {
    this.ruleType = this.ruleTypes.find((r) => r.id === typeId);
    if (this.ruleType) {
      if (
        this.ruleType.fields.aggregation &&
        this.ruleForm.get('aggregation').value === null
      ) {
        this.ruleForm
          .get('aggregation')
          .setValue(this.ruleType.fields.aggregation.options[0].id);
      }
      if (
        this.ruleType.fields.precipitationMinimum &&
        this.ruleForm.get('precipitationMinimum').value === null
      ) {
        this.ruleForm
          .get('precipitationMinimum')
          .setValue(this.ruleType.fields.precipitationMinimum.options[0].id);
      }

      const hasPoints = !!this.ruleType.fields.points;
      this.ruleForm.get(hasPoints ? 'idList' : 'points').clearValidators();
      this.ruleForm
        .get(hasPoints ? 'points' : 'idList')
        .setValidators(Validators.required);

      const wFutureCtrl = this.ruleForm.get('windowFuture');
      if (this.ruleType.fields.windowFuture && wFutureCtrl) {
        let value = Math.min(this.ruleType.window, wFutureCtrl.value);
        if (value === NaN) value = 360000;
        wFutureCtrl.setValue(value, { emitEvent: false });
      }
      if (this.ruleForm.dirty) {
        this.markers = [];
      }

      this.filterSources(typeId);
      this.prepControls();
      this.prepUnits();
    }
  };

  getDDFDurationLabel = (t: number): string =>
    t > 2 * 3.6e6 && t < 72 * 3.6e6
      ? t / 3.6e6 + ' hours'
      : t < 1 * 3.6e6 && t > 0
      ? t / 6e4 + ' minutes'
      : moment.duration(t).humanize();

  prepUnits = (dataType?: string): void => {
    if (this.ruleType && this.ruleType.fields.unit) {
      const unitType =
        dataType && dataType.toLowerCase().includes('disc') ? 'flow' : null;
      this.units = this.service.getUnitOptions(
        this.ruleType.id,
        this.project,
        unitType
      );
      const unitCtrl = this.ruleForm.get('unit');
      unitCtrl.setValue(this.units[0] ? this.units[0].id : null);
      if (this.units[0]?.id === '%') {
        this.ruleForm.get('threshold').setValue('50');
      }
      if (this.units.length < 2) {
        unitCtrl.disable({ emitEvent: false });
      } else {
        unitCtrl.enable({ emitEvent: false });
      }
    }
  };

  private prepControls = (): void => {
    if (!this.ruleType) return;

    // clear all validators
    for (const key in this.ruleForm.controls) {
      if (Object.prototype.hasOwnProperty.call(this.ruleForm.controls, key)) {
        if (key === 'meta') {
          const meta = (this.ruleForm.get('meta') as FormGroup).controls;
          for (const metaKey in meta) {
            if (Object.prototype.hasOwnProperty.call(meta, metaKey)) {
              meta[metaKey].clearValidators();
              if (!this.ruleType.fields[metaKey] && metaKey !== 'id') {
                meta[metaKey].reset(null, { emitEvent: false });
              }
            }
          }
        } else {
          this.ruleForm.controls[key].clearValidators();
          if (!this.ruleType.fields[key]) {
            this.ruleForm.controls[key].reset(null, { emitEvent: false });
          }
        }
      }
    }

    setTimeout(() => {
      // set available validators
      for (const key in this.ruleType.fields) {
        if (Object.prototype.hasOwnProperty.call(this.ruleType.fields, key)) {
          const field = this.ruleType.fields[key];
          if (!field.optional) {
            const keyMap = { RuleID: 'meta.id' };
            let name = keyMap[key] || key;
            let ctrl = this.ruleForm.get(name);
            if (name === 'idList' && this.ruleType.fields.points) continue;
            if (name === 'meta.id' && this.newRule) continue;
            if (!ctrl) {
              name = 'meta.' + key;
              ctrl = this.ruleForm.get(name);
            }
            if (ctrl) {
              const validators = [Validators.required];
              if (name === 'threshold' || name === 'probabilityThreshold') {
                validators.push(Validators.min(0.01));
              }
              ctrl.setValidators(validators);
              ctrl.updateValueAndValidity({ emitEvent: false });
            }
          }
        }
      }
    });
  };

  private onSourcesChange = (sources: mapboxgl.Layer[]): void => {
    /**
     * setTimeout because layer valueChange event doesn't always get caught.
     * Only noticed on SARA inundation ¯\_(ツ)_/¯
     */
    setTimeout(() => {
      if (this.ruleForm.dirty) {
        if (sources[0]) {
          const meta = sources[0].metadata;
          const hasShapes =
            this.ruleType.group === 'PreVieux' || this.ruleType.id === 'HRRR';
          const filteredID = hasShapes ? meta.id : meta.symbolicName;
          if (hasShapes) {
            this.ruleForm
              .get('sourceId')
              .setValue(filteredID, { emitEvent: false });
            this.ruleForm.get('layer').setValue(meta.symbolicName);
          } else {
            this.ruleForm.get('layer').setValue(filteredID);
          }
        } else this.onLayerChange();
      } else if (!this.ruleForm.get('layer').value) {
        // if form's clean and doesn't have a layer defined, it has user defined points
        const symbolicName = sources[0]
          ? sources[0].metadata.symbolicName
          : null;
        this.onLayerChange(symbolicName);
      }
    });
  };

  private onLayerChange = (layer?: string): void => {
    if (layer && this.ruleType) {
      this.filteredSources$.pipe(take(1)).subscribe((s) => {
        const layerObj = {
          ...s.find((l: mapboxgl.Layer) => {
            const layerType = l.metadata.layerType || '';
            const typeKey = this.ruleType.id === 'HRRR' ? 'id' : 'group';
            const switchCase = this.ruleType[typeKey]
              .toLowerCase()
              .replace(' ', '-');
            switch (switchCase) {
              case 'hrrr':
                return layerType === 'hrrr';

              case 'previeux':
                return (
                  layerType === 'previeux' &&
                  l.metadata.id === this.ruleForm.get('sourceId').value
                );

              case 'watchpoints':
                return layerType === 'watchpoints';

              case 'gauges':
                return layerType === 'gauges';

              case 'inundation':
                return layerType === 'inundation';

              default:
                return (
                  l.metadata.symbolicName === layer &&
                  layerType ===
                    this.ruleType[layerType === 'hrrr' ? 'id' : 'group']
                      .toLowerCase()
                      .replace(' ', '-')
                );
            }
          }),
        };

        if (this.ruleType.group === 'PreVieux' || this.ruleType.id === 'HRRR') {
          const meta = layerObj.metadata;
          if (layer === 'Radar' && this.ruleType.id !== 'HRRR') {
            this.layer = layerObj;
            this.ruleForm.get('idList').setValue(null);
          } else {
            const shape = meta.shapes.find((s) => s.id === layer);
            if (shape) {
              this.layer = this.service.getForecastLayer(
                meta,
                this.project.symbolicName,
                shape
              );
              this.ruleForm.get('points').setValue([]);
            }
          }
        } else {
          this.layer = layerObj;
        }
        this.prepDDFRule(this.layer);
      });
      if (this.ruleForm.dirty) this.features = [];
    } else if (this.ruleType && this.ruleType.group === 'Forecast') {
      this.layer = {
        metadata: {
          markerMode: true,
          mask: this.service.getMaskURL(
            { id: this.ruleType.id },
            this.project.symbolicName
          ),
        },
      } as mapboxgl.Layer;
    } else this.layer = null;
  };

  private prepDDFRule = (layer: mapboxgl.Layer) => {
    if (layer && layer.metadata && layer.metadata.layerType === 'ddf') {
      this.service
        .getDDFData(
          this.project.symbolicName,
          layer.metadata.symbolicName,
          layer.metadata.tableId
        )
        .pipe(take(1))
        .subscribe((data) => {
          this.frequencies = data.getDDFFrequencyLabels;
          this.durations = data.getDDFDurations.map((d) => d * 6e4);
          const current = this.ruleForm.get('duration').value;
          if (!this.durations.includes(current)) {
            this.ruleForm.get('duration').setValue(0, { emitEvent: false });
          }
        });
    }
  };

  private filterSources = (typeId: string): void => {
    if (!!typeId) {
      const type = this.ruleTypes.find((t) => t.id === typeId);
      this.service.sources$
        .pipe(
          takeWhile(() => typeId === this.ruleType.id),
          take(1)
        )
        .subscribe((allLayers) => {
          const filtered = allLayers.filter((s) => this.sourceFilter(s, type));
          this.filteredSources$.next(filtered);
          this.layers = filtered;
          if (this.ruleForm.dirty) this.features = [];
        });
    }
  };

  private sourceFilter(layer: any, type: RuleType): boolean {
    return (
      layer.metadata &&
      layer.metadata.layerType &&
      type.group &&
      layer.metadata.layerType.toLowerCase() ===
        type[type.id === 'HRRR' ? 'id' : 'group']
          .toLowerCase()
          .replace(' ', '-')
    );
  }

  durationDisplay = (v: number, longForm = false): string => {
    const duration = moment.duration(v);
    const times = [
      duration.months(),
      duration.days(),
      duration.hours(),
      duration.minutes(),
    ];

    if (!longForm) {
      const hours = '0' + (v / 3.6e6).toString().split('.')[0];
      return `${hours.substr(hours.length - 2)}:${moment.utc(v).format('mm')}`;
    }

    let msg = '';
    if (times[0]) msg += times[0] + ' month' + (times[0] > 1 ? 's ' : ' ');
    if (times[1]) msg += times[1] + ' day' + (times[1] > 1 ? 's ' : ' ');
    if (times[2]) msg += times[2] + ' hour' + (times[2] > 1 ? 's ' : ' ');
    if (times[3]) msg += times[3] + ' minute' + (times[3] > 1 ? 's ' : ' ');
    return msg || '0 minutes';
  };
}
