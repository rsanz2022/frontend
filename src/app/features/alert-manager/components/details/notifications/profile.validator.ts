import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Profile } from '@app/features/alert-manager/alert-manager.models';

export function invalidProfile(list: Profile[]): ValidatorFn {
  return ({ value }: AbstractControl): ValidationErrors | null => {
    const profile = list.findIndex((p) => p.id === value);
    return profile > -1 ? null : { invalidProfile: true };
  };
}

export function duplicateProfile(savedList: any): ValidatorFn {
  return ({ value }: AbstractControl): ValidationErrors | null => {
    const profile = savedList.findIndex((a) => a === value);
    return profile > -1 ? { duplicateProfile: true } : null;
  };
}

export function duplicatePhoneNumber(savedList: any): ValidatorFn {
  return ({ value }: AbstractControl): ValidationErrors | null => {
    const profile = savedList.findIndex((a) => {
      if (a.area) {
        if (JSON.stringify(a) === JSON.stringify(value)) {
          return 1;
        }
      }
    });

    return profile > -1 ? { duplicatePhoneNumber: true } : null;
  };
}
