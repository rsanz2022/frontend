import {
  Component,
  EventEmitter,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChange,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { Project } from '@app/shared/models';
import * as extent from '@mapbox/geojson-extent';
import { debounceTime, take } from 'rxjs/operators';
import { Profile, RuleType } from '../../../alert-manager.models';
import * as mapboxgl from 'mapbox-gl';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw';
import { AlertManagerService } from '@app/features/alert-manager/alert-manager.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import {
  duplicatePhoneNumber,
  duplicateProfile,
  invalidProfile,
} from './profile.validator';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnChanges, OnDestroy {
  empties = {};
  nType: string;
  mapgl: mapboxgl.Map;
  draw: MapboxDraw;
  rxSubs: Subscription[] = [];
  hasItems: { [key: string]: boolean } = {};
  profileList: Profile[] = [];
  @Input() types: RuleType[];
  @Input() notifications: FormGroup;
  @Input() project: Project;
  @Input() groups: Profile[];
  @Input() users: Profile[];
  @Input() isShown = false;
  @Input() ruleId: string;
  @Output() ids = new EventEmitter<string[]>();
  @Output() isInvalid = new EventEmitter<boolean>();
  @ViewChild('profileDialog', { static: true }) profileDialog: TemplateRef<any>;
  constructor(
    private _fb: FormBuilder,
    private zone: NgZone,
    private service: AlertManagerService,
    private _snack: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.notifications) this.onNotifications(changes.notifications);
  }
  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
  }
  onClickExample = (nId: string): void => {
    this.service
      .testNotification(this.project.symbolicName, nId, this.ruleId)
      .pipe(take(1))
      .subscribe((res) => {
        const msg = res
          ? 'Example notification sent to contacts'
          : 'There was a problem. Try again later.';
        this._snack.open(msg, null, {
          panelClass: res ? '' : 'error',
          duration: res ? 4000 : 6000,
          horizontalPosition: 'right',
        });
      });
  };
  onMapLoad = (mapgl: mapboxgl.Map): void => {
    this.mapgl = mapgl;
    this.draw = new MapboxDraw({
      displayControlsDefault: false,
      controls: {
        point: true,
        polygon: true,
        trash: true,
      },
    });
    const mapList = this.notifications.get('Map.list');
    this.mapgl.addControl(this.draw);
    if (mapList && mapList.value[0]) {
      const features = JSON.parse(mapList.value[0]);
      this.draw.add(features);
      this.mapgl.fitBounds(extent(features), {
        duration: 0,
        padding: 16,
        maxZoom: 15,
      });
    }
    this.mapgl.on('draw.create', () => this.zone.run(this.onMapEvent));
    this.mapgl.on('draw.delete', () => this.zone.run(this.onMapEvent));
    this.mapgl.on('draw.update', () => this.zone.run(this.onMapEvent));
  };
  private onMapEvent = () => {
    const features = this.draw.getAll();
    const ctrl = (this.notifications.get('Map.list') as FormArray).at(0);
    ctrl.setValue(features.features.length ? JSON.stringify(features) : '');
    ctrl.markAsDirty();
  };
  private onNotifications = (change: SimpleChange): void => {
    if (!!change.currentValue) {
      this.notifications.valueChanges
        .pipe(debounceTime(100))
        .subscribe(this.emptyControlCheck);
      Object.keys(this.notifications.controls).forEach((key: string) => {
        const formCtrl = this.notifications.get(key);
        this.rxSubs.push(
          formCtrl
            .get('timeoutDisplay')
            .valueChanges.subscribe((v) => this.onTimeoutChange(key, v))
        );
        setTimeout(() => {
          this.hasItems[key] =
            this.hasItems[key] ||
            (formCtrl.get('list') as FormArray).value.length > 0;
        }, 500);
      });
    }
  };
  private onTimeoutChange = (key: string, value: number): void =>
    this.notifications
      .get(key + '.timeout')
      .setValue(value * 3.6e6, { emitEvent: false });
  private emptyControlCheck = (): void => {
    const notificationIds: string[] = [];
    let isInvalid = true;
    Object.keys(this.notifications.controls).forEach((key, index) => {
      const formCtrl = this.notifications.get(key);
      const formArr = formCtrl.get('list') as FormArray;
      this.empties[key] = formArr
        .getRawValue()
        .map((v) => (v ? v.toString() : ''))
        .join();
      // sets the id
      const group = this.notifications.get(key);
      const idCtrl = group.get('id');
      if (group.valid) {
        isInvalid = false;
        if (idCtrl.value) {
          notificationIds.push(idCtrl.value);
        } else idCtrl.setValue('-' + (index + 1));
      }
    });
    this.isInvalid.emit(isInvalid);
    if (this.notifications.dirty) this.ids.emit(notificationIds);
  };

  public removeControl = (id: string, index: number): void => {
    const list = this.notifications.get(id + '.list') as FormArray;
    const types = this.notifications.get(id + '.types') as FormArray;
    const value = list.at(index).value;
    list.removeAt(index);
    types.removeAt(index);
    if (value && value.toString()) list.markAsDirty();
  };

  public addControl = (
    id: string,
    profile: 'SIMPLE' | 'USER' | 'GROUP',
    isEmail = false,
    value = ''
  ): void => {
    const list = this.notifications.get(id + '.list') as FormArray;
    const types = this.notifications.get(id + '.types') as FormArray;
    const validators = [Validators.required];

    if (isEmail && profile === 'SIMPLE') {
      validators.push(
        Validators.email,
        Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')
      );
    }

    if (profile !== 'SIMPLE') {
      const profiles = profile === 'USER' ? this.users : this.groups;
      validators.push(invalidProfile(profiles));
    }

    validators.push(duplicatePhoneNumber(list.value));
    validators.push(duplicateProfile(list.value));

    list.push(this._fb.control(value, validators));
    types.push(this._fb.control(profile));
  };

  public displayFn =
    (list: Profile[]) =>
    (id: string): string => {
      const profile = list.find((p) => p.id === id);
      return profile?.name || id;
    };
  public filteredOptions = (
    id: string,
    index: number,
    type: 'users' | 'groups'
  ): Profile[] => {
    let options = [...this[type]];
    const list = this.notifications.get(id + '.list') as FormArray;
    const name = list.at(index).value.toLowerCase();
    if (type === 'users' && (id === 'Phone' || id === 'Text')) {
      options = options.filter((p) => p.phone);
    }
    return options.filter((option) => option.name.toLowerCase().includes(name));
  };

  public onProfileBlur = (
    profiles: Profile[],
    typeId: string,
    index: number
  ): void => {
    const list = this.notifications.get(typeId + '.list') as FormArray;
    const control = list.at(index);
    if (control.invalid) {
      const value = control.value.toLowerCase();
      const match = profiles.find((p) => p.name.toLowerCase() === value);
      if (match) control.setValue(match.id);
    }
  };
}
