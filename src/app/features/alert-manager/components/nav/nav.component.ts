import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FilterCategory } from '../../alert-manager.models';
import { AlertManagerService } from '../../alert-manager.service';

@Component({
  selector: 'alert-manager-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  selectedGroup: string;
  selectedSub: string;
  filterCategories: FilterCategory[];
  @Input() set categories(types: FilterCategory) {
    if (types) {
      this.filterCategories.unshift(types);
    }
  }
  @Output() selection = new EventEmitter<any>();

  constructor(private service: AlertManagerService) {}

  ngOnInit(): void {
    this.filterCategories = [...this.service._filterCategories];
  }

  onSelection = (type?: string, group?: string, sub?: string): void => {
    this.selectedGroup = group;
    this.selectedSub = sub;
    this.selection.emit({ type, group, sub });
  }
}
