import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GaugeReportsComponent } from './gauge-reports.component';

const routes: Routes = [
  {
    path: '',
    component: GaugeReportsComponent,
    data: { title: 'Gauge Reports' }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class GaugeReportsRoutingModule { }
