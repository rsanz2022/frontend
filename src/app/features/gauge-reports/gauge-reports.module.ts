import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GaugeReportsComponent } from './gauge-reports.component';
import { GaugeReportsRoutingModule } from './gauge-reports-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TimepickerModule } from '@app/shared';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { TableComponent } from './table/table.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [GaugeReportsComponent, TableComponent],
  imports: [
    CommonModule,
    GaugeReportsRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,

    MatIconModule,
    MatRippleModule,
    MatTooltipModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDatepickerModule,
    MatMenuModule,
  ],
})
export class GaugeReportsModule {}
