import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GaugeReportsEvent, TableDatum } from '../gauge-reports.models';
import { GaugeReportsService } from '../gauge-reports.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  hasData = false;
  readonly groupHeader = {
    date: { col1: 'Event/Date', col2: 'Gauge ID:Reason' },
    id: { col1: 'Gauge ID', col2: 'Event/Date:Reason' },
    reason: { col1: 'Reason', col2: 'Event/Date:Gauge ID' },
  };
  dataSource = new MatTableDataSource<TableDatum>([]);
  @Input() loading = true;
  @Input() error: string | boolean;
  _grouping: string;
  @Input()
  set grouping(v: string) {
    this._grouping = v;
    this.dataSource.data = this.groupConfig(this._data);
  }
  _data: GaugeReportsEvent[];
  @Input()
  set data(v: GaugeReportsEvent[]) {
    if (!v) return;
    this.hasData = true;
    this._data = v;
    this.dataSource.data = this.groupConfig(v);
  }

  constructor(public service: GaugeReportsService) {}

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
  }

  getColumns = (): string[] => this.service.tableConfig.map((col) => col.def);

  groupConfig = (data: GaugeReportsEvent[]): TableDatum[] => {
    if (!data) return this.dataSource.data;
    const tableData = [];
    switch (this._grouping) {
      case 'id':
        const idEventReasons: any = {};
        for (let index = 0; index < data.length; index++) {
          const event = data[index];
          for (
            let iCol2List = 0;
            iCol2List < event.reasons.length;
            iCol2List++
          ) {
            const reason = event.reasons[iCol2List];
            for (let iReasons = 0; iReasons < reason.ids.length; iReasons++) {
              const id = reason.ids[iReasons];
              idEventReasons['_' + id] = {
                col1: id.replace(/-/g, '&#8209;'),
                col2: `${
                  idEventReasons['_' + id]
                    ? idEventReasons['_' + id].col2 + '&#9;'
                    : ''
                }<strong>${event.name}</strong>:${reason.name}`,
              };
            }
          }
        }
        tableData.push(...Object.values(idEventReasons));
        break;

      case 'reason':
        const reasonEventIDs: any = {};
        for (let index = 0; index < data.length; index++) {
          const event = data[index];
          for (
            let iCol2List = 0;
            iCol2List < event.reasons.length;
            iCol2List++
          ) {
            const reason = event.reasons[iCol2List];
            reasonEventIDs['_' + reason.name] = {
              col1: reason.name.replace(/-/g, '&#8209;'),
              col2: reasonEventIDs['_' + reason.name]?.col2 || '',
            };
            for (let iReasons = 0; iReasons < reason.ids.length; iReasons++) {
              const id = reason.ids[iReasons];
              reasonEventIDs[
                '_' + reason.name
              ].col2 += `${event.name}:<strong>${id}</strong>&#9;`;
            }
          }
        }
        for (const key in reasonEventIDs) {
          if (Object.prototype.hasOwnProperty.call(reasonEventIDs, key)) {
            const reason = reasonEventIDs[key];
            reason.col2 = reason.col2.slice(0, -4);
            tableData.push(reason);
          }
        }
        break;

      default:
        // grouping = date
        for (let index = 0; index < data.length; index++) {
          const event = data[index];
          const col2List: string[] = [];
          for (
            let iCol2List = 0;
            iCol2List < event.reasons.length;
            iCol2List++
          ) {
            const reason = event.reasons[iCol2List];
            const reasons = [];
            for (let iReasons = 0; iReasons < reason.ids.length; iReasons++) {
              const id = reason.ids[iReasons];
              reasons.push(`<strong>${id}</strong>:${reason.name}`);
            }
            col2List.push(reasons.join('&#9;'));
          }
          tableData.push({
            col1: event.name.replace(/-/g, '&#8209;'),
            col2: col2List.join('&#9;').replace(/-/g, '&#8209;'),
          });
        }
        break;
    }
    return tableData
  };

  private sortingDataAccessor = (item: TableDatum, property: string): string =>
    item[property];
}
