import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from '@app/core';
import { Project } from '@app/shared/models';
import { BehaviorSubject, ReplaySubject, Subject, take, takeUntil } from 'rxjs';
import { GaugeReportsService } from './gauge-reports.service';
import * as moment from 'moment-timezone';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { RainvieuxOption } from '../cal-stats/cal-stats.models';
import { EventGroup, GaugeReportsEvent, GaugeReportsMeta } from './gauge-reports.models';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-gauge-reports',
  templateUrl: './gauge-reports.component.html',
  styleUrls: ['./gauge-reports.component.scss'],
})
export class GaugeReportsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();
  loading$ = new BehaviorSubject(true);
  error$ = new BehaviorSubject<string | boolean>(false);
  project: Project;
  project$ = new ReplaySubject<Project>(1);
  mqAlias: string;
  tableData: GaugeReportsEvent[];
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });
  durationString = 'Loading...';
  dateString = this.durationString;
  timeState = 'historical';
  rvProducts$ = new ReplaySubject<RainvieuxOption[]>(1);
  rvProducts: RainvieuxOption[];
  events: [number, number][];
  meta: GaugeReportsMeta;
  product: RainvieuxOption;
  timeStateIcons = {
    'real-time': 'update',
    historical: 'undo',
    forecasted: 'redo',
  };
  private readonly CSV =
    'api/{project}/gaugereports/{product}/range/{start}/{end}/csv?groupBy={group}';
  csvURL = '';
  csvFilename = '';
  grouping = 'date';
  readonly groups = [
    ['Event/Date', 'date'],
    ['Gauge ID', 'id'],
    ['Reason', 'reason'],
  ];
  @ViewChild('timepicker', { static: true }) timepicker: ElementRef;
  @ViewChild('timeDialog', { static: true }) timeDialog: TemplateRef<any>;

  constructor(
    public dialog: MatDialog,
    private userService: UserService,
    private service: GaugeReportsService,
    private media: MediaObserver
  ) {}

  ngOnInit(): void {
    this.userService.project$
      .pipe(takeUntil(this.destroy$))
      .subscribe(this.onProject);

    this.media
      .asObservable()
      .pipe(takeUntil(this.destroy$))
      .subscribe((values: MediaChange[]) => (this.mqAlias = values[0].mqAlias));
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  private onProject = (project: Project): void => {
    this.project = project;
    this.project$.next(this.project);
    this.service.getProducts(this.project).subscribe((o) => {
      this.product = o.find((r) => r.isDefault) || o[0];
      this.onProduct();
      this.rvProducts = o;
      this.rvProducts$.next(this.rvProducts);
    });
  };

  onProduct = (product = this.product): void => {
    this.service
      .getEvents(this.project.symbolicName, product.symbolicName)
      .subscribe((res) => {
        const event = res[res.length - 1];
        this.range.setValue(
          {
            start: moment(event.start),
            end: moment(event.end),
          },
          { emitEvent: false }
        );
        this.updateTable();
        this.events = this.prepEvents(res);
      });
  };

  private prepEvents = (e: EventGroup[]): [number, number][] => {
    const events = [];
    for (let i = 0; i < e.length; i++) {
      const event = e[i];
      const prev = events[events.length - 1];
      if (i > 0 && prev && event.start === prev[1]) {
        prev[1] = event.end;
        continue;
      }
      events.push([event.start, event.end]);
    }
    return events;
  }

  updateTable = (): void => {
    this.loading$.next(true);
    const time = this.range.value;
    if (!this.product || !time.start) return;
    this.range.markAsPristine();
    this.service
      .getGaugeReports(
        this.project,
        this.product?.symbolicName,
        +moment.tz(time.start.format('YYYY-MM-DD'), this.project.timezone),
        +moment.tz(time.end.format('YYYY-MM-DD'), this.project.timezone)
      )
      .subscribe({
        next: ({events, meta}) => {
          this.tableData = events;
          this.meta = meta;
          this.error$
            .pipe(take(1))
            .subscribe((err) => (err ? this.error$.next(false) : null));
        },
        error: (err) => {
          this.tableData = [];
          this.error$.next(
            "We're having trouble loading data for this configuration. Try again later."
          );
          this.loading$.next(false);
          console.error(err);
        },
        complete: () => this.loading$.next(false),
      });
    this.updateCSV();
  };

  updateCSV = (): void => {
    const time = this.range.value;

    this.csvURL = this.CSV.replace('{project}', this.project.symbolicName)
      .replace('{product}', this.product.symbolicName)
      .replace(
        '{start}',
        +moment.tz(time.start.format('YYYY-MM-DD'), this.project.timezone) + ''
      )
      .replace(
        '{end}',
        +moment.tz(time.end.format('YYYY-MM-DD'), this.project.timezone) + ''
      )
      .replace('{group}', this.grouping);

    this.csvFilename =
      moment.tz(time.start, this.project.timezone).format('YYYY-MM-DDTHHmm') +
      '-' +
      moment.tz(time.end, this.project.timezone).format('YYYY-MM-DDTHHmm') +
      '_' +
      this.grouping +
      '.csv';
  };

  eventFilter = (day = moment()): boolean => {
    if (!this.events) return true;
    for (let index = 0; index < this.events.length; index++) {
      const group = this.events[index];
      if (day.isBetween(group[0], group[1], undefined, '[]')) {
        return true;
      }
    }
    return false;
  };
}
