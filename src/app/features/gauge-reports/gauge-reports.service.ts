import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Project } from '@app/shared/models';
import { Apollo, gql } from 'apollo-angular';
import { map, Observable } from 'rxjs';
import {
  EventGroup,
  GaugeReports,
  RainvieuxOption,
  RainvieuxOptions,
  TableConfig,
} from './gauge-reports.models';

interface Response {
  getGaugeReportsOptions?: RainvieuxOptions;
  getGaugeReports?: GaugeReports;
  getGaugeReportsEventGroups?: EventGroup[];
}

@Injectable({
  providedIn: 'root',
})
export class GaugeReportsService {
  readonly tableConfig: TableConfig[] = [
    { def: 'col1', name: 'Event/Date', type: 'title' },
    { def: 'col2', name: 'Gauge ID:Reason', type: 'data' },
  ];

  constructor(private apollo: Apollo, private http: HttpClient) {}

  getGaugeReports(
    project: Project,
    product: string,
    start: number,
    end: number
  ): Observable<GaugeReports> {
    const events = 'events { name reasons { name ids }}';
    const meta = 'meta { maxDatesCount maxDates maxIdsCount maxIds }';
    return this.apollo
      .query<Response>({
        variables: { product, start, end, project: project.symbolicName },
        query: gql`
          query Query($project: String! $product: String! $start: Long! $end: Long!) {
            getGaugeReports(
              project: $project
              product: $product
              start: $start
              end: $end
            ) { ${meta} ${events} }
          }
        `,
      })
      .pipe(map(({ data }) => data.getGaugeReports));
  }

  getEvents(project: string, product: string): Observable<EventGroup[]> {
    return this.apollo
      .query<Response>({
        variables: { project, product },
        query: gql`
          query Query($project: String!, $product: String!) {
            getGaugeReportsEventGroups(project: $project, product: $product) {
              name
              start
              end
            }
          }
        `,
      })
      .pipe(map(({ data }) => data.getGaugeReportsEventGroups));
  }

  getProducts(project: Project): Observable<RainvieuxOption[]> {
    const rainvieuxOption = `{ symbolicName displayName isDefault }`;
    return this.apollo
      .query<Response>({
        variables: { project: project.symbolicName },
        query: gql`query Query($project:String!) {
          getGaugeReportsOptions(project:$project) {
            products ${rainvieuxOption}
          }
        }`,
      })
      .pipe(
        map(({ data }) => {
          data.getGaugeReportsOptions.products.sort(this.sortByDisplayName);
          return data.getGaugeReportsOptions.products;
        })
      );
  }

  private sortByDisplayName = (
    a: RainvieuxOption,
    b: RainvieuxOption
  ): number => {
    if (a.displayName < b.displayName) return -1;
    if (a.displayName > b.displayName) return 1;
    return 0;
  };
}
