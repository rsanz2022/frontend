import { TestBed } from '@angular/core/testing';

import { GaugeReportsService } from './gauge-reports.service';

describe('GaugeReportsService', () => {
  let service: GaugeReportsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GaugeReportsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
