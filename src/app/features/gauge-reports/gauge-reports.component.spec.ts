import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GaugeReportsComponent } from './gauge-reports.component';

describe('GaugeReportsComponent', () => {
  let component: GaugeReportsComponent;
  let fixture: ComponentFixture<GaugeReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GaugeReportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GaugeReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
