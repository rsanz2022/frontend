export interface EventGroup {
  name: string;
  start: number;
  end: number;
}

export interface GaugeReports {
  meta: GaugeReportsMeta;
  events: GaugeReportsEvent[];
}

export interface GaugeReportsMeta {
  maxDatesCount: number;
  maxDates: string[];
  maxIdsCount: number;
  maxIds: string[];
}

export interface GaugeReportsEvent {
  name: string;
  reasons: GaugeReportsReason[];
}

export interface GaugeReportsReason {
  name: string;
  ids: string[];
}

export interface TableConfig {
  def: string;
  name: string;
  type?: 'date' | 'data' | 'percent' | 'number' | 'title';
  media?: string;
  description?: string;
}

export interface TableDatum {
  col1: string;
  col2: string;
}

export interface RainvieuxOptions {
  products: RainvieuxOption[];
  gaugeSets: RainvieuxOption[];
  radars: RainvieuxOption[];
  zrs: RainvieuxOption[];
}

export interface RainvieuxOption {
  symbolicName: string;
  displayName: string;
  isDefault: boolean;
}
