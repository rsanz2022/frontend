import {
  Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';
import { DataDownloadsService } from '../data-downloads.service';
import { UserService } from '@app/core';
import { Project, Time } from '@app/shared/models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment-timezone';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data-downloads-element',
  templateUrl: './data-downloads-element.component.html',
  styleUrls: ['./data-downloads-element.component.scss']
})
export class DataDownloadsElementComponent implements OnInit, OnDestroy, OnChanges {
  @Input() times: Time;
  @Output() processing = new EventEmitter<boolean>();
  rxSubs = [];
  project: Project;
  sources: any;
  ddForm: FormGroup;
  featureList = null;
  submitting = false;
  readonly allIntervals = [ 5, 15, 30, 60, 1440 ];
  options: any = {
    formats: [],
    intervals: this.allIntervals
  };

  constructor(
    private router: Router,
    private service: DataDownloadsService,
    private myUser: UserService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.rxSubs.push(
      this.myUser.project$.subscribe(this.updateProject)
    );
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach(s => s.unsubscribe());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.times) this.onTimeChanges();
  }

  buildForm = (): void => {
    this.ddForm = this.fb.group({
      domainCtrl: [{}, Validators.required],
      formatCtrl: ['', Validators.required],
      intervalCtrl: ['', Validators.required],
      includeShapefileCtrl: [false]
    });

    this.ddForm.get('domainCtrl').valueChanges.subscribe(this.onSourceChange);
  }

  onSourceChange = (layer): void => {
    const domainCtrl = this.ddForm.get('domainCtrl');
    if (layer && domainCtrl.status === 'VALID') {
      this.featureList = null;
      domainCtrl.disable({ emitEvent: false });
      this.service
        .getFormats(this.project.symbolicName, layer.metadata.layerType)
        .then(res => this.options.formats = res);
      this.service.getGeojson(layer.metadata.dataDownloads.geojson)
        .then((res) => {
          domainCtrl.enable({ emitEvent: false });
          this.featureList = res.features.map(f => f.properties);
        });
    }
  }

  updateProject = (prj: Project): void => {
    this.project = prj;
    this.options.intervals = this.allIntervals
      .filter(i => (i >= prj.interval));
    this.ddForm.get('intervalCtrl').setValue(this.options.intervals[0]);
    this.service.getLayers(this.project.symbolicName)
      .then(res => {
        this.sources = res;
        this.onTimeChanges();
        this.ddForm.patchValue({domainCtrl: this.sources[0]});
      });
  }

  private onTimeChanges = (): void => {
    if (!this.project || !this.times) return;
    const start = this.times.start.getTime();
    const end = moment(this.times.end.getTime());
    const duration = end.diff(start, 'minutes');
    this.options.intervals = this.allIntervals
      .filter(i => i >= this.project.interval && i <= duration);
  }

  humanizeDuration = (t: number): string =>
    moment.duration(t, 'minutes').humanize();

  submit(): void {
    this.submitting = true;
    this.ddForm.disable();
    this.processing.emit(true);
    const ctrls = this.ddForm.value;
    const input = {
      project: this.project.symbolicName,
      type: ctrls.domainCtrl.metadata.layerType,
      start: this.times.start.getTime(),
      end: this.times.end.getTime(),
      mosaicId: ctrls.domainCtrl.metadata.symbolicName,
      mosaicName: ctrls.domainCtrl.metadata.name,
      interval: ctrls.intervalCtrl,
      format: ctrls.formatCtrl,
      ids: this.featureList.map(f => f.id),
      shapefile: ctrls.includeShapefileCtrl
    };
    
    this.service.submitRequest(input).then(() => {
      this.router.navigate(['data-downloads/list']).then(() => {
        this.submitting = false;
        this.processing.emit(false);
        this.ddForm.enable();
      });
    });
  }
}
