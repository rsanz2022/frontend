import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDownloadsElementComponent } from './data-downloads-element.component';

describe('DataDownloadsElementComponent', () => {
  let component: DataDownloadsElementComponent;
  let fixture: ComponentFixture<DataDownloadsElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataDownloadsElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDownloadsElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
