export interface Format {
  name: string;
  id: string;
}

export interface Options {
  formats?: Format[];
  intervals?: number[];
  domains?: any[];
}