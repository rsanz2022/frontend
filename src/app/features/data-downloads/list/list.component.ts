import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { DataDownloadsService } from '../data-downloads.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabGroup } from '@angular/material/tabs';
import { FormControl } from '@angular/forms';
import { MediaObserver } from '@angular/flex-layout';
import * as moment from 'moment-timezone';
import { Globals } from '@app/globals';
import { UserService } from '@app/core';
import { Project } from '@app/shared/models';

interface ColumnItem {
  def: string;
  name: string;
  hint?: string;
  media?: string;
  isDate?: boolean;
  mono?: boolean;
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('tabs', { static: true }) tabGroup: MatTabGroup;
  isRequest = false;
  project: Project;
  rxSub: Subscription;
  dataSource = new MatTableDataSource();
  tableFilter = new FormControl();
  loading = true;
  pollTimer: Subscription;
  columns: ColumnItem[] = [
    { def: 'link', name: 'File' },
    { def: 'mosaicName', name: 'Source' },
    { def: 'start', name: 'Start', hint: 'Start date & time', media: 'gt-xs', isDate: true },
    { def: 'end', name: 'End', hint: 'End date & time', media: 'gt-xs', isDate: true },
    { def: 'interval', name: 'Timestep', media: 'gt-sm', mono: true },
    { def: 'format', name: 'Format', hint: 'Data format', media: 'gt-sm', mono: true },
    { def: 'featureCount', name: 'Features', hint: 'Number of features', media: 'gt-md' },
    { def: 'requested', name: 'Requested', hint: 'Date & time requested', isDate: true, media: 'gt-md' },
    { def: 'requestedBy', name: 'Requested By', hint: 'User that requested the download', media: 'gt-lg' }
  ];

  constructor(
    private userService: UserService,
    private service: DataDownloadsService,
    private mediaObserver: MediaObserver,
    public globals: Globals,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
    this.rxSub = this.userService.project$.subscribe(this.updateProject);
  }

  ngOnDestroy(): void {
    this.rxSub.unsubscribe();
    this.stopPolling();
  }

  startPolling(): void {
    this.stopPolling();
    this.pollTimer = timer(0, 1000 * 30)
      .subscribe(val => {
        this.service.downloads(this.project.symbolicName)
          .then(res => this.downloadsUpdate(res, val));
      });
  }

  private downloadsUpdate = (res, times): void => {
    this.loading = false;
    this.dataSource.data = res;
    this.processingCheck(res, times);
  }

  processingCheck(items, timesChecked): void {
    const processing = items.some(item => item.status === 'Processing');
    if (!processing) {
      this.stopPolling();
      if (timesChecked > 0) {
        const error = items.some(item => item.status.toLowerCase() === 'error');
        const msg = error ? 'All requests have finished processing' :
          'All requests are ready!';
        this._snackBar.open(msg, '', { duration: 5000 });
      }
    }
  }

  stopPolling(): void {
    if (this.pollTimer) {
      this.pollTimer.unsubscribe();
    }
  }

  getDisplayedColumns(): string[] {
    return this.columns.filter(
      col => !col.media || this.mediaObserver.isActive(col.media)
    ).map(col => col.def);
  }

  getProperty = (obj, col: ColumnItem): string => {
    const prop = col.def.split('.').reduce((o, p) => o && o[p], obj);
    if (col.isDate) {
      return prop ? moment(prop).tz(this.project.timezone).format('YYYY/MM/DD HH:mm') : 'N/A';
    } else if (col.def === 'link') {
      return this.getLinkHTML(prop, obj.status);
    } else if (col.def === 'mosaicName') {
      return prop ? prop : obj.mosaicId;
    } else if (col.def === 'interval' && !isNaN(prop)) {
      return moment.duration(parseInt(prop), 'minutes').humanize();
    } else if (col.def === 'featureCount') {
      return !isNaN(prop) ? (prop > 0 ? prop.toLocaleString() : 'All') : 'N/A'
    }
    return prop ? prop : 'N/A';
  }

  private getLinkHTML = (prop: string, status: string): string => {
    switch (status ? status.toLowerCase() : '') {
      case 'finished':
        return `<a href="${prop}" title="Download" download>
          <span class="material-icons">save</span></a>`;

      case 'processing':
        return '<span class="material-icons" title="In progress">update</span>';

      default:
        return '<span class="material-icons error" title="Error">error</span>';
    }
  }

  sortingDataAccessor = (item, property: string): string => item[property];

  updateProject = (proj: Project): void => {
    this.loading = true;
    this.project = proj;
    this.startPolling();
  }
}
