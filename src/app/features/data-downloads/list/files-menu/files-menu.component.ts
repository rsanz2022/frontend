import { Component, Input } from '@angular/core';

@Component({
  selector: 'ddl-files-menu',
  template: `<button mat-icon-button [matMenuTriggerFor]="menu">
      <mat-icon>folder_open</mat-icon>
    </button>
    <mat-menu #menu="matMenu">
      <a mat-menu-item [href]="item.link"><mat-icon color="primary">save</mat-icon>Data</a>
      <a mat-menu-item *ngIf="item.pdfLink" [href]="item.pdfLink" download><mat-icon color="primary">description</mat-icon>Report (PDF)</a>
      <a mat-menu-item *ngIf="item.docxLink" [href]="item.docxLink" download><mat-icon color="primary">description</mat-icon>Report (DOCX)</a>
    </mat-menu>`,
  styles: [`.mat-icon-button .mat-icon { color: #008ba3; }`],
})
export class FilesMenuComponent {
  @Input() item;
}
