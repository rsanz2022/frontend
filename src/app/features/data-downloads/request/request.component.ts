import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataDownloadsService } from '../data-downloads.service';
import { Project } from '@app/shared/models';
import { Subscription } from 'rxjs';
import { UserService } from '@app/core';
import * as moment from 'moment-timezone';
import * as DateValidator from '@app/shared';
import { Globals } from '@app/globals';
import { Options } from '../models';
import * as mapboxgl from 'mapbox-gl';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MediaObserver } from '@angular/flex-layout';
import { distinctUntilChanged } from 'rxjs/operators';
import { Router } from '@angular/router';

const MY_FORMATS = {
  parse: { dateInput: '' },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'YYYY/MM/DD',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class RequestComponent implements OnInit, OnDestroy {
  submitting = false;
  rxSub: Subscription;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  project: Project;
  groups: { name: string; id: string }[];
  featuresLoading = true;
  formatsLoading = true;
  groupLoading = false;
  layerIds: string[];
  isMouseDown = false;
  selectedFeatures: string[] = [];
  selectedIndex = 0;
  tzOffset = '';
  readonly allIntervals = [ 5, 15, 30, 60, 1440 ];
  domain: any;
  selectedLayer: any;
  options: Options = {
    intervals: this.allIntervals
  };
  shifted = false;
  alted = false;
  get max(): Date { return new Date(); }
  get features(): string[] {
    return this.firstFormGroup.get('selectedFeatures').value;
  }
  set features(v: string[]) {
    this.firstFormGroup.get('groupCtrl')
      .setValue('', { onlySelf: true, emitEvent: false });
    this.firstFormGroup.get('selectedFeatures').setValue(v);
  }

  constructor(
    private fb: FormBuilder,
    private service: DataDownloadsService,
    public userService: UserService,
    public globals: Globals,
    private snackBar: MatSnackBar,
    public media: MediaObserver,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.buildForms();
    this.rxSub = this.userService.project$.subscribe(this.updateProject);
  }

  ngOnDestroy(): void {
    this.rxSub.unsubscribe();
  }

  calStartFilter = (d: Date | null): boolean => {
    return moment(d).isBefore(this.getTime('end'));
  }

  calEndFilter = (d: Date | null): boolean => {
    return moment(d).isSameOrBefore(new Date());
  }

  buildForms = (): void => {
    this.firstFormGroup = this.fb.group({
      domainCtrl: [{value: {}, disabled: true}, Validators.required],
      groupCtrl: [''],
      selectedFeatures: [[], Validators.required],
    });
    this.secondFormGroup = this.fb.group({
      intervalCtrl: '',
      formatCtrl: [null, [Validators.required]],
      includeShapefileCtrl: [false],
      includeReport: [false],
      startDate: ['', [DateValidator.incompleteDate, Validators.required]],
      startTime: ['00:00', [DateValidator.incompleteTime, Validators.required]],
      endDate: ['', [DateValidator.incompleteDate, Validators.required]],
      endTime: ['00:00', [DateValidator.incompleteTime, Validators.required]],
      _timezone: '',
      maxDate: new Date()
    });

    this.secondFormGroup
      .setValidators([ DateValidator.range, DateValidator.maximums ]);

    this.firstFormGroup.get('domainCtrl').valueChanges
      .pipe(distinctUntilChanged()).subscribe(this.onSourceChange);
    this.firstFormGroup.get('groupCtrl').valueChanges
      .pipe(distinctUntilChanged()).subscribe(this.onGroupChange);
    this.firstFormGroup.get('selectedFeatures').valueChanges
      .pipe(distinctUntilChanged()).subscribe(v => this.selectedFeatures = v);

    const timeCtrls = ['startDate', 'startTime', 'endDate', 'endTime'];
    timeCtrls.forEach(ctrl => this.secondFormGroup.get(ctrl).valueChanges
      .pipe(distinctUntilChanged())
      .subscribe(() => { this.onTimeChanges() }));
  }

  onGroupChange = (value: string): void => {
    if (value) {
      this.setFeaturesLoading(true);
      this.service.getGroup({
        mosaic: this.domain,
        name: value
      }).then(features => {
        this.features = features;
        this.setFeaturesLoading(false);
        setTimeout(() => this.firstFormGroup.get('groupCtrl')
          .setValue(value, { emitEvent: false }));
      });
    }
  }

  public setFeaturesLoading = (v: boolean): void => {
    if (this.featuresLoading !== v) {
      this.featuresLoading = v;
      this.firstFormGroup.get('domainCtrl')[v ? 'disable' : 'enable']();
    }
  }

  onSourceChange = (newLayer: mapboxgl.Layer): void => {
    if (newLayer && newLayer.metadata) {
      this.setFeaturesLoading(true);
      this.groups = newLayer.metadata.dataDownloads.groups;
      this.domain = newLayer.metadata.symbolicName;
      this.selectedLayer = newLayer;

      this.service
        .getFormats(this.project.symbolicName, newLayer.metadata.layerType)
        .then(res => {
          this.options.formats = res;
          this.secondFormGroup.get('formatCtrl').setValue(null);
        });
    }
  }

  humanizeDuration = (t: number): string =>
    moment.duration(t, 'minutes').humanize();

  updateProject = (res: Project): void => {
    if (res) {
      if (this.firstFormGroup) this.setFeaturesLoading(true);
      this.project = res;
      const days = [
        moment().tz(res.timezone).subtract(1, 'days'),
        moment().tz(res.timezone)
      ];

      this.secondFormGroup.get('_timezone')
        .setValue(res.timezone, { emitEvent: false });
      this.secondFormGroup.get('startDate')
        .setValue(days[0], { emitEvent: false });
      this.secondFormGroup.get('endDate')
        .setValue(days[1], { emitEvent: false });
      this.onTimeChanges();

      this.secondFormGroup.get('intervalCtrl')
        .setValue(this.options.intervals[0]);
      this.tzOffset = res.getOffset(new Date());
      this.service.getLayers(res.symbolicName).then((layers) => {
        this.options.domains = layers;
        this.firstFormGroup.get('domainCtrl').setValue(layers[0]);
      });
    }
  }

  private onTimeChanges = (): void => {
    if (!this.project) return;
    const start = this.getTime('start');
    const end = this.getTime('end');
    const duration = end.diff(start, 'minutes');
    const intervalCtrl = this.secondFormGroup.get('intervalCtrl');
    this.options.intervals = this.allIntervals
      .filter(i => i >= this.project.interval && i <= duration);
    if (!this.options.intervals.includes(intervalCtrl.value)) {
      intervalCtrl.setValue(this.options.intervals[0]);
    }

  }

  getTime(prop: string): moment.Moment {
    const ctrlDate = this.secondFormGroup.get(prop + 'Date').value;
    const ctrlTime = this.secondFormGroup.get(prop + 'Time').value;
    const dtString = `${
        ctrlDate && ctrlDate.isValid() ? ctrlDate.format('YYYY-MM-DD') : ''
      }T${ctrlTime}`;
    return moment.tz(dtString, 'YYYY-MM-DDTHH:mm', true, this.project.timezone);
  }

  isLargeRequest = (): boolean => {
    const threshold = 2016e4; // 10k features for 7 days at 5 minute intervals
    const start = this.getTime('start').valueOf();
    const end = this.getTime('end').valueOf();
    const selected = this.firstFormGroup.get('selectedFeatures').value;
    return this.selectedIndex === 2 &&
      threshold < (((end - start) / 3e5) * selected.length);
  }

  submit(): void {
    this.submitting = true;
    const ctrls = {
      ...this.firstFormGroup.value,
      ...this.secondFormGroup.value
    };
    const options = {
      project: this.project.symbolicName,
      type: ctrls.domainCtrl.metadata.layerType,
      start: this.getTime('start').valueOf(),
      end: this.getTime('end').valueOf(),
      mosaicId: ctrls.domainCtrl.metadata.symbolicName,
      mosaicName: ctrls.domainCtrl.metadata.name,
      interval: ctrls.intervalCtrl,
      format: ctrls.formatCtrl.id,
      ids: Array.from(new Set(ctrls.selectedFeatures)),
      shapefile: ctrls.includeShapefileCtrl,
      report: ctrls.includeReport,
      reportFormat: ['PDF', 'DOCX']
    };

    this.service.submitRequest(options).then(
      () => this.router.navigate(['data-downloads/list']),
      () => this.snack('There was a problem with your request.', true)
    );
  }

  snack = (msg: string, isError?: boolean): void => {
    this.snackBar.open(msg, null, {
      duration: isError ? 6000 : 4000,
      panelClass: isError ? 'error' : null
    });
  }
}
