import { Apollo, gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Format } from './models';
import { FeatureCollection } from 'geojson';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Layer } from 'mapbox-gl';

interface Response {
  getListOfDownloads?: any[];
  getLayers?: string[];
  getFormats?: Format[];
  getGrouping?: {ids: string[]};
}

@Injectable({ providedIn: 'root' })
export class DataDownloadsService {
  private readonly _icons = {
    symbol: {id:'fiber_manual_record'},
    raster: {id:'layers'},
    'fill-extrusion': {svg:true, id:'3d-layer'},
    line: {svg:true, id:'layers-outlined'},
    fill: {id:'layers'}
  };

  constructor(
    private http: HttpClient,
    private apollo: Apollo
  ) {}

  downloads(project: string): Promise<any> {
    return this.apollo
      .query<Response>({
        query: gql`query DataDownloads($project:String!) {
          getListOfDownloads(project: $project) {
            _id
            start
            end
            format
            interval
            project
            mosaicId
            mosaicName
            requested
            requestedBy
            link
            status
            statusDescription
            featureCount
            docxLink
            pdfLink
          }
        }`,
        variables: { project }
      })
      .pipe(map(res => res.data.getListOfDownloads.reverse()))
      .toPromise();
  }

  getLayers(project: string): Promise<any> {
    return this.apollo
      .query<Response>({
        query: gql`query Query($project:String!) {
          getLayers(projectName: $project)
        }`,
        variables: { project }
      })
      .pipe(map(res => res.data.getLayers
        .map(layerJSON => {
          const layer = JSON.parse(layerJSON);
          layer.metadata.icon = {
            ...this._icons[layer.type], ...layer.metadata.icon
          };
          return layer;
        })
        .filter(l => l.metadata?.dataDownloads &&
          (l.metadata.betaOnly ? !environment.production : true)
        ).sort((a: Layer, b: Layer) => {
          const aName = a.metadata.name.toLowerCase(),
                bName = b.metadata.name.toLowerCase();
          if (aName < bName) return -1;
          if (aName > bName) return 1;
          return 0;
        })
      ))
      .toPromise();
  }

  getFormats(project: string, layerType: string): Promise<Format[]> {
    const formatQuery = gql`query Query($project:String! $layerType:String!) {
      getFormats(project: $project layerType: $layerType) { id name }
    }`;

    return this.apollo.query<Response>({
      query: formatQuery,
      variables: { project, layerType }
    }).pipe(map(res => res.data.getFormats))
    .toPromise();
  }

  getGroup(input: { mosaic: string; name: string }): Promise<string[]> {
    const groupQuery = gql`query DataDownloads($input:groupingInput!) {
      getGrouping(input:$input) { ids }
    }`;

    return this.apollo.query<Response>({
      query: groupQuery,
      variables: { input }
    }).pipe(map(res => res.data.getGrouping.ids))
    .toPromise();
  }

  submitRequest(input): Promise<any> {
    return this.apollo
      .query<Response>({
        query: gql`query DataDownloads($input: submitRequestInput!) {
          submitRequest(input: $input)
        }`,
        variables: { input }
      })
      .toPromise();
  }

  getGeojson = (url: string): Promise<FeatureCollection> => this.http
    .get<FeatureCollection>(url).toPromise();
}
