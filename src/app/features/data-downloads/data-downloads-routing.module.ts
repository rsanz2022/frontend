import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestComponent } from './request/request.component';
import { ListComponent } from './list/list.component';
import { DataDownloadsComponent } from './data-downloads.component';

const routes: Routes = [
  {
    path: '',
    component: DataDownloadsComponent,
    children: [
      {
        path: '',
        redirectTo: 'request',
        pathMatch: 'full'
      },
      {
        path: 'request',
        component: RequestComponent,
        data: { title: 'Data Downloads: Request' }
      },
      {
        path: 'list',
        component: ListComponent,
        data: { title: 'Data Downloads: List' }
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class DataDownloadsRoutingModule { }
