import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

interface NavLink {
  label: string;
  link: string;
  index: number;
}

@Component({
  selector: 'app-data-downloads',
  templateUrl: './data-downloads.component.html',
  styleUrls: ['./data-downloads.component.scss']
})
export class DataDownloadsComponent implements OnInit {
  rxSub: Subscription;
  navLinks: NavLink[] = [
    {
      label: 'Request',
      link: './request',
      index: 0
    }, {
      label: 'List',
      link: './list',
      index: 1
    }
  ];
  activeLinkIndex = -1;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.rxSub = this.router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe(this.setIndex);
    this.setIndex();
  }

  ngOnDestroy(): void {
    this.rxSub.unsubscribe();
  }

  private setIndex = () => {
    const url = './' + this.router.url.split('/').reverse()[0];
    const link = this.navLinks.find(tab => tab.link === url);
    this.activeLinkIndex = this.navLinks.indexOf(link);
  }
}
