import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataDownloadsComponent } from './data-downloads.component';
import { DataDownloadsRoutingModule } from './data-downloads-routing.module';
import { RequestComponent } from './request/request.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { SearchModule } from '@app/shared';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ListComponent } from './list/list.component';
import { FeatureSelectMapModule } from '@app/shared/feature-select-map/feature-select-map.module';
import { MatMenuModule } from '@angular/material/menu';
import { FilesMenuComponent } from './list/files-menu/files-menu.component';

@NgModule({
  declarations: [
    DataDownloadsComponent,
    RequestComponent,
    ListComponent,
    FilesMenuComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    DataDownloadsRoutingModule,
    SearchModule,
    FeatureSelectMapModule,

    MatFormFieldModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    MatButtonModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatIconModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatMenuModule,
  ]
})
export class DataDownloadsModule { }
