import { TestBed } from '@angular/core/testing';

import { DataDownloadsService } from './data-downloads.service';

describe('DataDownloadsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataDownloadsService = TestBed.get(DataDownloadsService);
    expect(service).toBeTruthy();
  });
});
