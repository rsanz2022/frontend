import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsComponent } from './maps.component';
import { MapsRoutingModule } from './maps-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { environment } from '@env/environment';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSliderModule } from '@angular/material/slider';
import { LayerPaneComponent } from './components/layer-pane/layer-pane.component';
import { LayerListComponent } from './components/layer-pane/layer-list/layer-list.component';
import { LayerSelectComponent } from './components/layer-pane/layer-list/layer-select/layer-select.component';
import { HyetographModule } from '@app/shared/charts/hyetograph/hyetograph.module';
import { HydrographModule } from '@app/shared/charts/hydrograph/hydrograph.module';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { PopupContentComponent } from './components/popup-content/popup-content.component';
import { TimeControlsComponent } from './components/time-controls/time-controls.component';
import { PickerPanelComponent } from './components/time-controls/picker-panel/picker-panel.component';
import { TimepickerModule } from '@app/shared/timepicker/timepicker.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LayerIconComponent } from './components/layer-pane/layer-list/layer-icon/layer-icon.component';
import { ControlsComponentComponent } from './components/controls/controls.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ScalesComponent } from './components/scales/scales.component';
import { ElementsComponent } from './components/elements-controls/elements.component';
import { AnimationControlsComponent } from './components/time-controls/animation-controls/animation-controls.component';
import { DataDownloadsElementModule } from '../data-downloads/element/data-downloads-element.module';
import { DDFElementModule } from '../ddf/element/ddf-element.module';
import { WatchpointsElementModule } from '../watchpoints/element/watchpoints-element.module';
import { OptionsPaneComponent } from './components/layer-pane/options-pane/options-pane.component';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SearchModule } from '@app/shared';
import { AnimationFormComponent } from './components/time-controls/animation-form/animation-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { AnimationAlertComponent } from './components/time-controls/animation-controls/animation-alert/animation-alert.component';
import { NotificationsElementModule } from '@app/features/notifications/element/notifications-element.module';
import { GaugeStatusElementModule } from '../gauge-status/element/gauge-status-element.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { LayerCreatorComponent } from './components/layer-pane/layer-list/layer-creator/layer-creator.component';
import { CorePipesModule } from '@app/core';

@NgModule({
  declarations: [
    MapsComponent,
    LayerPaneComponent,
    LayerListComponent,
    LayerSelectComponent,
    PopupContentComponent,
    TimeControlsComponent,
    PickerPanelComponent,
    LayerIconComponent,
    ControlsComponentComponent,
    ScalesComponent,
    ElementsComponent,
    AnimationControlsComponent,
    OptionsPaneComponent,
    AnimationFormComponent,
    AnimationAlertComponent,
    LayerCreatorComponent,
  ],
  imports: [
    CommonModule,
    MapsRoutingModule,
    FlexLayoutModule,
    DragDropModule,
    HyetographModule,
    HydrographModule,
    TimepickerModule,
    FormsModule,
    ReactiveFormsModule,
    DataDownloadsElementModule,
    DDFElementModule,
    WatchpointsElementModule,
    NotificationsElementModule,
    GaugeStatusElementModule,
    SearchModule,
    CorePipesModule,

    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatSliderModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatInputModule,
    MatDividerModule,
    MatMenuModule,
    MatDialogModule,
    MatTooltipModule,
    MatRippleModule,
    MatSelectModule,
    MatFormFieldModule,
    MatExpansionModule,

    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken
    })
  ]
})
export class MapsModule { }
