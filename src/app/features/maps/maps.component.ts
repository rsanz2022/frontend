import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, ReplaySubject } from 'rxjs';
import { UserService } from '@app/core';
import { Project } from '@app/shared/models';
import { LayersStoreService } from './resources/stores/layers-store.service';
import { MapsService } from './maps.service';
import { debounceTime } from 'rxjs/operators';
import { PaneStatesService } from './resources/services/pane-states.service';
import { StylesService } from './resources/services/styles.service';
import { MediaObserver } from '@angular/flex-layout';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import * as mapboxgl from 'mapbox-gl';
import { TimeControlsService } from './resources/services/time-controls.service';
import { AppConfigStoreService } from '@app/shared/app-config-store.service';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TimesStoreService } from './resources/stores/times-store.service';
import { ElementEvent } from '@app/shared/models/element.model';
import Auth from '@aws-amplify/auth';

interface Tooltip {
  name: string;
  displayValue: string | number;
  value: string | number;
}

interface MapUI {
  popups: mapboxgl.MapboxGeoJSONFeature[];
  tooltips: mapboxgl.MapboxGeoJSONFeature[];
}

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss'],
})
export class MapsComponent implements OnInit, OnDestroy {
  mainMap: mapboxgl.Map;
  clickedFeatures = [];
  popup: any = {};
  popupLayers: { [key: string]: { id: string; hideOverlaps?: boolean } } = {};
  tooltips: Tooltip[];
  tooltipLayers: {
    [key: string]: {
      decimals: number;
      show: boolean;
      hideOverlaps?: boolean;
    };
  } = {};
  tooltipInterval: Subscription;
  scaleLayers: any[] = [];
  layerPaneOpen = this.paneService.showLayers;
  smallScreen = this.mediaObserver.isActive('lt-sm');
  leftPadding = this.layerPaneOpen && !this.smallScreen ? 280 : 0;
  project: Project;
  subscriptions: Subscription[] = [];
  basemap: any = {};
  layerList = [];
  layerListIds: string[] = [];
  icons = ['soil-moisture', 'watchpoints', 'gauges', 'lwc', 'hexagon'];
  loadedIcons = 0;
  loading = true;
  onStyleData$ = new ReplaySubject<mapboxgl.MapStyleDataEvent>(1);
  buildingsId: string;
  isAnimating = false;
  svgIcons = [
    ['3d-layer', 'assets/images/3d-24px.svg'],
    ['layers-outlined', 'assets/images/layers_outlined-24px.svg'],
  ];
  onMouseMove$ = new ReplaySubject<mapboxgl.MapMouseEvent>(1);
  token: string;

  constructor(
    private userService: UserService,
    private mediaObserver: MediaObserver,
    private sanitizer: DomSanitizer,
    private timeControlsSvc: TimeControlsService,
    private timesStore: TimesStoreService,
    private configStore: AppConfigStoreService,
    private route: ActivatedRoute,
    private _router: Router,
    private serializer: UrlSerializer,
    private snack: MatSnackBar,
    public stylesService: StylesService,
    public service: MapsService,
    public layersStore: LayersStoreService,
    public paneService: PaneStatesService,
    iconRegistry: MatIconRegistry
  ) {
    this.svgIcons.forEach((icon) =>
      iconRegistry.addSvgIcon(
        icon[0],
        this.sanitizer.bypassSecurityTrustResourceUrl(icon[1])
      )
    );
  }

  ngOnInit(): void {
    this.layersStore.active = true;
    this.subscriptions.push(
      this.service.basemap$.subscribe(this.setBasemap),
      this.paneService.showLayers$.subscribe(this.onPaneToggle),
      this.userService.project$.subscribe(this.updateProject),
      this.onStyleData$.pipe(debounceTime(500)).subscribe(this.onStyleData),
      this.service.onSourceData$.subscribe(this.onSourceData),
      this.mediaObserver.asObservable().subscribe(this.onMediaObserver),
      this.onMouseMove$.subscribe(this.toggleTooltip),
      this.timeControlsSvc.isAnimating$.subscribe(this.onIsAnimating),
      this.timeControlsSvc.animationLoading$.subscribe(
        (v) => (this.loading = v)
      )
    );

    Auth.currentSession().then(
      (session) => (this.token = session.getIdToken().getJwtToken())
    );
  }

  ngOnDestroy(): void {
    this.layersStore.setStorageLayers(this.project.symbolicName);
    this.onLayers([]);
    this.layersStore.active = false;
    this.layersStore.layers = [];
    this.service.setMap(null);
    this.subscriptions.forEach((s) => s.unsubscribe());
  }

  private onMediaObserver = () => {
    this.smallScreen = this.mediaObserver.isActive('lt-sm');
    const includePadding = this.paneService.showLayers && !this.smallScreen;
    this.leftPadding = includePadding ? 280 : 0;
  };

  public saveBounds = (): void => {
    if (!this.mainMap) return;
    const bounds = this.mainMap.getBounds();
    const type = 'map';
    const configs = [
      {
        type,
        id: 'maps-bounds',
        temp: true,
        pid: this.project.id,
        value: bounds.toArray() as mapboxgl.LngLatBoundsLike,
      },
    ];
    this.configStore.setConfigs(configs);
  };

  private onIsAnimating = (isAnim) => {
    if (!this.mainMap) return;
    const action = isAnim ? 'disable' : 'enable';
    this.isAnimating = isAnim;
    this.mainMap.dragPan[action]();
    this.mainMap.scrollZoom[action]();
    if (isAnim) this.closePopup();
  };

  public onMapLoad = (event: mapboxgl.Map): void => {
    this.subscriptions.push(
      this.layersStore.layers$.subscribe(this.onLayers),
      this.layersStore.layerRemoved$.subscribe(this.onRemoveLayer),
      this.layersStore.layerAdded$.subscribe(this.onAddLayer),
      this.layersStore.layerEdited$.subscribe(this.onLayerEdited),
      this.layersStore.layerLayout$.subscribe(this.updateLayout),
      this.layersStore.layerMetadata$.subscribe(this.updateMetadata),
      this.layersStore.layerPaint$.subscribe(this.updatePaint)
    );
    this.mainMap = event;
    this.service.setMap(event);
  };

  public pointerCheck = (mFeat: MapUI): void => {
    if (!this.mainMap) return;
    const isPointer = mFeat.popups.length || mFeat.tooltips.length;
    this.mainMap.getCanvas().style.cursor = isPointer ? 'pointer' : '';
  };

  public onMapClick = (
    event: mapboxgl.MapMouseEvent,
    id?: string,
    source?: string
  ): void => {
    if (event.point && !this.isAnimating) {
      this.clickedFeatures = this.getMouseFeatures(event).popups.filter(
        (feat) =>
          !id ||
          !source ||
          (feat.properties.id === id && feat.source === source)
      );
      if (this.clickedFeatures.length) {
        const geometry = this.clickedFeatures[0].geometry;
        this.popup = {
          lngLat:
            geometry && geometry.type === 'Point'
              ? geometry.coordinates
              : event.lngLat,
        };
        this.tooltips = null;
      }
    }
  };

  public closePopup = (): void => {
    this.popup = {};
  };

  public trackLayerBy = (idx: number, data: { id: string }): string => data.id;

  private toggleTooltip = (e?: mapboxgl.MapMouseEvent): void => {
    if (!this.mainMap) return;
    const features = this.getMouseFeatures(e);
    this.pointerCheck(features);
    if (
      !e ||
      e.type !== 'mousemove' ||
      this.popup.lngLat ||
      this.smallScreen ||
      this.isAnimating
    ) {
      this.tooltips = null;
    } else {
      if (features.tooltips.length) {
        const node = document.getElementById('map-tooltip');
        this.tooltips = features.tooltips.map(this.getTooltipObj);
        node.style.left = `${e.originalEvent.clientX + 10}px`;
        node.style.top = `${e.originalEvent.clientY + 12}px`;
      } else {
        this.tooltips = null;
      }
    }
  };

  private getTooltipObj = (f: mapboxgl.MapboxGeoJSONFeature): Tooltip => ({
    name: f.properties.name || f.properties.id || f.layer.metadata.name,
    displayValue:
      typeof f.state.val === 'string'
        ? f.state.val
        : f.state.val.toFixed(f.layer.metadata.tooltip.decimals || 2),
    value: f.state.val,
  });

  private onSourceData = (event): void => {
    const i = this.layerListIds.indexOf(event.sourceId);
    if (i >= 0) {
      const layer = this.layerList[i];
      setTimeout(() => {
        if (
          this.service.hasFeatureState(layer) &&
          this.mainMap.isSourceLoaded(layer.id)
        ) {
          this.service.updateAllFeatureStates(layer);
        }
      });
    }
  };

  public onElement = (ev: ElementEvent): void => {
    const event = {
      type: 'click',
      point: this.mainMap.project(ev.properties.lngLat),
      lngLat: ev.properties.lngLat,
    };
    this.mainMap.setCenter(ev.properties.lngLat);
    this.onMapClick(
      event as mapboxgl.MapMouseEvent,
      ev.properties.id,
      ev.properties.source
    );
  };

  private onStyleData = (event): void => {
    if (event && this.mainMap) {
      if (this.buildingsId) {
        const buildings = this.mainMap.getLayer(this.buildingsId);
        const mapBuildings = this.mainMap.getLayer('building');
        if (mapBuildings && buildings) {
          const color = this.mainMap.getPaintProperty('building', 'fill-color');
          this.mainMap.setPaintProperty(
            this.buildingsId,
            'fill-extrusion-color',
            color
          );
        }
      }
    }
  };

  private getMouseFeatures = (ev?: mapboxgl.MapMouseEvent): MapUI => {
    const features: any[] = this.mainMap.queryRenderedFeatures(ev?.point, {
      layers: this.layerListIds,
    });
    return {
      tooltips: features.filter(this.ttipFilter),
      popups: [
        ...features
          .filter(
            (f, i, a) =>
              this.popupLayers[f.layer.id] &&
              (this.popupLayers[f.layer.id].hideOverlaps && i !== 0
                ? f.layer.id !== a[i - 1].layer.id
                : true)
          )
          .map((f) => ({ ...ev, ...f })),
        ...this.layerList
          .filter(
            (l) =>
              l.type === 'raster' &&
              l.metadata.popup &&
              this.mainMap.getLayoutProperty(l.id, 'visibility') === 'visible'
          )
          .map((l) => ({ ...ev, layer: l })),
      ],
    };
  };

  private ttipFilter = (f, i, a): boolean =>
    this.tooltipLayers[f.layer.id] &&
    this.tooltipLayers[f.layer.id].show &&
    !!f.state.val &&
    (this.tooltipLayers[f.layer.id].hideOverlaps && i !== 0
      ? f.layer.id !== a[i - 1].layer.id
      : true);

  private setBasemap = (res): void => {
    if (res && this.basemap.url !== res.url) {
      this.basemap = res;
      const layers = this.layerList;
      if (layers && layers.length) {
        this.onLayers([]);
        if (this.mainMap) {
          layers.forEach((layer) => {
            if (layer.metadata.data && layer.metadata.data.featureState) {
              layer.metadata.data.featureState.hasUpdate = true;
            }
            this.removeSource(layer.id);
            if (
              layer.type === 'symbol' &&
              layer.metadata.labels &&
              layer.metadata.labels.colors
            ) {
              layer.paint['text-color'] =
                layer.metadata.labels.colors[this.basemap.dark ? 0 : 1];
              layer.paint['text-halo-color'] = this.basemap.dark
                ? 'rgba(0,0,0,0.4)'
                : 'rgba(255,255,255,0.7)';
            }
          });
        }
        setTimeout(() => this.onLayers(layers));
      }
    }
  };

  private onAddLayer = (layer): void => {
    if (!!layer.metadata.popup) {
      this.popupLayers[layer.id] = {
        id: layer.id,
        hideOverlaps: layer.metadata.hideOverlaps,
      };
    }
    if (!!layer.metadata.tooltip) {
      this.tooltipLayers[layer.id] = {
        hideOverlaps: layer.metadata.hideOverlaps,
        ...layer.metadata.tooltip,
      };
    }
    if (layer.id.includes('3d_buildings')) {
      this.buildingsId = layer.id;
    }
    this.addScale(layer);
  };

  private onRemoveLayer = (id): void => {
    this.removeSource(id);
    this.removeScale(id);
    if (id.includes('3d_buildings')) {
      this.buildingsId = '';
    }
    delete this.popupLayers[id];
    delete this.tooltipLayers[id];
  };

  private updateLayout = (layer): void => {
    const i = this.layerListIds.indexOf(layer.id);
    if (i >= 0) {
      this.layerList[i].layout = {
        ...this.layerList[i].layout,
        ...layer.layout,
      };
    }
    this.addScale(layer);
  };

  private updatePaint = (layer): void => {
    const i = this.layerListIds.indexOf(layer.id);
    if (i >= 0) {
      this.layerList[i].paint = {
        ...this.layerList[i].paint,
        ...layer.paint,
      };
    }
    this.addScale(layer);
  };

  private addScale = (layer: mapboxgl.Layer): void => {
    if (!!layer.metadata.scale && !this.isAnimating) {
      const scales = [...this.scaleLayers];
      const i = scales.findIndex((sl) => layer.id === sl.layer.id);
      const dataType = layer.metadata.dataType;
      const unitConfig = dataType ? this.project.getUnitConfig(dataType) : null;
      const unitString = unitConfig ? `: ${unitConfig.options[0].abbr}` : '';
      const scale = {
        ...layer.metadata.scale,
        tableId: layer.metadata.tableId,
        visible: layer.layout.visibility !== 'none',
        label: `${layer.metadata.name}${unitString}`,
        layer: layer,
      };
      if (i < 0) {
        scales.push(scale);
      } else {
        scales[i] = scale;
      }
      this.scaleLayers = scales;
    }
  };

  private removeScale = (id: string): void => {
    const i = this.scaleLayers.findIndex((sl) => id === sl.layer.id);
    const scales = [...this.scaleLayers];
    if (i >= 0) scales.splice(i, 1);
    this.scaleLayers = scales;
  };

  private removeSource = (id: string): void => {
    if (id && !id.includes('3d_buildings')) {
      setTimeout(() => {
        const source = this.mainMap.getSource(id);
        if (source) {
          this.mainMap.removeSource(id);
          if (this.mainMap.getSource('labels-' + id)) {
            this.mainMap.removeSource('labels-' + id);
          }
        }
      });
    }
  };

  public getLayerBefore = (id: string): string => {
    const i = this.layerListIds.indexOf(id);
    const layer = this.layerList[i - 1];
    const staticType = layer?.metadata.staticType;
    if (staticType === 'polygon') {
      return 'labels-' + layer.id;
    }
    return layer?.id || '';
  };

  private onLayers = (layers: mapboxgl.Layer[]): void => {
    this.layerList = layers;
    this.layerListIds = layers.map((l: mapboxgl.Layer) => l.id);
  };

  private updateMetadata = (layer): void => {
    const i = this.layerListIds.indexOf(layer.id);
    if (i !== -1) {
      this.layerList[i] = layer;
      if (layer.metadata.tooltip) {
        this.tooltipLayers[layer.id] = layer.metadata.tooltip;
      }
    }
  };

  private onLayerEdited = (layer): void => {
    this.updateLayout(layer);
    const source = this.mainMap.getSource(layer.id);
    if (source) {
      const s = layer.source;
      const style = this.mainMap.getStyle();
      switch (source.type) {
        case 'geojson': {
          source.setData(s.data);
          break;
        }
        case 'raster': {
          style.sources[layer.id]['tiles'] = s.tiles;
          this.mainMap.setStyle(style);
          break;
        }
        case 'vector': {
          if (
            this.service.hasFeatureState(layer) &&
            layer.metadata.data.featureState.hasUpdate
          ) {
            if (source.tiles && s.tiles[0] !== source.tiles[0]) {
              source.tiles = s.tiles;
              this.mainMap['style'].sourceCaches[layer.id].clearTiles();
              this.mainMap['style'].sourceCaches[layer.id].update(
                this.mainMap['transform']
              );
              this.mainMap.triggerRepaint();
            }
            this.service.updateAllFeatureStates(layer);
          }
          break;
        }
      }
    }
  };

  private onPaneToggle = (res): void => {
    this.leftPadding =
      this.paneService.showLayers && !this.smallScreen ? 280 : 0;
    if (this.mainMap) {
      const xDistance = res ? -140 : 140;
      this.mainMap.panBy([xDistance, 0]);
    }
  };

  public resetMapBounds = (): void => {
    this.mainMap.fitBounds(this.project.bounds, {
      duration: 0,
      padding: { left: this.leftPadding, top: 0, bottom: 0, right: 0 },
    });
  };

  public copyLink = (): void => {
    const url = window.location.origin;
    const mglBounds = this.mainMap.getBounds();
    const p = this.project.id;
    const t = [
      this.timesStore.times.start.getTime(),
      this.timesStore.times.end.getTime(),
      +this.timesStore.times.realtime,
    ].join();
    const l = this.layerList.map((l) => l.id).join();
    const b = [
      mglBounds.getWest(),
      mglBounds.getSouth(),
      mglBounds.getEast(),
      mglBounds.getNorth(),
    ]
      .map((coord) => coord.toFixed(4))
      .join();
    const tree = this._router.createUrlTree([], {
      queryParams: { l, t, b, p },
    });
    const treeString = url + this.serializer.serialize(tree);
    if (navigator.clipboard) {
      navigator.clipboard
        .writeText(treeString)
        .then(() => {
          this.snack.open(
            'A link to this map has been copied to your clipboard',
            '',
            {
              duration: 4000,
            }
          );
        })
        .catch(() => {
          this.snack.open('There was a problem copying this link', '', {
            duration: 4000,
            panelClass: 'error',
          });
          window.prompt('Copy to clipboard: Ctrl+C, Enter', treeString);
        });
    } else {
      window.prompt('Copy to clipboard: Ctrl+C, Enter', treeString);
    }
  };

  private initBounds = (project: Project): void => {
    this.route.queryParams.subscribe((params): void => {
      let urlBounds: mapboxgl.LngLatBoundsLike;
      if (params.p) {
        if (params.p == project.id) {
          urlBounds = params.b.split(',').map((b) => parseFloat(b));
        } else {
          this.snack.open('This link is for a different project', 'OK', {
            duration: 8000,
          });
        }
      }
      const boundsConfig = this.configStore.getConfig(
        'maps-bounds',
        project.id
      );
      if (!this.project && boundsConfig) this.leftPadding = 0;
      const bounds =
        urlBounds ??
        (this.project
          ? project.bounds
          : (boundsConfig?.value as mapboxgl.LngLatBoundsLike) ||
            project.bounds);
      this.service.bounds$.next(bounds);
    });
  };

  private updateProject = (proj: Project): void => {
    this.initBounds(proj);
    this.service.setBasemap(proj.id); // initialize default basemap
    this.project = proj;
    this.loading = false;
    this.clickedFeatures = [];
    this.popup = {};
  };

  public transformRequest = (url: string): mapboxgl.RequestParameters => {
    const protocol = window.location.protocol;
    const host = window.location.host;
    if (
      url.startsWith(`${protocol}//${host}/api/`) ||
      url.startsWith(`/api/`) ||
      url.startsWith(`api/`)
    ) {
      return {
        url,
        headers: { Authorization: 'Bearer ' + this.token },
      };
    }
  };
}
