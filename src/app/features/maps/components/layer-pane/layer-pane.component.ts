import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PaneStatesService } from '../../resources/services/pane-states.service';
import { OptionsService } from './options-pane/options.service';

@Component({
  selector: 'app-layer-pane',
  templateUrl: './layer-pane.component.html',
  styleUrls: ['./layer-pane.component.scss']
})
export class LayerPaneComponent implements OnInit, OnDestroy {
  open: boolean;
  rxSubs: Subscription[] = [];
  year = new Date().getFullYear();

  constructor(
    private paneService: PaneStatesService,
    public optionsService: OptionsService
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(
      this.paneService.showLayers$.subscribe(res => this.open = res)
    );
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach(s => s.unsubscribe());
  }

  togglePane(): void {
    this.paneService.showLayers = !this.open;
  }

}
