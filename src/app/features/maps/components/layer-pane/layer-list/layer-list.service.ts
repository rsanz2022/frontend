import { Apollo, gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { map, switchMap, take } from 'rxjs/operators';
import { environment } from '@env/environment';
import * as mapboxgl from 'mapbox-gl';
import { TimepickerService } from '@app/shared/timepicker/timepicker.service';
import { LayerMaxTime, Project } from '@app/shared/models';
import { LayersStoreService } from '@app/features/maps/resources/stores/layers-store.service';
import { forkJoin, from, Observable, of, ReplaySubject } from 'rxjs';
import { FeatureCollection } from 'geojson';
import { HttpClient } from '@angular/common/http';
import { CustomFile } from './layer-select/layer-select.models';
import Auth from '@aws-amplify/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { LayerPrepService } from '@app/features/maps/resources/services/layer-prep.service';

interface Response {
  getLayers?: string[];
}

@Injectable({ providedIn: 'root' })
export class LayerListService {
  layersMaxTime = 0;
  readonly layersMaxTime$ = new ReplaySubject<number>(1);

  private buildings = {
    filter: [
      'all',
      ['!=', 'type', 'building:part'],
      ['==', 'underground', 'false'],
    ],
    id: '3d_buildings',
    metadata: {
      name: '3D Buildings',
      styleId: '3d-buildings',
    },
    source: 'composite',
    'source-layer': 'building',
    type: 'fill-extrusion',
  };

  constructor(
    private apollo: Apollo,
    private timePickerService: TimepickerService,
    private layersStore: LayersStoreService,
    private layerPrepService: LayerPrepService,
    private http: HttpClient,
    private router: Router,
    private aRoute: ActivatedRoute
  ) {}

  setLayersMaxTime = (v: number): void => {
    if (this.layersMaxTime === v) return;
    this.layersMaxTime = v;
    this.layersMaxTime$.next(v);
  };

  getLayers(project: Project, showAll = true): Observable<mapboxgl.Layer[]> {
    const projectName = project.symbolicName;
    const layerReq = this.apollo
      .query<Response>({
        fetchPolicy: 'cache-first',
        query: gql`
          query Query($projectName: String!) {
            getLayers(projectName: $projectName)
          }
        `,
        variables: { projectName },
      })
      .pipe(
        map((res) => [
          ...res.data.getLayers.map(this.prepLayer).sort(this.sortLayers),
          this.buildings,
        ])
      );
    return this.getQPLayers(project.id).pipe(
      switchMap((layerIds) => {
        let req: Observable<mapboxgl.Layer[]>;
        if (layerIds && !showAll) {
          req = layerReq.pipe(
            map((res) => res.filter((l) => layerIds.includes(l.id)))
          );
        } else {
          const lsObj = this.layersStore.getStorageLayers();
          if (!showAll && lsObj.p[projectName] && lsObj.p[projectName].length) {
            lsObj.p[projectName].forEach(
              (layer, i) => (layer.metadata.defaultOrder = i)
            );
            req = of(lsObj.p[projectName] as mapboxgl.Layer[]);
          } else {
            req = layerReq.pipe(
              map((res) =>
                res.filter(
                  (l) =>
                    (l.metadata.betaOnly ? !environment.production : true) &&
                    (showAll ? true : l.metadata.defaultOrder !== undefined)
                )
              )
            );
          }
        }

        return forkJoin([
          this.timePickerService.maxtimes$.pipe(take(1)),
          req,
        ]).pipe(
          map((res) => res[1].map((l) => this.addLayerMax(l, res[0]) || l))
        );
      })
    );
  }

  private getQPLayers = (projId: number): Observable<string[]> => {
    return this.aRoute.queryParams.pipe(
      map((params): string[] => {
        if (params.l && params.p && params.p == projId) {
          return params.l.split(',');
        } else return null;
      })
    );
  };

  prepLayer = (l: string): any => {
    const layer = JSON.parse(l);
    if (layer.metadata && layer.metadata.betaOnly) {
      layer.metadata.name = layer.metadata.name + ' **';
    }
    return layer;
  };

  sortLayers(a: mapboxgl.Layer, b: mapboxgl.Layer): number {
    const aName = a.metadata.name.toLowerCase(),
      bName = b.metadata.name.toLowerCase();
    if (aName < bName) return -1;
    if (aName > bName) return 1;
    return 0;
  }

  convertShapefile = (
    files: CustomFile[],
    nameField?: string
  ): Observable<FeatureCollection> => {
    const url = this.router
      .createUrlTree(['/api/shapefileToJson'], {
        queryParams: { nameField },
      })
      .toString();

    return from(Auth.currentSession()).pipe(
      switchMap((session) => {
        const token = session.getIdToken().getJwtToken();

        const body: FormData = new FormData();
        for (let index = 0; index < files.length; index++) {
          body.append(files[index].extension, files[index].data);
        }
        const Authorization = 'Bearer ' + token;
        return this.http.post<FeatureCollection>(url, body, {
          headers: { Authorization },
        });
      })
    );
  };

  /* returns updated layer if there is a newer max time */
  addLayerMax = (
    layer: mapboxgl.Layer,
    maxes: LayerMaxTime[]
  ): mapboxgl.Layer | void => {
    const meta = layer.metadata;
    if (meta.symbolicName && meta.layerType) {
      const maxItem = maxes.find(
        (m) =>
          m.layerName === meta.symbolicName && m.layerType === meta.layerType
      );
      const lMax =
        maxItem ||
        this.timePickerService.getMaxInfo(
          maxes.filter(
            (m) => m.layerType !== 'previeux' && m.layerType !== 'nws'
          )
        );
      if (!meta.maxInfo || meta.maxInfo.maxTime < lMax.maxTime) {
        meta.maxInfo = lMax;
        if (!lMax) console.error(layer.id, 'missing max time');
        return layer;
      }
    }
  };

  getUserLayer(geoJSON: FeatureCollection): mapboxgl.Layer {
    const polygon = {
      staticType: 'polygon',
      labels: {
        source: {
          type: 'geojson',
          data: this.layerPrepService.getLabelPositions(geoJSON),
        },
      },
    };
    const layerMap = {
      LineString: { staticType: 'polyline', styleId: 'line' },
      MultiLineString: { staticType: 'polyline', styleId: 'line' },
      Point: { type: 'symbol', styleId: 'point', staticType: 'point' },
      Polygon: polygon,
      MultiPolygon: polygon,
    };
    const type = geoJSON.features[0].geometry.type;
    return {
      type: layerMap[type].type || 'line',
      id: 'local-geojson',
      metadata: {
        styleId: layerMap[type].styleId,
        staticType: layerMap[type].staticType,
        labels: layerMap[type].labels,
      },
      source: {
        type: 'geojson',
        data: geoJSON,
      },
    } as mapboxgl.Layer;
  }
}
