import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-layer-icon',
  templateUrl: './layer-icon.component.html',
  styleUrls: ['./layer-icon.component.scss']
})
export class LayerIconComponent {
  @Input() layer: any;

  isVisible = (): boolean => {
    const vis = this.layer.layout ? this.layer.layout.visibility : true;
    return vis === undefined || vis === 'visible';
  }
}
