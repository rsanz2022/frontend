import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LayerSelectComponent } from './layer-select/layer-select.component';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import * as mapboxgl from 'mapbox-gl';
import * as moment from 'moment-timezone';
import { UserService } from '@app/core';
import { LayerListService } from './layer-list.service';
import { LayersStoreService } from '../../../resources/stores/layers-store.service';
import { Project, Time, LayerMaxTime } from '@app/shared/models';
import { MapsService } from '../../../maps.service';
import { LayerPrepService } from '../../../resources/services/layer-prep.service';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { TimesStoreService } from '../../../resources/stores/times-store.service';
import { ExtentsStoreService } from '../../../resources/stores/extents-store.service';
import { HttpClient } from '@angular/common/http';
import { debounceTime, take, first } from 'rxjs/operators';
import { OptionsService } from '../options-pane/options.service';
import { Globals } from '@app/globals';
import { TimeControlsService } from '@app/features/maps/resources/services/time-controls.service';
import { forkJoin } from 'rxjs';
import { TimepickerService } from '@app/shared/timepicker/timepicker.service';

@Component({
  selector: 'app-layer-list',
  templateUrl: './layer-list.component.html',
  styleUrls: ['./layer-list.component.scss']
})
export class LayerListComponent implements OnInit, OnDestroy {
  rxSubs = [];
  layers: mapboxgl.Layer[] = [];
  duration: string;
  time: Time = this.timesStore.times;
  activeMediaQuery: string;
  project: Project;
  basemap = {
    list: [],
    selected: null
  };
  loading = {
    ids: [],
    layers: []
  };
  updatingLayers: string[] = [];

  constructor(
    public dialog: MatDialog,
    public layersStore: LayersStoreService,
    public optionsService: OptionsService,
    public timeControls: TimeControlsService,
    private listService: LayerListService,
    private mapsService: MapsService,
    private mediaObserver: MediaObserver,
    private userService: UserService,
    private layerPrep: LayerPrepService,
    private timesStore: TimesStoreService,
    private extentsStore: ExtentsStoreService,
    private http: HttpClient,
    private globals: Globals,
    private timePickerService: TimepickerService
  ) {}

  ngOnInit(): void {
    forkJoin([
      this.mapsService.map$.pipe(first(m => !!m)),
      this.timePickerService.maxtimes$.pipe(take(1))
    ]).subscribe(this.onMap);
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach(s => s.unsubscribe());
    setTimeout(
      () => this.layers.forEach(l => this.layersStore.removeLayer(l.id)));
  }

  private onMap = (): void => {
    this.rxSubs.push(
      this.layersStore.layers$.subscribe(this.onLayersNext),
      this.layersStore.layerMetadata$.subscribe(this.updateMetadata),
      this.timesStore.times$.subscribe(this.updateTime),
      this.mediaObserver.asObservable().subscribe((change: MediaChange[]) =>
        this.activeMediaQuery = change ? change[0].mqAlias : ''),
      this.userService.project$.subscribe(this.updateProject),
      this.mapsService.onSourceData$.pipe(debounceTime(250))
        .subscribe(this.sourcedataHandler),
      this.mapsService.onError$.subscribe(this.errorHandler),
      this.timePickerService.maxtimes$.subscribe(this.onMaxtimes)
    );

    this.mapsService.getBasemaps().then((res) => {
      this.basemap = {
        list: res,
        selected: this.mapsService.defaultBasemap
      };
    });
  }

  private updateTime = t => {
    this.time = t;
    this.duration = this.durationString();
    this.layers.forEach(l => this.updateLayer(l));
  }

  private onMaxtimes = (maxes): void => {
    const layers = [];
    const currentLayerMaxes = []
    this.layers.forEach(l => {
      const layer = this.listService.addLayerMax(l, maxes);
      if (layer) {
        this.setMaxDurationText(layer);
        layers.push(layer);
        currentLayerMaxes.push(layer.metadata.maxInfo);
      }
    });
    if (layers.length) {
      const hasNewMax = this.newMaxCheck(currentLayerMaxes);
      if (this.time.realtime === true) {
        if (!hasNewMax) layers.forEach(l => this.updateLayer(l));
      } else {
        layers.filter(l => l.metadata.maxInfo.maxTime <=
          this.listService.layersMaxTime).forEach(l => this.updateLayer(l));
      }
    }
  }

  public onDrop(event: CdkDragDrop<string[]>): void {
    this.layersStore.moveLayer(event.previousIndex, event.currentIndex);
  }

  public onDeleteLayer(layer: mapboxgl.Layer): void {
    this.layersStore.removeLayer(layer.id);
    const maxes = this.layers
      .filter(l => layer.id !== l.id && l.metadata.maxInfo)
      .map(l => l.metadata.maxInfo);
    if (maxes.length) this.newMaxCheck(maxes, true);
  }

  public selectBasemap(selected: { name: string; url: string }): void {
    this.mapsService.setBasemap(this.project.id, selected);
    this.basemap.selected = selected;
  }

  public durationString(): string {
    const start = this.time.start.getTime();
    const end = this.time.end.getTime();
    const minutes = (end - start) / 60 / 1000;
    return this.globals.PERIODS.includes(minutes) ?
      moment.duration(minutes, 'minutes').humanize({ m: 60, h: 24, d: 31 }) :
      'Custom';
  }

  public toggleVisibility = (layer): void => {
    const vis = layer.layout.visibility;
    layer.layout.visibility =
      vis === undefined || vis === 'visible' ? 'none' : 'visible';
    this.layersStore.editLayout(layer);
  }

  public isVisibleLayer(layer): boolean {
    const vis = layer.layout ? layer.layout.visibility : true;
    return vis === undefined || vis === 'visible';
  }

  public addLayerClick(): void {
    const widths = {
      xs: '340px',
      sm: '420px',
      md: '500px',
      lg: '580px',
      xl: '660px'
    };
    const dialogRef = this.dialog.open(LayerSelectComponent, {
      minWidth: widths[this.activeMediaQuery],
      autoFocus: false,
      data: this.project
    });

    dialogRef.beforeClosed().subscribe(newLayers => {
      if (newLayers) {
        const maxes = [...this.layers, ...newLayers]
          .filter(l => l.metadata.maxInfo)
          .map(l => l.metadata.maxInfo);
        this.newMaxCheck(maxes, !this.layers.length);
        newLayers.reverse().forEach(layer => {
          const layerIndex = this.layers.findIndex(l => l.id === layer.id);
          if (layerIndex >= 0) {
            this.layersStore.removeLayer(layer.id);
          }
          setTimeout(() => this.addLayer(layer));
        });
      }
    });
  }

  private newMaxCheck = (
    maxes: LayerMaxTime[],
    allowBackwards = false
  ): boolean => {
    const max = this.timePickerService.getMaxInfo(maxes).maxTime;
    const newMax = this.listService.layersMaxTime < max;
    if (allowBackwards || newMax) this.listService.setLayersMaxTime(max);
    return newMax;
  }

  private updateMetadata = (layer): void => {
    const i = this.layers.findIndex(l => l.id === layer.id);
    if (i >= 0) this.layers[i] = layer;
    this.layers = [...this.layers];
  }

  private mapboxSourceCheck = (e): boolean => (
    e.source && e.source['url'] && e.source['url'].includes('mapbox')
  );

  private errorHandler = (e): void => {
    if (!e) { return; }
    const layer = this.layers.find(layer => layer.id === e.sourceId);
    if (layer && !this.mapboxSourceCheck(e)) {
      this.setUpdating(layer, false);
      if (!layer['errors']) {
        layer['errors'] = [e.error];
      } else {
        layer['errors'].unshift(e.error);
      }
      console.warn(layer.id, layer['errors']);
    }
  }

  private sourcedataHandler = (e): void => {
    if (!e || e.sourceId === 'composite') return;
    const layer = this.layers.find(layer => layer.id === e.sourceId);
    if (!!layer) {
      const updating = this.updateCheck(e.sourceId);
      layer['sourceLoading'] = updating;
      this.setUpdating(layer, updating);
      if (updating) {
        setTimeout(() => this.sourcedataHandler(e), 500);
      } else if (layer['errors'] && layer['errors'][0].name) {
        delete layer['errors'];
      }
    }
  }

  private updateCheck(id: string): boolean {
    return !!this.mapsService.mapgl ? this.mapsService.mapgl.getSource(id) &&
        !this.mapsService.mapgl.isSourceLoaded(id)
      : true;
  }

  private onLayersNext = (layers): void => {
    const extents = layers
      .filter(layer => layer.metadata.time && layer.metadata.time.extents)
      .flatMap(layer => layer.metadata.time.extents);

    // Update extents and set default when no extents exist
    this.extentsStore.updateExtents(extents.length ? extents : [[null, null]]);
    this.layers = layers;
  }

  private addLayer = (layer, initialLoad?: boolean): void => {
    this.setLoading(layer);
    this.setMaxDurationText(layer);
    this.layerPrep
      .getLayer(layer, this.getLayerTime(layer))
      .then(l => {
        l.project = this.project.symbolicName;
        this.getFeatureStateValues(l).then(values => {
          if (values) {
            l.metadata.data.featureState.values = values;
            l.metadata.data.featureState.hasUpdate = true;
          }
          this.layersStore.addLayer(l, initialLoad);
          this.setLoading(l, false);
        });
      })
      .catch(l => {
        const message = l.statusText ?
          l.statusText :
          l.metadata ?
            'Console Error: ' + l.metadata.name :
            'Console Error';
        console.error(l);
        l.project = this.project.symbolicName;
        l.errors = [{ message }];
        this.layersStore.addLayer(l);
        this.setLoading(l, false);
      });
  }

  private setMaxDurationText = (layer): void => {
    const maxInfo = layer.metadata.maxInfo;
    if (!maxInfo) return;
    const layerTime = this.getLayerTime(layer);
    const duration = layerTime.end.getTime() - layerTime.start.getTime();
    maxInfo.durationText = moment.duration(duration, "milliseconds")
      .humanize({ m: 60, h: 24, d: 31 });
  }

  private getFeatureStateValues(layer): Promise<any> {
    if (this.mapsService.hasFeatureState(layer)) {
      const meta = layer.metadata;
      const url = meta.data.featureState.url;
      const params = this.layerPrep
        .getParams(url, this.getLayerTime(layer), meta.symbolicName);
      const endpoint = this.layerPrep.getEndpointWithParams(url, params);
      return this.http.get(endpoint).toPromise();
    } else {
      return Promise.resolve();
    }
  }

  private getLayerTime(layer: mapboxgl.Layer, time = this.time): Time {
    const maxInfo = layer.metadata.maxInfo;
    if (maxInfo) {
      const end = this.time.end.getTime() > maxInfo.maxTime ||
        time.realtime ? new Date(maxInfo.maxTime) : time.end;
      const start = moment(this.time.start).isBefore(end) ? this.time.start :
        moment(end).subtract(5, 'minutes').toDate();
      return { ...time, end, start };
    }
    return time;
  }

  private setUpdating(layer, isUpdating = true): void {
    layer['updating'] = isUpdating;
    const i = this.updatingLayers.indexOf(layer.id);
    if (isUpdating && i < 0) {
      this.updatingLayers.push(layer.id);
    } else if (i >= 0) {
      this.updatingLayers.splice(i, 1);
    }
    this.layersStore.loading = isUpdating || !!this.updatingLayers.length || !!this.loading.ids.length;
  }

  private setLoading(layer, isLoading = true): void {
    const i = this.loading.ids.indexOf(layer.id);
    if (isLoading) {
      this.loading.ids.push(layer.id);
      this.loading.layers.push(layer);
    } else if (!(i < 0)) {
      this.loading.ids.splice(i, 1);
      this.loading.layers.splice(i, 1);
    }
    this.layersStore.loading = isLoading || !!this.updatingLayers.length || !!this.loading.ids.length;
  }

  private updateProject = (proj): void => {
    if (!!this.project) {
      for (let index = 0; index < this.layersStore.layers.length; index++) {
        const layer = this.layersStore.layers[index];
        this.layersStore.removeLayer(layer.id);
      }
    }
    this.project = proj;
    this.listService.getLayers(proj, false)
      .pipe(take(1))
      .subscribe(res => {
        const maxes = res.filter(l => l.metadata.maxInfo)
          .map(l => l.metadata.maxInfo);
        this.newMaxCheck(maxes, !this.layers.length);
        res.sort((a,b) => a.metadata.defaultOrder - b.metadata.defaultOrder)
          .forEach(l => this.addLayer(l, true));
      });
  }

  private updateLayer = (layer: mapboxgl.Layer): void => {
    const initialized = (this.time && this.project && this.layers.length);
    if (initialized) {
      this.setUpdating(layer);
      delete layer['errors'];
      this.setMaxDurationText(layer);
      const layerTime = this.getLayerTime(layer);
      if (layer.source['type'] === 'vector') {
        this.layerPrep.getLayer(layer, layerTime).then(l => {
          this.getFeatureStateValues(l).then(values => {
            if (values) {
              l.metadata.data.featureState.values = values;
              l.metadata.data.featureState.hasUpdate = true;
              this.layersStore.editSource(l);
            }
            this.setUpdating(layer, false);
          });
        });
      } else {
        this.layerPrep.getLayer(layer, layerTime).then(
          (l) => {
            this.layersStore.editSource(l);
            this.setUpdating(layer, false);
          }, l => {
            this.setUpdating(layer, false);
            layer['errors'] = [{message: 'Console Error: ' + l.metadata.name}];
        });
      }
    }
  }
}
