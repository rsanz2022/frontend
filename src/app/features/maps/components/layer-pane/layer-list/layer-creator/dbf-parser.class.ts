export class DBFParser {
  /**
   * Parses through the .dbf file byte by byte
   * @param {arraybuffer} arrayBuffer the ArrayBuffer
   * @return {string[]} f An array of field names in the .dbf file.
   */
  static parse(arrayBuffer: ArrayBuffer): any {
    const f: string[] = [];
    const dv = new DataView(arrayBuffer);
    let idx = 32;
    while (true) {
      const nameArray = [];
      // Field names for shapefiles and dBASE tables can be
      // no more than 10 characters in length.
      for (let i = 0; i < 10; i++) {
        const letter = dv.getUint8(idx);
        if (letter != 0) nameArray.push(String.fromCharCode(letter));
        idx += 1;
      }
      idx += 22;
      f.push(nameArray.join(''));
      // Checks for end of field descriptor array.
      // Valid .dbf files will have this flag.
      if (dv.getUint8(idx) == 0x0d) break;
    }

    return f;
  }
}
