import {
  Component,
  Output,
  ViewChild,
  EventEmitter,
  Input,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatExpansionPanel } from '@angular/material/expansion';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FeatureCollection } from 'geojson';
import { Layer } from 'mapbox-gl';
import { take } from 'rxjs/operators';
import { LayerListService } from '../layer-list.service';
import {
  CustomFile,
  HTMLInputEvent,
} from '../layer-select/layer-select.models';
import { DBFParser } from './dbf-parser.class';

@Component({
  selector: 'app-layer-creator',
  templateUrl: './layer-creator.component.html',
  styleUrls: ['./layer-creator.component.scss'],
})
export class LayerCreatorComponent {
  newCustomLayer = false;
  @Input() customLayer: Layer;
  @Output() customLayerChange = new EventEmitter<Layer>();
  customLoading = false;
  converting = false;
  files: CustomFile[] = [];
  layerForm = this._fb.group({ labels: [null, [Validators.required]] });
  hasDBF = false;
  dbfFields: string[];
  totalFileSize = '0 Bytes';
  @ViewChild('filePanel') filePanel: MatExpansionPanel;

  constructor(
    private _fb: FormBuilder,
    private layerService: LayerListService,
    private _snack: MatSnackBar
  ) {}

  chooseFile = (): void => {
    this.customLoading = true;
    document.getElementById('user-upload').click();
  };

  fileChange = ({ target }: HTMLInputEvent): void => {
    if (!target.files.length) {
      return this.filePanel.close();
    }
    let title: string;
    let geoJSONFile: File;
    let uploadSize = 0;
    this.dbfFields = undefined;

    for (let i = 0; i < target.files.length; i++) {
      const file = target.files.item(i);
      let extension = file.name.split('.').reverse()[0].toLowerCase();
      if (extension === 'geojson' || extension === 'json') {
        extension = 'json';
        geoJSONFile = file;
      } else if (extension === 'dbf') {
        file.arrayBuffer().then((ab) => (this.dbfFields = DBFParser.parse(ab)));
      }

      uploadSize += file.size;

      if (this.files.findIndex((f) => extension === f.extension) >= 0) {
        return this.displayError(`Please choose one .${extension} at a time.`);
      }

      this.files.push({
        extension,
        displaySize: this.humanFileSize(file.size),
        data: file,
      });

      title = file.name;
    }

    this.totalFileSize = this.humanFileSize(uploadSize);
    if (uploadSize >= 10485760) {
      return this.displayError('Total file size exceeds 10MB');
    }

    this.hasDBF = this.files.findIndex((f) => 'dbf' === f.extension) >= 0;

    if (geoJSONFile) {
      const reader = new FileReader();
      reader.onload = (pEvent: ProgressEvent<FileReader>): void => {
        const jsonObj = JSON.parse(
          pEvent.target.result as string
        ) as FeatureCollection;
        this.setLayer(jsonObj, title);
      };
      reader.readAsText(geoJSONFile);
      this.customLoading = false;
    } else if (!this.hasDBF) {
      this.convertShapeFile();
    }
  };

  convertShapeFile = (hasDBF = false): void => {
    this.layerForm.disable();
    this.converting = true;
    let labels;
    if (hasDBF) {
      labels = this.layerForm.get('labels').value;
    }
    this.layerService
      .convertShapefile(this.files, labels)
      .pipe(take(1))
      .subscribe({
        next: (res: FeatureCollection) =>
          this.setLayer(res, this.files[0].data.name),
        error: (error) => this.displayError(error.error),
        complete: () => {
          this.customLoading = false;
          this.converting = false;
        },
      });
  };

  displayError = (msg: string): void => {
    this._snack.open(msg, null, {
      panelClass: 'error',
      duration: 6000,
      horizontalPosition: 'right',
    });
    this.filePanel.close();
    this.converting = false;
    this.newCustomLayer = false;
  };

  resetFiles = (): void => {
    this.files = [];
    this.hasDBF = false;
    this.customLoading = false;
    this.layerForm.enable();
    this.totalFileSize = '0 Bytes';
  };

  setLayer = (geoJSON: FeatureCollection, title: string): void => {
    const layer = this.layerService.getUserLayer(geoJSON);
    const regex = /\.(geojson|json|shp|prj|shx|dbf)$/;
    layer.metadata.name = title.replace(regex, '');
    this.customLayer = layer;
    this.customLayerChange.emit(this.customLayer);
    this.newCustomLayer = true;
  };

  private humanFileSize(bytes?: number): string {
    const thresh = 1024;
    if (!bytes) {
      return '0 Bytes';
    } else if (Math.abs(bytes) < thresh) {
      return bytes + ' B';
    }
    const units = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let u = -1;
    do {
      bytes /= thresh;
      ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);

    return bytes.toFixed(1) + ' ' + units[u];
  }
}
