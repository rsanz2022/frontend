import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayerCreatorComponent } from './layer-creator.component';

describe('LayerCreatorComponent', () => {
  let component: LayerCreatorComponent;
  let fixture: ComponentFixture<LayerCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayerCreatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayerCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
