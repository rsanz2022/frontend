import { Component, OnInit, Inject } from '@angular/core';
import { LayerListService } from '../layer-list.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LayersStoreService } from '@app/features/maps/resources/stores/layers-store.service';
import { take } from 'rxjs/operators';
import { Layer } from 'mapbox-gl';
import { MatSelectionList } from '@angular/material/list';
import { Project } from '@app/shared/models';

@Component({
  selector: 'app-layer-select',
  templateUrl: './layer-select.component.html',
  styleUrls: ['./layer-select.component.scss'],
})
export class LayerSelectComponent implements OnInit {
  loading = true;
  shownLayers: Layer[] = [];
  dialogWidths = {
    xs: '340px',
    sm: '420px',
    md: '500px',
    lg: '580px',
    xl: '660px',
  };
  newCustomLayer = false;
  customLayer: Layer;

  constructor(
    private layerService: LayerListService,
    private layersStore: LayersStoreService,
    @Inject(MAT_DIALOG_DATA) private project: Project,
    public dialogRef: MatDialogRef<LayerSelectComponent>
  ) {}

  ngOnInit(): void {
    this.layerService
      .getLayers(this.project)
      .pipe(take(1))
      .subscribe((res: Layer[]) => {
        const layers = [...this.layersStore.layers];
        this.loading = false;
        if (layers.length) {
          this.shownLayers = res.filter(
            (l) => !layers.some((a) => a.id === l.id)
          );
          const customIndex = layers.findIndex(l => l.id === 'local-geojson');
          if (customIndex >= 0) {
            this.customLayer = layers[customIndex];
            layers.splice(customIndex, 1);
          }
        } else {
          this.shownLayers = res;
        }
      });
  }

  onAddLayers(selection: MatSelectionList): void {
    const layerValues = selection.selectedOptions.selected.map(
      (item) => item.value
    );
    if (this.customLayer) layerValues.unshift(this.customLayer);
    this.dialogRef.close(layerValues);
  }

  onCustomLayerChange = (layer: Layer): void => {
    this.customLayer = layer;
    this.newCustomLayer = true;
  }
}
