export interface CustomFile {
  extension: string;
  displaySize: string;
  data: File
}

export interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}