import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionsPaneComponent } from './options-pane.component';

describe('OptionsPaneComponent', () => {
  let component: OptionsPaneComponent;
  let fixture: ComponentFixture<OptionsPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionsPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionsPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
