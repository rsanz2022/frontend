import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class OptionsService {

  private readonly _selectedLayer = new BehaviorSubject<mapboxgl.Layer>(null);
    public selectedLayer$ = this._selectedLayer.asObservable();
    get selectedLayer(): mapboxgl.Layer {
      return this._selectedLayer.getValue();
    }
    set selectedLayer(l: mapboxgl.Layer) {
      this._selectedLayer.next(l);
    }

  openOptions(layer: mapboxgl.Layer): void {
    this._selectedLayer.next(layer);
  }

  closeOptions(): void {
    this.selectedLayer = null;
  }

  getNewExpression(exp, option, stateVals): mapboxgl.Expression {
    const min = option.range ? option.range[0] : 0.5;
    let max: number = option.value;
    if (option.autoMax && option.autoMax.checked) {
      const maxState = Math.max(...stateVals.map((o) => o.value));
      max = (maxState < min) ? min : maxState.toFixed(2);
    }
    const change = max / exp[exp.length - 2];
    return exp.map(
      (item, index) => index > 2 && index % 2 !== 0 ?
        Math.round((item * change) * 1e5) / 1e5 :
        item
    );
  }
}
