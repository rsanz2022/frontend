import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { OptionsService } from './options.service';
import { MapsService } from '@app/features/maps/maps.service';
import { LayersStoreService } from '../../../resources/stores/layers-store.service';
import { MediaObserver } from '@angular/flex-layout';
import { LayerPrepService } from '@app/features/maps/resources/services/layer-prep.service';
import { TimesStoreService } from '@app/features/maps/resources/stores/times-store.service';

export interface ChangeEvent {
  value: string;
}

export interface Option {
  id: string;
  label: string;
  type: 'slider' | 'input';
  value?: number | string;
  autoMax?: { show: boolean; checked: boolean };
  range?: number[];
  target?: string[];
  queryParam?: string;
  expression?: boolean;
  description?: string;
}

@Component({
  selector: 'app-options-pane',
  templateUrl: './options-pane.component.html',
  styleUrls: ['./options-pane.component.scss'],
})
export class OptionsPaneComponent implements OnInit, OnDestroy {
  private rxSubs: Subscription[] = [];
  private map: mapboxgl.Map;
  public options: Option[];
  public layer: mapboxgl.Layer;
  smallScreen = true;

  constructor(
    private mediaObserver: MediaObserver,
    public optionsService: OptionsService,
    private mapsService: MapsService,
    private layersStore: LayersStoreService,
    private layerPrep: LayerPrepService,
    private timeStore: TimesStoreService
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(
      this.mapsService.map$.subscribe(this.onMap),
      this.mediaObserver
        .asObservable()
        .subscribe(
          () => (this.smallScreen = this.mediaObserver.isActive('lt-md'))
        )
    );
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
    this.optionsService.closeOptions();
  }

  onMap = (m): void => {
    if (!m) return;
    this.map = m;
    this.layer = this.optionsService.selectedLayer;
    this.options = this.layer.metadata.options.map((op) => {
      let value: any = op.value || this.getTargetValue(op.target);
      if (op.expression && !op.value) {
        value = value[value.length - 2];
      }
      if (op.binaries) {
        value = !!op.binaries.findIndex((b) => b === value);
      }
      op.value = value;
      return op;
    });
  };

  onChange = (option: Option, event = { value: option.value }): void => {
    const value = this.getNewValue(option, event);
    if (option.target) {
      let i;
      let current: any = this.layer;
      for (i = 0; i < option.target.length - 1; i++) {
        current = current[option.target[i]];
      }
      current[option.target[i]] = value;
      this.layersStore.editLayer(this.layer, option.target[0]);
    } else if (option.queryParam) {
      option.value = event.value;
      this.layerPrep
        .getLayer(this.layer, this.timeStore.times)
        .then(this.layersStore.editSource);
    }
  };

  getTargetValue = (target): any => {
    let current: any = {
      ...this.layersStore.getLayer(this.layer.id),
      metadata: this.layer.metadata,
    };
    for (let i = 0; i < target.length; i++) {
      if (current[target[i]] === undefined) {
        const fName =
          'get' +
          target[0].charAt(0).toUpperCase() +
          target[0].slice(1) +
          'Property';
        current = this.map[fName](this.layer.id, target[1]);
        break;
      } else {
        current = current[target[i]];
      }
    }
    return current;
  };

  getNewValue = (
    option: Option,
    event = { value: option.value }
  ): number | string | mapboxgl.Expression => {
    let newValue: any = event.value;
    if (option.range) {
      if (newValue < option.range[0]) {
        newValue = option.range[0];
      } else if (newValue > option.range[1]) {
        newValue = option.range[1];
      }
    }

    if (option.expression) {
      const exp = this.getTargetValue(option.target) as mapboxgl.Expression;
      const stateVals = this.layer.metadata.data.featureState.values;
      newValue = this.optionsService.getNewExpression(exp, option, stateVals);
      option.value = newValue[newValue.length - 2];
    }
    return newValue;
  };
}
