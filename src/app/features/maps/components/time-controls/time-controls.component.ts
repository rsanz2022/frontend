import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PickerPanelComponent } from './picker-panel/picker-panel.component';
import { Project, Time } from '@app/shared/models';
import { UserService } from '@app/core';
import { Globals } from '@app/globals';
import { Subscription } from 'rxjs';
import * as moment from 'moment-timezone';
import { TimesStoreService } from '../../resources/stores/times-store.service';
import { TimeControlsService } from '../../resources/services/time-controls.service';
import { LayerListService } from '../layer-pane/layer-list/layer-list.service';
import { take } from 'rxjs/operators';
import { AppConfigStoreService } from '@app/shared/app-config-store.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-time-controls',
  templateUrl: './time-controls.component.html',
  styleUrls: ['./time-controls.component.scss'],
})
export class TimeControlsComponent implements OnInit, OnDestroy {
  @ViewChild('timepicker', { static: true }) timepicker: ElementRef;
  private rxSubs = new Subscription();
  project: Project;
  times: Time;
  dateString: string;
  durationString: string;
  timeState = 'real-time';
  timeStateIcons = {
    'real-time': 'update',
    historical: 'undo',
    forecasted: 'redo',
  };

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    private timesStore: TimesStoreService,
    private configStore: AppConfigStoreService,
    private activeRoute: ActivatedRoute,
    public globals: Globals,
    public service: TimeControlsService,
    private listService: LayerListService
  ) {}

  ngOnInit(): void {
    this.rxSubs.add(this.userService.project$.subscribe(this.setProject));
    this.rxSubs.add(this.timesStore.times$.subscribe(this.setTimes));
  }

  ngOnDestroy(): void {
    this.rxSubs.unsubscribe();
    this.timesStore.stopRealtime();
  }

  private setTimes = (v: Time): void => {
    if (v) {
      this.times = v;
      this.timeState = v.realtime ? 'real-time' : 'historical';
    }
    this.durationString = this.getDurationString();
    this.dateString = this.getDateString();
  };

  public openPickerPanel(): void {
    const getRect = (side: string): number =>
      this.timepicker.nativeElement.getBoundingClientRect()[side];
    const center = (getRect('right') - getRect('left')) / 2 + getRect('left');
    const panelWidth = 320;
    const dialogRef = this.dialog.open(PickerPanelComponent, {
      data: { project: this.project, time: this.times },
      autoFocus: false,
      backdropClass: 'hide-backdrop',
      panelClass: 'drop-down',
      width: panelWidth + 'px',
      maxWidth: 'calc(100% - 16px)',
      position: {
        bottom: '60px',
        left: (screen.width < 337 ? 8 : center - panelWidth / 2) + 'px',
      },
    });

    dialogRef
      .beforeClosed()
      .subscribe((t) => (t ? this.timesStore.setTimes(t) : null));
  }

  private setProject = (proj: Project): void => {
    this.project = proj;
    this.timesStore.stopRealtime();
    const timeConfig = this.configStore.getConfig('timepicker', proj.id);

    this.activeRoute.queryParams.subscribe((params): void => {
      const storedTime =
        this.parseTimeParam(params, proj.id) || (timeConfig?.value as Time);

      if (storedTime && storedTime.realtime === false) {
        this.timesStore.setTimes(storedTime);
      } else if (storedTime?.realtime || proj.isNRT === true) {
        let duration: number;
        if (storedTime) {
          duration = moment(storedTime.end).diff(storedTime.start, 'minute');
        }
        this.timesStore.startRealtime(duration);
      } else {
        this.listService.layersMaxTime$
          .pipe(take(1))
          .subscribe((max) =>
            this.timesStore.setTimes(
              this.timesStore.getIntervalTime(new Date(max || Date.now()))
            )
          );
      }
    });
  };

  private parseTimeParam = (params: Params, id: number): Time => {
    if (params.t && params.p && params.p == id) {
      const paramTimeArray = params.t.split(',').map((t) => parseInt(t));
      if (
        moment(paramTimeArray[0]).isValid() &&
        moment(paramTimeArray[1]).isValid()
      ) {
        return {
          start: new Date(paramTimeArray[0]),
          end: new Date(paramTimeArray[1]),
          realtime: !!paramTimeArray[2],
        };
      }
    }
  };

  private getDateString(): string {
    if (!this.times || !this.times.end) {
      return 'Loading...';
    }
    if (this.times.realtime) {
      return moment(this.times.end)
        .tz(this.project.timezone)
        .format('YYYY-MM-DD HH:mm');
    }

    const mStart = moment(this.times.start).tz(this.project.timezone);
    const mEnd = moment(this.times.end).tz(this.project.timezone);
    const start = mStart.format('YYYY-MM-DD HH:mm');
    let end = ' - ';

    if (mEnd.isSame(mStart, 'day')) {
      end = end + mEnd.format('HH:mm');
    } else if (mEnd.isSame(mStart, 'month')) {
      end = end + mEnd.format('DD HH:mm');
    } else if (mEnd.isSame(mStart, 'year')) {
      end = end + mEnd.format('MM-DD HH:mm');
    } else {
      end = end + mEnd.format('YYYY-MM-DD HH:mm');
    }

    return `${start}${end}`;
  }

  private getDurationString(): string {
    if (!this.times || !this.times.end || !this.times.start) {
      return 'Loading...';
    }
    const start = this.times.start.getTime();
    const end = this.times.end.getTime();
    const minutes = (end - start) / 60 / 1000;
    return this.globals.PERIODS.includes(minutes)
      ? moment.duration(minutes, 'minutes').humanize({ m: 60, h: 24, d: 31 })
      : 'Custom';
  }
}
