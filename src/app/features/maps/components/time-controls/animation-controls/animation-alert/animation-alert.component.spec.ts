import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationAlertComponent } from './animation-alert.component';

describe('AnimationAlertComponent', () => {
  let component: AnimationAlertComponent;
  let fixture: ComponentFixture<AnimationAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimationAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimationAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
