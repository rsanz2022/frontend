import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-animation-alert',
  templateUrl: './animation-alert.component.html',
  styleUrls: ['./animation-alert.component.scss']
})
export class AnimationAlertComponent {

  constructor(public dialogRef: MatDialogRef<AnimationAlertComponent>) {}

}
