import { Component, OnInit, OnDestroy, Input, ElementRef, ViewChild } from '@angular/core';
import * as moment from 'moment-timezone';
import { Time, Project } from '@app/shared/models';
import { ExtentsStoreService } from '../../../resources/stores/extents-store.service';
import { Subscription } from 'rxjs';
import { TimesStoreService } from '../../../resources/stores/times-store.service';
import { LayersStoreService } from '../../../resources/stores/layers-store.service';
import { MatDialog } from '@angular/material/dialog';
import { AnimationFormComponent } from '../animation-form/animation-form.component';
import { TimeControlsService } from '../../../resources/services/time-controls.service';
import { AnimationAlertComponent } from './animation-alert/animation-alert.component';
import { LayerListService } from '../../layer-pane/layer-list/layer-list.service';

@Component({
  selector: 'app-animation-controls',
  templateUrl: './animation-controls.component.html',
  styleUrls: ['./animation-controls.component.scss']
})
export class AnimationControlsComponent implements OnInit, OnDestroy {
  rxSubs: Subscription[] = [];
  time: Time;
  max: number;
  extentStates: string[];
  loading = true;
  animationLoading = false;
  afterEnabled = false;
  intervalOverride: number;
  disablePlay = false;
  @Input() project: Project;
  @ViewChild('aniBtn', { static: true }) aniBtn: ElementRef;

  constructor(
    public timesStore: TimesStoreService,
    public service: TimeControlsService,
    private extentsStore: ExtentsStoreService,
    private layersStore: LayersStoreService,
    private dialog: MatDialog,
    private timeControls: TimeControlsService,
    private listService: LayerListService,
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(
      this.listService.layersMaxTime$.subscribe(m => {
        this.max = m;
        this.setAvailability(this.time);
      }),
      this.timesStore.times$.subscribe(this.updateTimes),
      this.extentsStore.states$.subscribe(v => (this.extentStates = v)),
      this.layersStore.loading$.subscribe(v => this.loading = v)
    );
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach(s => s.unsubscribe());
  }

  onAnimate = (): void => {
    if (!this.timeControls.isAnimating) {
      const requests = this.timeControls.getRequests();
      if (requests.length > 50) {
        const dialogRef = this.dialog.open(AnimationAlertComponent, {
          width: '364px'
        });
        dialogRef.beforeClosed().subscribe(accepted => {
          if (accepted) this.timeControls.preloadData(requests);
        });
      } else this.timeControls.preloadData(requests);
    } else this.timeControls.setIsAnimating(false);
  }

  updateTimes = (time: Time): void => {
    if (this.service.isAnimating) return;
    this.time = time;
    const duration = moment(time.end).diff(time.start, 'minute');
    this.disablePlay = duration <= this.project.interval;
    this.setAvailability(time);
  }

  setAvailability = (time: Time): void => {
    if (time) {
      this.afterEnabled = moment(time.end).isBefore(this.max);
      const diff = moment(this.max).diff(time.end, 'minute');
      const useDiff = diff > 0 && diff < this.project.interval;
      this.intervalOverride = useDiff ? diff : null;
    }
  }

  public showAniForm(): void {
    const getRect = (side: string): number => this.aniBtn.nativeElement
      .getBoundingClientRect()[side];
    const center = ((getRect('right') - getRect('left')) / 2) + getRect('left');
    const panelWidth = 180;
    const dialogRef = this.dialog.open(AnimationFormComponent, {
      data: {
        prjInterval: this.project.interval,
        time: this.time
      },
      autoFocus: false,
      backdropClass: 'hide-backdrop',
      panelClass: 'drop-down',
      width: panelWidth + 'px',
      maxWidth: 'calc(100% - 16px)',
      position: {
        bottom: '60px',
        left: (screen.width < 337 ? 8 : center - (panelWidth / 2)) + 'px'
      }
    });

    dialogRef.beforeClosed()
      .subscribe(t => t ? this.timesStore.setTimes(t) : null);
  }
}
