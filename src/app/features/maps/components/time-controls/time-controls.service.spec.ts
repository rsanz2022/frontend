import { TestBed } from '@angular/core/testing';

import { TimeControlsService } from './time-controls.service';

describe('TimeControlsService', () => {
  let service: TimeControlsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimeControlsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
