import { Component, OnInit, Inject } from '@angular/core';
import { Globals } from '@app/globals';
import { Time } from '@app/shared/models';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment-timezone';
import { TimeControlsService } from '../../../resources/services/time-controls.service';

@Component({
  selector: 'app-animation-form',
  templateUrl: './animation-form.component.html',
  styleUrls: ['./animation-form.component.scss']
})
export class AnimationFormComponent implements OnInit {
  aggregationOptions: { name: string; value: number }[] = [];
  private interval: number;
  private time: Time;

  constructor(
    @Inject(MAT_DIALOG_DATA) data: { prjInterval: number; time: Time },
    private globals: Globals,
    public service: TimeControlsService
  ) {
    this.interval = data.prjInterval;
    this.time = data.time;
  }

  ngOnInit(): void {
    const start = this.time.start.getTime();
    const end = this.time.end.getTime();
    const duration = (end - start) / 1000 / 60;
    this.aggregationOptions = this.globals.PERIODS
      .filter((a: number) => a <= (duration / 2) && a >= this.interval)
      .map((a: number) => ({
        value: a,
        name: moment.duration(a, 'minutes').humanize({ m: 60, h: 24, d: 31 })
      }));
      
    const agg = this.service.aggregation;
    if (!agg.value) {
      agg.setValue(this.aggregationOptions[0].value);
    }
    if (agg.disabled && this.aggregationOptions.length > 1) {
      agg.enable();
    }
  }
}
