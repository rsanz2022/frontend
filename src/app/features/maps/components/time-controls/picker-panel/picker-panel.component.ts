import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TimepickerService } from '@app/shared/timepicker/timepicker.service';
import { LayerListService } from '../../layer-pane/layer-list/layer-list.service';
import { Project, Time } from '@app/shared/models';

/* dialog container for time picker */
@Component({
  selector: 'app-picker-panel',
  styles: [':host { display: block; padding: 8px; }'],
  template: `
    <app-timepicker
      [time]="data.time"
      [max]="listService.layersMaxTime$ | async"
      [enableRealtime]="!!data.project.isNRT"
      (timesChange)="dialogRef.close($event)">
    </app-timepicker>`
})
export class PickerPanelComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { project: Project; time: Time },
    public timePickerService: TimepickerService,
    public dialogRef: MatDialogRef<PickerPanelComponent>,
    public listService: LayerListService,
  ) {}
}
