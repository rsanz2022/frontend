import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickerPanelComponent } from './picker-panel.component';

describe('PickerPanelComponent', () => {
  let component: PickerPanelComponent;
  let fixture: ComponentFixture<PickerPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickerPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickerPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
