import { TestBed } from '@angular/core/testing';

import { ControlsComponentService } from './controls.service';

describe('ControlsComponentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ControlsComponentService = TestBed.get(ControlsComponentService);
    expect(service).toBeTruthy();
  });
});
