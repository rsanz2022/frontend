import { Component, EventEmitter, Output, OnInit} from '@angular/core';
import { PaneStatesService } from '../../resources/services/pane-states.service';
import { MapsService } from '../../maps.service';
import { LayersStoreService } from '../../resources/stores/layers-store.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss']
})
export class ControlsComponentComponent implements OnInit {
  rxSub: Subscription
  showSearch = false;
  layerIds = [];
  sources = [];
  searchMarker;
  @Output() onResetMapCam = new EventEmitter<void>();
  @Output() onCopyLink = new EventEmitter<MouseEvent>();

  constructor(
    public paneStates: PaneStatesService,
    public mapsService: MapsService,
    private layerStore: LayersStoreService
  ) {}

  ngOnInit(): void {
    this.rxSub = this.layerStore.layers$
      .subscribe(layers => {
        const list = layers.filter(l =>
          !l.layout || l.layout.visibility === 'visible');
        this.layerIds = list
          .filter(l => l.type !== 'line' && l.source['type'] !== 'geojson')
          .map(l => l.id);
        this.sources = list
          .filter(l => l.source['type'] === 'geojson')
          .map(l => ({
            layer: l,
            type: 'geojson',
            features: l.source['data'].features
          }));
      });
  }

  ngOnDestroy(): void {
    this.rxSub.unsubscribe();
  }
}
