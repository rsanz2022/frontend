import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlsComponentComponent } from './controls.component';

describe('ControlsComponentComponent', () => {
  let component: ControlsComponentComponent;
  let fixture: ComponentFixture<ControlsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
