import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { LayersStoreService } from '../../resources/stores/layers-store.service';
import { TimesStoreService } from '../../resources/stores/times-store.service';
import { Time } from '@app/shared/models';
import { MapsService } from '../../maps.service';
import { LayerPrepService } from '../../resources/services/layer-prep.service';

@Component({
  selector: 'app-popup-content',
  templateUrl: './popup-content.component.html',
  styleUrls: ['./popup-content.component.scss']
})
export class PopupContentComponent implements OnInit, OnDestroy {
  private _features: any[];
  layers: mapboxgl.Layer[] = [];
  selectedFeature: any;
  meta: any;
  subscriptions = [];
  showCharts: string[] = [];
  timestamp: number;
  times: Time;
  @Input()
    get features(): any[] { return this._features; }
    set features(features) {
      this._features = this.prepFeatures(features);
      this.featuresUpdate();
    }

  constructor(
    private layersStore: LayersStoreService,
    private mapsService: MapsService,
    private timesStore: TimesStoreService,
    private prepService: LayerPrepService
  ) { }

  ngOnInit(): void {
    this.times = this.timesStore.times;
    this.subscriptions.push(
      this.timesStore.times$.subscribe(t => {
        this.times = t;
        this.timestamp = Date.now();
      })
    );
  }

  ngOnDestroy(): void {
    this.layers = [];
    this.selectedFeature = undefined;
    this.meta = undefined;
    this.showCharts = [];
    this.subscriptions.forEach(s => s.unsubscribe());
    this.removeShapeLayer();
  }

  prepFeatures(features: any[]): any[] {
    return features.map(feature => {
      const props = feature.properties;
      if (props) {
        props.displayId = props.id;
        props.btnName = props.name || props.id;
        if (feature.layer.metadata.layerType === 'velocity') {
          const ids = props.id.split('_');
          props.displayId = ids[0];
          props.feed = ids[1];
          props.btnName = feature.layer.metadata.name;
        }
      }
      return feature;
    });
  }

  featuresUpdate(): void {
    this.selectedFeature = undefined;
    this.meta = undefined;
    this.removeShapeLayer();
    this.features.forEach(feat => {
      const layer = this.layersStore.getLayer(feat.layer.id);
      feat.layer.metadata = layer.metadata;
    });
    if (this.features.length === 1) {
      this.selectFeature(this.features[0]);
    }
  }

  selectFeature(feature): void {
    this.selectedFeature = feature;
    this.meta = feature.layer.metadata;
    const charts = this.meta.popup.charts.map((chart: string) => {
      const attrStart = chart.indexOf('[');
      return {
        type: chart.substr(0, attrStart < 0 ? undefined : attrStart),
        attributes: chart.substring(
          attrStart + 1, chart.lastIndexOf(']')
          ).split(',')
      };
    });
    if (feature.properties) {
      this.addShapeLayer(feature.properties.id, this.meta.popup.outlineUrl);
    }
    this.showCharts = charts;
  }

  addShapeLayer = (id, url): void => {
    if (url) {
      this.mapsService.mapgl.addLayer({
        'id': 'mosaic-feature',
        'type': 'line',
        'source': {
          'type': 'geojson',
          'data': this.prepService.getEndpointWithParams(url, {id})
        },
        'layout': { 'line-cap': 'round', 'line-join': 'round' },
        'paint': {
          'line-width': [
            "interpolate", ["exponential", 1.5], ["zoom"], 5, 0.75, 18, 32
          ]
        }
      });
    }
  }

  removeShapeLayer = (): void => {
    const layer = this.mapsService.mapgl.getLayer('mosaic-feature');
    if (layer) {
      this.mapsService.mapgl.removeLayer('mosaic-feature');
      this.mapsService.mapgl.removeSource('mosaic-feature');
    }
  }
}
