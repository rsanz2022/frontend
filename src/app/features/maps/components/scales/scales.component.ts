import { Component, Input } from '@angular/core';
import { LayerPrepService } from '../../resources/services/layer-prep.service';

@Component({
  selector: 'app-scales',
  templateUrl: './scales.component.html',
  styleUrls: ['./scales.component.scss']
})
export class ScalesComponent {
  private _scales: any[];
  @Input()
    get scales(): any[] { return this._scales; }
    set scales(scales) {
      if (scales) {
        scales.forEach((scale) => {
          const params = this.layerPrep.getParams(scale.url, null, scale.tableId);
          if (scale.layer.metadata.options) {
            scale.layer.metadata.options.forEach(
              option => {
                if (option.components && option.components.includes('ScalesComponent')) {
                  params[option.id] = option.value || 6;
                }
              }
            );
          }
          scale.url = this.layerPrep.getEndpointWithParams(scale.url, params);
        });
        this._scales = scales.filter((scale) => scale.visible);
      }
    }

  constructor(private layerPrep: LayerPrepService) {}
}
