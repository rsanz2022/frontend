import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input,
} from '@angular/core';
import { TimesStoreService } from '../../resources/stores/times-store.service';
import { MapsService } from '../../maps.service';
import { ElementEvent } from '@app/shared/models/element.model';
import { ElementsService } from '@app/shared/elements.service';
import { UserService } from '@app/core';
import { PaneStatesService } from '../../resources/services/pane-states.service';
import { Project, Time } from '@app/shared/models';
import { TimeControlsService } from '../../resources/services/time-controls.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-elements',
  templateUrl: './elements.component.html',
  styleUrls: ['./elements.component.scss'],
})
export class ElementsComponent implements OnInit, OnDestroy {
  rxSubs: Subscription[] = [];
  load = false;
  _allElements: any[];
  elements = [];
  times: Time;
  proj: Project;
  mapPadding: mapboxgl.PaddingOptions = {
    top: 45,
    bottom: 45,
    left: 80 + (this.paneService.showLayers ? 280 : 0),
    right: 360,
  };
  @Input() layers: any[];
  @Output() element = new EventEmitter<ElementEvent>();

  constructor(
    private elService: ElementsService,
    private userService: UserService,
    private timeControls: TimeControlsService,
    public timesStore: TimesStoreService,
    public mapsService: MapsService,
    public paneService: PaneStatesService
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(
      this.userService.project$.subscribe(this.onProjChange),
      this.timeControls.isAnimating$.subscribe((v) => {
        if (v && this.paneService.showElements) {
          this.paneService.showElements = false;
        }
      }),
      this.paneService.showLayers$.subscribe(
        (shown) => (this.mapPadding.left = 80 + (shown ? 280 : 0))
      )
    );
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
  }

  onProjChange = (proj: Project): void => {
    this.proj = proj;
    this.elService.getElements().then((res: any[]) => {
      this.elements = res.filter((el) => proj.products.includes(el.id));
    });
  };

  /*
   * Use below when there are more than just a few elements.
   * Remember to add ElementsDialogComponent to
   * MapsModule (imports & entryComponents).
   */

  // onShowElements = (showElements): void => {
  //   if (showElements && this.elements.length === 0) {
  //     this.openAddElementDialog();
  //   }
  // }

  // openAddElementDialog(): void {
  //   const dialogRef = this.dialog.open(ElementsDialogComponent, {
  //     width: '250px',
  //     data: { current: this.elements }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.elements = this.elements.concat(result);
  //     }
  //   });
  // }

  // remove(element): void {
  //   const i = this.elements.findIndex(e => e.id === element.id);
  //   this.elements.splice(i, 1);
  // }
}
