import { TestBed } from '@angular/core/testing';

import { LayersStoreService } from './layers-store.service';

describe('LayersStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LayersStoreService = TestBed.get(LayersStoreService);
    expect(service).toBeTruthy();
  });
});
