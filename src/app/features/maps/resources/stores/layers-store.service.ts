import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { Layer } from 'mapbox-gl';

interface StorageLayers {
  [key: string]: Layer[];
}

interface StorageObj {
  v: number;
  p: StorageLayers;
}

@Injectable({ providedIn: 'root' })
export class LayersStoreService {
  readonly localLayerVersion = 0.01;
  readonly layerRemoved$ = new Subject<string>();
  readonly layerAdded$ = new Subject<Layer>();
  readonly layerEdited$ = new Subject<Layer>();
  readonly layerLayout$ = new Subject<Layer>();
  readonly layerMetadata$ = new Subject<Layer>();
  readonly layerPaint$ = new Subject<Layer>();
  readonly toggleVisibility$ = new BehaviorSubject<string>(null);
  active = false;

  private readonly _layers = new BehaviorSubject<Layer[]>([]);
  readonly layers$ = this._layers.asObservable();
  get layers(): Layer[] {
    return this._layers.getValue();
  }
  set layers(val: Layer[]) {
    this._layers.next(val);
  }

  private readonly _loading = new BehaviorSubject<boolean>(true);
  readonly loading$ = this._loading.asObservable();
  get loading(): boolean {
    return this._loading.getValue();
  }
  set loading(v: boolean) {
    this._loading.next(v);
  }

  getLayer = (id: string): Layer => {
    if (!this.active) {
      return;
    }
    return this.layers.find((layer) => layer.id === id);
  };

  addLayer = (layer: Layer, initialLoad?: boolean): void => {
    if (!this.active) return;
    const layers = [layer].concat(this.layers).filter(this.uniqueLayerFilter);
    if (initialLoad) {
      layers.sort((a, b) => a.metadata.defaultOrder - b.metadata.defaultOrder);
    }
    this.layers = layers;
    this.layerAdded$.next(layer);
    this.setStorageLayers(layer['project']);
  };

  private uniqueLayerFilter = (
    layer: Layer,
    i: number,
    a: Layer[]
  ): boolean => {
    return i === a.findIndex((l) => l.id === layer.id);
  };

  removeLayer = (id: string): void => {
    if (!this.active) return;
    const index = this.layers.findIndex((l) => l.id === id);
    const project = (this.layers[index] as any).project;
    this.layers = this.layers.filter((l) => l.id !== id);
    this.layerRemoved$.next(id);
    this.setStorageLayers(project);
  };

  moveLayer = (previousIndex: number, currentIndex: number): void => {
    if (!this.active) {
      return;
    }
    moveItemInArray(this.layers, previousIndex, currentIndex);
    this._layers.next(this.layers);
    const project = (this.layers[0] as any).project;
    this.setStorageLayers(project);
  };

  editSource = (layer: Layer): void => {
    if (!this.active) return;
    const listLayer = this.getLayerObject(layer.id);
    if (listLayer) {
      listLayer.source = layer.source;
      this._layers.next(this.layers);
      this.layerEdited$.next(layer);
    }
  };

  editLayer = (layer: Layer, prop: string): void => {
    if (!this.active) {
      return;
    }
    const listLayer = this.getLayerObject(layer.id);
    if (listLayer) {
      listLayer[prop] = layer[prop];
      this._layers.next(this.layers);
      const funcName = `layer${prop.charAt(0).toUpperCase() + prop.slice(1)}$`;
      if (!!this[funcName]) {
        this[funcName].next(listLayer);
      }
    }
  };

  editLayout = (layer: Layer): void => {
    if (!this.active) {
      return;
    }
    const listLayer = this.getLayerObject(layer.id);
    if (listLayer) {
      listLayer.layout = layer.layout;
      this._layers.next(this.layers);
      this.layerLayout$.next(listLayer);
    }
  };

  editPaint = (layer: Layer): void => {
    if (!this.active) {
      return;
    }
    const listLayer = this.getLayerObject(layer.id);
    if (listLayer) {
      listLayer.layout = layer.layout;
      this._layers.next(this.layers);
      this.layerPaint$.next(listLayer);
    }
  };

  editMetadata = (layer: Layer): void => {
    if (!this.active) {
      return;
    }
    const listLayer = this.getLayerObject(layer.id);
    if (listLayer) {
      listLayer.metadata = layer.metadata;
      this._layers.next(this.layers);
      this.layerMetadata$.next(listLayer);
    }
  };

  private getLayerObject = (id: string): Layer => {
    if (!this.active) {
      return;
    }
    const i = this.layers.findIndex((l) => l.id === id);
    return i >= 0 ? this.layers[i] : null;
  };

  getStorageLayers = (): StorageObj => {
    const lsItem = localStorage.getItem('ml');
    const emptyStorage = { v: this.localLayerVersion, p: {} } as StorageObj;
    if (!lsItem) return emptyStorage;
    const lsObj = JSON.parse(decodeURIComponent(atob(lsItem)));
    return this.localLayerVersion === lsObj.v ? lsObj : emptyStorage;
  };

  setStorageLayers = (project: string): void => {
    const lsObj = this.getStorageLayers();
    lsObj.p[project] = this.layers
      .map((l) => {
        const storedLayer = JSON.parse(
          JSON.stringify({ ...l, source: { type: l.source['type'] } } as any)
        );

        if (storedLayer.metadata.data?.featureState) {
          delete storedLayer.metadata.data.featureState.values;
        }

        if (storedLayer.errors) {
          delete storedLayer.errors;
        }

        return storedLayer;
      })
      .filter((l) => l.id !== 'local-geojson');

    try {
      const storageString = encodeURIComponent(JSON.stringify(lsObj));
      localStorage.setItem('ml', btoa(storageString));
    } catch {
      console.error(
        "Your browser's local storage is full and cannot remember your settings."
      );
    }
  };
}
