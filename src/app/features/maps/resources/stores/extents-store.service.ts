import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment-timezone';

@Injectable({ providedIn: 'root' })
export class ExtentsStoreService {

  private readonly _extents = new BehaviorSubject<Date[]>(this.getDefaults());
    extents$ = this._extents.asObservable();
    get extents(): Date[] { return this._extents.getValue(); }
    set extents(extents: Date[]) {
      this.errorCheck(extents);
      this._extents.next(extents);
    }

  private readonly _states = new BehaviorSubject<string[]>([]);
    states$ = this._states.asObservable();
    get states(): string[] { return this._states.getValue(); }
    set states(states: string[]) {
      this._states.next(states);
    }
  
  get start(): Date { return this._extents.getValue()[0]; }
  set start(startDate: Date) {
    startDate = (startDate === null) ? this.getDefaults()[0] : startDate;
    const extents = [startDate, this.end];
    this.errorCheck(extents);
    this._extents.next(extents);
  }
  get end(): Date { return this._extents.getValue()[1]; }
  set end(endDate: Date) {
    endDate = (endDate === null) ? this.getDefaults()[1] : endDate;
    const extents = [this.start, endDate];
    this.errorCheck(extents);
    this._extents.next(extents);
  }

  updateStates(states: string[]): void {
    this.states = states;
  }

  updateExtents = (allExtents: Date[][]): void => {
    if (allExtents.length < 2) { return; }

    const flatExtents = allExtents.flatMap(ext => {
      // convert to moment
      const mExt = ext.map(
        (e, i) => e === null ? moment(this.getDefaults()[i]) : moment(e)
      );
      return mExt;
    });

    const extents = [
      moment.min(flatExtents).toDate(),
      moment.max(flatExtents).toDate()
    ];
    this.extents = extents;
  }

  private errorCheck = (extents): void => {
    if (
        extents.length !== 2 ||
        !this.isDate(extents[0]) ||
        !this.isDate(extents[1]) ||
        moment(extents[1]).isSameOrBefore(extents[0])
    ) {
      throw new Error(`Time extents are misconfigured. start:${extents[0]} end:${extents[1]} `);
    }
  }

  private isDate(date): boolean {
    return Object.prototype.toString.call(date) === '[object Date]';
  }

  private getDefaults(): Date[] {
    const dates = [
      new Date('2001-01-01T00:00:00Z'),
      new Date()
    ];
    dates.forEach(d => d.setSeconds(0, 0));
    return dates;
  }
}
