import { TestBed } from '@angular/core/testing';

import { TimesStoreService } from './times-store.service';

describe('TimesStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimesStoreService = TestBed.get(TimesStoreService);
    expect(service).toBeTruthy();
  });
});
