import { Injectable } from '@angular/core';
import { ReplaySubject, Subscription } from 'rxjs';
import { Time } from '@app/shared/models';
import { UserService } from '@app/core';
import * as moment from 'moment-timezone';
import { LayerListService } from '../../components/layer-pane/layer-list/layer-list.service';

@Injectable({ providedIn: 'root' })
export class TimesStoreService {
  readonly TimerDelay = 1000 * 60 * 2.5; // millis
  updateInterval: Subscription;
  private duration = 60; // minutes;
  private _times: Time;
  times$ = new ReplaySubject<Time>(1);
  get times(): Time {
    return this._times || this.getIntervalTime(new Date());
  }
  set times(v: Time) {
    this._times = v;
    this.duration = moment(v.end).diff(v.start, 'minutes');
    this.times$.next(v);
  }

  constructor(
    private userService: UserService,
    private listService: LayerListService
  ) {}

  stopRealtime = (): void => {
    if (this.updateInterval) this.updateInterval.unsubscribe();
  };

  startRealtime = (duration?: number): void => {
    this.stopRealtime();
    this.updateInterval = this.listService.layersMaxTime$.subscribe((max) => {
      this.times = this.getIntervalTime(new Date(max || Date.now()), duration);
    });
  };

  setTimes(times: Time): void {
    if (!times.start === undefined || !this.isDate(times.start)) {
      throw new TypeError(`'start' is not of type Date`);
    }
    if (!times.end === undefined || !this.isDate(times.end)) {
      throw new TypeError(`'end' is not of type Date`);
    }

    if (times.realtime !== this.times.realtime) {
      if (times.realtime) {
        this.startRealtime();
        return;
      } else this.stopRealtime();
    }

    this.times = times;
  }

  shift(advance = true, interval = this.userService.project.interval): void {
    const mFunc = advance ? 'add' : 'subtract';
    const times = {
      ...this.times,
      end: moment(this.times.end)[mFunc](interval, 'm').toDate(),
      start: moment(this.times.start)[mFunc](interval, 'm').toDate(),
    };

    this.setTimes(times);
  }

  getIntervalTime(end: Date, duration = this.duration): Time {
    if (!end) throw new Error('Maps Times Store: missing end time');
    end.setSeconds(0, 0);
    const start = new Date(end.getTime() - 1000 * 60 * duration);
    const time: Time = {
      realtime: this.userService.project.isNRT,
      start,
      end,
    };

    return time;
  }

  isDate(obj): boolean {
    return Object.prototype.toString.call(obj) === '[object Date]';
  }
}
