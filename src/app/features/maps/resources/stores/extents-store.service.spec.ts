import { TestBed } from '@angular/core/testing';

import { ExtentsStoreService } from './extents-store.service';

describe('ExtentsStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExtentsStoreService = TestBed.get(ExtentsStoreService);
    expect(service).toBeTruthy();
  });
});
