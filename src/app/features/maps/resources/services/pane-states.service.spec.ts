import { TestBed } from '@angular/core/testing';

import { PaneStatesService } from './pane-states.service';

describe('PaneStatesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaneStatesService = TestBed.get(PaneStatesService);
    expect(service).toBeTruthy();
  });
});
