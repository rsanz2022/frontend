import { Injectable } from '@angular/core';
import { SourceService } from './source.service';
import { StylesService } from './styles.service';
import { UserService } from '@app/core';
import { HttpClient } from '@angular/common/http';
import { Time } from '@app/shared/models';
import * as polylabel from '@mapbox/polylabel';
import { Option } from '../../components/layer-pane/options-pane/options-pane.component';

interface Params {
  [key: string]: string | number;
}

interface Input {
  input?: Params;
}

@Injectable({ providedIn: 'root' })
export class LayerPrepService {
  icons = {
    symbol: { id: 'fiber_manual_record' },
    raster: { id: 'layers' },
    'fill-extrusion': { svg: true, id: '3d-layer' },
    line: { svg: true, id: 'layers-outlined' },
    fill: { id: 'layers' },
  };

  constructor(
    private userService: UserService,
    private sourceService: SourceService,
    private stylesService: StylesService,
    private http: HttpClient
  ) {}

  getLayer(layer, time: Time): Promise<any> {
    return new Promise((resolve, reject): any => {
      const meta = layer.metadata;
      const requests = [this.stylesService.getLayerStyle(layer)];
      layer.metadata.icon = {
        ...this.icons[layer.type],
        ...layer.metadata.icon,
      };
      if (meta.data) {
        if (meta.data.source) {
          const src = meta.data.source;
          let params;
          switch (src.type) {
            case 'graphql':
              params = this.getParams(src.params, time);
              requests.push(
                this.sourceService.getGraphQL(src.query, src.methodName, params)
              );
              break;

            case 'geojson':
              params = this.getParams(src.endpoint, time);
              const endpoint = this.getEndpointWithParams(src.endpoint, params);
              requests.push(this.http.get(endpoint).toPromise());
              break;

            case 'raster':
              layer.source = {
                ...src,
                tiles: src.tiles.map((tile) => {
                  params = this.getParams(
                    tile,
                    time,
                    meta.tableId,
                    layer.metadata.options
                  );
                  return this.getEndpointWithParams(tile, params);
                }),
              };
              break;

            case 'vector': {
              layer.source = {
                ...layer.source,
                tiles: src.tiles.map((tile) => {
                  const tileParams = this.getParams(src.params || tile, time);
                  const tileUrl = this.getEndpointWithParams(tile, tileParams);
                  return window.location.origin + '/' + tileUrl;
                }),
                maxzoom: 19,
                promoteId: src.promoteId,
                minzoom: src.minzoom || 0,
              };
              break;
            }
          }
        }
        if (meta.data.interval) {
          const interval = meta.data.interval;
          const params = this.getParams(interval.params || interval.url, time);
          if (!interval.params) {
            const endpoint = this.getEndpointWithParams(interval.url, params);
            requests.push(this.http.get(endpoint).toPromise());
          } else {
            requests.push(
              this.sourceService.getGraphQL(
                interval.query,
                interval.methodName,
                params
              )
            );
          }
        }
      }

      Promise.all(requests).then(
        (res) => {
          if (
            layer.source.type === 'vector' &&
            layer.metadata.scale &&
            layer.metadata.scale.expression
          ) {
            res[0].paint[layer.type + '-color'] =
              layer.metadata.scale.expression;
          }
          if (res[1]) {
            if (res[2]) {
              res[1].features = this.propertyMerge(
                res[1].features,
                res[2],
                layer.metadata.labels.key
              );
            }
            layer.source.data = res[1];
            if (layer.metadata.staticType === 'polygon') {
              layer.metadata.labels = {
                ...layer.metadata.labels,
                source: {
                  type: 'geojson',
                  data: this.getLabelPositions(res[1]),
                },
              };
            }
          }

          if (res[2] === null) {
            reject({
              ...res[0],
              ...layer,
            });
            throw new Error(
              `${layer.metadata.data.interval.methodName} for ${layer.metadata.name} returns null for selected time`
            );
          } else {
            resolve({
              ...res[0],
              ...layer,
            });
          }
        },
        (e) => {
          reject({ ...layer, statusText: e.statusText });
        }
      );
    });
  }

  getEndpointWithParams(endpoint, params): string {
    const matches = endpoint.match(/\{(.*?)\}/g);
    if (matches) {
      matches.forEach((m) => {
        const key = m.substring(1, m.length - 1);
        const hasValue = params.hasOwnProperty(key);
        if (hasValue) {
          endpoint = endpoint.replace(m, params[key]);
        }
      });
    }
    return endpoint;
  }

  getParams(
    paramArr?,
    time?: Time,
    layerId?: string,
    options?: Option[]
  ): Params | Input {
    if (typeof paramArr === 'string' || paramArr instanceof String) {
      paramArr = paramArr.match(/[^{\}]+(?=})/g);
    }
    const params: Params = {};
    const hasInput = paramArr && typeof paramArr[0] === 'object';
    const paramList: string[] = hasInput ? paramArr[0] : paramArr;

    if (!paramList || !paramList.length) {
      return {};
    }

    paramList.forEach((p) => {
      switch (p) {
        case 'project':
          params[p] = this.userService.project.symbolicName;
          break;

        case 'start':
          params[p] = time.start.getTime();
          break;

        case 'end':
          params[p] = time.end.getTime();
          break;

        case 'table':
          params[p] = layerId || '1';
          break;

        case 'pixelRatio':
          params[p] = window.devicePixelRatio;
          break;

        case 'mosaicLayer':
          params[p] = layerId || '';
          break;

        default:
          if (options && !'xyz'.includes(p)) {
            const opt = options.find(o => o.queryParam === p);
            params[p] = opt.value || '';
          }
          break;
      }
    });
    return hasInput ? { input: params } : params;
  }

  private propertyMerge(features: any, values: any, key?: string): any {
    const valuesObj = {};
    for (const value of values) {
      valuesObj['v' + value.id] = value;
    }
    features.forEach((feature) => {
      const valObj = valuesObj['v' + feature.properties.id];
      const value = valObj ? valObj[key || 'value'] : null;
      if (value >= 0) {
        feature.properties = {
          ...feature.properties,
          value: typeof value === 'number' ? value.toFixed(2) : value,
        };
      }
    });
    return features;
  }

  getLabelPositions = (source): any => ({
    ...source,
    features: source.features
      .filter(
        (feature) => feature.geometry && feature.geometry.type === 'Polygon'
      )
      .map((feature) => ({
        ...feature,
        geometry: {
          ...feature.geometry,
          coordinates:
            feature.properties.labelPos ||
            polylabel(feature.geometry.coordinates),
          type: 'Point',
        },
      })),
  });
}
