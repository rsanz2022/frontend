import {Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SourceService {

  constructor(private apollo: Apollo) {}

  getGraphQL = (queryString: string, methodName: string, params): Promise<any> =>
    this.apollo
      .query<Response>({
        fetchPolicy: 'no-cache',
        variables: { ...params },
        query: gql`${queryString}`
      })
      .pipe(map(res => res.data[methodName]))
      .toPromise();
}
