import { TestBed } from '@angular/core/testing';

import { LayerPrepService } from './layer-prep.service';

describe('LayerPrepService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LayerPrepService = TestBed.get(LayerPrepService);
    expect(service).toBeTruthy();
  });
});
