import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Observable, forkJoin } from 'rxjs';
import { LayersStoreService } from '../stores/layers-store.service';
import { Time } from '@app/shared/models';
import { TimesStoreService } from '../stores/times-store.service';
import { UserService } from '@app/core';
import { LayerPrepService } from './layer-prep.service';
import { MapsService } from '../../maps.service';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

interface ReqInfo { url: string; params: string[]; tiles?: string[] }

@Injectable({ providedIn: 'root' })
export class TimeControlsService {
  aggregation = new FormControl({value: 5, disabled: true});
  accumulation = new FormControl(false);
  speed = new FormControl(1);
  isAnimating = false;
    readonly isAnimating$ = new BehaviorSubject(this.isAnimating);
  readonly animationLoading$ = new BehaviorSubject<boolean>(false);
  readonly animationError$ = new BehaviorSubject<string>('');
  private times: Time[];
  private selectedTime: Time;
  private scheduledFrame;

  constructor(
    private layersStore: LayersStoreService,
    private timesStore: TimesStoreService,
    private layerPrep: LayerPrepService,
    private mapsService: MapsService,
    private http: HttpClient,
    userService: UserService
  ) {
    this.aggregation.setValue(userService.project.interval);
  }

  setIsAnimating(v: boolean): void {
    if (this.isAnimating !== v) {
      this.isAnimating = v;
      this.mapsService.isAnimating = v && this.accumulation.value;
      if (v) {
        this.selectedTime = this.timesStore.times;
        this.startAnimating();
      } else this.stopAnimating();
      this.isAnimating$.next(v);
    }
  }

  getTimeFrames = (): Time[] =>  {
    const timestep = this.aggregation.value * 60 * 1000;
    const start = this.timesStore.times.start.getTime();
    const end = this.timesStore.times.end.getTime();
    let target = end - timestep;
    let targetHolder = end;
    const times: Time[] = [];
    while (target >= start) {
      times.unshift({
        start: this.accumulation.value ? new Date(start) : new Date(target),
        end: new Date(targetHolder)
      });
      targetHolder = target;
      target = target - timestep;
    }
    return times;
  }

  preloadData(requests: Observable<string>[]): void {
    this.animationLoading$.next(true);
    forkJoin(requests).pipe(take(1)).subscribe(
      () => this.setIsAnimating(true),
      this.preloadError,
      () => this.animationLoading$.next(false)
    );
  }

  getRequests(): Observable<string>[] {
    this.times = this.getTimeFrames();
    const reqs: Observable<string>[] = [];
    const reqsInfo = this.layersStore.layers
      .filter(this.hasIntervalData)
      .map(this.getRequestInfo);

    for (let index = 0; index < reqsInfo.length; index++) {
      const reqInfo = reqsInfo[index];
      let endpoints = this.getEndpoints(reqInfo, this.times);
      if (reqInfo.url.includes('{z}/{x}/{y}')) {
        const coords = this.mapsService.getVisibleTileCoords();
        const xyzEndpoints = [];
        endpoints.forEach((ep) => {
          const epCoords = coords['xy'].map(c => ep.replace(
            '{z}/{x}/{y}',
            `${coords['zoom']}/${c.x}/${c.y}`
          ));
          xyzEndpoints.push(...epCoords);
        });
        endpoints = xyzEndpoints;
      }

      reqs.push(...(endpoints.map(e => 
        this.http.get(e, { responseType: 'text' })
      )));
    }

    return reqs;
  }

  private startAnimating = (): void => {
    this.timesStore.setTimes(this.times[0]);
    this.scheduleFrame(1);
  }

  private stopAnimating = (): void => {
    if (!!this.selectedTime) {
      clearTimeout(this.scheduledFrame);
      this.timesStore.setTimes(this.selectedTime);
      this.selectedTime = undefined;
    }
  }

  private scheduleFrame = (frame: number): void => {
    const nextFrame = frame < this.times.length - 1 ? frame + 1 : 0; 
    const waittime = 750 / this.speed.value * (!frame ? 2 : 1);
    this.scheduledFrame = setTimeout(() => {
      this.timesStore.setTimes(this.times[frame]);
      this.scheduleFrame(nextFrame);
    }, waittime);
  }

  private preloadError = (error: Error): void => {
    this.isAnimating = false;
    this.animationError$.next('Error loading animations');
    console.error(error);
  }

  private getEndpoints = (reqInfo: ReqInfo, times: Time[]): string[] => {
    const endpoints = [];
    for (let index = 0; index < times.length; index++) {
      const time = times[index];
      const params = this.layerPrep
        .getParams(reqInfo.params || reqInfo.url, time);
      endpoints.push(this.layerPrep.getEndpointWithParams(reqInfo.url, params));
    }
    return endpoints;
  }

  private hasIntervalData(l: mapboxgl.Layer): boolean {
    return l.metadata && l.metadata.data && l.layout.visibility !== 'none' && (
      l.metadata.data.interval || l.metadata.data.featureState || (
        l.metadata.data.source && l.metadata.data.source.type === 'raster'
      )
    )
  }

  private getRequestInfo(layer): ReqInfo {
    const layerData = layer.metadata.data;
    const d = layerData.interval || layerData.featureState || layerData.source;
    return { url: d.url || d.tiles[0], params: d.params };
  }
}
