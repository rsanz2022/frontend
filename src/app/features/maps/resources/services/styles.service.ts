import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class StylesService {

  labels = {
    polyline: {
      layout: {
        'symbol-placement': 'line',
        'text-field': ['get', 'name'],
        'text-size': 12,
        'text-anchor': 'bottom',
        'text-pitch-alignment': 'viewport',
        'text-rotation-alignment': 'map',
        'text-padding': 1,
        'text-max-angle': 30
      },
      paint: {
        'text-halo-color': '#FFFFFF',
        'text-halo-width': 1,
        'text-halo-blur': 1
      }
    },
    polygon: {
      layout: {
        'symbol-placement': 'point',
        'text-field': ['get', 'name'],
        'text-size': 12
      },
      paint: {
        'text-halo-color': '#FFFFFF',
        'text-halo-width': 1,
        'text-halo-blur': 1
      }
    }
  };

  constructor( private http: HttpClient ) { }

  getLayerStyle(layer): Promise<any> {
    return this.http.get('/assets/styles.json')
      .pipe(
        map((styles: any) => {
          let style = {};
          switch (layer.type) {
            case 'raster': {
              style = styles.raster;
              break;
            }
            case 'fill': {
              style = styles.fill;
              break;
            }
            default: {
              const styleId = layer.metadata.styleId || layer.metadata.staticType || layer.type;
              if (styles[styleId]) {
                style = styles[styleId];
              } else {
                console.warn(`Missing style for layer ${styleId}`);
                style = {
                  layout: {
                    visibility: 'visible'
                  }
                };
              }
              break;
            }
          }

          return style;
        })
      ).toPromise();
  }

  getStaticStyle(layer): any {
    return {
      paint: {
        ...this.labels[layer.metadata.staticType].paint,
        ...layer.paint
      },
      layout: {
        ...this.labels[layer.metadata.staticType].layout,
        ...layer.layout
      }
    };
  }
}
