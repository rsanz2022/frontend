import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MediaObserver } from '@angular/flex-layout';

@Injectable({ providedIn: 'root' })
export class PaneStatesService {

  private readonly isMobile = this.mediaObserver.isActive('gt-xs');
  private readonly _showLayers = new BehaviorSubject<boolean>(this.isMobile);
  readonly showLayers$ = this._showLayers.asObservable();
    get showLayers(): boolean { return this._showLayers.getValue(); }
    set showLayers(value: boolean) { this._showLayers.next(value); }

  private readonly _showElements = new BehaviorSubject<boolean>(false);
    readonly showElements$ = this._showElements.asObservable();
    get showElements(): boolean { return this._showElements.getValue(); }
    set showElements(value: boolean) { this._showElements.next(value); }

  constructor(private mediaObserver: MediaObserver) {}
}
