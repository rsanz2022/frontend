import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { Basemap } from '@app/shared/models';
import { AppConfigStoreService } from '@app/shared/app-config-store.service';
import { OptionsService } from './components/layer-pane/options-pane/options.service';
import { LayersStoreService } from './resources/stores/layers-store.service';
import * as mapgl from 'mapbox-gl';

interface TileCoords {
  zoom: number;
  xy: {
    x: number;
    y: number;
  }[];
}

@Injectable({ providedIn: 'root' })
export class MapsService {
  isAnimating = false;
  defaultBasemap = {
    name: 'Streets',
    url: 'mapbox://styles/mapbox/streets-v9',
  };
  basemap$ = new BehaviorSubject(null);
  onSourceData$ = new ReplaySubject<mapgl.MapSourceDataEvent>(1);
  onError$ = new ReplaySubject<mapgl.ErrorEvent>(1);
  bounds$ = new ReplaySubject<mapgl.LngLatBoundsLike>(1);

  public mapgl: mapgl.Map;
  readonly map$ = new ReplaySubject<mapgl.Map>(1);

  constructor(
    private http: HttpClient,
    private optService: OptionsService,
    private layerStore: LayersStoreService,
    private configStore: AppConfigStoreService
  ) {}

  setMap(mapgl: mapgl.Map): void {
    this.map$.next(mapgl);
    this.mapgl = mapgl;
  }

  getBasemaps = (): Promise<Basemap[]> =>
    this.http.get<Basemap[]>('assets/basemaps.json').toPromise();

  setBasemap = (pid: number, map?: { name: string; url: string }): void => {
    const id = 'maps-basemap';
    const config = {
      id, pid,
      type: 'map',
      temp: false,
      ...this.configStore.getConfig(id, pid),
    };
    config.value = map || config?.value || this.defaultBasemap;
    this.basemap$.next(config.value);
    this.configStore.setConfigs([config]);
  };

  hasFeatureState(l?: mapgl.Layer): boolean {
    const meta = l?.metadata;
    return meta && meta.data && meta.data.featureState;
  }

  // Find the index of the first symbol layer in
  // the map style to add temporary shapes
  getFirstSymbolId(): string {
    const layers = this.mapgl.getStyle().layers;
    let firstSymbolId;
    for (let i = 0; i < layers.length; i++) {
      if (layers[i].type === 'symbol') {
        firstSymbolId = layers[i].id;
        break;
      }
    }

    return firstSymbolId;
  }

  updateAllFeatureStates = (layer: mapgl.Layer): void => {
    const values = layer.metadata.data.featureState.values as any[];
    const visible =
      layer.layout && layer.layout.visibility
        ? layer.layout.visibility === 'visible'
        : true;
    if (values && layer.metadata.data.featureState.hasUpdate) {
      if (!layer.metadata.data.featureState.allReplaced) {
        this.mapgl.removeFeatureState({
          source: layer.id,
          sourceLayer: layer['source-layer'],
        });
      }

      const setFeatures = (): void => {
        layer.metadata.data.featureState.hasUpdate = false;
        const option = layer.metadata.options.find((o) => o.autoMax);
        for (let index = 0; index < values.length; index++) {
          const element = values[index];
          const id =
            element[layer.metadata.data.source.promoteId] ||
            (isNaN(element.vector_tile_index)
              ? element.vectorTileIndex
              : element.vector_tile_index);
          const feature = {
            id,
            source: layer.id,
            sourceLayer: layer['source-layer'],
          };
          this.mapgl.setFeatureState(feature, { val: element.value });
        }
        if (
          !this.isAnimating &&
          this.mapgl &&
          option &&
          option.autoMax.checked
        ) {
          let exp = this.mapgl.getPaintProperty(
            layer.id,
            layer.type + '-color'
          );
          exp = this.optService.getNewExpression(exp, option, values);
          option.value = exp[exp.length - 2];
          layer.paint[layer.type + '-color'] = exp;
          this.layerStore.layerPaint$.next(layer);
        }
      };

      if (visible) {
        this.isAnimating ? setFeatures() : setTimeout(setFeatures);
      }
    }
  };

  getVisibleTileCoords = (map = this.mapgl): TileCoords => {
    const zoom = Math.round(map.getZoom());
    const tileCoordinateObject: TileCoords = { zoom: zoom, xy: [] };
    const bounds = map.getBounds();
    const nw = bounds.getNorthWest();
    const se = bounds.getSouthEast();
    const top_tile = this.lat2tile(nw.lat, zoom);
    const left_tile = this.lng2tile(nw.lng, zoom);
    const bottom_tile = this.lat2tile(se.lat, zoom);
    const right_tile = this.lng2tile(se.lng, zoom);

    for (let x = Math.abs(left_tile); x <= right_tile; x++) {
      for (let y = Math.abs(top_tile); y <= bottom_tile; y++) {
        tileCoordinateObject.xy.push({ x, y });
      }
    }

    return tileCoordinateObject;
  };

  lng2tile(lon: number, zoom: number): number {
    return Math.floor(((lon + 180) / 360) * Math.pow(2, zoom));
  }

  lat2tile(lat: number, zoom: number): number {
    return Math.floor(
      ((1 -
        Math.log(
          Math.tan((lat * Math.PI) / 180) + 1 / Math.cos((lat * Math.PI) / 180)
        ) /
          Math.PI) /
        2) *
        Math.pow(2, zoom)
    );
  }
}
