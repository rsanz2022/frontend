import { Apollo, gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { DataLayer, Bin, Category, BinOverview } from './ddf.models';
import * as moment from 'moment-timezone';
import { GeoJSON } from 'geojson';

interface DdfResponse {
  getDDFDataLayers?: DataLayer[];
  getDDFOverview?: BinOverview[];
  getDDFDurations?: number[];
  getDDFBins?: Bin[];
  getShapeOutline?: GeoJSON;
}

@Injectable({ providedIn: 'root' })
export class DDFService {
  basemap: 'streets' | 'dark' | 'light' = 'streets';

  constructor(private http: HttpClient, private apollo: Apollo) {}

  getMaxtime(project: string): Promise<{ [key: string]: number }> {
    return this.http
      .get(`api/${project}/maxtimes?layerTypes=ddf`)
      .pipe(map((res) => res['ddf']))
      .toPromise();
  }

  getDataLayers(project: string): Promise<DataLayer[]> {
    return this.apollo
      .query<DdfResponse>({
        query: gql`
          query DDF2($project: String!) {
            getDDFDataLayers(project: $project) {
              id
              name
              tableId
              sources
              categories {
                id
                label
                description
                color
                nextCategoryID
              }
            }
          }
        `,
        variables: { project },
      })
      .pipe(map((res) => res.data.getDDFDataLayers))
      .pipe(
        map((res: DataLayer[]) =>
          res.map((layer: DataLayer) => {
            layer.categories.sort((a, b) => a.id - b.id);
            layer.categories.forEach((cat, i, array) => {
              const index = array.findIndex((c) => c.id === cat.nextCategoryID);
              cat.nextColor = array[index].color;
            });
            return layer;
          })
        )
      )
      .toPromise();
  }

  getLayerTable(
    project: string,
    layerId: string,
    start: number,
    end: number,
    tableId: number,
    duration = 0
  ): Promise<{ getDDFOverview: BinOverview[]; getDDFDurations: number[] }> {
    tableId = tableId === -1 ? undefined : tableId;
    // expecting duration in ms;
    duration = duration * 1000 * 60;
    return this.apollo
      .query({
        query: gql`
          query DDF2(
            $project: String!
            $layerId: String!
            $start: Long!
            $end: Long!
            $duration: Int!
            $tableId: Int
          ) {
            getDDFOverview(
              project: $project
              layerId: $layerId
              start: $start
              end: $end
              duration: $duration
              tableId: $tableId
            ) {
              id
              name
              duration
              description
              amount
              rainStart
              rainEnd
              category
              nextCategory
              percent
            }
            getDDFDurations(
              project: $project
              layerId: $layerId
              tableId: $tableId
            )
          }
        `,
        variables: { project, layerId, start, end, duration, tableId },
      })
      .pipe(map((res) => res.data))
      .pipe(
        map(
          (res: {
            getDDFOverview: BinOverview[];
            getDDFDurations: number[];
          }) => {
            return res;
          }
        )
      )
      .toPromise();
  }

  getBin(
    project: string,
    layerId: string,
    start: number,
    end: number,
    id: string,
    tableId: number,
    duration = 0
  ): Promise<any> {
    tableId = tableId === -1 ? undefined : tableId;
    return this.apollo
      .query({
        query: gql`
          query DDF2(
            $project: String!
            $layerId: String!
            $start: Long!
            $end: Long!
            $bins: [String]
            $id: String
            $duration: Int
            $tableId: Int
          ) {
            getDDFBins(
              project: $project
              layerId: $layerId
              start: $start
              end: $end
              bins: $bins
              duration: $duration
              tableId: $tableId
            ) {
              id
              name
              binResult {
                duration
                percent
                category
                categoryDescription
                amount
                rainStart
                rainEnd
                durationInterval {
                  duration
                  intervals {
                    depth
                    category {
                      id
                      label
                      description
                      color
                      kml
                    }
                  }
                }
              }
            }
            getDDFDurations(
              project: $project
              layerId: $layerId
              binID: $id
              tableId: $tableId
            )
          }
        `,
        variables: {
          project,
          layerId,
          start,
          end,
          bins: [id],
          duration,
          tableId,
        },
      })
      .toPromise();
  }

  getShapeOutline(proj: string, tID: string, bID?: string): Promise<GeoJSON> {
    return this.http
      .get<GeoJSON>(
        `/api/${proj}/ddf/outline/${encodeURIComponent(
          tID
        )}/outline.json?binId=${encodeURIComponent(bID)}`
      )
      .toPromise();
  }

  humanizeDuration = (t: number): string =>
    t > 120 && t <= 4320
      ? t / 60 + ' hours'
      : t < 60 && t > 0
        ? t + ' minutes'
        : moment.duration(t, 'minute').humanize();

  getOverviewCategories = (
    bins: BinOverview[],
    categories: Map<number, Category>
  ): BinOverview[] =>
    bins.map(
      (bin: BinOverview): BinOverview => {
        bin.categoryDetails = categories.get(bin.category);
        bin.displayPercentage = this.getDisplayPercentage(bin);
        return bin;
      }
    );

  private getDisplayPercentage(bin: BinOverview): string {
    return bin.percent === -1 ? 'N/A' : bin.percent + '%';
  }
}
