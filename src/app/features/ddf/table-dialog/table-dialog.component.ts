import { AfterContentChecked, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Project } from '@app/shared/models';
import * as moment from 'moment-timezone';
import { BinOverview } from '../ddf.models';

interface DialogData {
  list: BinOverview[];
  project: Project;
}

interface TableBinOverview extends BinOverview {
  rainStartDisplay: string;
  rainEndDisplay: string;
}

@Component({
  selector: 'app-table-dialog',
  templateUrl: './table-dialog.component.html',
  styleUrls: ['./table-dialog.component.scss']
})
export class TableDialogComponent implements AfterContentChecked {
  loading = true;
  constructor( @Inject(MAT_DIALOG_DATA) public data: DialogData ) {}

  ngAfterContentChecked(): void {
    setTimeout(() => {
      for (let index = 0; index < this.data.list.length; index++) {
        const bin = this.data.list[index] as TableBinOverview;
        bin.rainStartDisplay = this.getTimeString(bin.rainStart);
        bin.rainEndDisplay = this.getTimeString(bin.rainEnd);
      }
      this.loading = false;
    });
  }

  getTimeString = (t: number): string =>
    (t === -1) ? 'N/A' : moment(t)
      .tz(this.data.project.timezone)
      .format('YYYY/MM/DD HH:mm z');
}
