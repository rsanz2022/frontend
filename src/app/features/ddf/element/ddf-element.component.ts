import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import * as mapboxgl from 'mapbox-gl';
import { UserService } from '@app/core';
import { Project, Time } from '@app/shared/models';
import { DDFService } from '../ddf.service';
import * as extent from '@mapbox/geojson-extent';
import { DataLayer, BinOverview, Category } from '../ddf.models';

@Component({
  selector: 'app-ddf-element',
  templateUrl: './ddf-element.component.html',
  styleUrls: ['./ddf-element.component.scss'],
})
export class DdfElementComponent implements OnInit, OnDestroy {
  rxSubs: Subscription[] = [];
  loading = true;
  error = '';
  ids: BinOverview[] = [];
  humanizeDuration = this.service.humanizeDuration;
  durations = [];
  private _selectedDuration = 0;
  get selectedDuration(): number {
    return this._selectedDuration;
  }
  set selectedDuration(v: number) {
    if (this._selectedDuration !== v) {
      this._selectedDuration = v;
      this.loadLayer();
    }
  }
  hideShadow = {
    top: true,
    bottom: true,
  };
  private _selectedBin = ''; // Bin ID
  get selectedBin(): string {
    return this._selectedBin;
  }
  set selectedBin(v: string) {
    this.removeOutline();
    if (this._selectedBin === v) {
      this._selectedBin = '';
    } else {
      this._selectedBin = v;
      if (v) {
        this.addOutline();
      }
    }
  }
  layers: DataLayer[] = [];
  private _selectedLayer: DataLayer;
  get selectedLayer(): DataLayer {
    return this._selectedLayer;
  }
  set selectedLayer(selectedLayer: DataLayer) {
    this._selectedLayer = selectedLayer;
    this.selectedDuration = 0;
    this.selectedBin = '';
    this.loadLayer();
  }
  @Input() map: mapboxgl.Map;
  @Input() mapPadding: mapboxgl.PaddingOptions;
  private _times: Time;
  @Input() get times(): Time {
    return this._times;
  }
  set times(times) {
    const isChange = !!this._times;
    this._times = times;
    if (isChange) {
      this.loadLayer();
    }
  }

  constructor(private userService: UserService, private service: DDFService) {}

  ngOnInit(): void {
    this.rxSubs.push(this.userService.project$.subscribe(this.updateProject));
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
    this.removeOutline(this.map);
  }

  private updateProject = (proj: Project): void => {
    this.loading = true;
    this.error = '';

    this.service.getDataLayers(proj.symbolicName).then((res) => {
      this.loading = false;
      if (res.length) {
        this.layers = res;
        this.selectedLayer = res[0];
      } else {
        this.error = `There was an error getting DDF tables for ${proj.name}.`;
      }
    });
  };

  private addOutline(): void {
    if (!!this.map && this.selectedBin) {
      const id = this.selectedBin;
      this.service
        .getShapeOutline(
          this.userService.project.symbolicName,
          this.selectedLayer.id,
          this.selectedBin,
        )
        .then((geoJson: any) => {
          if (this.selectedBin !== id) {
            return;
          }
          const bounds = extent({ ...geoJson });
          this.map
            .addLayer({
              type: 'line',
              id: 'temporary-shape',
              layout: { 'line-cap': 'round', 'line-join': 'round' },
              paint: {
                'line-width': [
                  'interpolate',
                  ['exponential', 1.5],
                  ['zoom'],
                  5,
                  0.75,
                  18,
                  32,
                ],
              },
              source: {
                type: 'geojson',
                data: geoJson,
              },
            })
            .fitBounds(bounds, {
              padding: this.mapPadding,
            });
        });
    }
  }

  // map param for onDestroy
  private removeOutline(map = this.map): void {
    if (!!map) {
      const lineId = 'temporary-shape';
      const source = map.getSource(lineId);
      if (this.selectedBin && source) {
        map.removeLayer(lineId).removeSource(lineId);
      }
    }
  }

  private loadLayer = (): void => {
    this.error = '';
    this.loading = true;
    if (!this.times) {
      setTimeout(() => this.loadLayer(), 100);
      return;
    }
    this.service
      .getLayerTable(
        this.userService.project.symbolicName,
        this.selectedLayer.id,
        this.times.start.getTime(),
        this.times.end.getTime(),
        this.selectedLayer.tableId,
        this.selectedDuration,
      )
      .then((res) => {
        this.durations = res.getDDFDurations;
        // using map for faster lookup since the # of bins could be large
        const categoryMap: Map<number, Category> = new Map();
        this.selectedLayer.categories.forEach(cat => categoryMap.set(cat.id, cat));
        this.ids = this.service.getOverviewCategories(
          res.getDDFOverview,
          categoryMap,
        );
        setTimeout(() => {
          const tableEl = document.getElementById('ddf-table');
          if (tableEl) {
            this.scrollCheck({ target: tableEl });
          }
        });
        this.loading = false;
      });
  };

  public scrollCheck(event): void {
    const element = event.target || event.srcElement || event.currentTarget;
    if (
      element.scrollHeight > element.style.maxHeight &&
      element.scrollHeight > element.clientHeight
    ) {
      if (element.scrollHeight - element.scrollTop === element.clientHeight) {
        this.hideShadow.top = false;
        this.hideShadow.bottom = true;
      } else if (element.scrollTop === 0) {
        this.hideShadow.top = true;
        this.hideShadow.bottom = false;
      } else {
        this.hideShadow.top = false;
        this.hideShadow.bottom = false;
      }
    } else {
      this.hideShadow.top = true;
      this.hideShadow.bottom = true;
    }
  }

}
