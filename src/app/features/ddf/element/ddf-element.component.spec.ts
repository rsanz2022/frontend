import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DdfElementComponent } from './ddf-element.component';

describe('DdfElementComponent', () => {
  let component: DdfElementComponent;
  let fixture: ComponentFixture<DdfElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DdfElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DdfElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
