import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdfElementComponent } from './ddf-element.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ScrollingModule } from '@angular/cdk/scrolling';



@NgModule({
  declarations: [DdfElementComponent],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,

    MatSelectModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    ScrollingModule
  ],
  exports: [DdfElementComponent]
})
export class DDFElementModule { }
