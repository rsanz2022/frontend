import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DDFComponent } from './ddf.component';

describe('DDFComponent', () => {
  let component: DDFComponent;
  let fixture: ComponentFixture<DDFComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DDFComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DDFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
