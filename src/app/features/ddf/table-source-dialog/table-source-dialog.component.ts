import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-table-source-dialog',
  template: `<div fxLayout fxLayoutAlign="space-between center" mat-dialog-title><h3>
  Table source</h3><button mat-icon-button mat-dialog-close>
  <mat-icon>close</mat-icon></button></div>{{data}}`,
  styles: ['h3 { margin: 0; color: rgba(0,0,0,0.8);}']
})
export class TableSourceDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string) {}

}
