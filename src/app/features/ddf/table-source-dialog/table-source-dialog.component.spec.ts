import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSourceDialogComponent } from './table-source-dialog.component';

describe('TableSourceDialogComponent', () => {
  let component: TableSourceDialogComponent;
  let fixture: ComponentFixture<TableSourceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSourceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSourceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
