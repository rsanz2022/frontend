import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DdfDetailComponent } from './ddf-detail.component';

describe('DdfDetailComponent', () => {
  let component: DdfDetailComponent;
  let fixture: ComponentFixture<DdfDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DdfDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DdfDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
