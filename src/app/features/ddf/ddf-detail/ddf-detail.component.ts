import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Globals } from '@app/globals';
import { DDFService } from '../ddf.service';
import { Time, Project } from '@app/shared/models';
import { UserService } from '@app/core';
import { GeoJSON } from 'geojson';
import { Bin, Interval, Category, DataLayer } from '../ddf.models';
import * as extent from '@mapbox/geojson-extent';
import * as moment from 'moment-timezone';
import { LngLatBoundsLike, FillPaint } from 'mapbox-gl';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MediaObserver, MediaChange } from '@angular/flex-layout';

@Component({
  selector: 'app-ddf-detail',
  templateUrl: './ddf-detail.component.html',
  styleUrls: ['./ddf-detail.component.scss'],
})
export class DdfDetailComponent implements OnInit, OnDestroy {
  private rxSubs: Subscription[] = [];
  outline: GeoJSON;
  loading = true;
  fitBounds: LngLatBoundsLike;
  paint: FillPaint = {
    'fill-outline-color': '#FFFFFF',
    'fill-color': 'rgba(255,255,255,0.75)',
  };
  durations: number[] = [];
  error = false;
  isMobile = true;
  humanizeDuration = this.service.humanizeDuration;
  private _duration: number;
  @Input()
  get duration(): number {
    return this._duration;
  }
  set duration(minutes: number) {
    const isChange = this._duration !== undefined;
    this._duration = minutes;
    if (isChange) {
      this.loading = true;
      this.updateBin();
    }
  }
  intervals: Interval[];
  @Input() layer: DataLayer;
  @Input() time: Time;
  @Input() bin: Bin;
  @Input() categories: Category[];
  @Input() project: Project;

  constructor(
    public globals: Globals,
    public service: DDFService,
    public userService: UserService,
    private snackBar: MatSnackBar,
    private media: MediaObserver
  ) {}

  ngOnInit(): void {
    this.service
      .getShapeOutline(this.project.symbolicName, this.layer.id, this.bin.id)
      .then(
        (res: GeoJSON) => {
          this.outline = res;
          this.fitBounds = extent({ ...this.outline });
        },
        () => {
          this.error = true;
          this.snackBar.open(
            `Bin outline unavailable for ${this.bin.name}`,
            null,
            {
              duration: 6000,
            }
          );
        }
      );

    this.rxSubs.push(this.media.media$.subscribe(this.onMediaChange));

    this.updateBin();
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
  }

  onMediaChange = (change: MediaChange): void => {
    if (change) {
      this.isMobile = this.media.isActive('lt-sm');
    }
  };

  updateBin = (): void => {
    this.service
      .getBin(
        this.project.symbolicName,
        this.layer.id,
        this.time.start.valueOf(),
        this.time.end.valueOf(),
        this.bin.id,
        this.layer.tableId,
        this.duration
      )
      .then((res): void => {
        const binData = res.data.getDDFBins[0];
        const binResult = {
          ...binData.binResult,
          category: this.categories.find(
            (cat) => binData.binResult.category === cat.id
          ),
        };
        this.bin.binResult = binResult;
        this.intervals = binData.binResult.durationInterval.intervals.filter(
          (interval) => interval.depth > 0
        );
        const hexColor = this.bin.binResult.category['color'];
        this.paint = {
          'fill-outline-color': hexColor,
          'fill-color': this.hexa(hexColor, 0.75),
        };
        this.durations = res.data.getDDFDurations;
        this.loading = false;
      });
  };

  hexa(hex: string, alpha: number): string {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    const r = parseInt(result[1], 16);
    const g = parseInt(result[2], 16);
    const b = parseInt(result[3], 16);
    return result ? `rgba(${r},${g},${b},${alpha})` : null;
  }

  getDisplayTime = (t: number): string =>
    moment(t).tz(this.project.timezone).format('YYYY/MM/DD HH:mm');
}
