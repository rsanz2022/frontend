export interface DataLayer {
  id: string;
  name: string;
  categories: Category[];
  tableId: number;
  sources: string[];
}

export interface Category {
  id: number;
  label?: string;
  description?: string;
  color?: string;
  nextColor?: string;
  nextCategoryID?: number;
}

export interface Bin {
  id: string;
  name?: string;
  binResult?: BinResult;
}

export interface BinResult {
  duration?: number;
  percent?: number;
  category?: Category;
  categoryDescription?: string;
  amount?: number;
  rainStart?: number;
  rainEnd?: number;
  approximateDays?: number;
  durationInterval?: DurationInterval;
}

export interface BinOverview {
  id: string;
  name?: string;
  duration?: number;
  description?: string;
  amount?: number;
  rainStart?: number;
  rainEnd?: number;
  category?: number;
  nextCategory?: number;
  percent?: number;
  displayPercentage?: string;
  categoryDetails?: Category;
}

export interface DurationInterval {
  duration?: number;
  intervals?: Interval[];
}

export interface Interval {
  depth?: number;
  category?: Category;
}
