import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Time } from '@app/shared/models';
import { Bin } from '../ddf.models';

interface Data {
  layer: string;
  time: Time;
  bin: Bin;
  max: number;
  isNRT: boolean;
}

@Component({
  selector: 'app-time-controls',
  template: `
  <app-timepicker
    [time]="data.time"
    [max]="data.max"
    [enableRealtime]="data.isNRT"
    (timesChange)="dialogRef.close($event)">
  </app-timepicker>`,
  styles: [`:host {
    display: block;
    padding: 8px;
  }`]
})
export class TimeControlsComponent {
  constructor(
    public dialogRef: MatDialogRef<TimeControlsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Data
  ) { this.data = data; }
}
