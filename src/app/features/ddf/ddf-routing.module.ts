import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DDFComponent } from './ddf.component';

const routes: Routes = [
  {
    path: '',
    component: DDFComponent,
    data: { title: 'DDF' }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class DDFRoutingModule { }
