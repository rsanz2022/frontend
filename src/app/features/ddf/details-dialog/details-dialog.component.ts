import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Time, Project } from '@app/shared/models';
import { Bin, Category } from '../ddf.models';

interface DialogData {
  time: Time;
  layer: string;
  bin: Bin;
  duration: number;
  categories: Category[];
  project: Project;
}

@Component({
  selector: 'app-details-dialog',
  template: `
    <div class="dialog-title">
      <span>{{data.bin.name}}</span>
      <div fxFlex></div>
      <button mat-icon-button mat-dialog-close aria-label="Close Gauge Details Dialog">
        <mat-icon>close</mat-icon>
      </button>
    </div>
    <mat-dialog-content>
      <app-ddf-detail
        [layer]="data.layer"
        [time]="data.time"
        [bin]="bin"
        [duration]="data.duration"
        [project]="data.project"
        [categories]="data.categories"></app-ddf-detail>
    </mat-dialog-content>`,
  styles: [`
    .dialog-title {
      flex-direction: row;
      box-sizing: border-box;
      display: flex;
      place-content: center flex-start;
      align-items: center;
      margin: -12px 0 8px;
      font: 500 20px/32px 'Ubuntu', sans-serif;
    }
  `]
})
export class DetailsDialogComponent {
  bin: Bin;
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.bin = { ...this.data.bin };
  }
}
