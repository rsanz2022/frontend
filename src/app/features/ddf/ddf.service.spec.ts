import { TestBed } from '@angular/core/testing';

import { DDFService } from './ddf.service';

describe('DDFService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DDFService = TestBed.get(DDFService);
    expect(service).toBeTruthy();
  });
});
