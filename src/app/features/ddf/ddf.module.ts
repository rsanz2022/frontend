
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DDFComponent } from '../ddf/ddf.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { DDFRoutingModule } from './ddf-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DdfDetailComponent } from './ddf-detail/ddf-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TimeControlsComponent } from '@app/features/ddf/time-controls/time-controls.component';
import { TimepickerModule } from '@app/shared';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { environment } from '@env/environment';
import { MatRippleModule } from '@angular/material/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { DetailsDialogComponent } from './details-dialog/details-dialog.component';
import { TableDialogComponent } from './table-dialog/table-dialog.component';
import { TableSourceDialogComponent } from './table-source-dialog/table-source-dialog.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [DDFComponent, DdfDetailComponent, TimeControlsComponent, DetailsDialogComponent, TableDialogComponent, TableSourceDialogComponent],
  imports: [
    CommonModule,
    DDFRoutingModule,
    FlexLayoutModule,
    LayoutModule,
    MatPaginatorModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatRadioModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatRippleModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatTooltipModule,
    TimepickerModule,
    ScrollingModule,
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken
    })
  ]
})
export class DDFModule { }
