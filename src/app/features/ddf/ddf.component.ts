import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ElementRef,
} from '@angular/core';
import { DDFService } from './ddf.service';
import { UserService } from '@app/core';
import { Project, Time } from '@app/shared/models';
import { MatDialog } from '@angular/material/dialog';
import { MediaObserver } from '@angular/flex-layout';
import { TimeControlsComponent } from './time-controls/time-controls.component';
import { DetailsDialogComponent } from './details-dialog/details-dialog.component';
import { Globals } from '@app/globals';
import { Subscription, interval } from 'rxjs';
import { DataLayer, BinOverview, Category } from './ddf.models';
import * as moment from 'moment-timezone';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TableDialogComponent } from './table-dialog/table-dialog.component';
import { TableSourceDialogComponent } from './table-source-dialog/table-source-dialog.component';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-ddf',
  templateUrl: './ddf.component.html',
  styleUrls: ['./ddf.component.scss'],
})
export class DDFComponent implements OnInit, OnDestroy {
  @ViewChild('timeButton', { static: true }) timeButton: ElementRef;
  private rxSubs = [];
  private updateInterval: Subscription;
  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;
  download = {
    href: '',
    detailedHref: '',
    fileName: 'ddf-stuff.csv',
  };
  timerDelay = 15e4; // 2.5 minutes in milliseconds
  time: Time = {
    realtime: true,
    start: new Date(Date.now() - 36e5),
    end: new Date(),
  };
  tzOffset: string;
  maxtimes: { [key: string]: number };
  project: Project;
  error: string;
  loading = true;
  timepicker = {
    show: false,
    rect: (side): string =>
      this.timeButton.nativeElement.getBoundingClientRect()[side] + 'px',
  };
  dataLayers: DataLayer[];
  bins: BinOverview[];
  humanizeDuration = this.service.humanizeDuration;
  durations: number[];
  private _selectedDuration = 0;
    get selectedDuration(): number {
      return this._selectedDuration;
    }
    set selectedDuration(v: number) {
      this.setLoading(true);
      this._selectedDuration = v;
      this.loadSelected();
    }
  private _selectedLayer: DataLayer;
    get selectedLayer(): DataLayer {
      return this._selectedLayer;
    }
    set selectedLayer(v: DataLayer) {
      this.setLoading(true);
      this._selectedDuration = 0;
      this._selectedLayer = v;
      this.viewPort.scrollToIndex(0, 'auto');
      this.loadSelected();
    }

  private _selectedBin: BinOverview;
    get selectedBin(): BinOverview {
      return this._selectedBin;
    }
    set selectedBin(b: BinOverview) {
      this._selectedBin = b;
      this.openDetails(b);
    }

  constructor(
    public service: DDFService,
    private userService: UserService,
    private mediaObserver: MediaObserver,
    private dialog: MatDialog,
    public globals: Globals,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.rxSubs.push(this.userService.project$.subscribe(this.updateProject));
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
    this.stopInterval();
  }

  private updateDownload = (): void => {
    const proj = this.project.symbolicName;
    const start = this.time.start.getTime();
    const end = this.time.end.getTime();
    const layer = this.selectedLayer;
    const tree = this.router.createUrlTree([
      `api/${proj}/ddf/${start}/${end}/csv`,
    ]);
    tree.queryParams = {
      layerId: layer.id,
      tableId: layer.tableId >= 0 ? layer.tableId : '',
      duration: this.selectedDuration > 0 ? this.selectedDuration : '',
    };
    const detailedTree = this.router.createUrlTree([
      `api/${proj}/ddf/details/${start}/${end}/csv`,
    ]);
    detailedTree.queryParams = {
      layer: layer.id,
      table: layer.tableId >= 0 ? layer.tableId : '',
    };
    const fileName = `${this.selectedLayer.name.replace(
      /[\.\s\(\)]+/g,
      ''
    )}_${moment(start)
      .tz(this.project.timezone)
      .format('YYYYMMDDHHmm')}-${moment(end)
      .tz(this.project.timezone)
      .format('YYYYMMDDHHmm')}.csv`;
    this.download = {
      href: tree.toString(),
      detailedHref: detailedTree.toString(),
      fileName,
    };
  };

  openDetails = (bin: BinOverview): void => {
    if (bin) {
      const dialogRef = this.dialog.open(DetailsDialogComponent, {
        data: {
          layer: this.selectedLayer,
          time: this.time,
          duration: this.selectedDuration,
          categories: this.selectedLayer.categories,
          project: this.project,
          bin: { id: bin.id, name: bin.name },
        },
        autoFocus: false,
      });

      dialogRef.beforeClosed().subscribe(() => (this.selectedBin = null));
    }
  };

  updateProject = (project: Project): void => {
    this.stopInterval();
    this.project = project;
    this.time.realtime = this.project.isNRT;
    this.initData();
  };

  initData = (): void => {
    if (this.time.realtime) {
      this.startInterval();
    }

    Promise.all([
      this.service.getMaxtime(this.project.symbolicName),
      this.service.getDataLayers(this.project.symbolicName),
    ]).then((res) => {
      this.maxtimes = res[0];
      const selectedLayer = res[1][0];
      const layerMax = this.maxtimes[selectedLayer.id] || Date.now();
      this.time.start = new Date(layerMax - 36e5);
      this.time.end = new Date(layerMax);
      this.dataLayers = res[1];
      this.selectedLayer = selectedLayer;
    });
  };

  loadSelected = (): void => {
    this.updateDownload();
    this.service
      .getLayerTable(
        this.project.symbolicName,
        this.selectedLayer.id,
        this.time.start.getTime(),
        this.time.end.getTime(),
        this.selectedLayer.tableId,
        this.selectedDuration
      )
      .then((res) => {
        if (!res.getDDFOverview.length) {
          this.pageError('getDDFOverview query returned empty list');
        }
        // using map for faster lookup since the # of bins could be large
        const categoryMap: Map<number, Category> = new Map();
        this.selectedLayer.categories.forEach((cat) =>
          categoryMap.set(cat.id, cat)
        );
        this.bins = this.service.getOverviewCategories(
          res.getDDFOverview,
          categoryMap
        );
        this.durations = res.getDDFDurations;
        this.setLoading(false);
      });
  };

  pageError = (err: Error | string): void => {
    this.error = ((err as Error).message || err) as string;
    console.error(this.error);
    this.setLoading(false);
  };

  setLoading = (loading: boolean): void => {
    this.loading = loading;
    if (loading) this.error = null;
  };

  showSources = (): void => {
    const width = this.mediaObserver.isActive('gt-sm') ? 40 : 80;
    this.dialog.open(TableSourceDialogComponent, {
      data: this.selectedLayer.sources.join('; '),
      width: width + 'vw',
      autoFocus: false,
    });
  };

  openTimeControls = (): void => {
    const dialogRef = this.dialog.open(TimeControlsComponent, {
      data: {
        time: this.time,
        max: this.maxtimes[this.selectedLayer.id] || Date.now(),
        isNRT: this.project.isNRT,
      },
      autoFocus: false,
      backdropClass: 'hide-backdrop',
      panelClass: 'drop-down',
      width: '320px',
      position: {
        top: this.timepicker.rect('bottom'),
        left: this.timepicker.rect('left'),
      },
    });

    dialogRef.beforeClosed().subscribe(this.onCloseTimeControls);
  };

  onCloseTimeControls = (event: Time): void => {
    if (event) {
      if (this.time.realtime !== event.realtime) {
        if (event.realtime) {
          this.startInterval();
        } else {
          this.stopInterval();
        }
      }
      this.tzOffset = this.project.getOffset(event.end);
      this.time = event;
      this.setLoading(true);
      this.loadSelected();
    }
  };

  startInterval = (): void => {
    if (!this.updateInterval || this.updateInterval.closed) {
      this.updateInterval = interval(this.timerDelay).subscribe(() => {
        this.service.getMaxtime(this.project.symbolicName).then((res) => {
          let hasUpdate = !this.maxtimes;
          if (this.maxtimes) {
            for (const key in this.maxtimes) {
              if (this.maxtimes[key] !== res[key]) {
                hasUpdate = true;
              }
            }
          }
          if (hasUpdate) {
            this.snackBar.open('Updating values...', null, { duration: 3000 });
            this.maxtimes = res;
            const layerMax = this.maxtimes[this.selectedLayer.id] || Date.now();
            const range = moment(this.time.end).diff(
              this.time.start,
              'minutes'
            );
            const end = new Date(layerMax);
            this.time = {
              start: moment(end).subtract(range, 'minutes').toDate(),
              end: end,
              realtime: true,
            };
            this.tzOffset = this.project.getOffset(this.time.end);
            this.loadSelected();
          }
        });
      });
    }
  };

  getDateString(): string {
    if (!this.time || !this.time.end) {
      return 'Loading...';
    }
    if (this.time.realtime) {
      return moment(this.time.end)
        .tz(this.project.timezone)
        .format('YYYY-MM-DD HH:mm');
    }

    const mStart = moment(this.time.start).tz(this.project.timezone);
    const mEnd = moment(this.time.end).tz(this.project.timezone);
    const start = mStart.format('YYYY-MM-DD HH:mm');
    let end = ' - ';

    if (mEnd.isSame(mStart, 'day')) {
      end = end + mEnd.format('HH:mm');
    } else if (mEnd.isSame(mStart, 'month')) {
      end = end + mEnd.format('DD HH:mm');
    } else if (mEnd.isSame(mStart, 'year')) {
      end = end + mEnd.format('MM-DD HH:mm');
    } else {
      end = end + mEnd.format('YYYY-MM-DD HH:mm');
    }

    return `${start}${end}`;
  }

  getTimeTooltip = (): string => {
    const diff = moment(this.time.end).diff(this.time.start, 'minutes');
    return this.time.realtime
      ? moment.duration(diff, 'minutes').humanize()
      : null;
  };

  stopInterval = (): void => {
    if (!!this.updateInterval) {
      this.updateInterval.unsubscribe();
    }
  };

  openTableModal = (): void => {
    this.dialog.open(TableDialogComponent, {
      maxWidth: 'calc(100vw - 16px)',
      data: {
        list: this.bins,
        project: this.project,
      },
    });
  };
}
