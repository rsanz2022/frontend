export interface Result {
  id: string;
  project?: string;
  name?: string;
  description?: string;
  requested?: number;
  requestedDisplay?: string;
  requestedBy?: string;
  status?: string;
  config?: RunConfig;
}

export interface RunConfig {
  project?: string;
  name: string;
  description: string;
  solveTime: number;
  forecastId: string;
  adjustType: string;
  adjustValue: number;
  soilMoisture: number;
}

export interface RainSourceSelection {
  id: string;
  startTime: number;
  endTime: number;
  adjustments: SourceAdjustments;
}

export interface SoilMoistureSourceSelection {
  id: string;
  time: number;
  adjustments: SourceAdjustments;
}

export interface SourceAdjustments {
  set: number;
  multiply: number;
  add: number;
}

export interface WhatIfHyetograph {
  values: number[];
}

export interface ControlConfig {
  icon: string | (() => string);
  eventName: string;
  title: string;
  svg?: boolean;
}

export interface Option {
  name: string;
  id: string;
}

export interface Options {
  rainSources: [Option]
  boundaryConditions: [Option]
  bopFiles: [Option]
  evapotranspiration: [Option]
  inundation: [Option]
  state: [Option]
}