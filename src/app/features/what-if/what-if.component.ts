import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '@app/core';
import { Project } from '@app/shared/models';
import { BehaviorSubject, Subject, Subscription, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Result } from './what-if.models';
import { WhatIfService } from './what-if.service';

@Component({
  selector: 'app-what-if',
  templateUrl: './what-if.component.html',
  styleUrls: ['./what-if.component.scss'],
})
export class WhatIfComponent implements OnInit, OnDestroy {
  project: Project;
  pollTimer: Subscription;
  startTimer$ = new Subject();
  stopTimer$ = new Subject();
  destroy$ = new Subject();
  showRequest$ = new BehaviorSubject(false);
  results: Result[] = [];
  _result: Result;
  get result(): Result {
    return this._result;
  }
  set result(v: Result) {
    this._result = v;
    this.showRequest$.next(!!v);
  }

  constructor(
    private userService: UserService,
    private service: WhatIfService
  ) {}

  ngOnInit(): void {
    this.userService.project$
      .pipe(takeUntil(this.destroy$))
      .subscribe(this.onProject);
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  onProject = (proj: Project): void => {
    this.project = proj;
    this.startCheck();
  };

  onRequestClose = (ev: boolean): void => {
    this.showRequest$.next(false);
    this.result = null;
    if (ev) this.startCheck();
  };

  startCheck = (): void => {
    this.pollTimer?.unsubscribe();
    this.pollTimer = timer(0, 1000 * 60 * 2.5)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.service
          .listResults(this.project)
          .subscribe(this.loadResults);
      });
  };

  loadResults = (res: Result[]): void => {
    this.results = res;
    let keepPolling = false;
    for (let i = 0; i < res.length; i++) {
      if (['Loading', 'Processing'].includes(res[i].status)) {
        keepPolling = true;
        break;
      }
    }

    if (!keepPolling) this.pollTimer.unsubscribe();
  };
}
