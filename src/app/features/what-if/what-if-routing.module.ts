import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WhatIfComponent } from './what-if.component';

const routes: Routes = [
  {
    path: '',
    component: WhatIfComponent,
    data: { title: 'What-If?' }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class WhatIfRoutingModule { }
