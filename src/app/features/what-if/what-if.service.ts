import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Apollo, gql } from 'apollo-angular';
import { Observable } from 'rxjs';
import { Options, Result, RunConfig } from './what-if.models';
import { map } from 'rxjs/operators';
import * as moment from 'moment-timezone';
import { Project } from '@app/shared/models';

interface Response {
  getResults?: Result[];
  getWIOptions?: Options;
  startRun?: string;
  deleteRun?: boolean;
}

export interface InundationTime {
  timestamp: number; // timestamp
  description: string; // formatted datetime in project timezone
}

const runConfig = `config { name description solveTime forecastId adjustType adjustValue soilMoisture }`;

@Injectable({
  providedIn: 'root',
})
export class WhatIfService {
  constructor(private apollo: Apollo, private http: HttpClient) {}

  listResults(project: Project): Observable<Result[]> {
    return this.apollo
      .query<Response>({
        variables: { project: project.symbolicName },
        query: gql`
          query Query($project: String!) {
            getResults(project: $project) { id status requested ${runConfig} }
          }
        `,
      })
      .pipe(
        map(({ data }) =>
          data.getResults
            .filter((res) => !!res)
            .map((res) => {
              res.requestedDisplay = moment(res.requested)
                .tz(project.timezone)
                .format('YYYY/MM/DD HH:mm');
              return res;
            })
            .sort((a, b) => b.requested - a.requested)
        )
      );
  }

  getInundationTimes(proj: Project, id: string): Observable<InundationTime[]> {
    return this.http
      .get<number[]>(`/api/${proj.symbolicName}/whatif/${id}/times`)
      .pipe(
        map((res) =>
          res
            .filter((ts) => !!ts)
            .map((timestamp: number) => ({
              timestamp,
              description: moment(timestamp)
                .tz(proj.timezone)
                .format('YYYY-MM-DD HH:mm'),
            }))
        )
      );
  }

  getSources(proj: string, id: string): Observable<any[]> {
    return this.http.get<any[]>('/assets/what-if.results.json').pipe(
      map((res) => {
        return res.map((r) => {
          if (r.scale?.url) {
            r.scale.url = this.normalize(r.scale.url, proj);
          }
          if (r.data) {
            r.data = this.normalize(r.data, proj, id);
          }
          if (r.setData) {
            r.setData = this.normalize(r.setData, proj, id);
          }
          if (r.tiles) {
            r.tiles[0] = this.normalize(r.tilePath, proj, id);
          }
          const fState = r.featureState;
          if (fState) {
            fState.data = this.normalize(fState.data, proj, id);
          }

          return r;
        });
      })
    );
  }

  startRun(project: string, config: RunConfig): Observable<string> {
    return this.apollo
      .mutate<Response>({
        variables: { config: { project, ...config } },
        mutation: gql`
          mutation($config: RunConfigInput!) {
            startRun(config: $config)
          }
        `,
      })
      .pipe(map(({ data }) => data.startRun));
  }

  deleteRun(project: string, id: string): Observable<boolean> {
    return this.apollo
      .mutate<Response>({
        variables: { project, id },
        mutation: gql`
          mutation($project:String! $id:String!) {
            deleteRun(project:$project id:$id)
          }
        `,
      })
      .pipe(map(({ data }) => data.deleteRun));
  }


  normalize(url: string, proj?: string, id?: string, time?: number): any {
    const origin = window.location.origin;
    return url
      .replace('{origin}', origin)
      .replace('{project}', proj)
      .replace('{whatif}', id)
      .replace('{pixelRatio}', window.devicePixelRatio + '')
      .replace(time ? '{time}' : '&time={time}', time ? time + '' : '');
  }

  getFormOptions(proj: Project): Observable<Options> {
    const option = '{name id}';
    return this.apollo
      .query<Response>({
        variables: { project: proj.symbolicName },
        query: gql`
          query Query($project: String!) {
            getWIOptions(project: $project) {
              rainSources ${option}
              boundaryConditions ${option}
              bopFiles ${option}
              evapotranspiration ${option}
              inundation ${option}
              state ${option}
            }
          }
        `,
      })
      .pipe(map(({ data }) => data.getWIOptions));
  }
}
