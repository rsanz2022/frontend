import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Project } from '@app/shared/models';
import {
  GeoJSONSource,
  LngLat,
  LngLatBounds,
  LngLatBoundsLike,
  MapboxGeoJSONFeature,
  MapMouseEvent,
} from 'mapbox-gl';
import * as mapboxgl from 'mapbox-gl';
import { ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { ControlConfig, Result } from '../what-if.models';
import { InundationTime, WhatIfService } from '../what-if.service';
import { ForecastService } from '@app/features/forecast/forecast.service';
import { SourceService } from '@app/features/maps/resources/services/source.service';
import { FeatureCollection, Geometry } from 'geojson';
import { Auth } from '@aws-amplify/auth';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  mapgl$ = new ReplaySubject<mapboxgl.Map>(1);
  loading$ = new ReplaySubject<boolean>();
  mapgl: mapboxgl.Map;
  fitBounds: LngLatBoundsLike;
  sources;
  pLngLat: LngLat;
  initialView = true;
  popups: { layers: string[] } = { layers: [] };
  clickedFeatures: MapboxGeoJSONFeature[];
  inundationTimes: InundationTime[];
  inundationTime: InundationTime;
  readonly ctrlConfigs: ControlConfig[] = [
    // {
    //   eventName: 'mapStyle',
    //   icon: 'palette',
    //   title: 'Change map style',
    // },
    {
      eventName: 'bounds',
      icon: 'home',
      title: 'Reset map to domain',
    },
  ];
  token: string;

  private _result: Result;
  @Input()
  get result(): Result {
    return this._result;
  }
  set result(result: Result) {
    this.onResult(result);
  }

  private _project: Project;
  @Input()
  get project(): Project {
    return this._project;
  }
  set project(proj: Project) {
    this.onProject(proj);
  }

  @Input() scenarios: Result[];

  constructor(
    private service: WhatIfService,
    private http: HttpClient,
    public forecastService: ForecastService,
    private sourceService: SourceService
  ) {}

  ngOnInit(): void {
    Auth.currentSession().then(
      (session) => (this.token = session.getIdToken().getJwtToken())
    );
  }

  onProject = (proj: Project): void => {
    this._project = proj;
    this.result = null;
    this.fitBounds = proj.bounds;
  };

  onResult = (result: Result): void => {
    this._result = result;
    if (result) {
      this.initialView = false;
      this.loading$.next(true);
      this.service
        .getSources(this.project.symbolicName, result.id)
        .subscribe(this.onGetSources);
    } else {
      this.loading$.next(false);
      this.sources = null;
      this.pLngLat = null;
    }
  };

  // private onGetInundationTimes = (res: InundationTime[]): void => {
  //   const max: InundationTime = { timestamp: 0, description: 'Max Inundation' };
  //   res.unshift(max);
  //   this.inundationTimes = res;
  //   this.inundationTime = max;
  // };

  public setInundationTime = (v: InundationTime): void => {
    this.inundationTime = v;
    this.updateInundationTiles();
  };

  private updateInundationTiles = (): void => {
    const inunSource = this.sources.find((s) => s.id === 'inundation');
    if (inunSource) {
      const url = this.service.normalize(
        inunSource.tilePath,
        this.project.symbolicName,
        this.result.id,
        this.inundationTime?.timestamp
      );
      const style = this.mapgl.getStyle();
      style.sources['inundation']['tiles'] = [url];
      this.mapgl.setStyle(style);
      setTimeout(() => this.loading$.next(true));
    }
  };

  private onGetSources = (res: any[]) => {
    this.sources = res;
    this.popups.layers = [];
    for (let index = 0; index < res.length; index++) {
      const element = res[index];
      element.layers.forEach((layer) => {
        if (layer.metadata?.popup) this.popups.layers.push(layer.id);
      });
      if (element.featureState) {
        this.http.get(element.featureState.data).subscribe((res: any[]) => {
          this.mapgl$.pipe(take(1)).subscribe((mapgl) => {
            for (let index = 0; index < res.length; index++) {
              const feature = res[index];
              mapgl.setFeatureState(
                { id: feature[element.promoteId], source: element.id },
                feature
              );
            }
          });
        });
      }
      if (element.mergeProperties) {
        const timer = setTimeout(() => {
          if (this.mapgl && this.mapgl.isSourceLoaded(element.id)) {
            clearInterval(timer);
            this.mergeProperties(element);
          }
        }, 250);
      }
    }
  };

  private mergeProperties = (source): void => {
    const reqs = [this.http.get(source.setData).toPromise()];
    const mProps = source.mergeProperties;
    for (let index = 0; index < mProps.length; index++) {
      const mProp = mProps[index];
      const variables = {};
      for (let i = 0; i < mProp.params.length; i++) {
        const param = mProp.params[i];
        switch (param) {
          case 'project':
            variables[param] = this.project.symbolicName;
            break;

          default:
            break;
        }
      }
      reqs.push(
        this.sourceService.getGraphQL(mProp.gql, mProp.query, variables)
      );
    }
    Promise.all(reqs).then(([geoJSON, ...res]: [FeatureCollection, any[]]) => {
      for (let index = 0; index < res.length; index++) {
        const response = res[index] as any;
        for (let i = 0; i < geoJSON.features.length; i++) {
          const feature = geoJSON.features[i];
          for (let j = 0; j < response.length; j++) {
            const element = response[j];
            if (
              feature.properties[mProps[index].keyMap[0][0]] ===
              element[mProps[index].keyMap[0][1]]
            ) {
              feature.properties[mProps[index].keyMap[1][1]] =
                element[mProps[index].keyMap[1][0]];
            }
          }
        }
      }
      this.mapgl$.pipe(take(1)).subscribe(() => {
        (this.mapgl.getSource(source.id) as GeoJSONSource).setData(geoJSON);
      });
    });
  };

  public onMapClick = (ev: MapMouseEvent): void => {
    this.pLngLat = null;
    this.clickedFeatures = this.mapgl
      .queryRenderedFeatures(ev?.point, this.popups)
      .concat(this.checkRasterLayers(ev))
      .filter(
        (feat, index, self) =>
          index === self.findIndex((f) => f.id === feat.id && f.id === feat.id)
      );
    if (this.clickedFeatures.length) {
      const geometry = this.clickedFeatures[0].geometry as Geometry;
      this.pLngLat =
        geometry?.type === 'Point'
          ? LngLat.convert(geometry.coordinates as [number, number])
          : ev.lngLat;
    }
  };

  public trackById = (_: number, data: { id: string }): string => data.id;

  public customControl = ([eventName, ev]: [string, MouseEvent]): void => {
    switch (eventName) {
      case 'bounds':
        this.mapgl.fitBounds(this.project.bounds);
        break;

      default:
        console.warn(eventName, ev);
        break;
    }
  };

  // private toggleFullscreen = (): void => {
  //   if (document.fullscreenElement) {
  //     document.exitFullscreen();
  //     return;
  //   }
  //   const container = this.mapgl.getContainer() as any;
  //   const rfs =
  //     container.requestFullscreen ||
  //     container.webkitRequestFullScreen ||
  //     container.mozRequestFullScreen ||
  //     container.msRequestFullscreen;

  //   rfs.call(container);
  // };

  public onSourceData = (): void => {
    if (!this.sources || !this.mapgl) return;
    let loading = false;
    for (let index = 0; index < this.sources.length; index++) {
      const id: string = this.sources[index].id;
      if (!this.mapgl.isSourceLoaded(id)) {
        loading = true;
        break;
      }
    }
    this.loading$.next(loading);
  };

  private checkRasterLayers = (ev: MapMouseEvent): MapboxGeoJSONFeature[] => {
    const features: MapboxGeoJSONFeature[] = [];
    for (let i = 0; i < this.sources.length; i++) {
      const source = this.sources[i];
      if (source.type === 'raster') {
        const feat = {
          id: source.id,
          layer: source.layers[0],
          properties: {},
        } as MapboxGeoJSONFeature;
        if (source.bounds) {
          const bounds = new LngLatBounds(source.bounds);
          if (bounds.contains(ev.lngLat)) features.push(feat);
        } else {
          features.push(feat);
        }
      }
    }
    return features;
  };

  public pointerCheck = (ev: MapMouseEvent): void => {
    if (!ev || !this.mapgl || !this.sources) return;
    const features = this.mapgl
      .queryRenderedFeatures(ev?.point, this.popups)
      .concat(this.checkRasterLayers(ev));
    this.mapgl.getCanvas().style.cursor = features.length ? 'pointer' : '';
  };

  public transformRequest = (url: string): mapboxgl.RequestParameters => {
    const protocol = window.location.protocol;
    const host = window.location.host;
    if (
      url.startsWith(`${protocol}//${host}/api/`) ||
      url.startsWith(`/api/`) ||
      url.startsWith(`api/`)
    ) {
      return {
        url,
        headers: { Authorization: 'Bearer ' + this.token }
      };
    }
  };
}
