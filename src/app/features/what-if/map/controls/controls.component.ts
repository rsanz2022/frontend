import { Component, EventEmitter, Input, Output } from '@angular/core';

interface ControlConfig {
  icon: string;
  eventName: string;
  title: string;
}

@Component({
  selector: 'app-controls',
  template: `<button
    *ngFor="let config of configs"
    [title]="config.title"
    (click)="clicked.emit([config.eventName, $event])"
  >
    <mat-icon inline>{{ config.icon }}</mat-icon>
  </button>`,
  styleUrls: ['./controls.component.scss'],
})
export class ControlsComponent {
  @Input() configs: ControlConfig[];
  @Output() clicked = new EventEmitter<[string, MouseEvent]>();
}
