import { Component, Input } from '@angular/core';
import { POITime } from '@app/shared/charts/hydrograph/hydrograph.component';
import { LngLat, MapboxGeoJSONFeature } from 'mapbox-gl';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
})
export class PopupComponent {
  selection: MapboxGeoJSONFeature;
  timestamp = Date.now();
  @Input() whatif: string;
  @Input() lngLat: LngLat;
  @Input() times: POITime;
  private _features: MapboxGeoJSONFeature[] = [];
  @Input()
  get features(): MapboxGeoJSONFeature[] {
    return this._features;
  }
  set features(v: MapboxGeoJSONFeature[]) {
    this.selection = null;
    this.timestamp = Date.now();
    this._features = this.prepFeatures(v);
    if (this._features.length === 1) {
      setTimeout(() => (this.selection = this._features[0]));
    }
  }

  prepFeatures(features: MapboxGeoJSONFeature[]): MapboxGeoJSONFeature[] {
    return features.map((feature) => {
      const name =
        feature.properties.name ||
        feature.properties.id ||
        feature.layer.metadata.name;
      feature.properties.btnName = name.replace(/^([0-9]* - )/, '');

      const chart = feature.layer.metadata.popup;
      const attrStart = chart.indexOf('[');
      (feature.properties.type = chart.substr(
        0,
        attrStart < 0 ? undefined : attrStart
      )),
        (feature.properties.attributes = chart
          .substring(attrStart + 1, chart.lastIndexOf(']'))
          .split(','));
      return feature;
    });
  }
}
