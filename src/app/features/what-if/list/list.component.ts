import {
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  MatSelectionList,
  MatSelectionListChange,
} from '@angular/material/list';
import { BehaviorSubject } from 'rxjs';
import { Result } from '../what-if.models';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent {
  disableRequestBtn$ = new BehaviorSubject(true);
  readonly statusColors = {
    Loading: '#FFC107',
    Processing: '#4CAF50',
    Error: '#F44336',
  };
  @ViewChild('resultList') resultList: MatSelectionList;
  @Input() results: Result[] = [];
  private _result: Result;
  @Input()
  get result(): Result {
    return this._result;
  }
  set result(v: Result) {
    this._result = v;
    if (!v) this.resultList?.deselectAll();
  }
  @Output() resultChange = new EventEmitter<Result>();
  @Output() newRequest = new EventEmitter<boolean>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.results) {
      this.disableRequestBtn$.next(
        !!this.results.find((r) => ['Loading', 'Processing'].includes(r.status))
      );
    }
  }

  onSelectionChange = (ev: MatSelectionListChange): void => {
    this.resultChange.emit(ev.options[0].value);
    if (ev.options[0].value === null) this.newRequest.emit(true);
  };
}
