import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ForecastService } from '@app/features/forecast/forecast.service';
import { RadarInfo } from '@app/features/forecast/radar-info.model';
import { Project } from '@app/shared/models';
import { ReplaySubject, timer } from 'rxjs';
import { Result } from '../what-if.models';
import { WhatIfService } from '../what-if.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
})
export class RequestComponent implements OnChanges, OnInit, OnDestroy {
  private forecastIds = ['sara'];
  public forecasts: RadarInfo[];
  public selectedForecast: RadarInfo;
  public loading$ = new ReplaySubject(1);
  public rForm = this.fb.group({
    name: new FormControl('', [Validators.required]),
    description: '',
    solveTime: new FormControl(6 * 3.6e6, [Validators.required]),
    forecastId: new FormControl('', [Validators.required]),
    adjustType: 'multiply',
    adjustValue: new FormControl(1.5, [
      Validators.required,
      Validators.min(0.01),
      Validators.max(10),
    ]),
    soilMoisture: new FormControl(0, [
      Validators.required,
      Validators.min(0),
      Validators.max(10),
    ]),
  });
  private pollTimer;
  @Output() close = new EventEmitter<boolean>();
  @Input() scenario: Result;
  @Input() project: Project;

  constructor(
    private forecastsService: ForecastService,
    private fb: FormBuilder,
    private service: WhatIfService,
    private _snack: MatSnackBar,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['scenario']) {
      const ctrls = changes['scenario'].currentValue?.config;
      if (ctrls) {
        for (const key in ctrls) {
          if (Object.prototype.hasOwnProperty.call(ctrls, key)) {
            const rCtrl = this.rForm.get(key);
            if (rCtrl) rCtrl.setValue(ctrls[key]);
          }
        }
        this.rForm.disable();
      }
    }
  }

  ngOnInit(): void {
    this.rForm.get('adjustType').valueChanges.subscribe((change) => {
      const maxValue = change === 'add' ? 20 : 10;
      this.rForm
        .get('adjustValue')
        .setValidators([
          Validators.required,
          Validators.min(0.01),
          Validators.max(maxValue),
        ]);
    });
    if (!this.scenario) this.startPolling();
    else this.checkForecast();
  }

  ngOnDestroy(): void {
    this.pollTimer?.unsubscribe();
  }

  checkForecast = (): void => {
    this.forecastsService.getInfo(this.project).then((res) => {
      this.forecasts = res.filter((f) => this.forecastIds.includes(f.id));
      if (
        !this.selectedForecast ||
        this.selectedForecast.latest.time < this.forecasts[0].latest.time
      ) {
        this.selectedForecast = this.forecasts[0];
        this.rForm.get('forecastId').setValue(this.forecasts[0].id);
      }
    });
  };

  startPolling(): void {
    this.pollTimer?.unsubscribe();
    this.pollTimer = timer(0, 1000 * 60 * 5).subscribe(this.checkForecast);
  }

  onSubmit = (): void => {
    this.loading$.next(true);
    this.service
      .startRun(this.project.symbolicName, this.rForm.value)
      .subscribe(() => {
        const msg = 'Your scenario has been requested!';
        this._snack.open(msg, null, { duration: 4000 });
        this.close.emit(true);
      });
  };

  onDelete = (): void => {
    this.service
      .deleteRun(this.project.symbolicName, this.scenario.id)
      .subscribe(() => {
        const msg = 'Scenario has been deleted!';
        this._snack.open(msg, null, { duration: 4000 });
        this.close.emit(true);
      });
  }
}
