import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, forkJoin, from, ReplaySubject } from 'rxjs';
import {
  Expression,
  Layer,
  LngLatBoundsLike,
  Map,
  MapMouseEvent,
} from 'mapbox-gl';
import { Project } from '@app/shared/models';
import { RadarInfo } from '@app/features/forecast/radar-info.model';
import { ForecastService } from '@app/features/forecast/forecast.service';
import { take } from 'rxjs/operators';

interface VectorData {
  vid: number;
  val: number;
}

@Component({
  selector: 'app-source-map',
  templateUrl: './source-map.component.html',
  styleUrls: ['./source-map.component.scss'],
})
export class SourceMapComponent implements OnInit {
  mapgl$ = new ReplaySubject<Map>(1);
  loading$ = new BehaviorSubject<boolean>(true);
  mapgl: Map;
  fitBounds: LngLatBoundsLike;
  layer: Layer;
  fillColor$ = new ReplaySubject<Expression>(1);

  private _source: RadarInfo;
  @Input()
  get source(): RadarInfo {
    return this._source;
  }
  set source(v: RadarInfo) {
    if (!v) return;
    this._source = v;
    this.addSource();
  }
  @Input() project: Project;

  constructor(private service: ForecastService) {}

  ngOnInit(): void {
    this.service
      .getScaleExpression(this.project.symbolicName, 6)
      .then((res) => this.fillColor$.next(res));
  }

  public pointerCheck = (ev: MapMouseEvent): void => {
    if (!ev || !this.mapgl || !this.source) return;
    if (this.mapgl.getSource(this.source.id)) {
      const features = this.mapgl.queryRenderedFeatures(ev?.point, {
        layers: [this.source.id],
      });
      this.mapgl.getCanvas().style.cursor = features.length ? 'pointer' : '';
    }
  };

  public onSourceData = (): void => {
    if (this.mapgl && this.layer && this.mapgl.getSource(this.layer.id)) {
      this.loading$.next(!this.mapgl.isSourceLoaded(this.layer.id));
    }
  };

  private addSource = (): void => {
    this.layer = this.service.getLayer(this.source);
    this.layer.paint = { 'fill-color': 'rgba(0,0,0,0)' };

    this.fillColor$
      .pipe(take(1))
      .subscribe((fc) => (this.layer.paint = { 'fill-color': fc }));

    const frame = this.source.latest.frames[
      this.source.latest.frames.length - 1
    ];
    const reqs = [
      this.mapgl$.pipe(take(1)),
      from(
        this.service.getData(
          this.project.symbolicName,
          this.source,
          frame,
          true,
          false
        )
      ),
    ];

    forkJoin(reqs)
      .pipe(take(1))
      .subscribe(([mapgl, data]: [Map, VectorData[]]) => {
        for (let i = data.length - 1; i >= 0; i--) {
          const feature = {
            id: data[i].vid,
            source: 'forecast',
            sourceLayer: 'shapefile-layer',
          };
          if (this.mapgl)
            this.mapgl?.setFeatureState(feature, { val: data[i].val });
          else break;
        }
      });
  };
}
