import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhatIfComponent } from './what-if.component';
import { MapComponent } from './map/map.component';
import { ListComponent } from './list/list.component';
import { RequestComponent } from './request/request.component';
import { WhatIfRoutingModule } from './what-if-routing.module';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { environment } from '@env/environment';
import { PopupComponent } from './map/popup/popup.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { HydrographModule } from '@app/shared';
import { ControlsComponent } from './map/controls/controls.component';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SourceMapComponent } from './request/source-map/source-map.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CorePipesModule } from '@app/core';

@NgModule({
  declarations: [
    WhatIfComponent,
    MapComponent,
    ListComponent,
    RequestComponent,
    PopupComponent,
    ControlsComponent,
    SourceMapComponent,
  ],
  imports: [
    CommonModule,
    WhatIfRoutingModule,
    HydrographModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    CorePipesModule,

    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatListModule,
    MatTooltipModule,
    MatSnackBarModule,

    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken,
    }),
  ],
})
export class WhatIfModule {}
