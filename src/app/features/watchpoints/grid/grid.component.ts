import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input,
  ElementRef,
  HostListener,
} from '@angular/core';
import { WatchpointsService } from '../watchpoints.service';
import { Project, Time } from '@app/shared/models';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { filter, map, take } from 'rxjs/operators';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit, OnDestroy {
  subs = [];
  currentProj: Project;
  basins = [];
  gutterWidth = '8px';
  activeMediaQuery: string;
  smallScreen = true;
  cols = { xs: 2, sm: 3, md: 4, lg: 5, xl: 6 };
  error = false;

  @Input() detailsOpen = false;

  private _project: Project;
  @Input()
  get project(): Project {
    return this._project;
  }
  set project(proj: Project) {
    this.basins = [];
    this.setLoading(true);
    if (proj) {
      this._project = proj;
      this.service
        .getGrid(proj, this.time)
        .pipe(take(1))
        .subscribe(this.loadGrid, () => {
          this.error = true;
          this.setLoading(false);
        });
    }
  }

  private _time: Time;
  @Input()
  get time(): Time {
    return this._time;
  }
  set time(t: Time) {
    if (!!t) {
      const firstChange = !this._time;
      const rtChange =
        !t.realtime || (this._time && this._time.realtime !== t.realtime);
      this._time = t;
      if (!firstChange) {
        this.setLoading(rtChange);
        this.service
          .getGrid(this.project, t)
          .pipe(take(1))
          .subscribe(this.loadGrid, () => {
            this.error = true;
            this.setLoading(false);
          });
      }
    }
  }

  private _filter: string;
  @Input()
  get filter(): string {
    return this._filter;
  }
  set filter(f: string) {
    const firstChange = this._filter === undefined;
    this._filter = (f || '').toLowerCase().trim();
    if (!firstChange) {
      this.loadGrid();
      this.elRef.nativeElement.scrollTo(0, 0);
    }
  }

  private loading = true;
  @Output() gridLoading = new EventEmitter<boolean>();
  setLoading(v: boolean): void {
    if (this.loading !== v) {
      this.loading = v;
      this.gridLoading.emit(v);
    }
  }

  @HostListener('scroll', ['$event'])
  onScroll = (event: Event): void => this.service.gridScroll$.next(event);

  constructor(
    public elRef: ElementRef,
    private service: WatchpointsService,
    private mediaObserver: MediaObserver
  ) {}

  ngOnInit(): void {
    this.subs.push(
      this.mediaObserver
        .asObservable()
        .pipe(
          filter((changes: MediaChange[]) => changes.length > 0),
          map((changes: MediaChange[]) => changes[0])
        )
        .subscribe((change: MediaChange) => {
          if (this.activeMediaQuery !== change.mqAlias) {
            this.activeMediaQuery = change.mqAlias;
            this.smallScreen = this.mediaObserver.isActive('lt-md');
          }
        })
    );
  }

  updatePoints = (): void => {
    const project = this.project.symbolicName;
    this.service.getStatuses(project, this.time).then((res) => {
      this.basins.forEach((basin) => {
        const points = res.filter((p) => p.sortBasin.name === basin.name);
        points.forEach((p) => {
          const point = basin.points.find((bp) => p.pointId === bp.pointId);
          point.status = p.status;
        });
      });
    });
  };

  loadGrid = (data = this.basins): void => {
    this.error = false;
    if (!this.basins.length) this.basins = data;
    const statusSearch = this.getSearchString(this.filter, 'status');
    const groupSearch = this.getSearchString(this.filter, 'group');
    data.forEach((basin, basinI) => {
      this.basins[basinI].showCount = 0;
      basin.points.forEach((point, pointI) => {
        point.show =
          point.filterString.includes(this.filter) ||
          point.status === parseInt(statusSearch as string) ||
          this.groupCheck(point.groupingName, groupSearch);
        if (point.show) this.basins[basinI].showCount++;
        this.basins[basinI].points[pointI] = point;
      });
    });

    this.setLoading(false);
    this.service.gridScroll$.next(null);
  };

  groupCheck(group: string, search: string): boolean {
    return !search || !group
      ? false
      : search.startsWith('-')
      ? !group.toLowerCase().includes(search.substring(1))
      : group.toLowerCase().includes(search);
  }

  getSearchString = (filter: string, type: string): string =>
    filter.includes(type + ':')
      ? filter
          .split(type + ':')
          .pop()
          .trim()
      : null;

  colSpan(pLength: number): number {
    const columns =
      this.cols[this.activeMediaQuery] - (this.detailsOpen ? 1 : 0) || 5;
    return columns < pLength ? columns : pLength;
  }

  rowSpan(pLength: number): number {
    const columns =
      this.cols[this.activeMediaQuery] - (this.detailsOpen ? 1 : 0) || 5;
    return Math.ceil(pLength / columns);
  }

  flexCalc(pLength: number): string {
    const columns =
      this.cols[this.activeMediaQuery] - (this.detailsOpen ? 1 : 0) || 5;
    const isWrapped = columns < pLength;
    let calcString;
    if (isWrapped) {
      calcString = `0 1 calc(${100 / columns}%`;
    } else {
      const colSpan = this.colSpan(pLength);
      calcString = `calc(${((colSpan / columns) * 100) / pLength}%`;
    }

    return calcString + ` - ${this.gutterWidth})`;
  }

  trackByFn = (i, p): string => p.pointId || p.name;

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
