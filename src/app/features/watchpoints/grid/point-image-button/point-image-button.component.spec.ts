import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointImageButtonComponent } from './point-image-button.component';

describe('PointImageButtonComponent', () => {
  let component: PointImageButtonComponent;
  let fixture: ComponentFixture<PointImageButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointImageButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointImageButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
