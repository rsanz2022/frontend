import { Component, Input, ElementRef, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { WatchpointsService } from '../../watchpoints.service';
import { UserService } from '@app/core';
import { Router } from '@angular/router';
import { Time } from '@app/shared/models';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-point-image-button',
  templateUrl: './point-image-button.component.html',
  styleUrls: ['./point-image-button.component.scss']
})
export class PointImageButtonComponent implements OnInit, OnDestroy, AfterViewInit {
  placeholder = 'assets/images/watchpoint-placeholder.gif';
  url = '';
  loaded = false;
  error = false;
  private rxSubs: Subscription[] = [];
  private errorCount = 0;
  private width: number;
  private height: number;
  private hasUpdate = true;
  private _isVisible = false;
    get isVisible(): boolean {
      return this._isVisible;
    }
    set isVisible(v: boolean) {
      if (this._isVisible !== v) {
        this._isVisible = v;
        if (v) {
          if (this.hasUpdate) {
            this.hasUpdate = false;
            this.url = this.getImageUrl();
          }
        }
      }
    }

  @Input() point;
  @Input() isHidden = false;
  @Input() scrollContainer: ElementRef;
      
  private _time: Time;
    @Input() get time(): Time { return this._time; }
      set time(time: Time) {
        if (time) {
          const isFirstChange = this._time === undefined;
          const isDiff = this._time &&
            this._time.end.getTime() !== time.end.getTime();
          this._time = time;
          if (!isFirstChange) {
            if (this.isVisible) {
              this.loaded = !time.realtime;
              this.url = isDiff ? this.getImageUrl() : this.url;
            } else {
              this.hasUpdate = true;
            }
          }
        }
      }

  constructor(
    private elRef: ElementRef,
    public wpService: WatchpointsService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.rxSubs.push(
      this.wpService.gridScroll$.pipe(debounceTime(200)).subscribe(() => {
        this.isVisible = this.isInViewport();
      })
    );
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.isVisible = this.isInViewport());
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach(s => s.unsubscribe());
  }

  onImageLoad(ev): void {
    if (!ev.target.src.endsWith('placeholder.gif')) {
      this.loaded = true;
      this.errorCount = 0;
    }
  }

  onImageError(): void {
    this.error = true;
    this.errorCount++;
    if (this.errorCount < 5) {
      setTimeout(() => {
        if (this.error) {
          this.loaded = false;
          this.url = this.getImageUrl();
        }
      }, (5000 * this.errorCount));
    }
  }

  onClick = (event: MouseEvent): void => {
    this.wpService.setSelection(this.point);
    setTimeout(() => {
      if (!this.isInViewport()) {
        (event.target as Element).scrollIntoView();
      }
    });
  }

  isInViewport = (): boolean => {
    const btnBounds = this.elRef.nativeElement.getBoundingClientRect();
    const gridBounds = this.scrollContainer.nativeElement.getBoundingClientRect();
    return (btnBounds.top > gridBounds.top && btnBounds.top < gridBounds.bottom) ||
        (btnBounds.bottom > gridBounds.top && btnBounds.bottom < gridBounds.bottom)
  };

  getImageUrl = (): string => {
    const project = this.userService.project.symbolicName;
    const pixelRatio = window.devicePixelRatio;
    const tree = this.router.createUrlTree([`api/${project}/hydrograph/`]);
    const el = this.elRef.nativeElement;
    const time = this.time.end.getTime();
  
    this.width = Math.ceil(el.offsetWidth) || this.width;
    this.height = Math.ceil(el.offsetHeight) || this.height;
  
    tree.queryParams = {
      watchpointid: this.point.pointId,
      style: 'THUMBNAIL',
      width: this.width,
      height: this.height,
      pixelRatio
    };
  
    if (!this.time.realtime) {
      tree.queryParams['start'] = time - 1000 * 60 * 60 * 24;
      tree.queryParams['end'] = time + 1000 * 60 * 60 * 15;
      tree.queryParams['fakeNow'] = time;
    } else {
      tree.queryParams['r'] = time + this.errorCount;
    }
    
    this.error = false;
    return tree.toString();
  }
}
