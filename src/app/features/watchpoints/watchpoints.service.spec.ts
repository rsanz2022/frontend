import { TestBed } from '@angular/core/testing';

import { WatchpointsService } from './watchpoints.service';

describe('WatchpointsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WatchpointsService = TestBed.get(WatchpointsService);
    expect(service).toBeTruthy();
  });
});
