import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Time } from '@app/shared/models';

@Component({
  selector: 'app-time-controls',
  template: `
    <app-timepicker
      [time]="data"
      [max]="max"
      enableRealtime
      momentOnly
      (timesChange)="onTimesChange($event)"
    >
    </app-timepicker>
  `,
  styles: [
    `
      :host {
        display: block;
        padding: 8px;
      }
    `
  ]
})
export class TimeControlsComponent {
  max = Date.now();

  constructor(
    private dialogRef: MatDialogRef<TimeControlsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Time
  ) {}

  onTimesChange(event: Time): void {
    if (event) {
      event.end = event.realtime ? new Date() : event.end;
      this.dialogRef.close(event);
    } else {
      this.dialogRef.close();
    }
  }
}
