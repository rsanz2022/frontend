import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Project, Time } from '@app/shared/models';
import { Subscription } from 'rxjs';
import { WatchpointsService } from '../watchpoints.service';
import { UserService } from '@app/core';
import { take } from 'rxjs/operators';
import moment from 'moment';
import { ElementEvent } from '@app/shared/models/element.model';
import { Feature, Point } from 'geojson';

@Component({
  selector: 'app-watchpoints-element',
  templateUrl: './watchpoints-element.component.html',
  styleUrls: ['./watchpoints-element.component.scss'],
})
export class WatchpointsElementComponent
  implements OnInit, OnChanges, OnDestroy
{
  rxSubs: Subscription[] = [];
  project: Project;
  stageUnits: string;
  loading = true;
  error = false;
  points = [];
  sortType = 'Status';
  readonly sortTypes = ['Name', 'Status', 'Value'];
  @Input() time: Time = {
    realtime: true,
    end: new Date(),
  };
  features: Feature[];
  sourceId: string; // ID of source
  @Input()
  set layers(v: any[]) {
    const layer = v.find((layer) => layer.metadata.layerType === 'watchpoints');
    this.features = layer?.source.data.features;
    this.sourceId = layer?.id;
  }
  @Output() element = new EventEmitter<ElementEvent>();

  constructor(
    private service: WatchpointsService,
    private userService: UserService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    //load new watchpoints info when the desired time changes
    if (changes.time && changes.time.currentValue) {
      this.userService.project$.pipe(take(1)).subscribe((prj) => {
        this.project = prj;
        this.loadGrid();
      });
    }
  }

  ngOnInit(): void {
    this.rxSubs.push(this.userService.project$.subscribe(this.updateProject));
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
  }

  updateProject = (prj: Project): void => {
    this.project = prj;
    this.stageUnits =
      prj.getUnitConfig('stage').options[
        prj.measurement.system === 'metric' ? 1 : 0
      ].abbr;
  };

  clickPoint = ({ pointId: id }: any): void => {
    const feat = this.features.find((f) => f.properties.id === id);
    const lngLat = (feat?.geometry as Point).coordinates;
    if (lngLat) {
      this.element.emit({
        type: 'map.click',
        properties: { lngLat, id, source: this.sourceId },
      });
    }
  };

  loadGrid = (): void => {
    this.loading = true;
    let end = this.time.end.getTime();
    let start = this.time.start.getTime();
    if (this.time.realtime) {
      // data show 24 hours or more
      const durationPicked = moment(this.time.end).diff(
        this.time.start,
        'hour'
      );
      const duration = durationPicked > 24 ? durationPicked : 24;
      start = end - duration * 60 * 60 * 1000;
      end += 15 * 60 * 60 * 1000;
    }
    this.service
      .getPoints(this.project, start, end)
      .pipe(take(1))
      .subscribe({
        next: (points) => {
          this.points = this.sortPoints(points);
          this.error = false;
          this.service.gridScroll$.next(null);
        },
        error: () => (this.error = true),
        complete: () => (this.loading = false)
      });
  };

  sortPoints(points: any[]): any[] {
    //Sort the list of watchpoints
    return [
      ...points.sort((pointA, pointB) => {
        switch (this.sortType) {
          case 'Name':
            //Sort alphabetically by name
            return this.sortByName(pointA, pointB);
          case 'Status':
            //Sort by status, warning-watch-normal
            const result = pointB.status - pointA.status;
            if (result != 0) {
              return result;
            } else {
              //Points have the same status - fall back to alphabetic comparison
              return this.sortByName(pointA, pointB);
            }
          case 'Value':
            //Sort by observed stage value, high-to-low
            if (pointA.value !== null && pointB.value !== null) {
              return pointB.value - pointA.value;
            } else if (pointA.value !== null) {
              return -1;
            } else if (pointB.value !== null) {
              return 1;
            } else {
              //Neither point has a value - fall back to alphabetic comparison
              return this.sortByName(pointA, pointB);
            }
        }
      }),
    ];
  }

  sortByName(pointA, pointB): number {
    return pointA.name < pointB.name ? -1 : pointA.name > pointB.name ? 1 : 0;
  }

  setSortType(sortType: string): void {
    this.sortType = sortType;
    this.points = this.sortPoints(this.points);
  }
}
