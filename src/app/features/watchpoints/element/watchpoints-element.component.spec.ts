import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchpointsElementComponent } from './watchpoints-element.component';

describe('WatchpointsElementComponent', () => {
  let component: WatchpointsElementComponent;
  let fixture: ComponentFixture<WatchpointsElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchpointsElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchpointsElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
