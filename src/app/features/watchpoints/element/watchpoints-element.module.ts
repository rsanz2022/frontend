import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WatchpointsElementComponent } from './watchpoints-element.component';
import { DetailsPaneModule } from '../details-pane/details-pane.module';
import { HydrographModule } from '@app/shared/charts/hydrograph/hydrograph.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
  declarations: [WatchpointsElementComponent],
  bootstrap: [WatchpointsElementComponent],
  exports: [WatchpointsElementComponent],
  imports: [
    CommonModule,
    DetailsPaneModule,
    HydrographModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    ScrollingModule,
    MatDialogModule,
  ],
})
export class WatchpointsElementModule {}
