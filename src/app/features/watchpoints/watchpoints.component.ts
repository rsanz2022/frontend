import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { WatchpointsService } from './watchpoints.service';
import { FormControl } from '@angular/forms';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { filter, map, debounceTime } from 'rxjs/operators';
import { interval, Subscription } from 'rxjs';
import { Globals } from '@app/globals';
import { TimeControlsComponent } from './time-controls/time-controls.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Time, Project } from '@app/shared/models';
import { UserService } from '@app/core';

@Component({
  selector: 'app-watchpoints',
  templateUrl: './watchpoints.component.html',
  styleUrls: ['./watchpoints.component.scss']
})
export class WatchpointsComponent implements OnInit, OnDestroy {
  private rxSubs: Subscription[] = [];
  loading = true;
  gridLoading = true;
  initialized = false;
  project: Project;
  tzOffset: string;
  selection;
  activeMediaQuery: string;
  watchpointFilter = new FormControl();
  filter: string;
  groups: string[] = [];
  time: Time = {
    realtime: true,
    end: new Date()
  };
  private updateInterval: Subscription;
  paneSizes = { md: 50, lg: 30, xl: 20 };
  @ViewChild('timeButton', { static: true }) timeButton: ElementRef;
  timepicker = {
    show: false,
    rect: (side): string => this.timeButton.nativeElement.getBoundingClientRect()[side] + 'px'
  };
  statusCounts: number[] = [0,0,0,0,0,0];
  statuses = { watch: 0, warn: 0 };
  statusKey = [
    { class: 'status-0', title: 'Normal' },
    { class: 'status-1', title: 'Normal; recently watch' },
    { class: 'status-2', title: 'Normal; recently warning' },
    { class: 'status-3', title: 'Watch level exceeded' },
    { class: 'status-4', title: 'Watch; recently warning' },
    { class: 'status-5', title: 'Warning level exceeded' },
    { class: 'status--1', title: 'No Data' },
  ];

  constructor(
    private service: WatchpointsService,
    private userService: UserService,
    private mediaObserver: MediaObserver,
    private dialog: MatDialog,
    public globals: Globals,
    private snack: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.startInterval();
    this.rxSubs.push(
      this.userService.project$.subscribe(proj => this.project = proj),
      this.service.selection$.subscribe(this.setSelection),
      this.service.statuses$.subscribe(this.statusUpdate),
      this.service.groups$.subscribe(g => this.groups = g),
      this.watchpointFilter.valueChanges.pipe(debounceTime(250))
        .subscribe(f => this.filter = f.length > 1 ? f : ''),
      this.mediaObserver.asObservable()
        .pipe(
          filter((changes: MediaChange[]) => changes.length > 0),
          map((changes: MediaChange[]) => changes[0])
        )
        .subscribe((change: MediaChange) => {
          if (this.activeMediaQuery !== change.mqAlias) {
            this.activeMediaQuery = change.mqAlias;
          }
        }
      )
    );
  }

  private statusUpdate = (newCounts: number[]): void => {
    if (!!newCounts) {
      this.snack.dismiss();
      if (!this.time.realtime ||
          this.statusCounts[3] !== newCounts[3] ||
          this.statusCounts[4] !== newCounts[4] ||
          this.statusCounts[5] !== newCounts[5]) {
        this.statusSnackbar(newCounts);
      }

      this.statusCounts = newCounts;
    }
  }

  private statusSnackbar = (counts): void => {
    const watch = counts[3] + counts[4];
    const warn = counts[5];
    const msg = `STATUSES: ${watch} watch & ${warn} warning`;

    if (watch || warn) {
      this.snack.open(msg, 'OK', {duration: 60000});
    } else {
      this.snack.open(msg, null, {duration: 5000});
    }

    this.statuses = { watch, warn };
  }

  startInterval = (): void => {
    this.stopInterval();
    this.updateInterval = interval(this.service.timerDelay)
      .subscribe(() => {
        this.time = {
          realtime: true,
          end: new Date()
        };
        this.tzOffset = this.project.getOffset(this.time.end);
      });
  }

  stopInterval = (): void => {
    if (this.updateInterval) {
      this.updateInterval.unsubscribe();
    }
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach(s => s.unsubscribe());
    this.service.setSelection(undefined);
    this.updateInterval.unsubscribe();
    this.service.selection$.next(undefined);
    this.service.statuses$.next(null);
    this.service.groups$.next([]);
    this.snack.dismiss();
  }

  openTimeControls = (): void => {
    const dialogRef = this.dialog.open(TimeControlsComponent, {
      data: this.time,
      autoFocus: false,
      backdropClass: 'hide-backdrop',
      panelClass: 'drop-down',
      width: '320px',
      position: {
        top: this.timepicker.rect('bottom'),
        left: this.timepicker.rect('left')
      }
    });

    dialogRef.beforeClosed().subscribe(this.onCloseTimeControls);
  }

  onCloseTimeControls = (event: Time): void => {
    if (event) {
      if (this.time.realtime !== event.realtime) {
        if (event.realtime) {
          this.startInterval();
        } else {
          this.stopInterval();
        }
      }
      this.tzOffset = this.project.getOffset(event.end);
      this.time = event;
    }
  }


  setLoading = (e): void => {
    this.gridLoading = e;
    if (!this.initialized) {
      setTimeout(() => this.loading = e);
      this.initialized = true;
    } else if (e) {
      this.snack.open('Checking statuses...');
    }
  }

  setSelection = (res): void => this.selection = res || undefined;

}
