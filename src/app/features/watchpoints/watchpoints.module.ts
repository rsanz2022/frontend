import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { WatchpointsComponent } from '../watchpoints/watchpoints.component';
import { WatchpointsRoutingModule } from './watchpoints-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HyetographModule } from '@app/shared/charts/hyetograph/hyetograph.module';
import { PointImageButtonComponent } from './grid/point-image-button/point-image-button.component';
import { GridComponent } from './grid/grid.component';
import { DetailsPaneModule } from './details-pane/details-pane.module';
import { HydrographModule } from '@app/shared/charts/hydrograph/hydrograph.module';
import { TimeControlsComponent } from './time-controls/time-controls.component';
import { TimepickerModule } from '@app/shared/timepicker/timepicker.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [WatchpointsComponent, PointImageButtonComponent, GridComponent, TimeControlsComponent],
  imports: [
    CommonModule,
    WatchpointsRoutingModule,
    DetailsPaneModule,
    FlexLayoutModule,
    HyetographModule,
    HydrographModule,
    TimepickerModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatListModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatDialogModule,
    MatSnackBarModule,
    MatMenuModule,
    MatTooltipModule,
    MatCheckboxModule
  ]
})
export class WatchpointsModule {}
