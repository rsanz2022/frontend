import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchpointsComponent } from './watchpoints.component';

describe('WatchpointsComponent', () => {
  let component: WatchpointsComponent;
  let fixture: ComponentFixture<WatchpointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchpointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchpointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
