import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { WatchpointsService } from '../watchpoints.service';
import { POITime } from '@app/shared/charts/hydrograph/hydrograph.component';
import * as moment from 'moment-timezone';
import { Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { UserService } from '@app/core';

@Component({
  selector: 'app-details-pane',
  templateUrl: './details-pane.component.html',
  styleUrls: ['./details-pane.component.scss'],
  host: { '(document:keydown.escape)': 'close()' },
})
export class DetailsPaneComponent implements OnChanges {
  timestamp: number = Date.now();
  statuses = [
    'Normal Level',
    'Normal; Recently Watch',
    'Normal; Recently Warning',
    'Watch Level Exceeded',
    'Watch; Recently Warning',
    'Warning',
  ];
  downloadUrl: string;
  @Input() loading: boolean;
  @Input() timezone: string;
  @Input() point: any;
  private readonly times$ = new ReplaySubject<POITime>(1);
  @Input() times: POITime;
  @Input() showFakeNRT = true;

  constructor(
    private wpService: WatchpointsService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.times?.currentValue) {
      this.setTimes(changes.times.currentValue);
    }

    if (changes.point?.currentValue) {
      this.updateChart();
      this.setHighs();
    }
  }

  setTimes(t: POITime): void {
    if (!!t) {
      let start = t.end.getTime() - 1000 * 60 * 60 * 24;
      let end = t.end.getTime() + 1000 * 60 * 60 * 15;
      /* fake NRT isn't used on Maps, while it is used in the WatchPoints app.
         Difference being that time selection is a range vs a single point */
      if (!this.showFakeNRT && !this.times.realtime) {
        start = this.times.start.getTime();
        end = this.times.end.getTime();
      }
      this.times = {
        ...this.times,
        realtime: t.realtime,
        start: new Date(start),
        end: new Date(end),
        fakeNow: this.showFakeNRT ? t.end : null,
      };
      this.times$.next(this.times);
      this.updateChart();
    }
  }

  private setHighs = (): void =>
    this.point.highs.forEach((h) => {
      h.displayTime = moment
        .tz(h.time, this.timezone)
        .format('YYYY/MM/DD HH:mm');
    });

  close = (): void => {
    this.wpService.setSelection(undefined);
  };

  updateChart = (): void => {
    this.timestamp = Date.now();
    this.userService.project$
      .pipe(take(1))
      .subscribe((proj) => this.setDownloadUrl(proj.symbolicName));
  };

  private setDownloadUrl = (project: string): string =>
    (this.downloadUrl = this.router
      .createUrlTree(['/api', project, 'hydrograph'], {
        queryParams: {
          watchpointid: this.point.pointId,
          style: 'COMPLICATED',
          width: 800,
          height: 600,
          start: this.times.start.getTime(),
          end: this.times.end.getTime(),
          fakeNow: this.times.fakeNRT ? this.times.fakeNow?.getTime() : -1
        },
      })
      .toString());
}
