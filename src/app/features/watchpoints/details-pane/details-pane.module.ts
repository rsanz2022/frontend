import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsPaneComponent } from './details-pane.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { HydrographModule } from '@app/shared/charts/hydrograph/hydrograph.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [DetailsPaneComponent],
  bootstrap: [DetailsPaneComponent],
  exports: [DetailsPaneComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    HydrographModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    ScrollingModule,
    MatCheckboxModule,
  ]
})
export class DetailsPaneModule {}