import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WatchpointsComponent } from './watchpoints.component';

const routes: Routes = [
  {
    path: '',
    component: WatchpointsComponent,
    data: { title: 'WatchPoints' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WatchpointsRoutingModule { }
