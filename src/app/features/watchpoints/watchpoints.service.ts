import { Apollo, gql } from 'apollo-angular';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { Project, Time } from '@app/shared/models';

interface Response {
  getWatchpointsGrid?: any[];
}

@Injectable({ providedIn: 'root' })
export class WatchpointsService {
  private _selection;
  timerDelay = 1000 * 60 * 2.5;
  readonly gridScroll$ = new Subject<Event>();
  readonly selection$ = new ReplaySubject(1);
  readonly statuses$ = new ReplaySubject<number[]>(1);
  readonly groups$ = new ReplaySubject<string[]>(1);

  constructor(private apollo: Apollo) {}

  getPoints(project: Project, start: number, end: number): Observable<any[]> {
    return this.apollo
      .query<Response>({
        query: gql`query POI($project:String!  ${
          start && end ? '$start:Long $end:Long' : ''
        }) {
          getWatchpointsGrid(project:$project ${
            start && end ? 'start:$start end:$end' : ''
          }) {
            pointId currentStatus groupingName name basin basinOrder status
            highs { displayName time values { name value displayName units } }
            sortBasin { name order }
          }
        }`,
        variables: { project: project.symbolicName, start, end },
      })
      .pipe(
        map(({ data }) =>
          data.getWatchpointsGrid.map((p) => {
            const value = this.getPointValue(p);
            return {
              ...p,
              name: p.name.replace(/^([0-9]* - )/, ''),
              value,
              description: this.getPointDescription(p, value, project),
            };
          })
        )
      );
  }

  getGrid(project: Project, time?: Time): Observable<any> {
    let start: number, end: number;
    if (time) {
      start = time.end.getTime() - 1000 * 60 * 60 * 24;
      end = time.end.getTime() + 1000 * 60 * 60 * 15;
      end = end > Date.now() ? Date.now() : end;
    }
    return this.getPoints(project, start, end).pipe(
      map((points) => {
        this.updateStatuses(points);
        const basins = this.getBasins(points).map((b) => {
          const basinPoints = points.filter(
            (point) => point.sortBasin.order === b.order
          );
          return {
            ...b,
            points: basinPoints
              .map((p) => {
                let fontSize;
                if (p.name.length < 30) fontSize = 1.5;
                else if (p.name.length > 45) fontSize = 1;
                else fontSize = 1.5 - ((p.name.length - 30) / 15) * 0.5;

                fontSize = Number.parseFloat(fontSize).toFixed(2);

                return {
                  ...p,
                  style: { 'font-size': fontSize + 'em' },
                  show: true,
                  filterString: (p.name + p.pointId + b.name).toLowerCase(),
                };
              })
              .sort(this.compareOrders),
          };
        });

        const groupsSet: Set<string> = new Set(
          points.map((a) => a.groupingName)
        );
        const groups = [...groupsSet].filter((el) => !!el);
        this.groups$.next(groups);

        if (!!this._selection) {
          const p = points.find((p) => p.pointId === this._selection.pointId);
          this.setSelection(p);
        }

        return basins;
      })
    );
  }

  compareOrders(a, b): number {
    return a.sortBasin.order - b.sortBasin.order || a.basinOrder - b.basinOrder;
  }

  // returns array of unique basins sorted by order
  getBasins(points): any[] {
    return points
      .map((p) => p.sortBasin)
      .filter(
        (point, i, self) => self.findIndex((p) => p.name === point.name) >= i
      )
      .sort((a, b) => a.order - b.order);
  }

  getStatuses(project: string, time: Time): Promise<any> {
    const start = time.end.getTime() - 1000 * 60 * 60 * 24;
    let end = time.end.getTime() + 1000 * 60 * 60 * 15;
    end = end > Date.now() ? Date.now() : end;
    return this.apollo
      .query<Response>({
        query: gql`
          query POI($project: String!, $start: Long, $end: Long) {
            getWatchpointsGrid(project: $project, start: $start, end: $end) {
              pointId
              status
              sortBasin {
                name
              }
            }
          }
        `,
        variables: { project, start, end },
      })
      .pipe(
        map(({ data }) => {
          this.updateStatuses(data.getWatchpointsGrid);
          return data.getWatchpointsGrid;
        })
      )
      .toPromise();
  }

  private updateStatuses(points: any[]): void {
    const counts = [0, 0, 0, 0, 0, 0, 0];
    points.forEach((p) => counts[p.status === -1 ? 6 : p.status]++);
    this.statuses$.next(counts);
  }

  setSelection(value): void {
    this._selection = value;
    this.selection$.next(value);
  }

  //return the observed stage value, if it's present (otherwise null)
  private getPointValue = (point: any): number => {
    for (let i = 0; i < point.highs.length; ++i) {
      if (point.highs[i].displayName === 'Observed') {
        for (let j = 0; j < point.highs[i].values.length; ++j) {
          if (point.highs[i].values[j].name === 'obs-stage') {
            return point.highs[i].values[j].value;
          }
        }
        break;
      }
    }

    return null;
  };

  //return a string describing the observed stage value
  private getPointValueStringExtended = (
    value: number,
    project: Project
  ): string => {
    if (value !== null) {
      const unit = project.getUnitConfig('stage').options[0].abbr;
      return value.toString() + ' ' + unit;
    } else {
      return 'Stage level not available for this time';
    }
  };

  //return the point name and a string describing the observed stage value
  private getPointDescription = (
    point: any,
    value: number,
    project: Project
  ): string => {
    return point.name + ': ' + this.getPointValueStringExtended(value, project);
  };
}
