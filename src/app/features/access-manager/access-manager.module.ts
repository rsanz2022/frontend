import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

import { AccessManagerRoutingModule } from './access-manager-routing.module';
import { GroupComponent } from './organizations/organization/group/group.component';
import { UsersComponent } from './users/users.component';
import { OrganizationsComponent } from './organizations/organizations.component';
import { NewUserComponent } from './users/new-user/new-user.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { OrganizationComponent } from './organizations/organization/organization.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,

    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatListModule,
    MatSelectModule,
    MatCardModule,
    MatChipsModule,

    AccessManagerRoutingModule
  ],
  declarations: [
    GroupComponent,
    UsersComponent,
    OrganizationsComponent,
    NewUserComponent,
    UserDetailComponent,
    OrganizationComponent
  ]
})
export class AccessManagerModule { }
