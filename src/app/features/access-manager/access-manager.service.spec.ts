import { TestBed } from '@angular/core/testing';

import { AccessManagerService } from './access-manager.service';

describe('AccessManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccessManagerService = TestBed.get(AccessManagerService);
    expect(service).toBeTruthy();
  });
});
