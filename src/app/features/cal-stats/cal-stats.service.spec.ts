import { TestBed } from '@angular/core/testing';

import { CalStatsService } from './cal-stats.service';

describe('CalStatsService', () => {
  let service: CalStatsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalStatsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
