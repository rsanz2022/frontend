export interface RainvieuxOption {
  symbolicName: string;
  displayName?: string;
  isDefault?: boolean;
}

export interface RainvieuxOptions {
  products?: RainvieuxOption[];
  radars?: RainvieuxOption[];
  zrs?: RainvieuxOption[];
}

export interface CalStatsSummary {
  eventName: string;
  start: number;
  end: number;
  rangeString: string;
  meanBias: number;
  gaugesUsed: number;
  gaugesExcluded: number;
  adbc: number;
  lbcad: number;
}

export interface CalStatsDetails {
  usedGaugeDetails: UsedGaugeDetails[]
  excludedGaugeDetails: ExcludedGaugeDetails[]
}

export interface UsedGaugeDetails {
  id: string;
  value: number;
  uncalibratedRadarValue: number;
  calibratedRadarValue: number;
}

export interface ExcludedGaugeDetails {
  id: string;
  reason: string;
}

export interface TableConfig {
  def: string;
  name: string;
  type?: 'date' | 'data' | 'percent' | 'number' | 'title';
  media?: string;
  description?: string;
}