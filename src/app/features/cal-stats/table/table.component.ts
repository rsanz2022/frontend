import { formatDate } from '@angular/common';
import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Globals } from '@app/globals';
import { Project } from '@app/shared/models';
import { CalStatsSummary, TableConfig } from '../cal-stats.models';
import { CalStatsService } from '../cal-stats.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  hasData = false;
  dataSource = new MatTableDataSource<CalStatsSummary>([]);
  @Input() loading = true;
  @Input() error: string | boolean;
  @Output() onSelect = new EventEmitter<CalStatsSummary>();
  @Output() tableSource = new EventEmitter<
    MatTableDataSource<CalStatsSummary>
  >();
  @Input() project: Project;
  @Input() hasNames = false;
  @Input()
  set hideUncal(v: boolean) {
    this.dataSource.filter = v ? 'hide' : '';
  }
  @Input()
  set data(v: CalStatsSummary[]) {
    if (!v) return;
    this.hasData = true;
    this.dataSource.data = v;
  }

  constructor(
    public service: CalStatsService,
    public mediaObserver: MediaObserver,
    private globals: Globals
  ) {}

  ngOnInit(): void {
    this.tableSource.emit(this.dataSource);
    this.dataSource.filterPredicate = this.filterPredicate;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
  }

  getColumns = (): string[] =>
    this.service.tableConfig
      .filter((c) => !c.media || this.mediaObserver.isActive(c.media))
      .filter((c) => !this.hasNames && c.def === 'eventName' ? false : true)
      .map((col) => col.def);

  getProperty = (obj: CalStatsSummary, col: TableConfig): string | number => {
    const prop = col.def.split('.').reduce((o, p) => o && o[p], obj);
    switch (col.type) {
      case 'date':
        return prop
          ? formatDate(
              prop,
              this.globals.DATETIME_FORMAT,
              'en',
              this.project.getOffset(prop)
            )
          : 'N/A';

      case 'data':
        return prop >= 0 ? prop.toFixed(3) : 'N/A';

      case 'percent':
        return prop >= 0 ? prop.toFixed(1) + '%' : 'N/A';

      default:
        return prop ?? 'N/A';
    }
  };

  private filterPredicate = (d: CalStatsSummary, f: string): boolean =>
    f ? d.gaugesUsed > 0 : true;

  private sortingDataAccessor = (
    item: CalStatsSummary,
    property: string
  ): string => item[property];
}
