import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalStatsComponent } from './cal-stats.component';
import { CalStatsRoutingModule } from './cal-stats-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { TimepickerModule } from '@app/shared';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { TableComponent } from './table/table.component';
import { DetailsComponent } from './details/details.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { environment } from '@env/environment';
import { ScatterPlotModule } from '@app/shared';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [CalStatsComponent, TableComponent, DetailsComponent],
  imports: [
    CommonModule,
    CalStatsRoutingModule,
    FormsModule,
    FlexLayoutModule,
    TimepickerModule,
    ScatterPlotModule,

    MatIconModule,
    MatRippleModule,
    MatTooltipModule,
    MatDialogModule,
    MatSelectModule,
    MatFormFieldModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatTabsModule,
    MatButtonModule,

    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken,
    }),
  ],
})
export class CalStatsModule {}
