import { Injectable } from '@angular/core';
import { Project, Time } from '@app/shared/models';
import { Apollo, gql } from 'apollo-angular';
import { map } from 'rxjs/operators';
import {
  RainvieuxOptions,
  CalStatsSummary,
  TableConfig,
  CalStatsDetails,
  RainvieuxOption,
} from './cal-stats.models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import moment from 'moment';

interface CalStatsSummaryRes {
  summaries: CalStatsSummary[];
  hasNames: boolean;
}

interface Response {
  getCalStatsOptions?: RainvieuxOptions;
  getCalStatsSummary?: CalStatsSummary[];
  getCalStatsGaugeDetails?: CalStatsDetails;
}

const rainvieuxOption = `{ symbolicName displayName isDefault }`;

@Injectable({
  providedIn: 'root',
})
export class CalStatsService {
  readonly tableConfig: TableConfig[] = [
    { def: 'eventName', name: 'Event Name', type: 'title' },
    {
      def: 'rangeString',
      name: 'Range (start&nbsp;&#8209;&nbsp;end)',
      description: 'The time period of data that was calibrated',
    },
    {
      def: 'meanBias',
      name: 'Mean Field<br>Bias Correction',
      type: 'data',
      description:
        'A single corrective factor computed as the sum total of the rain gauge depth divided by the sum total of the uncalibrated radar depth over an area. This factor represents a summary of the adjustments made by the calibration method, although it is not directly used to adjust radar data.',
    },
    {
      def: 'gaugesUsed',
      name: 'Gauges Used',
      type: 'number',
      description: 'The number of rain gauges used for calibration',
    },
    {
      def: 'gaugesExcluded',
      name: 'Gauges Excluded',
      type: 'number',
      media: 'gt-sm',
      description: 'The number of rain gauges excluded from calibration',
    },
    {
      def: 'adbc',
      name: 'Average Difference<br>Before Calibration',
      type: 'percent',
      media: 'gt-xs',
      description:
        'The average of the absolute percent differences between the rain gauge data and uncalibrated radar data sampled over the gauges',
    },
    {
      def: 'lbcad',
      name: 'Local Bias Calibrated<br>Average Difference',
      type: 'percent',
      description:
        'The average of the absolute percent differences between the rain gauges and radar data, adjusted by Local Bias method,  sampled over the gauges',
    },
  ];

  constructor(private apollo: Apollo, private http: HttpClient) {}

  getOptions = (project: Project): Observable<RainvieuxOptions> => {
    return this.apollo
      .query<Response>({
        variables: { project: project.symbolicName },
        query: gql`query Query($project:String!) {
          getCalStatsOptions(project:$project) {
            products ${rainvieuxOption}
            radars ${rainvieuxOption}
            zrs ${rainvieuxOption}
          }
        }`,
      })
      .pipe(
        map(({ data }) => {
          data.getCalStatsOptions.products.sort(this.sortByDisplayName);
          data.getCalStatsOptions.radars.sort(this.sortByDisplayName);
          data.getCalStatsOptions.zrs.sort(this.sortByDisplayName);
          return data.getCalStatsOptions;
        })
      );
  };

  private sortByDisplayName = (
    a: RainvieuxOption,
    b: RainvieuxOption
  ): number => {
    if (a.displayName < b.displayName) return -1;
    if (a.displayName > b.displayName) return 1;
    return 0;
  };

  getTable = (
    project: Project,
    product: string,
    radar: string,
    zr: string,
    start: number,
    end: number
  ): Observable<CalStatsSummaryRes> => {
    return this.apollo
      .query<Response>({
        errorPolicy: 'none',
        variables: {
          project: project.symbolicName,
          product,
          radar,
          zr,
          start,
          end,
        },
        query: gql`
          query Query(
            $project: String!
            $product: String!
            $radar: String!
            $zr: String!
            $start: Long!
            $end: Long!
          ) {
            getCalStatsSummary(
              project: $project
              product: $product
              radar: $radar
              zr: $zr
              start: $start
              end: $end
            ) {
              eventName
              start
              end
              meanBias
              gaugesUsed
              gaugesExcluded
              adbc
              lbcad
            }
          }
        `,
      })
      .pipe(
        map(({ data }) => {
          let hasNames = false;
          const summaries = data.getCalStatsSummary.map((summary) => {
            if (summary.eventName) hasNames = true;
            return {
              ...summary,
              rangeString: this.getRangeString(
                { start: new Date(summary.start), end: new Date(summary.end) },
                project
              ),
            };
          });
          return { summaries, hasNames };
        })
      );
  };

  getDetails(
    project: string,
    product: string,
    radar: string,
    zr: string,
    start: number,
    end: number
  ): Observable<CalStatsDetails> {
    return this.apollo
      .query<Response>({
        variables: { project, product, radar, zr, start, end },
        query: gql`
          query Query(
            $project: String!
            $product: String!
            $radar: String!
            $zr: String!
            $start: Long!
            $end: Long!
          ) {
            getCalStatsGaugeDetails(
              project: $project
              product: $product
              radar: $radar
              zr: $zr
              start: $start
              end: $end
            ) {
              usedGaugeDetails {
                id
                value
                uncalibratedRadarValue
                calibratedRadarValue
              }
              excludedGaugeDetails {
                id
                reason
              }
            }
          }
        `,
      })
      .pipe(map(({ data }) => data.getCalStatsGaugeDetails));
  }

  getRangeString(time: Time, project: Project): string {
    if (!time || !time.end) {
      return 'Loading...';
    }
    if (time.realtime) {
      return moment(time.end).tz(project.timezone).format('YYYY-MM-DD HH:mm');
    }

    const mStart = moment(time.start).tz(project.timezone);
    const mEnd = moment(time.end).tz(project.timezone);
    const start = mStart.format('YYYY-MM-DD HH:mm');
    let end = ' - ';

    if (mEnd.isSame(mStart, 'day')) {
      end = end + mEnd.format('HH:mm');
    } else if (mEnd.isSame(mStart, 'month')) {
      end = end + mEnd.format('DD HH:mm');
    } else if (mEnd.isSame(mStart, 'year')) {
      end = end + mEnd.format('MM-DD HH:mm');
    } else {
      end = end + mEnd.format('YYYY-MM-DD HH:mm');
    }

    return `${start}${end}`;
  }

  getChartData(
    project: string,
    product: string,
    radar: string,
    zr: string,
    start: number,
    end: number
  ): Observable<any> {
    const url = `/api/${project}/calstats/${product}/radar/${radar}/${zr}/range/${start}/${end}/scatterplot.json`;
    return this.http.get(url).pipe(
      map((res: any) => ({
        data: res.data,
        axis: {
          x: res.axis.x.label.replace(' Values', ''),
          y: res.axis.y.label.replace(' Values', ''),
        },
      }))
    );
  }
}
