import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalStatsComponent } from './cal-stats.component';

describe('CalStatsComponent', () => {
  let component: CalStatsComponent;
  let fixture: ComponentFixture<CalStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
