import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalStatsComponent } from './cal-stats.component';

const routes: Routes = [
  {
    path: '',
    component: CalStatsComponent,
    data: { title: 'Calibration Statistics' }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CalStatsRoutingModule { }
