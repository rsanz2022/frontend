import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from '@app/core';
import { Globals } from '@app/globals';
import { Project, Time } from '@app/shared/models';
import * as moment from 'moment-timezone';
import { BehaviorSubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  CalStatsSummary,
  RainvieuxOption,
  RainvieuxOptions,
} from './cal-stats.models';
import { CalStatsService } from './cal-stats.service';
import { DetailsComponent } from './details/details.component';

@Component({
  selector: 'app-cal-stats',
  templateUrl: './cal-stats.component.html',
  styleUrls: ['./cal-stats.component.scss'],
})
export class CalStatsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();
  project: Project;
  rvRadars: RainvieuxOption[];
  rvProducts: RainvieuxOption[];
  rvZRs: RainvieuxOption[];
  radar: RainvieuxOption;
  product: RainvieuxOption;
  zr: RainvieuxOption;
  time: Time;
  durationString = 'Loading...';
  dateString = this.durationString;
  timeState = 'real-time';
  hideUncal = false;
  timeStateIcons = {
    'real-time': 'update',
    historical: 'undo',
    forecasted: 'redo',
  };
  tableData: CalStatsSummary[];
  tableSource: MatTableDataSource<CalStatsSummary>;
  detailDialog: MatDialogRef<DetailsComponent>;
  loading$ = new BehaviorSubject(true);
  error$ = new BehaviorSubject<string | boolean>(false);
  dialogSizes = {
    xs: 'calc(100% - 8px)',
    sm: 'calc(100% - 16px)',
    md: '90vw',
    lg: '75vw',
    xl: '60vw',
  };
  mqAlias: string;
  hasNames = false;
  @ViewChild('timepicker', { static: true }) timepicker: ElementRef;
  @ViewChild('timeDialog', { static: true }) timeDialog: TemplateRef<any>;

  constructor(
    private service: CalStatsService,
    private userService: UserService,
    private globals: Globals,
    private dialog: MatDialog,
    private media: MediaObserver
  ) {}

  ngOnInit(): void {
    this.userService.project$
      .pipe(takeUntil(this.destroy$))
      .subscribe(this.onProject);

    const end = new Date();
    this.setTime({
      end: end,
      start: new Date(end.getTime() - 3.6e6 * 48),
      realtime: this.project.isNRT,
    });
    this.media
      .asObservable()
      .pipe(takeUntil(this.destroy$))
      .subscribe((values: MediaChange[]) => {
        this.mqAlias = values[0].mqAlias;
        if (this.detailDialog) {
          this.detailDialog.updateSize(this.dialogSizes[this.mqAlias]);
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  setTime = (time: Time): void => {
    if (!time) return;
    this.time = time;
    this.timeState = time.realtime ? 'real-time' : 'historical';
    this.durationString = this.getDurationString(time);
    this.dateString = this.service.getRangeString(time, this.project);
    this.updateTable();
  };

  openPickerPanel = (): void => {
    const getRect = (side: string): number =>
      this.timepicker.nativeElement.getBoundingClientRect()[side];
    this.dialog
      .open(this.timeDialog, {
        autoFocus: false,
        backdropClass: 'hide-backdrop',
        panelClass: 'drop-down',
        width: '320px',
        position: {
          top: getRect('bottom') + 4 + 'px',
          left: getRect('left') + 'px',
        },
      })
      .beforeClosed()
      .pipe(take(1))
      .subscribe(this.setTime);
  };

  updateTable = (): void => {
    this.loading$.next(true);
    if (!this.rvProducts) return;
    this.service
      .getTable(
        this.project,
        this.product.symbolicName,
        this.radar.symbolicName,
        this.zr.symbolicName,
        this.time.start.getTime(),
        this.time.end.getTime()
      )
      .subscribe({
        next: (res) => {
          this.tableData = res.summaries;
          this.hasNames = res.hasNames;
          this.error$
            .pipe(take(1))
            .subscribe((err) => (err ? this.error$.next(false) : null));
        },
        error: (err) => {
          this.tableData = [];
          this.error$.next(
            "We're having trouble loading events for this configuration. Try again later."
          );
          this.loading$.next(false);
          console.error(err);

        },
        complete: () => this.loading$.next(false),
      });
  };

  onSelectEvent = (event: CalStatsSummary): void => {
    this.detailDialog = this.dialog.open(DetailsComponent, {
      data: {
        project: this.project,
        event: event,
        events: this.displayedEvents(),
        product: this.product.symbolicName,
        radar: this.radar.symbolicName,
        zr: this.zr.symbolicName,
      },
      width: this.dialogSizes[this.mqAlias],
      maxWidth: 'calc(100% - 16px)',
      maxHeight: 'calc(100% - 16px)',
      autoFocus: false,
      panelClass: 'details-dialog',
    });
  };

  private getDurationString(time: Time): string {
    if (!time || !time.end || !time.start) {
      return 'Loading...';
    }
    const start = time.start.getTime();
    const end = time.end.getTime();
    const minutes = (end - start) / 60 / 1000;
    return this.globals.PERIODS.includes(minutes)
      ? moment.duration(minutes, 'minutes').humanize({ m: 60, h: 24, d: 31 })
      : 'Custom';
  }

  private onOptions = ({ radars, products, zrs }: RainvieuxOptions): void => {
    this.rvRadars = radars;
    this.rvZRs = zrs;
    this.rvProducts = products;
    this.radar = this.getDefaultOption(radars);
    this.zr = this.getDefaultOption(zrs);
    this.product = this.getDefaultOption(products);
    this.updateTable();
  };

  private getDefaultOption(options: RainvieuxOption[]): RainvieuxOption {
    return options.find((r) => r.isDefault) || options[0];
  }

  private displayedEvents = (): CalStatsSummary[] =>
    this.tableSource.sortData(
      this.tableSource.filteredData,
      this.tableSource.sort
    );

  private onProject = (project: Project): void => {
    this.project = project;
    this.service.getOptions(project).subscribe(this.onOptions);
  };
}
