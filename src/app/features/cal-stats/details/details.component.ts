import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Sort } from '@angular/material/sort';
import { Globals } from '@app/globals';
import { Project } from '@app/shared/models';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import {
  CalStatsDetails,
  CalStatsSummary,
  ExcludedGaugeDetails,
  UsedGaugeDetails,
} from '../cal-stats.models';
import { CalStatsService } from '../cal-stats.service';
import { CirclePaint, GeoJSONSourceRaw, Map } from 'mapbox-gl';
import { take } from 'rxjs/operators';
import * as c3 from 'c3';
import Auth from '@aws-amplify/auth';

interface DialogData {
  project: Project;
  product: string;
  radar: string;
  zr: string;
  event: CalStatsSummary;
  events: CalStatsSummary[];
}

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  includedSorted: UsedGaugeDetails[];
  excludedSorted: ExcludedGaugeDetails[];
  included: UsedGaugeDetails[];
  excluded: ExcludedGaugeDetails[];
  loading$ = new BehaviorSubject(true);
  gaugeSource: GeoJSONSourceRaw;
  chartData: c3.ChartConfiguration;
  event: CalStatsSummary;
  prevEvent: CalStatsSummary;
  nextEvent: CalStatsSummary;
  tzOffset: string;
  mapgl: Map;
  mapgl$ = new ReplaySubject<Map>();
  private token: string;
  layerPaint: CirclePaint = {
    'circle-radius': ['interpolate', ['linear'], ['zoom'], 8, 3, 12, 6, 16, 12],
    'circle-color': [
      'match',
      ['feature-state', 'inclusion'],
      ['0'],
      '#f44336',
      ['1'],
      '#4caf50',
      '#9e9e9e',
    ],
  };
  decimals = { metric: '1.0-0', imperial: '1.2-2' };
  unit: string;

  constructor(
    public globals: Globals,
    private service: CalStatsService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<DetailsComponent>
  ) {}

  ngOnInit(): void {
    this.getEventDetails(this.data.event);
    const data = `/api/${this.data.project.symbolicName}/gauge_geo.json`;
    this.gaugeSource = { type: 'geojson', promoteId: 'id', data };
    this.tzOffset = this.data.project.getOffset(this.data.event.end);
    this.unit = this.data.project.getUnitConfig('rain').options[0].abbr;
    Auth.currentSession().then(
      (session) => (this.token = session.getIdToken().getJwtToken())
    );
  }

  getEventDetails = (event: CalStatsSummary): void => {
    this.loading$.next(true);
    const { project, product, radar, zr, events } = this.data;
    this.event = event;
    for (let index = 0; index < events.length; index++) {
      const ev = events[index];
      if (ev.end === event.end) {
        this.prevEvent = events[index + 1];
        this.nextEvent = events[index - 1];
        break;
      }
    }

    if (event.gaugesUsed) {
      this.service
        .getChartData(
          project.symbolicName,
          product,
          radar,
          zr,
          event.start,
          event.end
        )
        .subscribe((res) => (this.chartData = res));
    } else this.chartData = null;

    this.service
      .getDetails(
        project.symbolicName,
        product,
        radar,
        zr,
        event.start,
        event.end
      )
      .subscribe({
        next: this.loadDetails,
        complete: () => this.loading$.next(false),
      });
  };

  private loadDetails = (data: CalStatsDetails): void => {
    this.includedSorted = data.usedGaugeDetails?.slice();
    this.excludedSorted = data.excludedGaugeDetails?.slice();
    this.included = data.usedGaugeDetails;
    this.excluded = data.excludedGaugeDetails;
    this.mapgl$.pipe(take(1)).subscribe((mapgl) => {
      mapgl.removeFeatureState({ source: 'gauges' });
      setTimeout(this.setFeatures);
    });
  };

  private setFeatures = (): void => {
    this.mapgl.resize();
    for (let index = 0; index < this.included.length; index++) {
      const gauge = this.included[index];
      this.mapgl.setFeatureState(
        { source: 'gauges', id: gauge.id },
        { inclusion: '1' }
      );
    }
    for (let index = 0; index < this.excluded.length; index++) {
      const gauge = this.excluded[index];
      this.mapgl.setFeatureState(
        { source: 'gauges', id: gauge.id },
        { inclusion: '0' }
      );
    }
  };

  sortData(sort: Sort, type: 'included' | 'excluded'): void {
    const data = this[type].slice();
    if (!sort.active || sort.direction === '') {
      this[type + 'Sorted'] = data;
      return;
    }

    this[type + 'Sorted'] = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return (a[sort.active] < b[sort.active] ? -1 : 1) * (isAsc ? 1 : -1);
    });
  }

  onMapLoad = (map: Map): void => {
    this.mapgl$.next(map);
    this.mapgl = map;
  };

  public transformRequest = (url: string): mapboxgl.RequestParameters => {
    const protocol = window.location.protocol;
    const host = window.location.host;
    if (
      url.startsWith(`${protocol}//${host}/api/`) ||
      url.startsWith(`/api/`) ||
      url.startsWith(`api/`)
    ) {
      return {
        url,
        headers: { Authorization: 'Bearer ' + this.token },
      };
    }
  };
}
