import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HydroplotterComponent } from './hydroplotter.component';

describe('HydroplotterComponent', () => {
  let component: HydroplotterComponent;
  let fixture: ComponentFixture<HydroplotterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HydroplotterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HydroplotterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
