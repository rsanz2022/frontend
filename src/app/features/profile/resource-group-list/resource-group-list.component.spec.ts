import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceGroupListComponent } from './resource-group-list.component';

describe('ResourceGroupListComponent', () => {
  let component: ResourceGroupListComponent;
  let fixture: ComponentFixture<ResourceGroupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceGroupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
