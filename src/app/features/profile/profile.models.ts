export interface ResourceGroup {
  groupName?: string;
  resources?: Resource[];
}

export interface Resource {
  name?: string;
  url?: string;
  type?: string;
  mimeType?: string;
  size?: number;
  tooltip?: string;
}