import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { GaugeStatusElementComponent } from './gauge-status-element.component';

@NgModule({
  declarations: [GaugeStatusElementComponent],
  bootstrap:[GaugeStatusElementComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ScrollingModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [GaugeStatusElementComponent]
})

export class GaugeStatusElementModule { }
