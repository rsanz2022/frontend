import {
  Component,
  EventEmitter,
  OnInit,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { UserService } from '@app/core';
import { GaugeStatusService } from '../gauge-status.service';
import {
  Project,
  GaugeDetail,
  Time,
  GaugeSum,
  SimpleSum,
} from '@app/shared/models';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Feature, Point } from 'geojson';
import { ElementEvent } from '@app/shared/models/element.model';

interface GaugeDuration {
  label: string;
  value: number; // the index to the sum
}

@Component({
  selector: 'app-gauge-status-element',
  templateUrl: './gauge-status-element.component.html',
  styleUrls: ['./gauge-status-element.component.scss'],
})
export class GaugeStatusElementComponent implements OnInit, OnChanges {
  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;

  @Input() times: Time;

  features: Feature[];
  sourceId: string; // ID of source
  @Input()
  set layers(v) {
    const layer = v.find((layer) => layer.metadata.layerType === 'gauges');
    this.features = layer?.source.data.features;
    this.sourceId = layer?.id;
  }

  rxSubs: Subscription[] = [];
  loading: boolean;
  error: string;
  project: Project;
  unit: string;
  gaugeDetails: GaugeDetail[];
  gaugeSums: Map<string, number>;

  readonly sortTypes: string[] = ['Name', 'Status', 'Value'];
  readonly durations: GaugeDuration[] = [
    { label: '1 hr', value: 0 },
    { label: '6 hrs', value: 1 },
    { label: '24 hrs', value: 2 },
    { label: '48 hrs', value: 3 },
    { label: '72 hrs', value: 4 },
    { label: 'Maps Duration', value: 5 },
  ];
  // default options
  sortType: string = this.sortTypes[1];
  duration: number = this.durations[this.durations.length - 1].value;

  @Output() element = new EventEmitter<ElementEvent>();

  constructor(
    private service: GaugeStatusService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.gaugeSums = new Map<string, number>();
    this.rxSubs.push(
      this.userService.project$.subscribe((project) => {
        this.project = project;
        this.loadDetails();
      })
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.times && changes.times.currentValue) {
      this.userService.project$.pipe(take(1)).subscribe((prj) => {
        this.project = prj;
        this.loadDetails();
      });
    }
  }

  ngOnDestroy(): void {
    this.rxSubs.forEach((s) => s.unsubscribe());
  }

  loadDetails = (): void => {
    this.unit = this.project.getUnitConfig('rain').options[0].abbr;
    this.loading = true;
    this.error = '';
    const time = this.times?.end ?? new Date();
    this.service
      .getTableData(this.project.symbolicName, time)
      .then((res) => {
        if (res.data.getOverview && res.data.getAllDetails) {
          this.gaugeDetails = res.data.getAllDetails;
          this.error = undefined;
          this.loading = false;
          this.sortGauges();
        } else {
          this.elementError(
            'The Gauge Status application is not available for this project.'
          );
        }
      })
      .catch(this.elementError);
    // load the custum duration values
    this.service
      .getCustomRangeDetails(
        this.project.symbolicName,
        this.times.start,
        this.times.end
      )
      .subscribe((data: SimpleSum[]) => {
        this.gaugeSums.clear();
        data.forEach((sum) => {
          this.gaugeSums.set(sum.id, sum.value);
        });
      });
  };

  setSortType = (sortType: string): void => {
    if (this.sortType !== sortType) {
      this.sortType = sortType;
      this.sortGauges();
    }
  };

  sortGauges = (): void => {
    this.gaugeDetails = [...this.gaugeDetails.sort(this.compareGauges)];
    this.viewPort?.scrollToIndex(0, 'auto');
  };

  compareGauges = (a: GaugeDetail, b: GaugeDetail) => {
    let result = 0;
    if (this.sortType === 'Status') {
      result = b.status - a.status;
    } else if (this.sortType === 'Value') {
      const sumA = this.getSum(a.sums, a.vaiId);
      const sumB = this.getSum(b.sums, b.vaiId);
      if (sumA !== null && sumB !== null) {
        result = sumB - sumA;
      } else if (!sumA) {
        result = 1;
      } else if (!sumB) {
        result = -1;
      }
    }
    if (result === 0) {
      result = a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
    }
    return result;
  };

  getDisplaySum = ({ sums, vaiId: id }): string => {
    const sum = this.getSum(sums, id);
    if (!sum) {
      return '-';
    }
    return sum.toFixed(2);
  };

  getSum = (sums: GaugeSum[], id: string): number => {
    let sum: number;
    if (this.duration === 5) {
      sum = this.gaugeSums.get(id);
    } else {
      sum = sums[this.duration].sum;
    }
    return sum;
  };

  elementError = (err): void => {
    this.error = err.message || err;
    this.loading = false;
  };

  clickGauge = ({ vaiId: id }): void => {
    const feat = this.features.find((f) => f.properties.id === id);
    const lngLat = (feat?.geometry as Point).coordinates;
    if (lngLat) {
      this.element.emit({
        type: 'map.click',
        properties: { lngLat, id, source: this.sourceId },
      });
    }
  };
}
