import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { GaugeDetail, Project } from '@app/shared/models';
import { Globals } from '@app/globals';
import { GaugeStatusService } from '../gauge-status.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-gauge-detail',
  templateUrl: './gauge-detail.component.html',
  styleUrls: ['./gauge-detail.component.scss']
})
export class GaugeDetailComponent implements OnInit, OnDestroy {
  error: string;
  chartLoading = true;
  rawGauge: any;
  update: Subscription;
  timestamp = Date.now();
  unit: string;
  public gaugeIndex: number;

  constructor(
    public globals: Globals,
    private service: GaugeStatusService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<GaugeDetailComponent>
  ) {}

  ngOnInit(): void {
    this.gaugeIndex = this.data.gauges.findIndex(
      (x: GaugeDetail) => x.vaiId === this.data.gauge.vaiId
    );

    if (this.data.doUpdates) {
      this.unit = (this.data.project as Project)
        .getUnitConfig('rain').options[0].name;
      this.update = this.service.polling$.subscribe(_ => {
        this.timestamp = Date.now();
        this.service.getGauge(this.data.project.symbolicName, this.data.gauge.vaiId)
          .toPromise()
          .then((res: GaugeDetail) => this.data.gauge = res);
      });
    }
  }

  ngOnDestroy(): void {
    if (this.update) {
      this.update.unsubscribe();
    }
  }

  public onPrevGaugeClick(): void {
    this.gaugeIndex--;
    this.data.gauge = this.data.gauges[this.gaugeIndex];
  }

  public onNextGaugeClick(): void {
    this.gaugeIndex++;
    this.data.gauge = this.data.gauges[this.gaugeIndex];
  }

  public updateGauges(newList: GaugeDetail[]): void {
    const newIndex = newList.findIndex(
      (x: GaugeDetail) => x.vaiId === this.data.gauge.vaiId
    );
    if(newIndex >= 0) {
      this.gaugeIndex = newIndex;
      this.data.gauges = newList;
    }
  }
}
