import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaugeDetailComponent } from './gauge-detail.component';

describe('GaugeDetailComponent', () => {
  let component: GaugeDetailComponent;
  let fixture: ComponentFixture<GaugeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaugeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaugeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
