import { TestBed } from '@angular/core/testing';

import { GaugeStatusService } from './gauge-status.service';

describe('GaugeStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GaugeStatusService = TestBed.get(GaugeStatusService);
    expect(service).toBeTruthy();
  });
});
