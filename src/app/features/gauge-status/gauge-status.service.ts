import {Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { GaugeDetail, SimpleSum } from '@app/shared/models';
import { HttpClient } from '@angular/common/http';


interface Response {
  getAllDetails?: GaugeDetail[];
  getDetail?: GaugeDetail;
  getOverview?: any;
  hasGroups?: boolean;
  getGeoJson?: any
}

@Injectable({ providedIn: 'root' })
export class GaugeStatusService {
  public timerDelay = 1000 * 60 * 2.5;
  public polling$ = new Subject<any>();

  constructor(
    private apollo: Apollo,
    private http: HttpClient
   ) { }

  // only returns latest data
  getGauge(project: string, id: string): Observable<GaugeDetail> {
    const input: any = { project, id: encodeURI(id) };
    return this.apollo
      .query<Response>({
        query: gql`query RainGaugeStatus($input: GaugeDetailInput!) {
          getDetail(input: $input) {
            vaiId
            clientId
            name
            feedType
            grouping
            status
            timeFiltered
            timeRaw
            sums {
              hours
              sum
            }
          }
        }`,
        variables: {input}
      }).pipe(
        map(res => {
          res.data.getDetail.sums.sort(this.sortSums);
          return Object.assign(new GaugeDetail(), res.data.getDetail);
        })
    );
  }

  getTableData(project: string, time?: Date): Promise<any> {
    const input: any = { project };
    if (time) {
      input.start = time.getTime();
      input.end = time.getTime();
    }
    return this.apollo
      .query<Response>({
        query: gql`query RainGaugeStatus($input:AllDetailsInput!, $project:String!) {
          getAllDetails(input: $input) {
            vaiId
            clientId
            name
            feedType
            grouping
            status
            timeFiltered
            timeRaw
            sums {
              hours
              sum
            }
          }
          getOverview(project: $project) {
            recentlyReported
            total
            lastReport
          }
        }`,
        variables: { input, project }
      }).pipe(
        map(
          res => {
            let hasGroups = false;
            if (res.data.getAllDetails) {
              res.data.getAllDetails = res.data.getAllDetails.map(
                g => {
                  if (g.grouping) {
                    hasGroups = true;
                  }
                  g.sums.sort(this.sortSums);
                  return Object.assign(new GaugeDetail(), g);
                }
              );
            }
            res.data.hasGroups = hasGroups;
            return res;
          }
        )
      ).toPromise();
  }

  getCustomRangeDetails(project: string, start: Date, end: Date): Observable<SimpleSum[]> {
    const url = `${window.location.origin}/api/${project}/gauge/simpleGaugeData?start=${start.getTime()}&end=${end.getTime()}`;
    return this.http
      .get<SimpleSum[]>(url);
  } 

  private sortSums = (a: any, b: any) => {
    if (a.hours < b.hours) {
      return -1;
    }
    if (a.hours > b.hours) {
      return 1;
    }
    return 0;
  }
}
