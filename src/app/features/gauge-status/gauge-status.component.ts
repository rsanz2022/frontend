import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { formatDate } from '@angular/common';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GaugeStatusService } from './gauge-status.service';
import { GaugeDetail, Project } from '@app/shared/models';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { GaugeDetailComponent } from './gauge-detail/gauge-detail.component';
import { Globals } from '@app/globals';
import { UserService } from '@app/core';
import { filter, map } from 'rxjs/operators';
import { TimeControlsComponent } from './time-controls/time-controls.component';
import { interval } from 'rxjs';

@Component({
  selector: 'app-gauge-status',
  templateUrl: './gauge-status.component.html',
  styleUrls: ['./gauge-status.component.scss']
})
export class GaugeStatusComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('timeButton', { static: true }) timeButton: ElementRef;
  loading = true;
  updating = true;
  error: string;
  activeMediaQuery: string;
  subscriptions = [];
  tableFilter = new FormControl();
  project: Project;
  tzOffset = '';
  overview: any;
  units: string;
  hasGroups: false;
  dataSource = new MatTableDataSource < GaugeDetail > ();
  updateInterval: any;
  doUpdates = true;
  private gaugeDetailDialog: MatDialogRef<GaugeDetailComponent>;
  timepicker = {
    show: false,
    rect: (side): string => this.timeButton.nativeElement
      .getBoundingClientRect()[side] + 'px'
  };
  timepickerClick: EventListener;
  time: Date;
  columns = [
    { def: 'grouping', name: 'Group', media: 'gt-sm' },
    { def: 'name', name: 'Description' },
    { def: 'feedType', name: 'Network', media: 'gt-xs' },
    { def: 'vaiId', name: 'ID', media: 'gt-sm' },
    { def: 'sum1', name: '1 hr', index: 0, data: true, includeUnit: true },
    { def: 'sum6', name: '6 hrs', index: 1, data: true, includeUnit: true },
    { def: 'sum24', name: '24 hrs', index: 2, data: true, includeUnit: true },
    { def: 'sum48', name: '48 hrs', index: 3, data: true, includeUnit: true, media: 'gt-xs' },
    { def: 'sum72', name: '72 hrs', index: 4, data: true, includeUnit: true, media: 'gt-xs' },
    { def: 'timeFiltered', name: 'Filtered', media: 'gt-md', isDate: true },
    { def: 'timeRaw', name: 'Raw', media: 'gt-md', isDate: true }
  ];

  constructor(
    private service: GaugeStatusService,
    private userService: UserService,
    private mediaObserver: MediaObserver,
    private dialog: MatDialog,
    public globals: Globals
  ) {}

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
    this.startInterval();
    this.subscriptions.push(
      this.userService.project$.subscribe(proj => {
        this.loading = true;
        this.project = proj;
        this.getTable(proj);
        this.units = proj.getUnitConfig('rain').options[0].abbr;
      }),
      this.tableFilter.valueChanges
        .subscribe(f => this.applyFilter(f)),
      this.mediaObserver.asObservable()
        .pipe(
          filter((changes: MediaChange[]) => changes.length > 0),
          map((changes: MediaChange[]) => changes[0])
        )
        .subscribe((change: MediaChange) => {
          if (this.activeMediaQuery !== change.mqAlias) {
            this.activeMediaQuery = change.mqAlias;
          }
        })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(o => o.unsubscribe());
    this.updateInterval.unsubscribe();
  }

  getTable = (proj: Project, time?: Date): void => {
    const reqTime = time || new Date();
    this.time = null;
    this.service.getTableData(proj.symbolicName, time)
      .then(res => {
        this.hasGroups = res.data.hasGroups;
        if (res.data.getOverview && res.data.getAllDetails) {
          this.overview = res.data.getOverview;
          this.time = reqTime;
          this.tzOffset = proj.getOffset(this.time);
          this.dataSource.data = res.data.getAllDetails;
          this.error = undefined;
          this.loading = false;
          this.updating = false;
        } else {
          this.pageError('The Gauge Status application is not available for this project.');
        }
      }).catch(this.pageError);
}

  pageError = (err): void => {
    this.error = err.message || err;
    this.stopInterval();
    this.loading = false;
    this.updating = false;
  }

  sortingDataAccessor = (item, property): any => {
    if (/sum/.test(property)) {
      const col = this.columns.find(c => c.def === property);
      return item.sums[col.index].sum;
    }
    return item[property];
  }

  getProperty = (obj, path, col): any => {
    const prop = path.split('.').reduce((o, p) => o && o[p], obj);
    if (/sum/.test(col.def)) {
      return obj.sums[col.index].sum === null ? '-' : obj.sums[col.index].sum.toFixed(2);
    } else if (col.isDate) {
      return prop ?
        formatDate(
          prop,
          this.globals.DATETIME_FORMAT,
          'en',
          this.project.getOffset(prop)
        ) : 'N/A';
    }
    return prop ? prop : 'N/A';
  }

  getDisplayedColumns(): any[] {
    return this.columns.filter(
      col => {
        if (col.def === 'grouping') {
          if (this.hasGroups) {
            return this.mediaObserver.isActive(col.media);
          } else { return false; }
        }

        if (col.def === 'timeFiltered' || col.def === 'timeRaw') {
          return this.doUpdates && this.mediaObserver.isActive(col.media);
        }
        return !col.media || this.mediaObserver.isActive(col.media);
      }
    ).map(col => col.def);
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private displayedGauges(): GaugeDetail[] {
    return this.dataSource.sortData(this.dataSource.filteredData, this.dataSource.sort);
  }

  openDetailDialog(gauge: GaugeDetail): void {
    const widths = {
      xs: '95%',
      sm: '90%',
      md: '85%',
      lg: '80%',
      xl: '75%',
    };
    this.gaugeDetailDialog = this.dialog.open(GaugeDetailComponent, {
      minWidth: widths[this.activeMediaQuery],
      data: {
        project: this.project,
        time: this.time,
        doUpdates: this.doUpdates,
        gauges: this.displayedGauges(),
        gauge
      },
      autoFocus: false,
      panelClass: 'details-dialog'
    });
  }

  startInterval(): void {
    if (this.doUpdates) {
      this.updateInterval = interval(this.service.timerDelay)
        .subscribe(_ => {
          this.service.polling$.next('');
          this.updating = true;
          this.getTable(this.userService.project);
          this.gaugeDetailDialog?.componentInstance?.updateGauges(this.displayedGauges());
        });
    }
  }

  stopInterval(): void { this.updateInterval.unsubscribe(); }

  openTimeControls = (): void => {
    const dialogRef = this.dialog.open(TimeControlsComponent, {
      data: {
        start: null,
        end: this.time,
        realtime: this.doUpdates
      },
      autoFocus: false,
      backdropClass: 'hide-backdrop',
      panelClass: 'drop-down',
      width: '320px',
      position: {
        top: this.timepicker.rect('bottom'),
        left: this.timepicker.rect('left')
      }
    });

    dialogRef.beforeClosed().subscribe(this.onCloseTimeControls);
  }

  onCloseTimeControls = (res): void => {
    if (res) {
      this.stopInterval();
      this.updating = true;
      this.doUpdates = res.realtime;

      if (this.doUpdates) {
        this.getTable(this.project);
        setTimeout(this.startInterval);
      } else {
        if (res.end) {
          this.loading = true;
          this.getTable(this.project, res.end);
        } else {
          this.updating = false;
        }
      }
    }
  }
}
