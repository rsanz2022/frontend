import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GaugeStatusComponent } from './gauge-status.component';

const routes: Routes = [
  {
    path: '',
    component: GaugeStatusComponent,
    data: { title: 'Rain Gauge Status' }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class GaugeStatusRoutingModule { }
