import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GaugeStatusComponent } from './gauge-status.component';
import { GaugeStatusRoutingModule } from './gauge-status-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { OverlayModule } from '@angular/cdk/overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GaugeDetailComponent } from './gauge-detail/gauge-detail.component';
import { HyetographModule } from '@app/shared';
import { TimepickerModule } from '@app/shared/timepicker/timepicker.module';
import { TimeControlsComponent } from './time-controls/time-controls.component';

@NgModule({
  declarations: [
    GaugeStatusComponent,
    GaugeDetailComponent,
    TimeControlsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    OverlayModule,

    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatListModule,
    MatMenuModule,
    MatSlideToggleModule,

    HyetographModule,
    GaugeStatusRoutingModule,
    TimepickerModule
  ]
})
export class GaugeStatusModule { }
