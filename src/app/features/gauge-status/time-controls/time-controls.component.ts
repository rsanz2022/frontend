import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Time } from '@app/shared/models';

@Component({
  selector: 'app-time-controls',
  template: `
    <app-timepicker
      [time]="time"
      [max]="max"
      enableRealtime
      momentOnly
      (timesChange)="onTimesChange($event)"
    >
    </app-timepicker>
  `,
  styles: [
    `
      :host {
        display: block;
        padding: 8px;
      }
    `
  ]
})
export class TimeControlsComponent {
  max = Date.now();

  constructor(
    private dialogRef: MatDialogRef<TimeControlsComponent>,
    @Inject(MAT_DIALOG_DATA) public time: Time
  ) {}

  onTimesChange(event): void {
    if (event) {
      this.dialogRef.close(event);
    } else {
      this.dialogRef.close();
    }
  }
}
