import { Component, Input } from '@angular/core';
import { GenericAlert, TypeStyles } from '../notifications.models';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent {
  @Input() alert: GenericAlert;
  @Input() typeStyles: TypeStyles;

  importances = ['Critical', 'Moderate', 'Low Impact'];
}
