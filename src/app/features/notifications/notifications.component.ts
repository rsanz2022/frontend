import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '@app/core';
import { forkJoin } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { RuleType } from '../alert-manager/alert-manager.models';
import { GenericAlert, TypeStyles } from './notifications.models';
import { NotificationsService } from './notifications.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  alert: GenericAlert;
  alertForm = this._fb.group({ id: '' });
  typeStyles: TypeStyles;
  error = false;
  hasId = true;

  constructor(
    private _fb: FormBuilder,
    private route: ActivatedRoute,
    private service: NotificationsService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    forkJoin([
      this.service.getStyles(),
      this.service.getRuleTypes(this.userService.project.symbolicName).pipe(take(1)),
    ]).subscribe((res) => {
      this.typeStyles = res[1]
        .map((type: RuleType) => ({
          ...type,
          ...res[0][type.group.toLowerCase()],
        }))
        .reduce((obj, item) => ((obj[item.id] = item), obj), {});
    });
    
    this.loadAlert();
  }

  loadAlert = (): void => {
    this.route.paramMap.pipe(
      take(1),
      switchMap(params => {
        const id = params.get('id');
        this.hasId = !!id;
        if (id) {
          this.alertForm.get('id').setValue(id);
          return this.service.getAlert(id, this.userService.project).pipe(take(1));
        }
      })
    ).subscribe(
      (alert): void => {
        this.error = false
        this.alert = alert;
      }, (): void => {
        this.error = true;
    });
  }
}
